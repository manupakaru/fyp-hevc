`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   11:25:22 09/10/2013
// Design Name:   dequant
// Module Name:   C:/Users/Maleen/Uni/HEVC/Transform/dequant_test.v
// Project Name:  Transform
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: dequant
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////
`define Trafo32 2'b11
`define Trafo16 2'b10
`define Trafo8 2'b01
`define Trafo4 2'b00

module dequant_test;

	// Inputs
	reg [31:0] in,in_mem;
	reg row_col;
	reg [5:0] QP;
	reg [1:0] trafoSize;
	reg clk;

	// Outputs
	wire [31:0] out;

	// Instantiate the Unit Under Test (UUT)
	dequant uut (
		.in(in), 
		.in_mem(in_mem),
		.row_col(row_col),
		.QP(QP), 
		.trafoSize(trafoSize), 
		.clk(clk), 
		.out(out)
	);

	initial begin
		// Initialize Inputs
		in = 0;
		in_mem=0;
		row_col = 0;
		QP = 0;
		trafoSize = 0;
		clk = 0;

		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here
		#1;
		in = {16'd100, -16'd100};
		QP = 37;
		trafoSize = `Trafo8;

	end
	always 
		#5 clk = ~clk;
      
endmodule

