`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    19:28:43 09/10/2013 
// Design Name: 
// Module Name:    downward_push 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
`define WORD(A) 32*A+31:32*A
module downward_push(
    input [1023:0] in,
    input [2:0] block_no,
	 input transform_en,
    output reg [35:0] out
    );

	reg signed [31:0] in0, in1, in2, in3;
	reg signed [31:0] in0_s, in1_s, in2_s, in3_s;
	reg signed [8:0] out0, out1, out2, out3;
	
	always @ (*) begin
		case (block_no) 
			0: in0 <= in[`WORD(31)]; 
			1: in0 <= in[`WORD(27)];
			2: in0 <= in[`WORD(23)];
			3: in0 <= in[`WORD(19)];
			4: in0 <= in[`WORD(15)];
			5: in0 <= in[`WORD(11)];
			6: in0 <= in[`WORD(7)];
			7: in0 <= in[`WORD(3)];
		endcase
		case (block_no) 
			0: in1 <= in[`WORD(30)]; 
			1: in1 <= in[`WORD(26)];
			2: in1 <= in[`WORD(22)];
			3: in1 <= in[`WORD(18)];
			4: in1 <= in[`WORD(14)];
			5: in1 <= in[`WORD(10)];
			6: in1 <= in[`WORD(6)];
			7: in1 <= in[`WORD(2)];
		endcase
		case (block_no) 
			0: in2 <= in[`WORD(29)]; 
			1: in2 <= in[`WORD(25)];
			2: in2 <= in[`WORD(21)];
			3: in2 <= in[`WORD(17)];
			4: in2 <= in[`WORD(13)];
			5: in2 <= in[`WORD(9)];
			6: in2 <= in[`WORD(5)];
			7: in2 <= in[`WORD(1)];
		endcase
		case (block_no) 
			0: in3 <= in[`WORD(28)]; 
			1: in3 <= in[`WORD(24)];
			2: in3 <= in[`WORD(20)];
			3: in3 <= in[`WORD(16)];
			4: in3 <= in[`WORD(12)];
			5: in3 <= in[`WORD(8)];
			6: in3 <= in[`WORD(4)];
			7: in3 <= in[`WORD(0)];
		endcase
		in0_s <= transform_en ? (in0 + 2048) >>> 12 : ((in0 << 7)+2048)  >>> 12 ;
		in1_s <= transform_en ? (in1 + 2048) >>> 12 : ((in1 << 7)+2048)  >>> 12 ;
		in2_s <= transform_en ? (in2 + 2048) >>> 12 : ((in2 << 7)+2048)  >>> 12 ;
		in3_s <= transform_en ? (in3 + 2048) >>> 12 : ((in3 << 7)+2048)  >>> 12 ;
		
		if (in0_s> 255) begin
			out0 <= 255;
		end else if (in0_s < -256) begin
			out0 <= -256;
		end else begin
			out0 <= in0_s[8:0];
		end
		
		if (in1_s> 255) begin
			out1 <= 255;
		end else if (in1_s < -256) begin
			out1 <= -256;
		end else begin
			out1 <= in1_s[8:0];
		end
		if (in2_s> 255) begin
			out2 <= 255;
		end else if (in2_s < -256) begin
			out2 <= -256;
		end else begin
			out2 <= in2_s[8:0];
		end
		if (in3_s> 255) begin
			out3 <= 255;
		end else if (in3_s < -256) begin
			out3 <= -256;
		end else begin
			out3 <= in3_s[8:0];
		end
		
		out <= {out0,out1,out2,out3};
	end
	
	
endmodule
