`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    21:50:59 08/29/2013 
// Design Name: 
// Module Name:    o 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module e(
    input signed[15:0] in,
    output reg [511:0] out,
    input [3:0] sel,
    input valid
    );
	reg signed [31:0] a90,	a88,	a85, a82, a78, a73, a67, a61, a54, a46, a38, a31, a22, a13, a4;
	always @(*) begin

			
		a90 <= in*90;
		a88 <= in*88;
		a85 <= in*85;
		a82 <= in*82;
		a78 <= in*78;
		a73 <= in*73;
		a67 <= in*67;
		a61 <= in*61;
		a54 <= in*54;
		a46 <= in*46;
		a38 <= in*38;
		a31 <= in*31;
		a22 <= in*22;
		a13 <= in*13;
		a4 <= in*4;
	
		if (valid) begin
			case (sel)
			0: out <= {a90,	a90,	a88,	a85,	a82,	a78,	a73,	a67,	a61,	a54,	a46,	a38,	a31,	a22,	a13,	a4};
			1: out <= {a90,	a82,	a67,	a46,	a22,	-a4,	-a31,	-a54,	-a73,	-a85,	-a90,	-a88,	-a78,	-a61,	-a38,	-a13};
			2: out <= {a88,	a67,	a31,	-a13,	-a54,	-a82,	-a90,	-a78,	-a46,	-a4,	a38,	a73,	a90,	a85,	a61,	a22};
			3: out <= {a85,	a46,	-a13,	-a67,	-a90,	-a73,	-a22,	a38,	a82,	a88,	a54,	-a4,	-a61,	-a90,	-a78,	-a31};
			4: out <= {a82,	a22,	-a54,	-a90,	-a61,	a13,	a78,	a85,	a31,	-a46,	-a90,	-a67,	a4,	a73,	a88,	a38};
			5: out <= {a78,	-a4,	-a82,	-a73,	a13,	a85,	a67,	-a22,	-a88,	-a61,	a31,	a90,	a54,	-a38,	-a90,	-a46};
			6: out <= {a73,	-a31,	-a90,	-a22,	a78,	a67,	-a38,	-a90,	-a13,	a82,	a61,	-a46,	-a88,	-a4,	a85,	a54};
			7: out <= {a67,	-a54,	-a78,	a38,	a85,	-a22,	-a90,	a4,	a90,	a13,	-a88,	-a31,	a82,	a46,	-a73,	-a61};
			8: out <= {a61,	-a73,	-a46,	a82,	a31,	-a88,	-a13,	a90,	-a4,	-a90,	a22,	a85,	-a38,	-a78,	a54,	a67};
			9: out <= {a54,	-a85,	-a4,	a88,	-a46,	-a61,	a82,	a13,	-a90,	a38,	a67,	-a78,	-a22,	a90,	-a31,	-a73};
			10: out <= {a46,	-a90,	a38,	a54,	-a90,	a31,	a61,	-a88,	a22,	a67,	-a85,	a13,	a73,	-a82,	a4,	a78};
			11: out <= {a38,	-a88,	a73,	-a4,	-a67,	a90,	-a46,	-a31,	a85,	-a78,	a13,	a61,	-a90,	a54,	a22,	-a82};
			12: out <= {a31,	-a78,	a90,	-a61,	a4,	a54,	-a88,	a82,	-a38,	-a22,	a73,	-a90,	a67,	-a13,	-a46,	a85};
			13: out <= {a22,	-a61,	a85,	-a90,	a73,	-a38,	-a4,	a46,	-a78,	a90,	-a82,	a54,	-a13,	-a31,	a67,	-a88};
			14: out <= {a13,	-a38,	a61,	-a78,	a88,	-a90,	a85,	-a73,	a54,	-a31,	a4,	a22,	-a46,	a67,	-a82,	a90};
			15: out <= {a4,	-a13,	a22,	-a31,	a38,	-a46,	a54,	-a61,	a67,	-a73,	a78,	-a82,	a85,	-a88,	a90,	-a90};

			endcase
			
		end else begin
			out <= 511'b0;
		end

	end 
endmodule
