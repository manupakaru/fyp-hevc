`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    20:37:04 08/29/2013 
// Design Name: 
// Module Name:    eeo 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module eeo(
    input signed [15:0] in,
    output reg [127:0] out,
    input [1:0] sel,
	 input valid // dependent on block no
    );
	 reg signed [31:0]a89, a75, a50, a18;
	always @(*) begin
		a89 <= in*89;
		a75 <= in*75;
		a50 <= in*50;
		a18 <= in*18;
		if (valid) begin
			case (sel)
			0: out <= {a89,a75,a50,a18};
			1: out <= {a75,-a18,-a89,-a50};
			2: out <= {a50,-a89,a18,a75};
			3: out <= {a18,-a50,a75,-a89};
			endcase
			
		end else begin
			out <= 127'b0;
		end

	end 

endmodule
