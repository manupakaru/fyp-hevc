`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    12:26:13 08/29/2013 
// Design Name: 
// Module Name:    partial_transform 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
`define Trafo32 2'b11
`define Trafo16 2'b10
`define Trafo8 2'b01
`define Trafo4 2'b00
`define WORD(A) 32*A+31:32*A
`define WORD_RANGE(A,B) 32*A+31:32*B

module partial_transform(
    input [31:0] in, //16*2
    output reg [1023:0] out, //32*32 ( LSB 5:0 is not needed -> 32*26=832)
    input [1:0] trafoSize,
    input [3:0] block_no,
	 input transform_en,
    input dst,
	 input valid,
	 input clk
    );
		wire signed[15:0] in_0, in_1;
		reg signed[31:0] out_0, out_1, out_2, out_3;
		wire[1023:0] idct_out;
		wire[1023:0] idst_out;
		wire[1023:0] trans_out;
		wire[1023:0] sum_out;
		
		reg[1023:0] acc;
		
		assign in_1 = in[31:16];
		assign in_0 = in[15:0];
	 idct	idct_d(
		 .in_0(in_0),
		 .in_1(in_1),
		 .trafoSize(trafoSize),
		 .block_no(block_no),
		 .out(idct_out)
		 );
	
	 always @(*) begin
		if (block_no[0]==1'b0) begin
			out_0 <= in_0*29 + in_1*74;
			out_1 <= in_0*55 + in_1*74;
			out_2 <= in_0*74 + in_1*0;
			out_3 <= in_0*84 - in_1*74;
		end else begin
			out_0 <= in_0*84 + in_1*55;
			out_1 <= -in_0*29 - in_1*84;
			out_2 <= -in_0*74 + in_1*74;
			out_3 <= in_0*55 - in_1*29;		
		end
	 end
	 assign idst_out = {out_0, out_1,out_2, out_3, 896'b0};
	 assign trans_out = dst ? idst_out : idct_out;
		assign sum_out[`WORD(31)] = (block_no==4'b0000) ? trans_out[`WORD(31)] : trans_out[`WORD(31)]+acc[`WORD(31)];
		assign sum_out[`WORD(30)] = (block_no==4'b0000) ? trans_out[`WORD(30)] : trans_out[`WORD(30)]+acc[`WORD(30)];
		assign sum_out[`WORD(29)] = (block_no==4'b0000) ? trans_out[`WORD(29)] : trans_out[`WORD(29)]+acc[`WORD(29)];
		assign sum_out[`WORD(28)] = (block_no==4'b0000) ? trans_out[`WORD(28)] : trans_out[`WORD(28)]+acc[`WORD(28)];
		assign sum_out[`WORD(27)] = (block_no==4'b0000) ? trans_out[`WORD(27)] : trans_out[`WORD(27)]+acc[`WORD(27)];
		assign sum_out[`WORD(26)] = (block_no==4'b0000) ? trans_out[`WORD(26)] : trans_out[`WORD(26)]+acc[`WORD(26)];
		assign sum_out[`WORD(25)] = (block_no==4'b0000) ? trans_out[`WORD(25)] : trans_out[`WORD(25)]+acc[`WORD(25)];
		assign sum_out[`WORD(24)] = (block_no==4'b0000) ? trans_out[`WORD(24)] : trans_out[`WORD(24)]+acc[`WORD(24)];
		assign sum_out[`WORD(23)] = (block_no==4'b0000) ? trans_out[`WORD(23)] : trans_out[`WORD(23)]+acc[`WORD(23)];
		assign sum_out[`WORD(22)] = (block_no==4'b0000) ? trans_out[`WORD(22)] : trans_out[`WORD(22)]+acc[`WORD(22)];
		assign sum_out[`WORD(21)] = (block_no==4'b0000) ? trans_out[`WORD(21)] : trans_out[`WORD(21)]+acc[`WORD(21)];
		assign sum_out[`WORD(20)] = (block_no==4'b0000) ? trans_out[`WORD(20)] : trans_out[`WORD(20)]+acc[`WORD(20)];
		assign sum_out[`WORD(19)] = (block_no==4'b0000) ? trans_out[`WORD(19)] : trans_out[`WORD(19)]+acc[`WORD(19)];
		assign sum_out[`WORD(18)] = (block_no==4'b0000) ? trans_out[`WORD(18)] : trans_out[`WORD(18)]+acc[`WORD(18)];
		assign sum_out[`WORD(17)] = (block_no==4'b0000) ? trans_out[`WORD(17)] : trans_out[`WORD(17)]+acc[`WORD(17)];
		assign sum_out[`WORD(16)] = (block_no==4'b0000) ? trans_out[`WORD(16)] : trans_out[`WORD(16)]+acc[`WORD(16)];
		assign sum_out[`WORD(15)] = (block_no==4'b0000) ? trans_out[`WORD(15)] : trans_out[`WORD(15)]+acc[`WORD(15)];
		assign sum_out[`WORD(14)] = (block_no==4'b0000) ? trans_out[`WORD(14)] : trans_out[`WORD(14)]+acc[`WORD(14)];
		assign sum_out[`WORD(13)] = (block_no==4'b0000) ? trans_out[`WORD(13)] : trans_out[`WORD(13)]+acc[`WORD(13)];
		assign sum_out[`WORD(12)] = (block_no==4'b0000) ? trans_out[`WORD(12)] : trans_out[`WORD(12)]+acc[`WORD(12)];
		assign sum_out[`WORD(11)] = (block_no==4'b0000) ? trans_out[`WORD(11)] : trans_out[`WORD(11)]+acc[`WORD(11)];
		assign sum_out[`WORD(10)] = (block_no==4'b0000) ? trans_out[`WORD(10)] : trans_out[`WORD(10)]+acc[`WORD(10)];
		assign sum_out[`WORD(9)] = (block_no==4'b0000) ? trans_out[`WORD(9)] : trans_out[`WORD(9)]+acc[`WORD(9)];
		assign sum_out[`WORD(8)] = (block_no==4'b0000) ? trans_out[`WORD(8)] : trans_out[`WORD(8)]+acc[`WORD(8)];
		assign sum_out[`WORD(7)] = (block_no==4'b0000) ? trans_out[`WORD(7)] : trans_out[`WORD(7)]+acc[`WORD(7)];
		assign sum_out[`WORD(6)] = (block_no==4'b0000) ? trans_out[`WORD(6)] : trans_out[`WORD(6)]+acc[`WORD(6)];
		assign sum_out[`WORD(5)] = (block_no==4'b0000) ? trans_out[`WORD(5)] : trans_out[`WORD(5)]+acc[`WORD(5)];
		assign sum_out[`WORD(4)] = (block_no==4'b0000) ? trans_out[`WORD(4)] : trans_out[`WORD(4)]+acc[`WORD(4)];
		assign sum_out[`WORD(3)] = (block_no==4'b0000) ? trans_out[`WORD(3)] : trans_out[`WORD(3)]+acc[`WORD(3)];
		assign sum_out[`WORD(2)] = (block_no==4'b0000) ? trans_out[`WORD(2)] : trans_out[`WORD(2)]+acc[`WORD(2)];
		assign sum_out[`WORD(1)] = (block_no==4'b0000) ? trans_out[`WORD(1)] : trans_out[`WORD(1)]+acc[`WORD(1)];
		assign sum_out[`WORD(0)] = (block_no==4'b0000) ? trans_out[`WORD(0)] : trans_out[`WORD(0)]+acc[`WORD(0)];

	 always @ (posedge clk) begin
		if (valid) begin
		
			acc <= sum_out;
			if (transform_en) begin
				case (trafoSize) 
					`Trafo4 : out <= (block_no==4'b0001) ? sum_out : out;
					`Trafo8 : out <= (block_no==4'b0011) ? sum_out : out;
					`Trafo16 : out <= (block_no==4'b0111) ? sum_out : out;
					`Trafo32 : out <= (block_no==4'b1111) ? sum_out : out;
				endcase 
			end else begin
				case (block_no)
					0: out[`WORD(31)] <= in_0;
					1: out[`WORD(29)] <= in_0; 
					2: out[`WORD(27)] <= in_0; 
					3: out[`WORD(25)] <= in_0; 
					4: out[`WORD(23)] <= in_0;
					5: out[`WORD(21)] <= in_0; 
					6: out[`WORD(19)] <= in_0; 
					7: out[`WORD(17)] <= in_0; 
					8: out[`WORD(15)] <= in_0;
					9: out[`WORD(13)] <= in_0; 
					10: out[`WORD(11)] <= in_0; 
					11: out[`WORD(9)] <= in_0; 
					12: out[`WORD(7)] <= in_0;
					13: out[`WORD(5)] <= in_0; 
					14: out[`WORD(3)] <= in_0; 
					15: out[`WORD(1)] <= in_0; 
				endcase
				case (block_no)
					0: out[`WORD(30)] <= in_1;
					1: out[`WORD(28)] <= in_1; 
					2: out[`WORD(26)] <= in_1; 
					3: out[`WORD(24)] <= in_1; 
					4: out[`WORD(22)] <= in_1;
					5: out[`WORD(20)] <= in_1; 
					6: out[`WORD(18)] <= in_1; 
					7: out[`WORD(16)] <= in_1; 
					8: out[`WORD(14)] <= in_1;
					9: out[`WORD(12)] <= in_1; 
					10: out[`WORD(10)] <= in_1; 
					11: out[`WORD(8)] <= in_1; 
					12: out[`WORD(6)] <= in_1;
					13: out[`WORD(4)] <= in_1; 
					14: out[`WORD(2)] <= in_1; 
					15: out[`WORD(0)] <= in_1; 
				endcase
			end 
		
		end else begin
			acc <= acc;
			out <= out;
		end 
	 end
	 
//	 wire[31:0] temp_31,temp_30,temp_29,temp_28,temp_27;
//	 assign temp_31 = out[`WORD(31)];
//	 assign temp_30 = out[`WORD(30)];
//	 assign temp_29 = out[`WORD(29)];
//	 assign temp_28 = out[`WORD(28)];
//	 assign temp_27 = out[`WORD(27)];
endmodule
