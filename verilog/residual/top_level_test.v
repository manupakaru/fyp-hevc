`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   11:10:49 09/12/2013
// Design Name:   top_level
// Module Name:   C:/Users/Maleen/Uni/HEVC/Transform/top_level_test.v
// Project Name:  Transform
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: top_level
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module top_level_test;

	// Inputs
	reg [31:0] data_in;
	reg clk;
	reg rst;
	reg empty;

	// Outputs
	wire read_next;
	wire [35:0] data_out;
	wire output_valid;
	wire output_sel;

	integer   fd, fdw;
   integer   line;
   reg [32*1:1] str;

	// Instantiate the Unit Under Test (UUT)
	top_level uut (
		.data_in(data_in), 
		.read_next(read_next), 
		.data_out(data_out), 
		.output_sel(output_sel),
		.output_valid(output_valid), 
		.clk(clk), 
		.rst(rst), 
		.empty(empty)
	);

	initial begin
		// Initialize Inputs
		data_in = 0;
		clk = 0;
		rst = 1; #5 rst=0;
		line =-4;
		empty = 1;
		fd = $fopen("cabac_to_residual","rb"); 
		fdw = $fopen("residual_to_inter_sim","wb");
		// Wait 100 ns for global reset to finish
		#95;
        
		// Add stimulus here
		rst = 1;
		empty = 0;
		while (line<180000) begin
			if (read_next) begin
				data_in[ 7: 0] = $fgetc(fd);
				data_in[15: 8] = $fgetc(fd);
				data_in[23:16] = $fgetc(fd);
				data_in[31:24] = $fgetc(fd);
				//$display("data  = %h", data);
				
				line = line +4;
			end
			#10;	
		end
		$fclose(fdw);
		$finish;
	end
	always 
		#5 clk = ~clk;
   
	
	
	always @ (posedge clk) begin
		if ($time> 110) begin
			if (output_valid) begin
				if (output_sel == 1) begin
					$fwrite(fdw, "%c", data_out[7:0]); 
					$fwrite(fdw, "%c", data_out[15:8]);
					$fwrite(fdw, "%c", data_out[23:16]);
					//if ($time==590)  $fwrite(fdw, "%c", data_out[23:16]);
					$fwrite(fdw, "%c", data_out[31:24]);
					//$display( "sel %d %h",$time, data_out[31:0]);
				end else begin
					$fwrite(fdw, "%c", data_out[34:27]);
					$fwrite(fdw, "%c", data_out[35] ? 8'hff : 8'h00); 
					$fwrite(fdw, "%c", data_out[25:18]);
					$fwrite(fdw, "%c", data_out[26] ? 8'hff : 8'h00); 
					$fwrite(fdw, "%c", data_out[16:9]);
					$fwrite(fdw, "%c", data_out[17] ? 8'hff : 8'h00); 
					$fwrite(fdw, "%c", data_out[7:0]);
					$fwrite(fdw, "%c", data_out[8] ? 8'hff : 8'h00); 
				
					$display( "%d %h",$time, data_out[31:0]);
				end
			end
		end
	end
endmodule

