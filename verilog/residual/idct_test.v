`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   14:35:44 09/03/2013
// Design Name:   idct
// Module Name:   C:/Users/Maleen/Uni/HEVC/Transform/idct_test.v
// Project Name:  Transform
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: idct
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////
`define Trafo32 2'b11
`define Trafo16 2'b10
`define Trafo8 2'b01
`define Trafo4 2'b00
`define WORD(A) 32*A+31:32*A
module idct_test;

	// Inputs
	reg [15:0] in_0;
	reg [15:0] in_1;
	reg [1:0] trafoSize;
	reg [3:0] block_no;

	// Outputs
	wire [1023:0] out;
	reg[31:0] a[0:31];
	
	integer i,j;
	// Instantiate the Unit Under Test (UUT)
	idct uut (
		.in_0(in_0), 
		.in_1(in_1), 
		.trafoSize(trafoSize), 
		.block_no(block_no), 
		.out(out)
	);

	initial begin
		// Initialize Inputs
		in_0 = 0;
		in_1 = 0;
		trafoSize = 0;
		block_no = 0;
		for (i=0;i<32;i=i+1) begin
			a[i] = 0;
		end
		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here
		for (i=0;i<16;i=i+1) begin
			trafoSize = `Trafo32;
			block_no = i;
			in_0 = i*2; in_1 = i*2+1; #5;
		
			a[0] = a[0] + out[`WORD(0)];
			a[1] = a[1] + out[`WORD(1)];
			a[2] = a[2] + out[`WORD(2)];
			a[3] = a[3] + out[`WORD(3)];
			a[4] = a[4] + out[`WORD(4)];
			a[5] = a[5] + out[`WORD(5)];
			a[6] = a[6] + out[`WORD(6)];
			a[7] = a[7] + out[`WORD(7)];
			a[8] = a[8] + out[`WORD(8)];
			a[9] = a[9] + out[`WORD(9)];
			a[10] = a[10] + out[`WORD(10)];
			a[11] = a[11] + out[`WORD(11)];
			a[12] = a[12] + out[`WORD(12)];
			a[13] = a[13] + out[`WORD(13)];
			a[14] = a[14] + out[`WORD(14)];
			a[15] = a[15] + out[`WORD(15)];
			a[16] = a[16] + out[`WORD(16)];
			a[17] = a[17] + out[`WORD(17)];
			a[18] = a[18] + out[`WORD(18)];
			a[19] = a[19] + out[`WORD(19)];
			a[20] = a[20] + out[`WORD(20)];
			a[21] = a[21] + out[`WORD(21)];
			a[22] = a[22] + out[`WORD(22)];
			a[23] = a[23] + out[`WORD(23)];
			a[24] = a[24] + out[`WORD(24)];
			a[25] = a[25] + out[`WORD(25)];
			a[26] = a[26] + out[`WORD(26)];
			a[27] = a[27] + out[`WORD(27)];
			a[28] = a[28] + out[`WORD(28)];
			a[29] = a[29] + out[`WORD(29)];
			a[30] = a[30] + out[`WORD(30)];
			a[31] = a[31] + out[`WORD(31)];
			
		//end
			#5;
		end
		
	end
      
endmodule

