`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   08:57:15 09/11/2013
// Design Name:   controller
// Module Name:   C:/Users/Maleen/Uni/HEVC/Transform/control_test.v
// Project Name:  Transform
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: controller
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module control_test;

	// Inputs
	reg [31:0] in; 
	reg empty;
	reg clk;
	reg rst;

	// Outputs
	wire read_next;
	wire [31:0] out;
	wire output_valid;
	wire output_sel;
	wire [5:0] QP;
	wire quant_en;
	wire row_col;
	wire [1:0] trafoSize;
	wire [3:0] block_no_pt;
	wire dst;
	wire valid_pt;
	wire [3:0] block_no_mem;
	wire [4:0] row_no;
	wire transform_en_mem;
	wire wr_en;
	wire [2:0] block_no_push;


	integer   fd;
   integer   line;
   reg [32*1:1] str;
	
	// Instantiate the Unit Under Test (UUT)
	controller uut (
		.in(in), 
		.empty(empty),
		.read_next(read_next), 
		.out(out), 
		.output_valid(output_valid), 
		.output_sel(output_sel), 
		.clk(clk),
		.rst(rst),		
		.QP(QP), 
		.quant_en(quant_en), 
		.row_col(row_col), 
		.trafoSize(trafoSize), 
		.block_no_pt(block_no_pt), 
		.dst(dst), 
		.valid_pt(valid_pt), 
		.block_no_mem(block_no_mem), 
		.row_no(row_no), 
		.transform_en_mem(transform_en_mem), 
		.wr_en(wr_en), 
		.block_no_push(block_no_push)
	);

	initial begin
		// Initialize Inputs
		in = 0;
		clk = 0;
		rst = 1; #5 rst=0;
		line =-4;
		empty = 1;
		fd = $fopen("cabac_to_residual","rb"); 
		// Wait 100 ns for global reset to finish
		#95;
        
		// Add stimulus here
		rst = 1;
		empty = 0;
		while (line<600) begin
			if (read_next) begin
				in[ 7: 0] = $fgetc(fd);
				in[15: 8] = $fgetc(fd);
				in[23:16] = $fgetc(fd);
				in[31:24] = $fgetc(fd);
				//$display("data  = %h", data);
				
				line = line +4;
			end
			#10;	
		end
		$finish;
	end
	always 
		#5 clk = ~clk;
      
endmodule

