`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    15:18:14 09/10/2013 
// Design Name: 
// Module Name:    trans_mem 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
`define WORD(A) 32*A+31:32*A
module trans_mem(
    input [1023:0] in,
    output reg[31:0] out,
	 input transform_en,
    input [3:0] block_no,
    input [4:0] row_no,
    input clk,
    input wr_en
    );
	
	reg [4:0] row_no_D1;
	reg [8:0] addr0, addr1;
	reg [15:0] din0, din1;
	wire [15:0] dout0, dout1;
	
	reg signed[31:0] in0, in1;
	reg signed[31:0] in0_64, in1_64;
	reg signed[15:0] clipped0, clipped1;
	
	mem_bank bank0 (
	  .clka(clk), // input clka
	  .wea(wr_en), // input [0 : 0] wea
	  .addra(addr0), // input [8 : 0] addra
	  .dina(din0), // input [15 : 0] dina
	  .douta(dout0) // output [15 : 0] douta
	);
	mem_bank bank1 (
	  .clka(clk), // input clka
	  .wea(wr_en), // input [0 : 0] wea
	  .addra(addr1), // input [8 : 0] addra
	  .dina(din1), // input [15 : 0] dina
	  .douta(dout1) // output [15 : 0] douta
	);
	always @(posedge clk) begin
		row_no_D1 <= row_no;
	end
	always @(*) begin
		case (block_no) 
			0: in0 <= in[`WORD(31)]; 
			1: in0 <= in[`WORD(29)]; 
			2: in0 <= in[`WORD(27)]; 
			3: in0 <= in[`WORD(25)]; 
			4: in0 <= in[`WORD(23)]; 
			5: in0 <= in[`WORD(21)]; 
			6: in0 <= in[`WORD(19)]; 
			7: in0 <= in[`WORD(17)]; 
			8: in0 <= in[`WORD(15)]; 
			9: in0 <= in[`WORD(13)]; 
			10: in0 <= in[`WORD(11)]; 
			11: in0 <= in[`WORD(9)]; 
			12: in0 <= in[`WORD(7)]; 
			13: in0 <= in[`WORD(5)]; 
			14: in0 <= in[`WORD(3)]; 
			15: in0 <= in[`WORD(1)]; 
		endcase
		case (block_no) 
			0: in1 <= in[`WORD(30)]; 
			1: in1 <= in[`WORD(28)]; 
			2: in1 <= in[`WORD(26)]; 
			3: in1 <= in[`WORD(24)]; 
			4: in1 <= in[`WORD(22)]; 
			5: in1 <= in[`WORD(20)]; 
			6: in1 <= in[`WORD(18)]; 
			7: in1 <= in[`WORD(16)]; 
			8: in1 <= in[`WORD(14)]; 
			9: in1 <= in[`WORD(12)]; 
			10: in1 <= in[`WORD(10)]; 
			11: in1 <= in[`WORD(8)]; 
			12: in1 <= in[`WORD(6)]; 
			13: in1 <= in[`WORD(4)]; 
			14: in1 <= in[`WORD(2)]; 
			15: in1 <= in[`WORD(0)]; 
		endcase
		in0_64 <= transform_en ? (in0 + 64)>>>7 : in0;
		in1_64 <= transform_en ? (in1 + 64)>>>7 : in1;
		//in1 <= in[64*block_no + 31 : 64*block_no + 0]; 
		//if (transform_en) begin
			if (in0_64 > 32767) begin
				clipped0 <= 32767;
			end else if (in0_64 < -32768) begin
				clipped0 <= -32768;
			end else begin
				clipped0 <= in0_64[15:0];
			end
		//end else begin
		//	clipped0 <= skip_in[31:16];
		//end 
		
		//if (transform_en) begin
			if (in1_64 > 32767) begin
				clipped1 <= 32767;
			end else if (in1_64 < -32768) begin
				clipped1 <= -32768;
			end else begin
				clipped1 <= in1_64[15:0];
			end
		//end else begin
		//	clipped1 <= skip_in[15:0];
		//end
	end
	
	always @(*) begin
		if (wr_en) begin
			addr0 <= {row_no, block_no};
			addr1 <= {row_no, block_no};
			out <= 32'b0;
		end else begin
			if (~row_no[0]) begin // even
				addr0 <= {block_no[3:0], 1'b0, row_no[4:1]};
				addr1 <= {block_no[3:0], 1'b1, row_no[4:1]};
				
			end else begin
				addr0 <= {block_no[3:0], 1'b1, row_no[4:1]};
				addr1 <= {block_no[3:0], 1'b0, row_no[4:1]};	
								
			end
			if (~row_no_D1[0]) begin
				out <= {dout1, dout0};
			end else begin
				out <= {dout0, dout1};
			end
		end
	end
	
	always @(*) begin
		if (~row_no[0]) begin
			din0 <= clipped0;
			din1 <= clipped1;
		end else begin
			din0 <= clipped1;
			din1 <= clipped0;
		end	
		
	end

endmodule
