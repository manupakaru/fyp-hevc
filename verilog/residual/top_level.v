`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    19:55:00 09/10/2013 
// Design Name: 
// Module Name:    top_level 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module top_level(
    input [31:0] data_in,
    output read_next,
    output [35:0] data_out,
    output output_valid,
	 output output_sel, // for testbench only
    input clk,
    input rst,
	 input empty
    );

	 wire [35:0] out_dp;
	 wire [31:0] out_con;
	 //wire output_sel;
	// Dequantize
    wire [5:0] QP ;
	 wire quant_en;
	 wire row_col; // 0 -row, 1-col
	 // Transform
	 wire [1:0] trafoSize;
	 wire [3:0] block_no_pt;
	 wire dst;
	 wire valid;
	 // Memory
	 wire [3:0] block_no_mem;
	 wire [4:0] row_no;
	 wire transform_en_mem;
	 wire wr_en;
	 // Push
	 wire [2:0] block_no_push;

	data_path data_path(
	 .in(data_in),
	 .clk(clk),
    .out(out_dp),
	 // Dequantize
    .QP(QP), 
	 .quant_en(quant_en), 
	 .row_col(row_col), // 0 -row, 1-col
	 // Transform
	 .trafoSize(trafoSize),  
	 .block_no_pt(block_no_pt),
	 .dst(dst),
	 .valid(valid),
	 // Memory
	 .block_no_mem(block_no_mem),
	 .row_no(row_no),
	 .transform_en_mem(transform_en_mem),
	 .wr_en(wr_en),
	 // Push
	.block_no_push(block_no_push)
    );
	 
	 controller controller(
    .in(data_in),
	 .empty(empty),
	 .read_next(read_next),
    .clk(clk),
	 .rst(rst),
    .out(out_con),
    .output_valid(output_valid),
	 .output_sel(output_sel),
    .QP(QP), 
	 .quant_en(quant_en), 
	 .row_col(row_col), // 0 -row, 1-col
	 // Transform
	 .trafoSize(trafoSize),  
	 .block_no_pt(block_no_pt),
	 .dst(dst),
	 .valid_pt(valid),
	 // Memory
	 .block_no_mem(block_no_mem),
	 .row_no(row_no),
	 .transform_en_mem(transform_en_mem),
	 .wr_en(wr_en),
	 // Push
	.block_no_push(block_no_push)
    );
	 
	 assign data_out = output_sel ? {4'b0000, out_con} : out_dp;
endmodule
