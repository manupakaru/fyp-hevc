`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    09:49:58 09/10/2013 
// Design Name: 
// Module Name:    dequant 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
`define Trafo32 2'b11
`define Trafo16 2'b10
`define Trafo8 2'b01
`define Trafo4 2'b00

module dequant(
    input [31:0] in,
	 input [31:0] in_mem,
    input [5:0] QP,
	 input quant_en, // skip scaling
	 input [1:0] trafoSize,
	 input row_col, // 0 -col, 1-row
	 input clk,
    output reg [31:0] out
    );
	wire signed [15:0] in_0, in_1;
	reg signed[18:0] factor;
	reg[3:0] QPby6;
	reg [2:0] QPmod6;
	reg signed [31:0] out_0, out_1;
	reg signed [15:0]  out_1_clipped, out_0_clipped;
	reg [2:0] bdShift_m1;
	reg [3:0] bdShift;
	assign in_1 = in[31:16]; assign in_0 = in[15:0]; // swapped the two here
	reg[1:0] out_1_sel, out_0_sel; //1->32767, 0->-32768
	
	
	always @(*) begin
		
		case (QP) 
		0,6,12,18,24,30,36,42,48 : QPmod6 <= 0;
		1,7,13,19,25,31,37,43,49 : QPmod6 <= 1;
		2,8,14,20,26,32,38,44,50 : QPmod6 <= 2;
		3,9,15,21,27,33,39,45,51 : QPmod6 <= 3;
		4,10,16,22,28,34,40,46: QPmod6 <= 4;
		5,11,17,23,29,35,41,47: QPmod6 <= 5;
		default : QPmod6 <= 3'bxxx;
		endcase
		// values increased by 4 to reflect multiply by 16
		case (QP) 
		0,1,2,3,4,5 :  QPby6 <= 4;
		6,7,8,9,10,11 :  QPby6 <= 5;
		12,13,14,20,26,32 :  QPby6 <= 6;
		18,19,20,21,22,23 :  QPby6 <= 7;
		24,25,26,27,28,29 :  QPby6 <= 8;
		30,31,32,33,34,35 :  QPby6 <= 9;
		36,37,38,39,40,41 :  QPby6 <= 10;
		42,43,44,45,46,47 :  QPby6 <= 11;
		48,49,50,51,52,53 :  QPby6 <= 12;
		default :  QPby6 <= 4'd4;
		
		endcase
		
		case (QPmod6)
		0: factor <= (7'd40<<QPby6);
		1: factor <= (7'd45<<QPby6);
		2: factor <= (7'd51<<QPby6);
		3: factor <= (7'd57<<QPby6);
		4: factor <= (7'd64<<QPby6);
		5: factor <= (7'd72<<QPby6);
		default : factor <= 19'dx;
		endcase
		
		case (trafoSize)
			`Trafo4: bdShift <= 5;
			`Trafo8: bdShift <= 6;
			`Trafo16: bdShift <= 7;
			`Trafo32: bdShift <= 8;
		endcase
		case (trafoSize)
			`Trafo4: bdShift_m1 <= 4;
			`Trafo8: bdShift_m1 <= 5;
			`Trafo16: bdShift_m1 <= 6;
			`Trafo32: bdShift_m1 <= 7;
		endcase
		
		out_1 <= ( (factor*in_1)+ (1<<bdShift_m1) ) >>> bdShift;
		out_1_sel[1] = (out_1 > 32767)?1'b1:1'b0;  out_1_sel[0] = (out_1 < -32768)?1'b1:1'b0;

		out_0 <= ( (factor*in_0)+ (1<<bdShift_m1) ) >>> bdShift;
		out_0_sel[1] = (out_0 > 32767)?1'b1:1'b0;  out_0_sel[0] = (out_0 < -32768)?1'b1:1'b0;

	end
	always @(*) begin
		case (out_1_sel)
		2'b00: out_1_clipped <= out_1[15:0];
		2'b01: out_1_clipped <= -32768;
		2'b10: out_1_clipped <=  32767;
		default: out_1_clipped <= 16'bx;
		endcase
//		if (out_1 > 32767) begin
//			out_1_clipped <= 32767;
//		end else if (out_1 < -32768) begin
//			out_1_clipped <= -32768;
//		end else begin
//			out_1_clipped <= out_1;
//		end
		case (out_0_sel)
		2'b00: out_0_clipped <= out_0[15:0];
		2'b01: out_0_clipped <= -32768;
		2'b10: out_0_clipped <=  32767;
		default: out_0_clipped <= 16'bx;
		endcase
//		if (out_0 > 32767) begin
//			out_0_clipped <= 32767;
//		end else if (out_0 < -32768) begin
//			out_0_clipped <= -32768;
//		end else begin
//			out_0_clipped <= out_0;
//		end
	end
	always @(posedge clk) begin

		if (!row_col) begin
			if (quant_en) begin 
				out <= {out_1_clipped, out_0_clipped};
			end else begin
				out <= in;
			end
		end else begin
			out <= in_mem;
		end
	end
endmodule
