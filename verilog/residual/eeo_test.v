`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   22:09:25 08/29/2013
// Design Name:   eeo
// Module Name:   C:/Users/Maleen/Uni/HEVC/Transform/eeo_test.v
// Project Name:  Transform
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: eeo
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module eeo_test;

	// Inputs
	reg [15:0] in;
	reg [1:0] sel;
	reg valid;

	// Outputs
	wire [127:0] out;

	// Instantiate the Unit Under Test (UUT)
	eeo uut (
		.in(in), 
		.out(out), 
		.sel(sel), 
		.valid(valid)
	);

	initial begin
		// Initialize Inputs
		in = 0;
		sel = 0;
		valid = 0;

		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here
		in = -16'd5;
		sel = 1;
	end
      
endmodule

