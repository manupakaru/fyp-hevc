`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   13:50:04 09/10/2013
// Design Name:   partial_transform
// Module Name:   C:/Users/Maleen/Uni/HEVC/Transform/partial_test.v
// Project Name:  Transform
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: partial_transform
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////
`define Trafo32 2'b11
`define Trafo16 2'b10
`define Trafo8 2'b01
`define Trafo4 2'b00
module partial_test;

	// Inputs
	reg [31:0] in;
	reg [1:0] trafoSize;
	reg [3:0] block_no;
	reg dst;
	reg valid;
	reg clk;

	// Outputs
	wire [1023:0] out;

	// Instantiate the Unit Under Test (UUT)
	partial_transform uut (
		.in(in), 
		.out(out), 
		.trafoSize(trafoSize), 
		.block_no(block_no), 
		.dst(dst), 
		.valid(valid),
		.clk(clk)
	);

	initial begin
		// Initialize Inputs
		in = 0;
		trafoSize = `Trafo4;
		block_no = 0;
		dst = 0;
		valid = 0;
		clk = 0;

		// Wait 100 ns for global reset to finish
		#100;
      #1;  
		// Add stimulus here
		in = {16'd1, 16'd2};
		block_no = 0; valid = 1;
		#10;
		in = {16'd3, 16'd4};
		block_no = 1; valid = 1;
		#10;
		
		in = {-16'd1, 16'd2};
		block_no = 0; valid = 1;
		#10;
		in = {16'd3, -16'd4};
		block_no = 1; valid = 1;
		#10
		valid =0;
		
		trafoSize = `Trafo8;
		in = {16'd1, 16'd2};
		block_no = 0; valid = 1;
		#10;
		in = {16'd3, 16'd4};
		block_no = 1; valid = 1;
		#10
		in = {16'd5, 16'd6};
		block_no = 2; valid = 1;
		#10;
		in = {16'd7, 16'd8};
		block_no = 3; valid = 1;
		#10
		valid =0;
	end
	always 
		#5 clk = ~ clk;
      
endmodule

