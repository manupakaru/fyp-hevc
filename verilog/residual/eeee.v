`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    22:14:59 08/29/2013 
// Design Name: 
// Module Name:    eee 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module eeee(
    input signed[15:0] in,
    output reg[63:0] out,
    input sel,
    input valid // driven by trafosize
    );
	 reg signed [31:0]a64;
	always @(*) begin
		a64 <= in*64;


		if (valid) begin
			case (sel)
			0: out <= {a64,a64};
			1: out <= {a64, -a64};
			endcase
			
		end else begin
			out <= 63'b0;
		end

	end 

endmodule
