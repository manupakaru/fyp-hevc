`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    21:07:22 08/29/2013 
// Design Name: 
// Module Name:    eo 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module eo(
    input signed [15:0] in,
    output reg [255:0] out,
    input [2:0] sel,
    input valid
    );
	reg signed [31:0] a90,	a87,	a80,	a70,	a57,	a43,	a25,	a9;
	reg [31:0] a1, a4 ,a5, a8, a16, a36, a35, a40, a45, a32,  a86 ;

	always @(*) begin
//		a1 <= in; 
//		a4 <= a1<<2; a5 <= a1+a4; a8 <= a1<<3; a9 <= a1+a8;
//		a16 <= a1<<4; a25 <= a16+a9; a36 <= a9<<2; a35<=a36-a1;
//		a40 <= a5<<3; a45<=a5+a40; a43 <= a35+a8; a32<=a1<<5; 
//		a57 <= a25+ a32; a86 <= a43<<1; a87 <= a1+a86; a90 <= a45<<1;
//		a80 <= a5 <<4; a70 <= a35<<1;
		
		a90 <= in*90;
		a87 <= in*87;
		a80 <= in*80;
		a70 <= in*70;
		a57 <= in*57;
		a43 <= in*43;
		a25 <= in*25;
		a9 <= in*9;
		if (valid) begin
			case (sel)
			0: out <= {a90,	a87,	a80,	a70,	a57,	a43,	a25,	a9};
			1: out <= {a87,	a57,	a9,	-a43,	-a80,	-a90,	-a70,	-a25};
			2: out <= {a80,	a9,	-a70,	-a87,	-a25,	a57,	a90,	a43};
			3: out <= {a70,	-a43,	-a87,	a9,	a90,	a25,	-a80,	-a57};
			4: out <= {a57,	-a80,	-a25,	a90,	-a9,	-a87,	a43,	a70};
			5: out <= {a43,	-a90,	a57,	a25,	-a87,	a70,	a9,	-a80};
			6: out <= {a25,	-a70,	a90,	-a80,	a43,	a9,	-a57,	a87};
			7: out <= {a9,	-a25,	a43,	-a57,	a70,	-a80,	a87,	-a90};
			endcase
			
		end else begin
			out <= 255'b0;
		end

	end 

endmodule
