`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    19:58:18 09/10/2013 
// Design Name: 
// Module Name:    controller 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
`define Trafo32 2'b11
`define Trafo16 2'b10
`define Trafo8 2'b01
`define Trafo4 2'b00
module controller(
    input [31:0] in,
	 input empty,
	 // wires
	 output reg read_next,
    output reg [31:0] out,
    output reg output_valid,
	 output reg output_sel, // 0-datapath 1-controller
	 input clk,
	 input rst,
	 // registered out
    output reg[5:0] QP, //
    output reg quant_en, //
    output reg row_col,
    output reg [1:0] trafoSize, //
    output reg[3:0] block_no_pt,
    output reg dst, //
    output reg valid_pt,
    output reg [3:0] block_no_mem,
    output reg [4:0] row_no,
    output reg transform_en_mem, //
    output reg wr_en,
	 // wire
    output reg [2:0] block_no_push
    );
	 
	reg signed [11:0]  dequant_cycle;
	reg signed[11:0]	pt_cycle,mem_cycle,push_cycle ;
	reg dequant_stage, pt_stage;
	reg state; // 0-forward, 1-transform
	
	
	 
	reg [1:0] fwd_count;
	reg signed [5:0] QPY;
	reg signed [7:0] QP_raw;
	reg [5:0] QP_clip;
	reg signed [4:0] pps_cb_offset, pps_cr_offset, slice_cb_offset, slice_cr_offset; 
	reg transquant_bypass, transform_skip;
	reg res_present;
	reg mode_intra;

//	reg [9:0] num_coeff_words; // when transform begins in 0x40
//	reg [9:0] num_coeff_words_41;
	reg[1:0] trafoSize_local; //wire

	always @ (*) begin
		if (~rst) begin
			out <= 31'b0;
			read_next <= 1'b0;
			output_valid <= 1'b0;
			block_no_push <= 3'b000;
			output_sel <= 1'b1; // default controller
		end else begin
			if (~empty) begin
				if (~state) begin
					out <= in;
					read_next <= 1'b1;
					output_valid <= 1'b1;
					block_no_push <= 3'b000;
					output_sel <= 1'b1;
				end else begin
					
					read_next <= (dequant_cycle < (1<<(2*trafoSize+3)) );
					if (push_cycle >= 0) begin
						case (trafoSize) 
							`Trafo4 : output_valid <= (push_cycle[0] == 1'b0);
							`Trafo8 : output_valid <= (push_cycle[1] == 1'b0);
							`Trafo16: output_valid <= (push_cycle[2] == 1'b0);
							`Trafo32: output_valid <= (push_cycle[3] == 1'b0);
						endcase
						case (trafoSize)
							`Trafo4 : block_no_push <= 3'b000;
							`Trafo8 : block_no_push <= {2'b00, push_cycle[0]};
							`Trafo16: block_no_push <= {1'b0, push_cycle[1:0]};
							`Trafo32: block_no_push <= {push_cycle[2:0]};
						endcase;
					end else begin
						output_valid <= 1'b0; // should be 1 in the immediate cycle after entring
						block_no_push <= 3'b000;
					end
					output_sel <= 1'b0;
					out <=  31'b0;
				end
			end else begin
				out <= 31'b0;
				read_next <= 1'b0;
				output_valid <= 1'b0;
				block_no_push <= 3'b000;
				output_sel <= 1'b1;
			end
		end
		
	
	end
	
	reg [2:0] QP_adjust;
	always @(*) begin
		case (in[19:18])
			2'b00, 2'b11: QP_raw <= {2'b00, QPY};
			2'b01: QP_raw <= {2'b00, QPY} + pps_cb_offset + slice_cb_offset;
			2'b10: QP_raw <= {2'b00, QPY} + pps_cr_offset + slice_cr_offset;
		endcase
		if (QP_raw > 57 )begin
			QP_clip <= 57;
		end else if (QP_raw < 0) begin
			QP_clip <= 0;
		end else begin
			QP_clip <= QP_raw[5:0];
		end
		case (QP_clip) 
			30,31,32,33,34	: QP_adjust <= 3'd1;
			35,36				: QP_adjust <= 3'd2;
			37,38				: QP_adjust <= 3'd3;
			39,40				: QP_adjust <= 3'd4;
			41,42				: QP_adjust <= 3'd5;
			43,45,46,47,48,49,50,51,52,53,54,55,56,57 : QP_adjust <= 3'd6;
			default 			: QP_adjust <= 3'd0;
		endcase
		
		quant_en <= ~transquant_bypass;
		transform_en_mem <= ~(transquant_bypass | transform_skip);
	end
	
	
	always @(posedge clk or negedge rst)begin
		if (~rst) begin
			fwd_count <= 10'b0;
			state <= 1'b0;
			//output_valid <= 1'b0;
			//read_next <= 1'b0;
			QP <= 0; 
			QPY <=0;
			transform_skip <= 1'b0;
			transquant_bypass <= 1'b0;
			trafoSize <= `Trafo4;
			res_present <= 1'b0;
			dst <= 1'b0;
			mode_intra <= 1'b0;
			row_col <= 1'b0;
			valid_pt <= 1'b0;
			block_no_pt <= 4'b0;
			block_no_mem <= 4'b0;
			wr_en <= 1'b0;
			row_no <= 5'b0;
			//block_no_push <= 3'b0;
		end else begin
			if (~empty) begin
				if (~state) begin
					valid_pt<=1'b0;
					block_no_pt <= 4'b0;
					block_no_mem <= 4'b0;
					wr_en <= 1'b0;
					row_no <= 5'b0;
					//block_no_push <= 3'b0;
					if (fwd_count >0) begin
						fwd_count <= fwd_count -2'b1;
						//out <= in;
						//output_valid <= 1'b1;
						//read_next <= 1'b1;
					end else begin
						case (in[7:0])
							8'h10: fwd_count <= 2'd1; // POC
							8'h50: fwd_count <= (in[12] ? 2'd0 : 2'd2); // motion vector
							default : fwd_count <= 0;
						endcase
							// h_40					&     not luma           & res_present      h_41
						if ( ( (in[7:0] == 8'h40) & (in[19:18] != 2'b00) & (in[29]) ) | ( (in[7:0] == 8'h41) & res_present  ) ) begin
							dequant_cycle <=0;
							state <= 1'b1;
							row_col <= 1'b0;

						end
						//output_valid <= 1'b1;

						if (in[7:0] == 8'h01) begin
							pps_cb_offset <= in[15:11];
							pps_cr_offset <= in[20:16];
						end
						if (in[7:0] == 8'h12) begin
							slice_cb_offset <= in[12:8];
							slice_cr_offset <= in[17:13];
						end
						if (in[7:0] == 8'h30) begin
							transquant_bypass <= in[27];
							mode_intra <= ~in[23];
						end
						if (in[7:0] == 8'h40) begin
							case (in[19:18])
								2'b01: QP <= QP_clip - QP_adjust;
								2'b10: QP <= QP_clip - QP_adjust; // otherwise latch
							endcase
							transform_skip <= in[30];
							case (in[22:20]) 
								2: trafoSize <= `Trafo4;
								3: trafoSize <= `Trafo8;
								4: trafoSize <= `Trafo16;
								5: trafoSize <= `Trafo32;
								default: trafoSize <= `Trafo4;
							endcase
							res_present <= in[29];
							dst <= mode_intra & (in[19:18]==2'b00) & (in[22:20]==3'd2);
						end
						if (in[7:0] == 8'h41) begin
							QPY <= in[17:12];
							QP <= in[17:12];
						end
					end
				end else begin
					dequant_cycle <= dequant_cycle +12'd1;
					if (dequant_cycle < (1<<(2*trafoSize+3)) ) begin
						row_col <= 1'b0;
						//read_next <= 1'b1;
					end else begin
						row_col <= 1'b1;
						//read_next <= 1'b0;
					end
					valid_pt <= (pt_cycle < (1<<(2*trafoSize+3)));
					case (trafoSize)
						`Trafo4 : block_no_pt <= {3'b000, pt_cycle[0]};
						`Trafo8 : block_no_pt <= {2'b00, pt_cycle[1:0]};
						`Trafo16 : block_no_pt <= {1'b0, pt_cycle[2:0]};
						`Trafo32 : block_no_pt <= {pt_cycle[3:0]};
					endcase
					
					wr_en <= (mem_cycle >=0 & mem_cycle <(1<<(2*trafoSize+3))) ? 1'b1 : 1'b0;
					case (trafoSize)
						`Trafo4 : block_no_mem <= {3'b000, mem_cycle[0]};
						`Trafo8 : block_no_mem <= {2'b00, mem_cycle[1:0]};
						`Trafo16 : block_no_mem <= {1'b0, mem_cycle[2:0]};
						`Trafo32 : block_no_mem <= {mem_cycle[3:0]};
					endcase
					if (mem_cycle >=0 & mem_cycle <(1<<(2*trafoSize+3))) begin // writing
						case (trafoSize)
							`Trafo4 : row_no <= {3'b000,2'b11- mem_cycle[2:1]};
							`Trafo8 : row_no <= {2'b00,3'b111- mem_cycle[4:2]};
							`Trafo16 : row_no <= {1'b0,4'b1111- mem_cycle[6:3]};
							`Trafo32 : row_no <= {5'b11111- mem_cycle[8:4]};
						endcase
					end else begin // reading
						case (trafoSize)
							`Trafo4 : row_no <= {3'b000, mem_cycle[2:1]};
							`Trafo8 : row_no <= {2'b00, mem_cycle[4:2]};
							`Trafo16 : row_no <= {1'b0, mem_cycle[6:3]};
							`Trafo32 : row_no <= {mem_cycle[8:4]};
						endcase
					end

					if (dequant_cycle == dequant_limit) begin
						state <= 1'b0;
					end
				end
			end else begin // empty
				//out <= 36'b0;
				wr_en <= 1'b0;
				//output_valid <= 1'b0;
				//read_next <= 1'b0;
				valid_pt <= 1'b0;
			end
		
		end
	end
	
	
	reg[11:0] dequant_limit;
	reg[11:0] mem_offset, push_offset;
	reg[11:0] stage_boundary;
	reg[11:0] PT_cycle;

	always @(*) begin
		case (trafoSize) 
			0: dequant_limit <=  21;
			1: dequant_limit <=  72;
			2: dequant_limit <= 270;
			3: dequant_limit <=1050;
		endcase
		case (trafoSize) 
			0: mem_offset <=  2;
			1: mem_offset <=  4;
			2: mem_offset <=  8;
			3: mem_offset <= 16;
		endcase
		case (trafoSize) 
			0: stage_boundary <=  12;
			1: stage_boundary <=  38;
			2: stage_boundary <= 138;
			3: stage_boundary <= 530;
		endcase
		case (trafoSize)  //(T*T/2 + T + 3)
			0: push_offset <=  15;
			1: push_offset <=  43;
			2: push_offset <= 147;
			3: push_offset <= 547;
		endcase
		pt_stage <= (dequant_cycle >= stage_boundary);
		pt_cycle <= pt_stage ? dequant_cycle - stage_boundary : dequant_cycle;
		mem_cycle <= dequant_cycle - mem_offset;
		push_cycle <= (dequant_cycle - push_offset) ;
	end
endmodule
