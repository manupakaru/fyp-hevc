`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    10:40:00 09/03/2013 
// Design Name: 
// Module Name:    idct_4 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////

`define Trafo32 2'b11
`define Trafo16 2'b10
`define Trafo8 2'b01
`define Trafo4 2'b00
`define WORD(A) 32*A+31:32*A

module idct(
    input [15:0] in_0,
    input [15:0] in_1, // odd
    input [1:0] trafoSize,
    input [3:0] block_no,
    output [1023:0] out
    );

	wire eeee_valid, eeeo_valid, eeo_valid, eo_valid, o_valid;
	reg eeee_sel, eeeo_sel;
	reg[1:0] eeo_sel;
	reg[2:0] eo_sel;
	reg[3:0] o_sel;
	wire[15:0] eeee_in, eeeo_in, eeo_in, eo_in, o_in;
	wire[63:0] eeee, eeeo; wire[127:0] eeo; wire [255:0] eo; wire [511:0] o;
	wire[127:0] eee; 	wire[255:0] ee;	wire[511:0] e;
	
	assign eeee_valid = (trafoSize == `Trafo32 & block_no[2:0] == 3'b000) | (trafoSize == `Trafo16 & block_no[1:0] == 2'b00) | (trafoSize == `Trafo8 & block_no[0] == 1'b0) | (trafoSize == `Trafo4 ) ;
	assign eeeo_valid = (trafoSize == `Trafo32 & block_no[2:0] == 3'b100) | (trafoSize == `Trafo16 & block_no[1:0] == 2'b10) | (trafoSize == `Trafo8 & block_no[0] == 1'b1) | (trafoSize == `Trafo4 );
	assign eeo_valid  = (trafoSize == `Trafo32 & block_no[1:0] == 2'b10) | (trafoSize == `Trafo16 & block_no[0] == 1'b1) | (trafoSize == `Trafo8);
	assign eo_valid   = (trafoSize == `Trafo32 & block_no[0] == 1'b1) | (trafoSize == `Trafo16);
	assign o_valid    = (trafoSize == `Trafo32);
	
	always @(*) begin
		case (trafoSize)
			`Trafo32: eeee_sel <= block_no[3];
			`Trafo16: eeee_sel <= block_no[2];
			`Trafo8 : eeee_sel <= block_no[1];
			`Trafo4 : eeee_sel <= block_no[0];
		endcase
		case (trafoSize)
			`Trafo32: eeeo_sel <= block_no[3];
			`Trafo16: eeeo_sel <= block_no[2];
			`Trafo8 : eeeo_sel <= block_no[1];
			`Trafo4 : eeeo_sel <= block_no[0];
		endcase
		case (trafoSize)
			`Trafo32: eeo_sel <= block_no[3:2];
			`Trafo16: eeo_sel <= block_no[2:1];
			`Trafo8 : eeo_sel <= block_no[1:0];
			`Trafo4 : eeo_sel <= 2'bxx;
		endcase
		case (trafoSize)
			`Trafo32: eo_sel <= block_no[3:1];
			`Trafo16: eo_sel <= block_no[2:0];
			`Trafo8 : eo_sel <= 3'bxxx;
			`Trafo4 : eo_sel <= 3'bxxx;
		endcase
		case (trafoSize)
			`Trafo32: o_sel <= block_no[3:0];
			`Trafo16: o_sel <= 4'bxxxx;
			`Trafo8 : o_sel <= 4'bxxxx;
			`Trafo4 : o_sel <= 4'bxxxx;
		endcase
	end
	assign eeee_in = in_0;
	assign eeeo_in = (trafoSize == `Trafo4 ? in_1 : in_0);
	assign eeo_in = (trafoSize == `Trafo8 ? in_1:in_0);
	assign eo_in = (trafoSize == `Trafo16 ? in_1:in_0);
	assign o_in = in_1;
	
	eeee eeee_d (
		.in(eeee_in),
		.out(eeee),
		.sel(eeee_sel),
		.valid(eeee_valid) 
		);
	
	eeeo eeeo_d (
		.in(eeeo_in),
		.out(eeeo),
		.sel(eeeo_sel),
		.valid(eeeo_valid) 
		);
		
	eeo eeo_d (
		.in(eeo_in),
		.out(eeo),
		.sel(eeo_sel),
		.valid(eeo_valid) 
		);
	eo eo_d (
		.in(eo_in),
		.out(eo),
		.sel(eo_sel),
		.valid(eo_valid) 
		);
	o o_d (
		.in(o_in),
		.out(o),
		.sel(o_sel),
		.valid(o_valid) 
		);
	
	assign eee = {eeee[`WORD(1)] + eeeo[`WORD(1)], eeee[`WORD(0)] + eeeo[`WORD(0)],eeee[`WORD(0)] - eeeo[`WORD(0)],eeee[`WORD(1)] - eeeo[`WORD(1)]};
	assign ee = { eee[`WORD(3)] + eeo[`WORD(3)] , eee[`WORD(2)] + eeo[`WORD(2)], eee[`WORD(1)] + eeo[`WORD(1)], eee[`WORD(0)] + eeo[`WORD(0)],
					  eee[`WORD(0)] - eeo[`WORD(0)] , eee[`WORD(1)] - eeo[`WORD(1)], eee[`WORD(2)] - eeo[`WORD(2)], eee[`WORD(3)] - eeo[`WORD(3)] };
	assign e = { ee[`WORD(7)] + eo[`WORD(7)] , ee[`WORD(6)] + eo[`WORD(6)], ee[`WORD(5)] + eo[`WORD(5)], ee[`WORD(4)] + eo[`WORD(4)],
					 ee[`WORD(3)] + eo[`WORD(3)] , ee[`WORD(2)] + eo[`WORD(2)], ee[`WORD(1)] + eo[`WORD(1)], ee[`WORD(0)] + eo[`WORD(0)],
					 ee[`WORD(0)] - eo[`WORD(0)] , ee[`WORD(1)] - eo[`WORD(1)], ee[`WORD(2)] - eo[`WORD(2)], ee[`WORD(3)] - eo[`WORD(3)],
					 ee[`WORD(4)] - eo[`WORD(4)] , ee[`WORD(5)] - eo[`WORD(5)], ee[`WORD(6)] - eo[`WORD(6)], ee[`WORD(7)] - eo[`WORD(7)] };
	//assign out = {eeee, eeeo, eeo, eo, o}; // temp
	assign out[`WORD(31)] = e[`WORD(15)]+o[`WORD(15)]; assign out[`WORD(0)] = e[`WORD(15)]-o[`WORD(15)];
	assign out[`WORD(30)] = e[`WORD(14)]+o[`WORD(14)]; assign out[`WORD(1)] = e[`WORD(14)]-o[`WORD(14)];
	assign out[`WORD(29)] = e[`WORD(13)]+o[`WORD(13)]; assign out[`WORD(2)] = e[`WORD(13)]-o[`WORD(13)];
	assign out[`WORD(28)] = e[`WORD(12)]+o[`WORD(12)]; assign out[`WORD(3)] = e[`WORD(12)]-o[`WORD(12)];
	assign out[`WORD(27)] = e[`WORD(11)]+o[`WORD(11)]; assign out[`WORD(4)] = e[`WORD(11)]-o[`WORD(11)];
	assign out[`WORD(26)] = e[`WORD(10)]+o[`WORD(10)]; assign out[`WORD(5)] = e[`WORD(10)]-o[`WORD(10)];
	assign out[`WORD(25)] = e[`WORD(9)]+o[`WORD(9)]; assign out[`WORD(6)] = e[`WORD(9)]-o[`WORD(9)];
	assign out[`WORD(24)] = e[`WORD(8)]+o[`WORD(8)]; assign out[`WORD(7)] = e[`WORD(8)]-o[`WORD(8)];
	assign out[`WORD(23)] = e[`WORD(7)]+o[`WORD(7)]; assign out[`WORD(8)] = e[`WORD(7)]-o[`WORD(7)];
	assign out[`WORD(22)] = e[`WORD(6)]+o[`WORD(6)]; assign out[`WORD(9)] = e[`WORD(6)]-o[`WORD(6)];
	assign out[`WORD(21)] = e[`WORD(5)]+o[`WORD(5)]; assign out[`WORD(10)] = e[`WORD(5)]-o[`WORD(5)];
	assign out[`WORD(20)] = e[`WORD(4)]+o[`WORD(4)]; assign out[`WORD(11)] = e[`WORD(4)]-o[`WORD(4)];
	assign out[`WORD(19)] = e[`WORD(3)]+o[`WORD(3)]; assign out[`WORD(12)] = e[`WORD(3)]-o[`WORD(3)];
	assign out[`WORD(18)] = e[`WORD(2)]+o[`WORD(2)]; assign out[`WORD(13)] = e[`WORD(2)]-o[`WORD(2)];
	assign out[`WORD(17)] = e[`WORD(1)]+o[`WORD(1)]; assign out[`WORD(14)] = e[`WORD(1)]-o[`WORD(1)];
	assign out[`WORD(16)] = e[`WORD(0)]+o[`WORD(0)]; assign out[`WORD(15)] = e[`WORD(0)]-o[`WORD(0)];
endmodule
