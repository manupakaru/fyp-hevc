`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    22:14:59 08/29/2013 
// Design Name: 
// Module Name:    eee 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module eeeo(
    input signed[15:0] in,
    output reg[63:0] out,
    input sel,
    input valid
    );
	 reg signed [31:0]a83, a36;
	always @(*) begin
		a83 <= in*83;
		a36 <= in*36;

		if (valid) begin
			case (sel)
			0: out <= {a83,a36};
			1: out <= {a36, -a83};
			endcase
			
		end else begin
			out <= 63'b0;
		end

	end 

endmodule
