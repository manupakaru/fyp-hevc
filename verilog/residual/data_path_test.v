`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   21:00:03 09/10/2013
// Design Name:   data_path
// Module Name:   C:/Users/Maleen/Uni/HEVC/Transform/data_path_test.v
// Project Name:  Transform
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: data_path
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////
`define Trafo32 2'b11
`define Trafo16 2'b10
`define Trafo8 2'b01
`define Trafo4 2'b00
module data_path_test;

	// Inputs
	reg [31:0] in;
	reg clk;
	reg [5:0] QP;
	reg quant_en;
	reg row_col;
	reg [1:0] trafoSize;
	reg [3:0] block_no_pt;
	reg dst;
	reg valid;
	reg [3:0] block_no_mem;
	reg [4:0] row_no;
	reg transform_en_mem;
	reg wr_en;
	reg [2:0] block_no_push;

	// Outputs
	wire [35:0] out;

	// Instantiate the Unit Under Test (UUT)
	data_path uut (
		.in(in), 
		.clk(clk), 
		.out(out), 
		.QP(QP), 
		.quant_en(quant_en), 
		.row_col(row_col), 
		.trafoSize(trafoSize), 
		.block_no_pt(block_no_pt), 
		.dst(dst), 
		.valid(valid), 
		.block_no_mem(block_no_mem), 
		.row_no(row_no), 
		.transform_en_mem(transform_en_mem), 
		.wr_en(wr_en), 
		.block_no_push(block_no_push)
	);

	initial begin
		// Initialize Inputs
		in = 0;
		clk = 0;
		QP = 37;
		quant_en = 1;
		row_col = 0;
		trafoSize = `Trafo4;
		block_no_pt = 0;
		dst = 1;
		valid = 0;
		block_no_mem = 0;
		row_no = 0;
		transform_en_mem = 1;
		wr_en = 0;
		block_no_push = 0;

		// Wait 100 ns for global reset to finish
		#100;
      #1;  
		  
		row_col = 0;  
		// Add stimulus here
		in = {  -16'd2, 16'd1}; 
		block_no_pt = 0; valid = 0;
		block_no_mem = 0; row_no = 0; wr_en = 0; block_no_push = 0; #10 //0
		in = { 16'd0, 16'd0};
		block_no_pt = 0; valid = 1;
		block_no_mem = 0; row_no = 0; wr_en = 0; block_no_push = 0; #10 //1	
		in = { 16'd2, -16'd1};
		block_no_pt = 1; valid = 1;
		block_no_mem = 0; row_no = 0; wr_en = 0; block_no_push = 0; #10 //2
		in = { 16'd0, 16'd0};
		block_no_pt = 0; valid = 1;
		block_no_mem = 0; row_no = 0; wr_en = 1; block_no_push = 0; #10 //3
		in = { 16'd0, -16'd2};
		block_no_pt = 1; valid = 1;
		block_no_mem = 1; row_no = 0; wr_en = 1; block_no_push = 0; #10 //4
		in = { 16'd0, 16'd0};
		block_no_pt = 0; valid = 1;
		block_no_mem = 0; row_no = 1; wr_en = 1; block_no_push = 0; #10 //5
		in = { -16'd1, 16'd2};
		block_no_pt = 1; valid = 1;
		block_no_mem = 1; row_no = 1; wr_en = 1; block_no_push = 0; #10 //6
		in = { 16'd1, 16'd0};
		block_no_pt = 0; valid = 1;
		block_no_mem = 0; row_no = 2; wr_en = 1; block_no_push = 0; #10 //7
		block_no_pt = 1; valid = 1;
		block_no_mem = 1; row_no = 2; wr_en = 1; block_no_push = 0; #10 //8
		block_no_pt = 1; valid = 0;
		block_no_mem = 0; row_no = 3; wr_en = 1; block_no_push = 0; #10 //9
		block_no_mem = 1; row_no = 3; wr_en = 1; block_no_push = 0; #10 //10
		
		block_no_mem = 0; row_no = 0; wr_en = 0; block_no_push = 0; #10 //11
		block_no_pt = 0; valid = 0; row_col = 1;
		block_no_mem = 1; row_no = 0; wr_en = 0; block_no_push = 0; #10 //12
		
		block_no_pt = 0; valid = 1;
		block_no_mem = 0; row_no = 1; wr_en = 0; block_no_push = 0; #10 //13
		block_no_pt = 1; valid = 1;
		block_no_mem = 1; row_no = 1; wr_en = 0; block_no_push = 0; #10 //14
		block_no_pt = 0; valid = 1;
		block_no_mem = 0; row_no = 2; wr_en = 0; block_no_push = 0; #10 //15
		block_no_pt = 1; valid = 1;
		block_no_mem = 1; row_no = 2; wr_en = 0; block_no_push = 0; #10 //16
		block_no_pt = 0; valid = 1;
		block_no_mem = 0; row_no = 3; wr_en = 0; block_no_push = 0; #10 //17
		block_no_pt = 1; valid = 1;
		block_no_mem = 1; row_no = 3; wr_en = 0; block_no_push = 0; #10 //18
		block_no_pt = 0; valid = 1;
		block_no_mem = 0; row_no = 3; wr_en = 0; block_no_push = 0; #10 //19
		block_no_pt = 1; valid = 1;
		block_no_mem = 1; row_no = 3; wr_en = 0; block_no_push = 0; #10 //20
		valid = 0;
		;
		
	end
	
	always 
		#5 clk = ~clk;
      
endmodule

