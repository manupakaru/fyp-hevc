`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    17:25:37 08/18/2013 
// Design Name: 
// Module Name:    top_level 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module data_path(
	 input [31:0] in,
	 input clk,
    output [35:0] out,
	 // Dequantize
    input [5:0] QP, 
	 input quant_en, 
	 input row_col, // 0 -row, 1-col
	 // Transform
	 input [1:0] trafoSize,  
	 input [3:0] block_no_pt,
	 input dst,
	 input valid,
	 // Memory
	 input [3:0] block_no_mem,
	 input [4:0] row_no,
	 input transform_en_mem,
	 input wr_en,
	 // Push
	 input [2:0] block_no_push
    );
	wire[31:0] con;
	wire[31:0] mem_out;
	wire[31:0] skip_data;
	wire[1023:0] tr_out;
	
	dequant dequant_d(
		 .in(in),
		 .in_mem(mem_out),
		 .QP(QP),
		 .quant_en(quant_en),
		 .trafoSize(trafoSize),
		 .row_col(row_col), // 0 -row, 1-col
		 .clk(clk),
		 .out(con)
    );
	 
	 partial_transform pt_d(
		 .in(con), //16*2
		 .out(tr_out), //32*16
		 .trafoSize(trafoSize),
		 .transform_en(transform_en_mem),
		 .block_no(block_no_pt),
		 .dst(dst),
		 .valid(valid),
		 .clk(clk)
    );

	trans_mem mem_d(
    .in(tr_out),
    .out(mem_out),
	 .transform_en(transform_en_mem),
    .block_no(block_no_mem),
    .row_no(row_no),
    .clk(clk),
    .wr_en(wr_en)
    );
	 
	downward_push push_d(
    .in(tr_out),
	 .transform_en(transform_en_mem),
    .block_no(block_no_push),
    .out(out)
    );
endmodule
