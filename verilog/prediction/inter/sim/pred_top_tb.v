`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   02:39:58 12/01/2013
// Design Name:   pred_top
// Module Name:   D:/090250V/SEMESTER 7/HEVC decoder/HDL codes/pred_top/sim/pred_top_tb.v
// Project Name:  prediction
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: pred_top
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module pred_top_tb;

    `include "pred_def.v"
    `include "cache_configs_def.v"
    `include "inter_axi_def.v"
    localparam                          STATE_READ_WAIT = 0;
    localparam                          STATE_ACTIVE = 1;
    localparam                          STATE_WRITE_WAIT = 2;
    localparam                          STATE_ERROR = 3;
    localparam                          STATE_CURRENT_POC = 4;
    localparam                          STATE_CUR_PIC_FIL_IDX = 5;
    localparam                          STATE_REF_PIC_LIST_5_UPDATE = 6;
    localparam                          STATE_REF_PIC_LIST_5_SCAN   = 7;
    localparam                          STATE_SEND_POC_TO_DBF = 8;
    localparam                          STATE_WRITE_WAIT_CUR_DPB_IDX_TO_DBF = 9;
    localparam                          STATE_REF_PIC_WRITE_WAIT = 10;
    localparam                          STATE_DPB_ADD_PREV_PIC = 11;
    localparam                          STATE_POC_READ_WAIT = 12;
    localparam                          STATE_MVD1_READ_WAIT = 13;
    localparam                          STATE_MVD2_READ_WAIT = 14;
    localparam                          STATE_MVD1_READ = 15;
    localparam                          STATE_MVD2_READ = 16;
    localparam                          STATE_MV_RETURN_WAIT1 = 17;
    localparam                          STATE_MV_RETURN_WAIT2 = 18;
    localparam                          STATE_MV_RETURN_WAIT3 = 19;
    localparam                          STATE_INTER_PREFETCH_COL_WRITE_WAIT = 20;
    localparam                          STATE_GET_ST_RPS_ENTRY = 21;
    localparam                          STATE_GET_ST_RPS_HEADER = 22;
    localparam                          STATE_SLICE_1_WRITE_WAIT = 23;
    localparam                          STATE_REF_PIC_LIST_TRANSFER = 24;
    localparam                          STATE_SET_ST_RPS_ENTRY_ADDR_FOR_REF_POC5 = 25;
    
    localparam                          WRITE_REF_PIC_STATE_IDLE = 0;
    localparam                          WRITE_REF_PIC_STATE_CHECK = 1;
    localparam                          WRITE_REF_PIC_STATE_POC_WRITE = 2;
    localparam                          WRITE_REF_PIC_STATE_IDX_WRITE = 3;
    localparam                          WRITE_REF_PIC_STATE_DONE = 4;
    localparam                          WRITE_REF_PIC_FIND_FOL_IDX = 5;

    localparam                          STATE_SET_INPUTS                = 0;    // the state that is entered upon reset
    localparam                          STATE_TAG_READ                  = 1;
    localparam                          STATE_TAG_COMPARE               = 2;
    localparam                          STATE_C_LINE_HIT_FETCH          = 3;
    localparam                          STATE_C_LINE_MISS_FETCH         = 4;
    localparam                          STATE_DEST_FILL                 = 5;
    localparam                          STATE_CACHE_READY               = 6;
    localparam                          STATE_MISS_AR_REDY_WAIT         = 7;
    
    localparam                          STATE_MV_DERIVE_CONFIG_UPDATE = 0;
    localparam                          STATE_PREFETCH_COL_WRITE_WAIT = 1;
    localparam                          STATE_MV_DERIVE_PU_OVERWRIT_3 = 2;
    localparam                          STATE_MV_DERIVE_AVAILABLE_CHECK4 = 3;
    localparam                          STATE_MV_DERIVE_MV_AVAILABLE_MERGE_9 = 4;
    localparam                          STATE_MV_DERIVE_MV_AVAILABLE_MERGE_10 = 5;
    localparam                          STATE_MV_DERIVE_MV_AVAILABLE_MERGE_11 = 6;
    localparam                          STATE_MV_DERIVE_MV_AVAILABLE_MERGE_12 = 7;
    localparam                          STATE_MV_DERIVE_MV_AVAILABLE_MERGE_13 = 8;
    localparam                          STATE_MV_DERIVE_MV_PROCESS_MERGE_14     = 9;
    localparam                          STATE_MV_DERIVE_MV_PROCESS_MERGE_15     = 10;
    localparam                          STATE_MV_DERIVE_MV_PROCESS_MERGE_16     = 11;
    localparam                          STATE_MV_DERIVE_COMPARE_MVS       = 12;
    localparam                          STATE_MV_DERIVE_ZERO_MERGE            = 13;
    localparam                          STATE_MV_DERIVE_ADD_ZEROS_AMVP           = 14;
    localparam                          STATE_MV_DERIVE_MV_AVAILABLE_AMVP_9 = 16;
    localparam                          STATE_MV_DERIVE_MV_AVAILABLE_AMVP_10 = 17;
    localparam                          STATE_MV_DERIVE_MV_AVAILABLE_AMVP_11 = 18;
    localparam                          STATE_MV_DERIVE_MV_AVAILABLE_AMVP_12 = 19;
    localparam                          STATE_MV_DERIVE_MV_AVAILABLE_AMVP_13 = 20;
    localparam                          STATE_MV_DERIVE_MV_PROCESS_AMVP14   = 21;
    localparam                          STATE_MV_DERIVE_MV_PROCESS_AMVP15   = 22;
    localparam                          STATE_MV_DERIVE_MV_PROCESS_AMVP16   = 23;
    localparam                          STATE_MV_DERIVE_MV_PROCESS_AMVP17   = 24;
    localparam                          STATE_MV_DERIVE_MV_PROCESS_AMVP18   = 25;
    localparam                          STATE_MV_DERIVE_MV_PROCESS_AMVP19   = 26;
    localparam                          STATE_MV_DERIVE_MV_PROCESS_AMVP20   = 27;
    localparam                          STATE_MV_DERIVE_COL_MV_WAIT         = 28;
    localparam                          STATE_MV_DERIVE_MVD_ADD_AMVP       = 29;
    localparam                          STATE_MV_DERIVE_SET_DUMMY_INTRA_MV = 31;
    localparam                          STATE_MV_DERIVE_SET_PRE_INTRA_MV    = 32;    
    localparam                          STATE_MV_DERIVE_DONE    = 33;
    localparam                          STATE_MV_DERIVE_CTU_DONE    = 34;
    localparam                          STATE_MV_DERIVE_SET_INTER_MV    = 35;
    
    localparam                          STATE_MV_DERIVE_BI_PRED_CANDS1       = 36;
    localparam                          STATE_MV_DERIVE_BI_PRED_CANDS2       = 37;
    localparam                          STATE_MV_DERIVE_BI_PRED_CANDS3       = 38;
    localparam                          STATE_MV_DERIVE_BI_PRED_CANDS4       = 39;
 
    integer PIC_HEIGHT;
    integer PIC_WIDTH ; 
    
	// Inputs
	reg clk;
	reg reset;
	reg enable;
	reg [31:0] fifo_in;
	reg input_fifo_is_empty;
	reg output_fifo_is_full;
	reg [0:0] y_residual_fifo_in;
	reg y_residual_fifo_is_empty_in;
	reg [0:0] cb_residual_fifo_in;
	reg cb_residual_fifo_is_empty_in;
	reg [0:0] cr_residual_fifo_in;
	reg cr_residual_fifo_is_empty_in;
	reg y_dbf_fifo_is_full;
	reg cb_dbf_fifo_is_full;
	reg cr_dbf_fifo_is_full;

	reg mv_col_axi_awready;
	reg mv_col_axi_wready;
	reg [1:0] mv_col_axi_bresp;
	reg mv_col_axi_bvalid;
	reg mv_pref_axi_arready;
	reg [511:0] mv_pref_axi_rdata;
	reg [1:0] mv_pref_axi_rresp;
	reg mv_pref_axi_rlast;
	reg mv_pref_axi_rvalid;
    
    
	// Outputs
	wire read_en_out;
	wire [31:0] fifo_out;
	wire write_en_out;
	wire y_residual_fifo_read_en_out;
	wire cb_residual_fifo_read_en_out;
	wire cr_residual_fifo_read_en_out;

    wire mv_col_axi_awid;
	wire [7:0] mv_col_axi_awlen;
	wire [2:0] mv_col_axi_awsize;
	wire [1:0] mv_col_axi_awburst;
	wire mv_col_axi_awlock;
	wire [3:0] mv_col_axi_awcache;
	wire [2:0] mv_col_axi_awprot;
	wire mv_col_axi_awvalid;
	wire [31:0] mv_col_axi_awaddr;
	wire [63:0] mv_col_axi_wstrb;
	wire mv_col_axi_wlast;
	wire mv_col_axi_wvalid;
	wire [511:0] mv_col_axi_wdata;
	wire mv_col_axi_bready;
	wire [31:0] mv_pref_axi_araddr;
	wire [7:0] mv_pref_axi_arlen;
	wire [2:0] mv_pref_axi_arsize;
	wire [1:0] mv_pref_axi_arburst;
	wire [2:0] mv_pref_axi_arprot;
	wire mv_pref_axi_arvalid;
	wire mv_pref_axi_rready;
	wire mv_pref_axi_arlock;
	wire mv_pref_axi_arid;
	wire [3:0] mv_pref_axi_arcache;

    // axi master interface         
    wire        [32-1:0]                           ref_pix_axi_ar_addr     ;
    wire        [7:0]                               ref_pix_axi_ar_len      ;
    wire        [2:0]                               ref_pix_axi_ar_size     ;
    wire        [1:0]                               ref_pix_axi_ar_burst    ;
    wire        [2:0]                               ref_pix_axi_ar_prot     ;
    wire                                            ref_pix_axi_ar_valid    ;
    reg                                             ref_pix_axi_ar_ready    ;
                
    reg         [512-1:0]                            ref_pix_axi_r_data      ;
    reg         [1:0]                               ref_pix_axi_r_resp      ;
    reg                                             ref_pix_axi_r_last      ;
    reg                                             ref_pix_axi_r_valid     ;
    wire                                            ref_pix_axi_r_ready     ;
    
    wire new_poc = uut.state == STATE_SEND_POC_TO_DBF;
    integer ctu_start_time;
    integer cache_ctu_start_time;
	wire [0:0] temp_port;

    reg [31:0] soft_file_loc;
    
    integer file_in,rfile;
    integer file_out;    
    integer cache_in_file;
    integer return_value;
    reg [7:0] mem1,mem2, mem3,mem4;
    integer file_location;
    wire new_ctu = uut.state == STATE_INTER_PREFETCH_COL_WRITE_WAIT;
    
    reg [15:0] soft_dpb_filled;

    integer lx_frac, ly_frac, ref_l_startx, ref_l_starty;
    integer ch_x_frac, ch_y_frac, ref_cb_startx, ref_cb_starty;
    integer frame_offset_pred_pixel;
    
    reg                                  test_mv_field_pred_flag_l0;
    reg                                  test_mv_field_pred_flag_l1;
    reg [REF_IDX_LX_WIDTH -1:0]          test_mv_field_ref_idx_l0;
    reg [REF_IDX_LX_WIDTH -1:0]          test_mv_field_ref_idx_l1;
    reg signed [MVD_WIDTH -1:0]          test_mv_field_mv_x_l0;
    reg signed [MVD_WIDTH -1:0]          test_mv_field_mv_y_l0;
    reg signed [MVD_WIDTH -1:0]          test_mv_field_mv_x_l1;
    reg signed [MVD_WIDTH -1:0]          test_mv_field_mv_y_l1;    

    integer final_pixl_file;                                            
    integer pred_pixl_file;                                            
    
    
    inter_pred_iface inter_pred_iface_block();
	// Instantiate the Unit Under Test (UUT)
	pred_top uut (
		.clk(clk), 
		.reset(reset), 
		.enable(enable), 
		.fifo_in(fifo_in), 
		.input_fifo_is_empty(input_fifo_is_empty), 
		.read_en_out(read_en_out), 
		.fifo_out(fifo_out), 
		.output_fifo_is_full(output_fifo_is_full), 
		.write_en_out(write_en_out), 
		.y_residual_fifo_in(y_residual_fifo_in), 
		.y_residual_fifo_is_empty_in(y_residual_fifo_is_empty_in), 
		.y_residual_fifo_read_en_out(y_residual_fifo_read_en_out), 
		.cb_residual_fifo_in(cb_residual_fifo_in), 
		.cb_residual_fifo_is_empty_in(cb_residual_fifo_is_empty_in), 
		.cb_residual_fifo_read_en_out(cb_residual_fifo_read_en_out), 
		.cr_residual_fifo_in(cr_residual_fifo_in), 
		.cr_residual_fifo_is_empty_in(cr_residual_fifo_is_empty_in), 
		.cr_residual_fifo_read_en_out(cr_residual_fifo_read_en_out), 
		.y_dbf_fifo_is_full(y_dbf_fifo_is_full), 
		.cb_dbf_fifo_is_full(cb_dbf_fifo_is_full), 
		.cr_dbf_fifo_is_full(cr_dbf_fifo_is_full), 
     
        .ref_pix_axi_ar_addr(ref_pix_axi_ar_addr),        
        .ref_pix_axi_ar_len(ref_pix_axi_ar_len), 
        .ref_pix_axi_ar_size(ref_pix_axi_ar_size), 
        .ref_pix_axi_ar_burst(ref_pix_axi_ar_burst), 
        .ref_pix_axi_ar_prot(ref_pix_axi_ar_prot), 
        .ref_pix_axi_ar_valid(ref_pix_axi_ar_valid), 
        .ref_pix_axi_ar_ready(ref_pix_axi_ar_ready), 
        .ref_pix_axi_r_data(ref_pix_axi_r_data), 
        .ref_pix_axi_r_resp(ref_pix_axi_r_resp), 
        .ref_pix_axi_r_last(ref_pix_axi_r_last), 
        .ref_pix_axi_r_valid(ref_pix_axi_r_valid), 
        .ref_pix_axi_r_ready(ref_pix_axi_r_ready),
        
        .mv_col_axi_awid(mv_col_axi_awid), 
		.mv_col_axi_awlen(mv_col_axi_awlen), 
		.mv_col_axi_awsize(mv_col_axi_awsize), 
		.mv_col_axi_awburst(mv_col_axi_awburst), 
		.mv_col_axi_awlock(mv_col_axi_awlock), 
		.mv_col_axi_awcache(mv_col_axi_awcache), 
		.mv_col_axi_awprot(mv_col_axi_awprot), 
		.mv_col_axi_awvalid(mv_col_axi_awvalid), 
		.mv_col_axi_awaddr(mv_col_axi_awaddr), 
		.mv_col_axi_awready(mv_col_axi_awready), 
		.mv_col_axi_wstrb(mv_col_axi_wstrb), 
		.mv_col_axi_wlast(mv_col_axi_wlast), 
		.mv_col_axi_wvalid(mv_col_axi_wvalid), 
		.mv_col_axi_wdata(mv_col_axi_wdata), 
		.mv_col_axi_wready(mv_col_axi_wready), 
		.mv_col_axi_bresp(mv_col_axi_bresp), 
		.mv_col_axi_bvalid(mv_col_axi_bvalid), 
		.mv_col_axi_bready(mv_col_axi_bready), 
		.mv_pref_axi_araddr(mv_pref_axi_araddr), 
		.mv_pref_axi_arlen(mv_pref_axi_arlen), 
		.mv_pref_axi_arsize(mv_pref_axi_arsize), 
		.mv_pref_axi_arburst(mv_pref_axi_arburst), 
		.mv_pref_axi_arprot(mv_pref_axi_arprot), 
		.mv_pref_axi_arvalid(mv_pref_axi_arvalid), 
		.mv_pref_axi_arready(mv_pref_axi_arready), 
		.mv_pref_axi_rdata(mv_pref_axi_rdata), 
		.mv_pref_axi_rresp(mv_pref_axi_rresp), 
		.mv_pref_axi_rlast(mv_pref_axi_rlast), 
		.mv_pref_axi_rvalid(mv_pref_axi_rvalid), 
		.mv_pref_axi_rready(mv_pref_axi_rready), 
		.mv_pref_axi_arlock(mv_pref_axi_arlock), 
		.mv_pref_axi_arid(mv_pref_axi_arid), 
		.mv_pref_axi_arcache(mv_pref_axi_arcache), 
		.temp_port(temp_port)
	);

	initial begin
		// Initialize Inputs
        ctu_start_time = 0;
        soft_file_loc = 0;
        inter_pred_iface_block.sv_inter_pred_reset();
        inter_pred_iface_block.sv_inter_pred_step(soft_file_loc);
        file_in = $fopen("res_to_inter_cast","rb");
        file_out = $fopen("inter_to_dbf","wb");  
        final_pixl_file = $fopen("BQSquare_416x240_60_qp37_416x240_8bit_final.yuv","rb");
        pred_pixl_file = $fopen("BQSquare_416x240_60_qp37_416x240_8bit_predicted.yuv","rb");

        mem1 = $fgetc( file_in); 
        mem2 = $fgetc( file_in); 
        mem3 = $fgetc( file_in); 
        mem4 = $fgetc( file_in); 
        fifo_in = {mem4,mem3,mem2,mem1};
        
        mv_col_axi_awready = 1; 
        mv_col_axi_wready  = 1;
        mv_col_axi_bresp   = 0;
        mv_col_axi_bvalid  = 1;
        mv_pref_axi_arready = 1;
        mv_pref_axi_rdata = 0;
        mv_pref_axi_rresp = 0;
        mv_pref_axi_rvalid = 1;
        mv_pref_axi_rlast = 0;
        
        ref_pix_axi_r_valid = 1;
        ref_pix_axi_ar_ready = 1;
        file_location = -1;
		clk = 0;
		reset = 1;
		enable = 1;
        // fifo_in = 0;
		input_fifo_is_empty = 1;
		output_fifo_is_full = 0;
		y_residual_fifo_in = 0;
		y_residual_fifo_is_empty_in = 0;
		cb_residual_fifo_in = 0;
		cb_residual_fifo_is_empty_in = 0;
		cr_residual_fifo_in = 0;
		cr_residual_fifo_is_empty_in = 0;
		y_dbf_fifo_is_full = 0;
		cb_dbf_fifo_is_full = 0;
		cr_dbf_fifo_is_full = 0;

		// Wait 100 ns for global reset to finish
		#100;
        @(posedge clk);
        reset = 0;
        input_fifo_is_empty = 0;
        
		// Add stimulus here

	end
always #10 clk = ~clk;


always @(posedge clk) begin
    if(reset) begin
    end
    else begin
        if(read_en_out) begin
            if(file_location  >= soft_file_loc) begin
                inter_pred_iface_block.sv_inter_pred_step(soft_file_loc);
            end
            file_location <= file_location + 1;
            mem1 = $fgetc( file_in); 
            mem2 = $fgetc( file_in); 
            mem3 = $fgetc( file_in); 
            mem4 = $fgetc( file_in); 
            //$display("%d, header passed: %x",$time, mem1);
            fifo_in = {mem4,mem3,mem2,mem1};
            //$display("data passed: %x %x %x",mem4, mem3, mem2);

        end
        // else begin
            // if(mv_status_d == 1 && mv_status == 0) begin
                // inter_pred_iface_block.sv_inter_pred_step(soft_file_loc);
            // end
        // end
    end
    
end

always@(posedge clk) begin
    if(uut.state == 3) begin
        $display("STATE_ERROR");
        $stop();
    end
    if(new_poc) begin
        $display("new poc %d", uut.current__poc);
        //$stop();
    end
    ctu_count();
    cach_ctu_count();
    //dpb_fill_verify();
    //verify_dpb_poc();
    //ref_pic_list_verify();
    //current_mv_verify();
    cache_input_verify();
    //verify_luma_filter();
    //verify_ch_filter();
    final_pred_pixl_check();

end



task cach_ctu_count;
integer ctu_time;
begin
    if((uut.inter_top_block.pred_sample_gen_block.xT_in_min_tus_out[3:0] == 0) && (uut.inter_top_block.pred_sample_gen_block.yT_in_min_tus_out[3:0] == 0) && uut.inter_top_block.pred_sample_gen_block.cache_block_ready) begin
        ctu_time = ($time - cache_ctu_start_time)/20;
        if((ctu_time) >200)
        $display("ctu_time = %d poc %d, x=%d, y=%d",ctu_time,uut.current__poc,((uut.inter_top_block.pred_sample_gen_block.xT_in_min_tus_out*4)),((uut.inter_top_block.pred_sample_gen_block.yT_in_min_tus_out*4)));
        cache_ctu_start_time = $time;
       if(uut.pred_mode == `MODE_INTER && ctu_time > 20000) begin
           $stop(); 
       end
    end         
end
endtask

task ctu_count;
    integer ctu_time;
begin

    if(new_ctu) begin
       //  ctu_time = ($time - ctu_start_time)/20;
       //  if((ctu_time) >2000)
       // $display("ctu_time = %d poc %d, x=%d, y=%d",ctu_time,uut.current__poc,(uut.inter_top_block.mv_derive_block.ctb_xx),(uut.inter_top_block.mv_derive_block.ctb_yy));
       // ctu_start_time = $time;
       // if(uut.pred_mode == `MODE_INTER && ctu_time > 10000) begin
       //      $stop(); 
       // end
    end
    if(new_poc) begin
        PIC_HEIGHT = uut.pic_height;
        PIC_WIDTH = uut.pic_width;
    end
end
endtask
    
task verify_dpb_poc;
    integer i;
    reg [31:0] soft_dpb_poc;
    begin
        if(uut.write_ref_pic_state == WRITE_REF_PIC_STATE_DONE) begin
            for(i=0;i< 16; i=i+1) begin
                inter_pred_iface_block.sv_get_dpb_poc(i,soft_dpb_poc);
                if(uut.dpb_filled_flag_new[i] == 1) begin
                    if(soft_dpb_poc == uut.dpb_block.mem[i]) begin
                        $display("dpb_poc ok");
                    end
                    else begin
                        $display("dpb_poc no ok soft %d hard %d idx %d",soft_dpb_poc,uut.dpb_block.mem[i],i );
                        $stop();
                    end
                end
            end
        end
    end
endtask    
    
    
task dpb_fill_verify;
integer i;
begin
    for(i=0;i< 16; i=i+1) begin
        inter_pred_iface_block.sv_get_dpb_filled(i,soft_dpb_filled[i]);
    end
    if(uut.write_ref_pic_state == WRITE_REF_PIC_STATE_DONE) begin
        if(uut.dpb_filled_flag_new == soft_dpb_filled) begin
            $display("dpb ok");
        end
        else begin
            $display("dpb not ok");
            $stop();
        end
    end
end
endtask

task verify_luma_filter;
    integer i,j;
    integer quad_pred_pixl;
    integer x_ofset, y_ofset;
    begin
        if(uut.inter_top_block.pred_sample_gen_block.luma_filter_wrapper_block.block_ready_out) begin
            
            for(i=0;i<4;i=i+1)begin
                for(j=0;j<4;j=j+1) begin
                    case({lx_frac})
                        2'b00: begin
                            x_ofset = 0;   
                        end
                        2'b01,2'b10: begin
                            x_ofset = 3;
                        end
                        2'b11: begin
                            x_ofset = 2;
                        end
                    endcase
                    case({ly_frac})
                        2'b00: begin
                            y_ofset = 0;   
                        end
                        2'b01,2'b10: begin
                            y_ofset = 3;
                        end
                        2'b11: begin
                            y_ofset = 2;
                        end
                    endcase
                    inter_pred_iface_block.sv_get_frac_pixl(0,j+ref_l_startx+x_ofset,i+ref_l_starty+y_ofset,lx_frac,ly_frac,quad_pred_pixl);
                    if(quad_pred_pixl== $signed(uut.inter_top_block.pred_sample_gen_block.luma_filter_wrapper_block.out_store[i][j])) begin
                        //$display("%d luma filter out match @startx =%d, starty=%d, x=%d, y=%d, soft=%d, hard=%d",$time,uut.inter_top_block.pred_sample_gen_block.luma_filter_wrapper_block.xT_4x4_out,uut.inter_top_block.pred_sample_gen_block.luma_filter_wrapper_block.yT_4x4_out,j,i,quad_pred_pixl,uut.inter_top_block.pred_sample_gen_block.luma_filter_wrapper_block.out_store[i][j]);
                    end
                    else begin
                        $display("luma filter out mismatch @startx =%d, starty=%d, x=%d, y=%d, soft=%d, hard=%d",uut.inter_top_block.pred_sample_gen_block.luma_filter_wrapper_block.xT_4x4_out,uut.inter_top_block.pred_sample_gen_block.luma_filter_wrapper_block.yT_4x4_out,j,i,quad_pred_pixl,uut.inter_top_block.pred_sample_gen_block.luma_filter_wrapper_block.out_store[i][j]);
                        $stop();
                    end
                end
            end
        end
        if(uut.inter_top_block.pred_sample_gen_block.luma_filter_wrapper_block.valid_in) begin
            ref_l_startx = uut.inter_top_block.pred_sample_gen_block.luma_filter_wrapper_block.ref_l_start_x;
            ref_l_starty = uut.inter_top_block.pred_sample_gen_block.luma_filter_wrapper_block.ref_l_start_y;
            //ref_l_width = uut.inter_top_block.pred_sample_gen_block.luma_filter_wrapper_block.ref_l_width;
            //ref_l_hgt = uut.inter_top_block.pred_sample_gen_block.luma_filter_wrapper_block.ref_l_height;
            ly_frac = uut.inter_top_block.pred_sample_gen_block.luma_filter_wrapper_block.l_frac_y;
            lx_frac = uut.inter_top_block.pred_sample_gen_block.luma_filter_wrapper_block.l_frac_x;
            //$display("frac_x=%d, frac_y=%d",lx_frac,ly_frac);
        end
    end
endtask

task verify_ch_filter;
    integer i,j;
    integer quad_pred_pixl;
    integer x_ofset, y_ofset;
    begin
        if(uut.inter_top_block.pred_sample_gen_block.cb_filter_wrapper.block_ready_out) begin
            $display("frac_x=%d, frac_y=%d",ch_x_frac,ch_y_frac);
            for(i=0;i<2;i=i+1)begin
                for(j=0;j<2;j=j+1) begin
                    case({ch_x_frac})
                        3'b000: begin
                            x_ofset = 0;   
                        end
                        default: begin
                            x_ofset = 1;
                        end
                    endcase
                    case({ch_y_frac})
                        3'b000: begin
                            y_ofset = 0;   
                        end
                        default:begin
                            y_ofset = 1;
                        end
                    endcase
                    if(i==0) begin
                        inter_pred_iface_block.sv_get_cb_frac_pixl(0,j+ref_cb_startx+x_ofset,i+ref_cb_starty+y_ofset,ch_x_frac,ch_y_frac,quad_pred_pixl);
                        if(quad_pred_pixl== $signed(uut.inter_top_block.pred_sample_gen_block.cb_filter_wrapper.out_store_1r[j])) begin
                            $display("cb filter out match @startx =%d, starty=%d, x=%d, y=%d, soft=%d, hard=%d",uut.inter_top_block.pred_sample_gen_block.cb_filter_wrapper.xT_4x4_out,uut.inter_top_block.pred_sample_gen_block.cb_filter_wrapper.yT_4x4_out,j,i,quad_pred_pixl,uut.inter_top_block.pred_sample_gen_block.cb_filter_wrapper.out_store_1r[j]);
                        end
                        else begin
                            $display("cb filter out mismatch @startx =%d, starty=%d, x=%d, y=%d, soft=%d, hard=%d",uut.inter_top_block.pred_sample_gen_block.cb_filter_wrapper.xT_4x4_out,uut.inter_top_block.pred_sample_gen_block.cb_filter_wrapper.yT_4x4_out,j,i,quad_pred_pixl,uut.inter_top_block.pred_sample_gen_block.cb_filter_wrapper.out_store_1r[j]);
                            $stop();
                        end
                        inter_pred_iface_block.sv_get_cr_frac_pixl(0,j+ref_cb_startx+x_ofset,i+ref_cb_starty+y_ofset,ch_x_frac,ch_y_frac,quad_pred_pixl);
                        if(quad_pred_pixl== $signed(uut.inter_top_block.pred_sample_gen_block.cr_filter_wrapper.out_store_1r[j])) begin
                            $display("cr filter out match @startx =%d, starty=%d, x=%d, y=%d, soft=%d, hard=%d",uut.inter_top_block.pred_sample_gen_block.cr_filter_wrapper.xT_4x4_out,uut.inter_top_block.pred_sample_gen_block.cr_filter_wrapper.yT_4x4_out,j,i,quad_pred_pixl,uut.inter_top_block.pred_sample_gen_block.cr_filter_wrapper.out_store_1r[j]);
                        end
                        else begin
                            $display("cr filter out mismatch @startx =%d, starty=%d, x=%d, y=%d, soft=%d, hard=%d",uut.inter_top_block.pred_sample_gen_block.cr_filter_wrapper.xT_4x4_out,uut.inter_top_block.pred_sample_gen_block.cr_filter_wrapper.yT_4x4_out,j,i,quad_pred_pixl,uut.inter_top_block.pred_sample_gen_block.cr_filter_wrapper.out_store_1r[j]);
                            $stop();
                        end
                    end
                    else begin
                        inter_pred_iface_block.sv_get_cb_frac_pixl(0,j+ref_cb_startx+x_ofset,i+ref_cb_starty+y_ofset,ch_x_frac,ch_y_frac,quad_pred_pixl);
                        if(quad_pred_pixl== $signed(uut.inter_top_block.pred_sample_gen_block.cb_filter_wrapper.out_store_2r[j])) begin
                            $display("cb filter out match @startx =%d, starty=%d, x=%d, y=%d, soft=%d, hard=%d",uut.inter_top_block.pred_sample_gen_block.cb_filter_wrapper.xT_4x4_out,uut.inter_top_block.pred_sample_gen_block.cb_filter_wrapper.yT_4x4_out,j,i,quad_pred_pixl,uut.inter_top_block.pred_sample_gen_block.cb_filter_wrapper.out_store_2r[j]);
                        end
                        else begin
                            $display("cb filter out mismatch @startx =%d, starty=%d, x=%d, y=%d, soft=%d, hard=%d",uut.inter_top_block.pred_sample_gen_block.cb_filter_wrapper.xT_4x4_out,uut.inter_top_block.pred_sample_gen_block.cb_filter_wrapper.yT_4x4_out,j,i,quad_pred_pixl,uut.inter_top_block.pred_sample_gen_block.cb_filter_wrapper.out_store_2r[j]);
                            $stop();
                        end
                        inter_pred_iface_block.sv_get_cr_frac_pixl(0,j+ref_cb_startx+x_ofset,i+ref_cb_starty+y_ofset,ch_x_frac,ch_y_frac,quad_pred_pixl);
                        if(quad_pred_pixl== $signed(uut.inter_top_block.pred_sample_gen_block.cr_filter_wrapper.out_store_2r[j])) begin
                            $display("cr filter out match @startx =%d, starty=%d, x=%d, y=%d, soft=%d, hard=%d",uut.inter_top_block.pred_sample_gen_block.cr_filter_wrapper.xT_4x4_out,uut.inter_top_block.pred_sample_gen_block.cr_filter_wrapper.yT_4x4_out,j,i,quad_pred_pixl,uut.inter_top_block.pred_sample_gen_block.cr_filter_wrapper.out_store_2r[j]);
                        end
                        else begin
                            $display("cr filter out mismatch @startx =%d, starty=%d, x=%d, y=%d, soft=%d, hard=%d",uut.inter_top_block.pred_sample_gen_block.cr_filter_wrapper.xT_4x4_out,uut.inter_top_block.pred_sample_gen_block.cr_filter_wrapper.yT_4x4_out,j,i,quad_pred_pixl,uut.inter_top_block.pred_sample_gen_block.cr_filter_wrapper.out_store_2r[j]);
                            $stop();
                        end
                    end
                end
            end
        end
        if(uut.inter_top_block.pred_sample_gen_block.cb_filter_wrapper.valid_in) begin
            ref_cb_startx = uut.inter_top_block.pred_sample_gen_block.cb_filter_wrapper.ref_c_start_x;
            ref_cb_starty = uut.inter_top_block.pred_sample_gen_block.cb_filter_wrapper.ref_c_start_y;
            //ref_l_width = uut.inter_top_block.pred_sample_gen_block.luma_filter_wrapper_block.ref_l_width;
            //ref_l_hgt = uut.inter_top_block.pred_sample_gen_block.luma_filter_wrapper_block.ref_l_height;
            ch_y_frac = uut.inter_top_block.pred_sample_gen_block.cb_filter_wrapper.c_frac_y;
            ch_x_frac = uut.inter_top_block.pred_sample_gen_block.cb_filter_wrapper.c_frac_x;
        end
    end
endtask

task ref_pic_list_verify;
integer i;
integer ref_pic_list0_num_pic;
integer ref_pic_list1_num_pic;
integer ref_pic_list0_list;
integer ref_pic_list0_idx;
integer ref_pic_list1_list;
integer ref_pic_list1_idx;

begin
    if(uut.write_ref_pic_state == WRITE_REF_PIC_STATE_DONE) begin
        inter_pred_iface_block.sv_get_ref_pic_list_num_pic(1,110,ref_pic_list0_num_pic);
        inter_pred_iface_block.sv_get_ref_pic_list_num_pic(0,110,ref_pic_list1_num_pic);
        for(i=0;i< 16; i=i+1) begin
            if(i < ref_pic_list0_num_pic) begin
                inter_pred_iface_block.sv_get_ref_pic_list_poc(i,0,ref_pic_list0_list);
                inter_pred_iface_block.sv_get_ref_pic_list_idx(i,0,ref_pic_list0_idx);
                if(ref_pic_list0_list == uut.inter_top_block.mv_derive_block.ref_pic_list_0.mem[i] && ref_pic_list0_idx == uut.inter_top_block.mv_derive_block.ref_pic_list0_dpb_idx.mem[i]) begin
                    
                end
                else begin
                    $display("ref pic list 0 not ok idx=%d, soft poc=%d, hard poc= %d, soft idx=%d, hard_idx=%d", i,ref_pic_list0_list,uut.inter_top_block.mv_derive_block.ref_pic_list_0.mem[i],ref_pic_list0_idx,uut.inter_top_block.mv_derive_block.ref_pic_list0_dpb_idx.mem[i]);
                    $stop();
                end
            end
            if(i < ref_pic_list1_num_pic) begin
                inter_pred_iface_block.sv_get_ref_pic_list_poc(i,1,ref_pic_list1_list);
                inter_pred_iface_block.sv_get_ref_pic_list_idx(i,1,ref_pic_list1_idx);
                if(ref_pic_list1_list == uut.inter_top_block.mv_derive_block.ref_pic_list_1.mem[i] && ref_pic_list1_idx == uut.inter_top_block.mv_derive_block.ref_pic_list1_dpb_idx.mem[i]) begin
                    
                end
                else begin
                    $display("ref pic list 1 not ok idx=%d, soft poc=%d, hard poc= %d, soft idx=%d, hard_idx=%d", i,ref_pic_list1_list,uut.inter_top_block.mv_derive_block.ref_pic_list_1.mem[i],ref_pic_list1_idx,uut.inter_top_block.mv_derive_block.ref_pic_list1_dpb_idx.mem[i]);
                    $stop();
                end
            end
        end
    end
    
end
endtask

task current_mv_verify;
begin
    if(uut.inter_top_block.mv_derive_block.current_mv_field_valid == 1) begin
        if(uut.inter_top_block.mv_derive_block.cb_pred_mode == `MODE_INTER) begin
            if(test_mv_field_pred_flag_l0 == uut.inter_top_block.mv_derive_block.current_mv_field_pred_flag_l0) begin
                
            end
            else begin
                $display(" pred flag l0 soft = %d hard = %d @ xP = %d, yP = %d",test_mv_field_pred_flag_l0,uut.inter_top_block.mv_derive_block.current_mv_field_pred_flag_l0,uut.inter_top_block.mv_derive_block.xx_pb,uut.inter_top_block.mv_derive_block.yy_pb);
                $stop();
            end
            if(test_mv_field_pred_flag_l1 ==uut.inter_top_block.mv_derive_block.current_mv_field_pred_flag_l1) begin
            
            end
            else begin
                $display(" pred flag l1 soft = %d hard = %d @ xP = %d, yP = %d",test_mv_field_pred_flag_l1,uut.inter_top_block.mv_derive_block.current_mv_field_pred_flag_l1,uut.inter_top_block.mv_derive_block.xx_pb,uut.inter_top_block.mv_derive_block.yy_pb);
                $stop();
            end
            if(test_mv_field_pred_flag_l0) begin
                if(test_mv_field_ref_idx_l0 == uut.inter_top_block.mv_derive_block.current_mv_field_ref_idx_l0) begin
                
                end
                else begin
                    $display(" ref idx l0 not equal soft %d hard %d @ xPb = %d, yPb = %d",test_mv_field_ref_idx_l0,uut.inter_top_block.mv_derive_block.current_mv_field_ref_idx_l0,uut.inter_top_block.mv_derive_block.xx_pb,uut.inter_top_block.mv_derive_block.yy_pb);
                    $stop();
                end
                if(test_mv_field_mv_x_l0 == uut.inter_top_block.mv_derive_block.current_mv_field_mv_x_l0) begin
                
                end
                else begin
                    $display(" x l0 not equal soft %d hard %d @ xPb = %d, yPb = %d",test_mv_field_mv_x_l0,uut.inter_top_block.mv_derive_block.current_mv_field_mv_x_l0,uut.inter_top_block.mv_derive_block.xx_pb,uut.inter_top_block.mv_derive_block.yy_pb);
                    $stop();
                end
                if(test_mv_field_mv_y_l0 == uut.inter_top_block.mv_derive_block.current_mv_field_mv_y_l0) begin
                
                end
                else begin
                    $display(" y l0 not equal soft %d hard %d @ xPb = %d, yPb = %d",test_mv_field_mv_y_l0,uut.inter_top_block.mv_derive_block.current_mv_field_mv_y_l0,uut.inter_top_block.mv_derive_block.xx_pb,uut.inter_top_block.mv_derive_block.yy_pb);
                    $stop();
                end
            end
            if(test_mv_field_pred_flag_l1) begin
                if(test_mv_field_ref_idx_l1 == uut.inter_top_block.mv_derive_block.current_mv_field_ref_idx_l1) begin
                
                end
                else begin
                    $display(" x l1 not equal soft %d hard %d @ xPb = %d, yPb = %d",test_mv_field_ref_idx_l1,uut.inter_top_block.mv_derive_block.current_mv_field_ref_idx_l1,uut.inter_top_block.mv_derive_block.xx_pb,uut.inter_top_block.mv_derive_block.yy_pb);
                    $stop();
                end
                if(test_mv_field_mv_x_l1 == uut.inter_top_block.mv_derive_block.current_mv_field_mv_x_l1) begin
                
                end
                else begin
                    $display(" x l1 not equal soft %d hard %d @ xPb = %d, yPb = %d",test_mv_field_mv_x_l1,uut.inter_top_block.mv_derive_block.current_mv_field_mv_x_l1,uut.inter_top_block.mv_derive_block.xx_pb,uut.inter_top_block.mv_derive_block.yy_pb);
                    $stop();
                end
                if(test_mv_field_mv_y_l1 == uut.inter_top_block.mv_derive_block.current_mv_field_mv_y_l1) begin
                
                end
                else begin
                    $display(" y l1 not equal soft %d hard %d @ xPb = %d, yPb = %d",test_mv_field_mv_y_l1,uut.inter_top_block.mv_derive_block.current_mv_field_mv_y_l1,uut.inter_top_block.mv_derive_block.xx_pb,uut.inter_top_block.mv_derive_block.yy_pb);
                    $stop();
                end
            end
        end
        if(uut.inter_top_block.mv_derive_block.cb_pred_mode == `MODE_INTRA) begin 
            if(uut.inter_top_block.mv_derive_block.current_mv_field_pred_flag_l0 == 0 && uut.inter_top_block.mv_derive_block.current_mv_field_pred_flag_l1 == 0) begin
                
            end
            else begin
                $display(" intra pu not equal @ xPb = %d, yPb = %d",uut.inter_top_block.mv_derive_block.xx_pb,uut.inter_top_block.mv_derive_block.yy_pb);
                    $stop();
            end
        end
    end
    inter_pred_iface_block.sv_get_current_mv(0,0,test_mv_field_pred_flag_l0);
    inter_pred_iface_block.sv_get_current_mv(1,0,test_mv_field_pred_flag_l1);
    inter_pred_iface_block.sv_get_current_mv(0,1,test_mv_field_ref_idx_l0);
    inter_pred_iface_block.sv_get_current_mv(1,1,test_mv_field_ref_idx_l1);
    inter_pred_iface_block.sv_get_current_mv(0,2,test_mv_field_mv_x_l0);
    inter_pred_iface_block.sv_get_current_mv(1,2,test_mv_field_mv_x_l1);
    inter_pred_iface_block.sv_get_current_mv(0,3,test_mv_field_mv_y_l0);
    inter_pred_iface_block.sv_get_current_mv(1,3,test_mv_field_mv_y_l1);
end
endtask

task cache_input_verify;
    reg       [X_ADDR_WDTH-1:0]    	            start_x_file_in;
    reg       [X_ADDR_WDTH-1:0]    	            end_x_file_in;    
    reg       [Y_ADDR_WDTH-1:0]    	            start_y_file_in;
    reg       [Y_ADDR_WDTH-1:0]    	            end_y_file_in;
    reg       [4-1:0]		        ref_idx_file_in; 
    integer      mv_block_nm;

	begin
        ref_pix_axi_data_set();
        // if(uut.inter_top_block.pred_sample_gen_block.cache_block.state == STATE_CACHE_READY && uut.inter_top_block.pred_sample_gen_block.cache_block.filer_idle_in) begin
            // rfile = $fscanf(cache_in_file,"%d,%d,%d,%d,%d,%d\n",start_x_file_in,end_x_file_in,start_y_file_in,end_y_file_in,ref_idx_file_in,mv_block_nm);
            // $display("%d,%d,%d,%d,%d,%d",start_x_file_in,end_x_file_in,start_y_file_in,end_y_file_in,ref_idx_file_in,mv_block_nm);
            // //$stop();
            // if(start_x_file_in == uut.inter_top_block.pred_sample_gen_block.cache_block.start_x_in) begin
            // end
            // else begin
                // $display("hard start x= %d soft start x = %d",uut.inter_top_block.pred_sample_gen_block.cache_block.start_x_in,start_x_file_in);
                // $stop();
            // end
            // if(start_y_file_in == uut.inter_top_block.pred_sample_gen_block.cache_block.start_y_in) begin
            // end
            // else begin
                // $display("hard start y= %d soft start y = %d",uut.inter_top_block.pred_sample_gen_block.cache_block.start_y_in,start_y_file_in);
                // $stop();
            // end
            
            // if((end_y_file_in -start_y_file_in) == uut.inter_top_block.pred_sample_gen_block.cache_block.rf_blk_hgt_in) begin
            // end
            // else begin
                // $display("hard hgt = %d soft hgt = %d",uut.inter_top_block.pred_sample_gen_block.cache_block.rf_blk_hgt_in,(end_y_file_in -start_y_file_in));
                // $stop();
            // end
            // if((end_x_file_in - start_x_file_in) == uut.inter_top_block.pred_sample_gen_block.cache_block.rf_blk_wdt_in) begin
            // end
            // else begin
                // $display("hard wdt = %d soft wdt = %d",uut.inter_top_block.pred_sample_gen_block.cache_block.rf_blk_wdt_in,(end_x_file_in -start_x_file_in));
                // $stop();
            // end
        // end
	end
endtask

task final_pred_pixl_check;
    integer i,j;    
    integer cb_offset_pred_pixel;
    integer cr_offset_pred_pixl;
    reg [7:0] pixl_arry_4x4;
    reg [7:0] hard_pixl_arry_4x4;
    integer xT_in_4x4_luma_out;
    integer yT_in_4x4_luma_out;
    integer xT_in_4x4_cr_out  ;
    integer yT_in_4x4_cr_out  ;
    integer yT_in_4x4_cb_out  ;
    integer xT_in_4x4_cb_out  ;
    integer k;
    begin
        if(uut.inter_top_block.luma_wght_pred_valid) begin
            if(uut.inter_top_block.xT_in_4x4_luma_out == 0 && uut.inter_top_block.yT_in_4x4_luma_out==0)begin
                frame_offset_pred_pixel = uut.current__poc*PIC_WIDTH*PIC_HEIGHT*3/2;
            end
        end
        if(uut.inter_top_block.cb_wght_pred_valid) begin
            if(uut.inter_top_block.xT_in_4x4_cb_out == 0 && uut.inter_top_block.yT_in_4x4_cb_out==0)begin
                cb_offset_pred_pixel = uut.current__poc*PIC_WIDTH*PIC_HEIGHT*3/2 + PIC_WIDTH*PIC_HEIGHT;
                cr_offset_pred_pixl = cb_offset_pred_pixel + PIC_WIDTH*PIC_HEIGHT/4;
            end
        end
        if(uut.inter_top_block.luma_wght_pred_valid && (frame_offset_pred_pixel != 1*PIC_WIDTH*PIC_HEIGHT*3/2)) begin//-----------------
            xT_in_4x4_luma_out = uut.inter_top_block.xT_in_4x4_luma_out;
            yT_in_4x4_luma_out = uut.inter_top_block.yT_in_4x4_luma_out;
            for(j=0;j<4;j=j+1)begin
                for(i=0;i<4;i=i+1)begin
                    rfile = $fseek(pred_pixl_file,(frame_offset_pred_pixel + (PIC_WIDTH)*(j + yT_in_4x4_luma_out*4) + xT_in_4x4_luma_out*4 + i),0);
                    pixl_arry_4x4= $fgetc(pred_pixl_file);
                    for(k=0;k<8;k=k+1)begin
                        hard_pixl_arry_4x4[k] = uut.inter_top_block.luma_wgt_pred_out[(4*j+i)*8+k];
                    end
                    if(pixl_arry_4x4 == hard_pixl_arry_4x4) begin
                        //$display("luma final pred pixl match @loc=%d, x=%d, y=%d, soft=%d, hard=%d",(frame_offset_pred_pixel + (PIC_WIDTH)*(j + yT_in_4x4_luma_out*4) + xT_in_4x4_luma_out*4 + i),xT_in_4x4_luma_out*4 + i,j + yT_in_4x4_luma_out*4,pixl_arry_4x4,hard_pixl_arry_4x4);
                    end
                    else begin
                        $display("luma final pred pixl mismatch @loc=%d, x=%d, y=%d, soft=%d, hard=%d",(frame_offset_pred_pixel + (PIC_WIDTH)*(j + yT_in_4x4_luma_out*4) + xT_in_4x4_luma_out*4 + i),xT_in_4x4_luma_out*4 + i,j + yT_in_4x4_luma_out*4,pixl_arry_4x4,hard_pixl_arry_4x4);
                        $stop();
                    end
                end
            end
        end
        if(uut.inter_top_block.cb_wght_pred_valid && (cb_offset_pred_pixel != 1*PIC_WIDTH*PIC_HEIGHT*3/2 + PIC_WIDTH*PIC_HEIGHT )) begin //============
            xT_in_4x4_cb_out = uut.inter_top_block.xT_in_4x4_cb_out>>1;
            yT_in_4x4_cb_out = uut.inter_top_block.yT_in_4x4_cb_out>>1;
            for(j=0;j<4;j=j+1)begin
                for(i=0;i<4;i=i+1)begin
                    rfile = $fseek(pred_pixl_file,(cb_offset_pred_pixel + (PIC_WIDTH>>1)*(j + yT_in_4x4_cb_out*4) + xT_in_4x4_cb_out*4 + i),0);
                    pixl_arry_4x4 = $fgetc(pred_pixl_file);
                    for(k=0;k<8;k=k+1)begin
                        hard_pixl_arry_4x4[k] = uut.inter_top_block.cb_wgt_pred_out[(4*j+i)*8+k];
                    end
                    if(pixl_arry_4x4 == hard_pixl_arry_4x4) begin
                        //$display("cb final pred pixl match @loc=%d, @ x=%d, y=%d, soft=%d, hard=%d",(cb_offset_pred_pixel + (PIC_WIDTH>>1)*(j + yT_in_4x4_cb_out*4) + xT_in_4x4_cb_out*4 + i),xT_in_4x4_cb_out*4+i,yT_in_4x4_cb_out*4+j,pixl_arry_4x4,hard_pixl_arry_4x4);
                    end
                    else begin
                        $display("cb final pred pixl mismatch @ x=%d, y=%d, soft=%d, hard=%d",xT_in_4x4_cb_out*4+i,yT_in_4x4_cb_out*4+j,pixl_arry_4x4,hard_pixl_arry_4x4);
                        $stop();
                    end
                    rfile = $fseek(pred_pixl_file,(cr_offset_pred_pixl + (PIC_WIDTH>>1)*(j + yT_in_4x4_cb_out*4) + xT_in_4x4_cb_out*4 + i),0);
                    pixl_arry_4x4 = $fgetc(pred_pixl_file);
                    for(k=0;k<8;k=k+1)begin
                        hard_pixl_arry_4x4[k] = uut.inter_top_block.cr_wgt_pred_out[(4*j+i)*8+k];
                    end
                    if(pixl_arry_4x4 == hard_pixl_arry_4x4) begin
                        //$display("cr final pred pixl match @loc=%d, @ x=%d, y=%d, soft=%d, hard=%d",(cr_offset_pred_pixl + (PIC_WIDTH>>1)*(j + yT_in_4x4_cb_out*4) + xT_in_4x4_cb_out*4 + i),xT_in_4x4_cb_out*4+i,yT_in_4x4_cb_out*4+j,pixl_arry_4x4,hard_pixl_arry_4x4);
                    end
                    else begin
                        $display("cr final pred pixl mismatch @ x=%d, y=%d, soft=%d, hard=%d",xT_in_4x4_cb_out*4+i,yT_in_4x4_cb_out*4+j,pixl_arry_4x4,hard_pixl_arry_4x4);
                        $stop();
                    end
                end
            end
        end
    end
endtask

task ref_pix_axi_data_set();
    reg [31:0] axi_addr;
    reg [31:0] ref_idx;
    integer ref_pic_idx;
    reg [31:0] x_val, y_val;
    integer i,j;
    integer frame_offset ;
    integer cb_offset;
    integer cr_offset;
    reg [7:0] temp_pixl;
    reg [7:0] y_pixl_arry[32-1:0];
    reg [7:0] cb_pixl_arry[8-1:0];
    reg [7:0] cr_pixl_arry[8-1:0];
    reg ref_poc_found;
    begin
        if(ref_pix_axi_ar_valid ==1) begin
            x_val = 0;
            y_val = 0;
            ref_idx = ref_pix_axi_ar_addr/`REF_PIX_FRAME_OFFSET;
            ref_poc_found = 0;
            for(i=0;i<16 && ref_poc_found==0;i=i+1) begin
                inter_pred_iface_block.sv_get_ref_pic_list_idx(i,0,ref_pic_idx);
                if(ref_pic_idx== ref_idx) begin
                    ref_poc_found =1;
                    inter_pred_iface_block.sv_get_ref_pic_list_poc(i,0,ref_idx);
                end
            end
            axi_addr = ref_pix_axi_ar_addr%`REF_PIX_FRAME_OFFSET;
            {y_val[9:4]} = axi_addr/`REF_PIX_IU_ROW_OFFSET;
            axi_addr = axi_addr%`REF_PIX_IU_ROW_OFFSET;
            {x_val[8:3]} = axi_addr/`REF_PIX_IU_OFFSET;
            axi_addr = axi_addr%`REF_PIX_IU_OFFSET;
            {y_val[3:0],x_val[2:0]} = axi_addr/`REF_PIX_BU_OFFSET;
            x_val = x_val << 3;
            y_val = y_val << 2;
            frame_offset = ref_idx*PIC_WIDTH*PIC_HEIGHT*3/2;
            cb_offset = frame_offset + PIC_WIDTH*PIC_HEIGHT;
            cr_offset = cb_offset + PIC_WIDTH*PIC_HEIGHT/4;
            for(j=0;j<4;j=j+1)begin
                for(i=0;i<8;i=i+1)begin
                    rfile = $fseek(final_pixl_file,(frame_offset + (PIC_WIDTH)*(j + y_val) + x_val + i),0);
                    y_pixl_arry[8*j+i] = $fgetc(final_pixl_file);
                end
            end
            x_val = x_val >>1;
            y_val = y_val >>1;
            for(j=0;j<2;j=j+1)begin
                for(i=0;i<4;i=i+1)begin
                    rfile = $fseek(final_pixl_file,(cb_offset + (PIC_WIDTH>>1)*(j + y_val) + x_val + i),0);
                    cb_pixl_arry[4*j+i] = $fgetc(final_pixl_file);
                end
            end
            for(j=0;j<2;j=j+1)begin
                for(i=0;i<4;i=i+1)begin
                    rfile = $fseek(final_pixl_file,(cr_offset + (PIC_WIDTH>>1)*(j + y_val) + x_val + i),0);
                    cr_pixl_arry[4*j+i] = $fgetc(final_pixl_file);
                end
            end
            ref_pix_axi_r_data = {
                                    cr_pixl_arry[7 ],
                                    cr_pixl_arry[6 ],
                                    cr_pixl_arry[5 ],
                                    cr_pixl_arry[4 ],
                                    cr_pixl_arry[3 ],
                                    cr_pixl_arry[2 ],
                                    cr_pixl_arry[1 ],
                                    cr_pixl_arry[0 ],
                                    cb_pixl_arry[7 ],
                                    cb_pixl_arry[6 ],
                                    cb_pixl_arry[5 ],
                                    cb_pixl_arry[4 ],
                                    cb_pixl_arry[3 ],
                                    cb_pixl_arry[2 ],
                                    cb_pixl_arry[1 ],
                                    cb_pixl_arry[0 ],
                                    y_pixl_arry[31],
                                    y_pixl_arry[30],
                                    y_pixl_arry[29],
                                    y_pixl_arry[28],
                                    y_pixl_arry[27],
                                    y_pixl_arry[26],
                                    y_pixl_arry[25],
                                    y_pixl_arry[24],
                                    y_pixl_arry[23],
                                    y_pixl_arry[22],
                                    y_pixl_arry[21],
                                    y_pixl_arry[20],
                                    y_pixl_arry[19],
                                    y_pixl_arry[18],
                                    y_pixl_arry[17],
                                    y_pixl_arry[16],
                                    y_pixl_arry[15],
                                    y_pixl_arry[14],
                                    y_pixl_arry[13],
                                    y_pixl_arry[12],
                                    y_pixl_arry[11],
                                    y_pixl_arry[10],
                                    y_pixl_arry[9 ],
                                    y_pixl_arry[8 ],
                                    y_pixl_arry[7 ],
                                    y_pixl_arry[6 ],
                                    y_pixl_arry[5 ],
                                    y_pixl_arry[4 ],
                                    y_pixl_arry[3 ],
                                    y_pixl_arry[2 ],
                                    y_pixl_arry[1 ],
                                    y_pixl_arry[0 ]

                                };
        end 
    end
endtask


endmodule

