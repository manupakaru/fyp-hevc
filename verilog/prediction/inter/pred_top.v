`timescale 1ns / 1ps
module pred_top
    (
        clk,
        reset,
        enable,

        fifo_in,
        input_fifo_is_empty,
        read_en_out,

        fifo_out,
        output_fifo_is_full,
        write_en_out,

        //luma residual fifo interface
        y_residual_fifo_in,
        y_residual_fifo_is_empty_in,
        y_residual_fifo_read_en_out,

        //cb residual fifo interface
        cb_residual_fifo_in,
        cb_residual_fifo_is_empty_in,
        cb_residual_fifo_read_en_out,

        //cr residual fifo interface
        cr_residual_fifo_in,
        cr_residual_fifo_is_empty_in,
        cr_residual_fifo_read_en_out,
        
        //OUTPUT FIFO INTERFACES
        
        //luma dbf fifo interface
        y_dbf_fifo_is_full,
        
        //cb dbf fifo interface
        cb_dbf_fifo_is_full,
        
        //cr dbf fifo interface
        cr_dbf_fifo_is_full,

        mv_col_axi_awid,     
        mv_col_axi_awlen,    
        mv_col_axi_awsize,   
        mv_col_axi_awburst,  
        mv_col_axi_awlock,   
        mv_col_axi_awcache,  
        mv_col_axi_awprot,   
        mv_col_axi_awvalid,
        mv_col_axi_awaddr,
        mv_col_axi_awready,
        mv_col_axi_wstrb,
        mv_col_axi_wlast,
        mv_col_axi_wvalid,
        mv_col_axi_wdata,
        mv_col_axi_wready,
        // mv_col_axi_bid,
        mv_col_axi_bresp,
        mv_col_axi_bvalid,
        mv_col_axi_bready,
        
        mv_pref_axi_araddr              ,
        mv_pref_axi_arlen               ,
        mv_pref_axi_arsize              ,
        mv_pref_axi_arburst             ,
        mv_pref_axi_arprot              ,
        mv_pref_axi_arvalid             ,
        mv_pref_axi_arready             ,

        mv_pref_axi_rdata               ,
        mv_pref_axi_rresp               ,
        mv_pref_axi_rlast               ,
        mv_pref_axi_rvalid              ,
        mv_pref_axi_rready              ,

        mv_pref_axi_arlock              ,
        mv_pref_axi_arid                ,
        mv_pref_axi_arcache             ,

        ref_pix_axi_ar_addr         ,
        ref_pix_axi_ar_len          ,
        ref_pix_axi_ar_size         ,
        ref_pix_axi_ar_burst        ,
        ref_pix_axi_ar_prot         ,
        ref_pix_axi_ar_valid        ,
        ref_pix_axi_ar_ready        ,
        
        ref_pix_axi_r_data          ,
        ref_pix_axi_r_resp          ,
        ref_pix_axi_r_last          ,
        ref_pix_axi_r_valid         ,
        ref_pix_axi_r_ready         ,   
        
        
        
        
        temp_port
        );
//---------------------------------------------------------------------------------------------------------------------
// Global constant headers
//---------------------------------------------------------------------------------------------------------------------
    /* STYLE_NOTES :
     * Always include global constant header files that have constant definitions before all other items
     * these files will contain constants in the form of localparams of `define directives
     */
     
    `include "pred_def.v"
    `include "inter_axi_def.v"
//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------
    /* STYLE_NOTES :
     * Define all paramters
     */

    
//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------
    /* STYLE_NOTES :
     * Define all localparams after parameters
     */
    localparam                          FIFO_IN_WIDTH       = 32;
    localparam                          FIFO_OUT_WIDTH      = 32;  


    localparam                          STATE_READ_WAIT = 0;
    localparam                          STATE_ACTIVE = 1;
    localparam                          STATE_WRITE_WAIT = 2;
    localparam                          STATE_ERROR = 3;
    localparam                          STATE_CURRENT_POC = 4;
    localparam                          STATE_CUR_PIC_FIL_IDX = 5;
    localparam                          STATE_REF_PIC_LIST_5_UPDATE = 6;
    localparam                          STATE_REF_PIC_LIST_5_SCAN   = 7;
    localparam                          STATE_SEND_POC_TO_DBF = 8;
    localparam                          STATE_WRITE_WAIT_CUR_DPB_IDX_TO_DBF = 9;
    localparam                          STATE_REF_PIC_WRITE_WAIT = 10;
    localparam                          STATE_DPB_ADD_PREV_PIC = 11;
    localparam                          STATE_POC_READ_WAIT = 12;
    localparam                          STATE_MVD1_READ_WAIT = 13;
    localparam                          STATE_MVD2_READ_WAIT = 14;
    localparam                          STATE_MVD1_READ = 15;
    localparam                          STATE_MVD2_READ = 16;
    localparam                          STATE_MV_RETURN_WAIT1 = 17;
    localparam                          STATE_MV_RETURN_WAIT2 = 18;
    localparam                          STATE_MV_RETURN_WAIT3 = 19;
    localparam                          STATE_INTER_PREFETCH_COL_WRITE_WAIT = 20;
    localparam                          STATE_GET_ST_RPS_ENTRY = 21;
    localparam                          STATE_GET_ST_RPS_HEADER = 22;
    localparam                          STATE_SLICE_1_WRITE_WAIT = 23;
    localparam                          STATE_REF_PIC_LIST_TRANSFER = 24;
    localparam                          STATE_SET_ST_RPS_ENTRY_ADDR_FOR_REF_POC5 = 25;
    localparam                          STATE_RU_PACKET_READ_WAIT = 26;
    localparam                          STATE_RU_PACKET_READ = 27;
    localparam                          STATE_RU_DONE_WAIT1 = 28;
    localparam                          STATE_RU_DONE_WAIT2 = 29;
    localparam                          STATE_RU_DONE_WAIT3 = 30;
    
    
    localparam                          WRITE_REF_PIC_STATE_IDLE = 0;
    localparam                          WRITE_REF_PIC_STATE_CHECK = 1;
    localparam                          WRITE_REF_PIC_STATE_POC_WRITE = 2;
    localparam                          WRITE_REF_PIC_STATE_IDX_WRITE = 3;
    localparam                          WRITE_REF_PIC_STATE_DONE = 4;
    localparam                          WRITE_REF_PIC_FIND_FOL_IDX = 5;
    
    
    input                                   clk;
    input                                   reset;
    input                                   enable;
    input                                   output_fifo_is_full;
    
    input           [FIFO_IN_WIDTH-1:0]     fifo_in;
    output      reg [FIFO_OUT_WIDTH-1:0]    fifo_out;
    input                                   input_fifo_is_empty;
    output       reg                        read_en_out;
    output       reg                        write_en_out;
    output          [REF_PIC_LIST_POC_DATA_WIDTH-1:0]       temp_port;


    //luma residual fifo interface
    input       [RESIDUAL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE - 1 :0]     y_residual_fifo_in;
    input                                                                       y_residual_fifo_is_empty_in;
    output                                                                      y_residual_fifo_read_en_out;
    //cb residual fifo interface
    input       [RESIDUAL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE - 1 :0]     cb_residual_fifo_in;
    input                                                                       cb_residual_fifo_is_empty_in;
    output                                                                      cb_residual_fifo_read_en_out;
    //cr residual fifo interface                                
    input       [RESIDUAL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE - 1 :0]     cr_residual_fifo_in;
    input                                                                       cr_residual_fifo_is_empty_in;
    output                                                                      cr_residual_fifo_read_en_out;

    // OUTPUT FIFO INTERFACES
    
    //luma dbf fifo interface
    input                                                                       y_dbf_fifo_is_full;
    
    //cb dbf fifo interface
    input                                                                       cb_dbf_fifo_is_full;
    
    //cr dbf fifo interface
    input                                                                       cr_dbf_fifo_is_full;

    
    //--------------axi interface
    output                                          mv_col_axi_awid    ;// = 0;
    output      [7:0]                               mv_col_axi_awlen   ;// = MV_COL_AXI_AX_LEN;
    output      [2:0]                               mv_col_axi_awsize  ;// = MV_COL_AXI_AX_SIZE;
    output      [1:0]                               mv_col_axi_awburst ;// = `AX_BURST_INC;
    output                        	                mv_col_axi_awlock  ;// = `AX_LOCK_DEFAULT;
    output      [3:0]                               mv_col_axi_awcache ;// = `AX_CACHE_DEFAULT;
    output      [2:0]                               mv_col_axi_awprot  ;// = `AX_PROT_DATA;
    output                                          mv_col_axi_awvalid;
    output      [31:0]                              mv_col_axi_awaddr;

    input                       	                mv_col_axi_awready;

    // write data channel
    output      [64-1:0]                            mv_col_axi_wstrb;// = 64'hFFFFF_FFFFF_FFFFF_FFFFF_FFFFF;       // 40 bytes
    output                                          mv_col_axi_wlast;
    output                                          mv_col_axi_wvalid;
    output      [MV_COL_AXI_DATA_WIDTH -1:0]        mv_col_axi_wdata;

    input	                                        mv_col_axi_wready;

    //write response channel
    // input                       	                mv_col_axi_bid;
    input       [1:0]                               mv_col_axi_bresp;
    input                       	                mv_col_axi_bvalid;
    output                                          mv_col_axi_bready;  

    output	    [AXI_ADDR_WDTH-1:0]		            mv_pref_axi_araddr  ;
    output wire [7:0]					            mv_pref_axi_arlen   ;
    output wire	[2:0]					            mv_pref_axi_arsize  ;
    output wire [1:0]					            mv_pref_axi_arburst ;
    output wire [2:0]					            mv_pref_axi_arprot  ;
    output 	   						                mv_pref_axi_arvalid ;
    input 								            mv_pref_axi_arready ;
            
    input		[MV_COL_AXI_DATA_WIDTH-1:0]		    mv_pref_axi_rdata   ;
    input		[1:0]					            mv_pref_axi_rresp   ;
    input 								            mv_pref_axi_rlast   ;
    input								            mv_pref_axi_rvalid  ;
    output 						                    mv_pref_axi_rready  ;

    output                        	                mv_pref_axi_arlock  ;
    output                                          mv_pref_axi_arid    ;    
    output      [3:0]                               mv_pref_axi_arcache ;     
    
    // axi master interface         
    output      [AXI_ADDR_WDTH-1:0]                 ref_pix_axi_ar_addr     ;
    output      [7:0]                               ref_pix_axi_ar_len      ;
    output      [2:0]                               ref_pix_axi_ar_size     ;
    output      [1:0]                               ref_pix_axi_ar_burst    ;
    output      [2:0]                               ref_pix_axi_ar_prot     ;
    output                                          ref_pix_axi_ar_valid    ;
    input                                           ref_pix_axi_ar_ready    ;
                
    input       [AXI_DATA_WDTH-1:0]                 ref_pix_axi_r_data      ;
    input       [1:0]                               ref_pix_axi_r_resp      ;
    input                                           ref_pix_axi_r_last      ;
    input                                           ref_pix_axi_r_valid     ;
    output                                          ref_pix_axi_r_ready     ;   
    //-----------end of axi interface
    
//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------
    /* STYLE_NOTES
     * Eacho wire and register must be defined on a seperate line
     * Although Verilog allows for implicit decleration of variables, all signals must be declared explicitly.
     * Group variables where necessary. Comment your variable by block and by signal.
     */


    integer                                         state;                      // the internal state of the module
    integer                                         next_state;
    integer                                         write_ref_pic_state;

    reg         [PIC_DIM_WIDTH-1:0]                 pic_width;
    reg         [PIC_DIM_WIDTH-1:0]                 pic_height;
    reg         [LOG2_CTB_WIDTH - 1: 0]             log2_ctb_size;
    reg                                             strong_intra_smoothing;
    reg                                             constrained_intra_pred;

    reg                                             rps_header_wr_en;
    reg         [RPS_HEADER_DATA_WIDTH -1:0]        rps_header_data_in;             //temp
    reg         [RPS_HEADER_ADDR_WIDTH -1:0]        rps_header_addr;
    reg         [RPS_HEADER_ADDR_WIDTH -1:0]        rps_header_addr_reg;
    
    wire        [RPS_HEADER_DATA_WIDTH -1:0]        rps_header_data_out;

    reg         [NUM_NEG_POS_POC_WIDTH - 1:0]       num_positive_poc;               // temp
    reg         [NUM_NEG_POS_POC_WIDTH - 1:0]       num_negative_poc;               // temp
    reg         [NUM_NEG_POS_POC_WIDTH - 1:0]       num_delta_poc;               // temp

    reg                                             rps_entry_wr_en;
    reg                                             st_rps_entry_wr_en;
    reg         [RPS_ENTRY_DATA_WIDTH -1:0]         rps_entry_data_in;              // temp
    reg         [RPS_ENTRY_DATA_WIDTH -1:0]         st_rps_entry_data_in;              // temp
    reg         [RPS_ENTRY_ADDR_WIDTH -1:0]         rps_entry_addr;
    reg         [NUM_NEG_POS_POC_WIDTH -1:0]        st_rps_entry_addr;
    
    wire        [RPS_ENTRY_DATA_WIDTH -1:0]         rps_entry_data_out;
    wire        [RPS_ENTRY_DATA_WIDTH -1:0]         st_rps_entry_data_out;
 
 
    reg         [SLICE_TYPE_WIDTH -1:0]             slice_type;
    wire        [SLICE_TYPE_WIDTH -1:0]                 slice_type_wire;
    reg         [SHORT_TERM_REF_PIC_IDX_WIDTH -1:0]     short_term_ref_pic_idx;
    wire        [SHORT_TERM_REF_PIC_IDX_WIDTH -1:0]    short_term_ref_pic_idx_wire;
    reg                                                 short_term_ref_pic_sps;    // used to indicate whether rps taken from short_term_rps memory or rps sent with the slice 
    wire                                                short_term_ref_pic_sps_wire;
    
    reg                                             slice_temporal_mvp_enabled_flag;
    
    reg         [NUM_REF_IDX_L0_MINUS1_WIDTH - 1:0]     num_active_ref_idx_l0;
    reg         [NUM_REF_IDX_L1_MINUS1_WIDTH - 1:0]     num_active_ref_idx_l1;
    
    reg         [DPB_ADDR_WIDTH -1: 0]              current_pic_dpb_idx;
    
    reg         [POC_WIDTH -1: 0]                   current__poc;
     
    reg         [INTER_TOP_CONFIG_BUS_MODE_WIDTH -1 :0] inter_config_mode_in;
    reg         [INTER_TOP_CONFIG_BUS_WIDTH -1:0]       inter_config_bus_in;
    
    reg         [INTER_TOP_CONFIG_BUS_WIDTH -1:0]       pu_config_bus_in_stack1;
    reg         [INTER_TOP_CONFIG_BUS_WIDTH -1:0]       pu_config_bus_in_stack2;
    reg         [INTER_TOP_CONFIG_BUS_WIDTH -1:0]       pu_config_bus_in_stack3;
    
    reg         [NUM_REF_IDX_L0_MINUS1_WIDTH - 1:0]     num_pic_st_curr_bef;
    reg         [NUM_REF_IDX_L0_MINUS1_WIDTH - 1:0]     num_pic_st_curr_aft;
    reg         [NUM_REF_IDX_L0_MINUS1_WIDTH - 1:0]     num_pic_ref_pic_5_total;
    reg         [NUM_REF_IDX_L0_MINUS1_WIDTH - 1:0]     num_pic_st_fol;
    reg         [NUM_REF_IDX_L0_MINUS1_WIDTH - 1:0]     num_pic_st_curr_total;
    
    reg                                                 ref_poc_list5_wr_en;   
    reg         [REF_POC_LIST5_DATA_WIDTH -1: 0]        ref_poc_list5_data_in;   
    reg         [REF_POC_LIST5_ADDR_WIDTH -1: 0]        ref_poc_list5_addr;   
    reg         [REF_POC_LIST5_ADDR_WIDTH -1: 0]        ref_poc_list5_addr_read;   
    wire        [REF_POC_LIST5_DATA_WIDTH -1: 0]        ref_poc_list5_data_out; 
    
    wire         [DPB_DATA_WIDTH -1: 0]                 dpb_data_out;
    reg         [DPB_DATA_WIDTH -1: 0]                  dpb_data_in;
    reg                                                 dpb_wr_en;
    reg         [DPB_ADDR_WIDTH -1: 0]                  dpb_addr;
    reg         [DPB_ADDR_WIDTH -1: 0]                  dpb_addr_read;
    reg         [(1<<DPB_ADDR_WIDTH) -1:0]              dpb_filled_flag;
    reg         [(1<<DPB_ADDR_WIDTH) -1:0]              dpb_filled_flag_new;
    
    reg         [NUM_NEG_POS_POC_WIDTH -1:0]        curr_rps_entry_idx;  // make this a temp country
    wire        [DELTA_POC_WIDTH -1:0]              delta_poc_wire;
    
    reg [REF_PIC_LIST_POC_DATA_WIDTH-1:0]     ref_pic_list_poc_data_in;
    reg                                       ref_pic_list0_poc_wr_en  ;
    reg                                       ref_pic_list1_poc_wr_en  ;
    reg [NUM_REF_IDX_L0_MINUS1_WIDTH-1:0]     ref_pic_list0_poc_addr  ;
    reg [NUM_REF_IDX_L0_MINUS1_WIDTH-1:0]     list0_poc_addr_temp  ;
    reg [NUM_REF_IDX_L0_MINUS1_WIDTH-1:0]     ref_pic_list1_poc_addr  ;
    reg [NUM_REF_IDX_L0_MINUS1_WIDTH-1:0]     list1_poc_addr_temp  ;
    
    reg [DPB_FRAME_OFFSET_WIDTH -1:0]           ref_pic_list_idx_data_in;
    reg                                         ref_pic_list_idx_wr_en;
    
    reg         [POC_WIDTH -1:0]                temp_poc;
    reg                                         initial_condition;
    
    reg                                         pred_mode;
    reg         [MAX_NTBS_SIZE - 1:0]           ntbs_reg;   //TODO : Geethan , feed this register with relavant data once recieved. 
   
    
    wire                                        mv_done;
    wire                                        pred_pixl_gen_idle_out_to_pred_top;
  

    //
    wire        [PIXEL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE - 1:0]             inter_y_predsample_4by4_int;
    wire                                                                            inter_y_predsample_4by4_valid_int;
    wire        [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]                            inter_y_predsample_4by4_x_int;
    wire        [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]                            inter_y_predsample_4by4_y_int;
    wire                                                                            inter_y_res_present_int;

    wire        [PIXEL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE - 1:0]             inter_cb_predsample_4by4_int;
    wire                                                                            inter_cb_predsample_4by4_valid_int;
    wire        [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]                            inter_cb_predsample_4by4_x_int;
    wire        [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]                            inter_cb_predsample_4by4_y_int;
    wire                                                                            inter_cb_res_present_int;

    wire        [PIXEL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE - 1:0]             inter_cr_predsample_4by4_int;
    wire                                                                            inter_cr_predsample_4by4_valid_int;
    wire        [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]                            inter_cr_predsample_4by4_x_int;
    wire        [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]                            inter_cr_predsample_4by4_y_int;
    wire                                                                            inter_cr_res_present_int;


    // Residual Adder Signals
    reg        [PIXEL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE - 1:0]  y_common_predsample_4by4_int;
    reg                                                                 y_common_predsample_4by4_valid_int;
    reg        [PIXEL_ADDR_LENGTH - 1:0]                                y_common_predsample_4by4_x_int;
    reg        [PIXEL_ADDR_LENGTH - 1:0]                                y_common_predsample_4by4_y_int;
    reg                                                                 y_common_res_present_int;
    wire       [PIXEL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE - 1:0]  y_res_added_int;
    wire       [PIXEL_WIDTH - 1:0]                                      y_res_added_int_arr[OUTPUT_BLOCK_SIZE - 1:0][OUTPUT_BLOCK_SIZE - 1:0];
    reg                                                                 y_valid_addition_reg;
 
    reg        [PIXEL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE - 1:0]  cb_common_predsample_4by4_int;
    reg                                                                 cb_common_predsample_4by4_valid_int;
    reg        [PIXEL_ADDR_LENGTH - 1:0]                                cb_common_predsample_4by4_x_int;
    reg        [PIXEL_ADDR_LENGTH - 1:0]                                cb_common_predsample_4by4_y_int;
    reg                                                                 cb_common_res_present_int;
    wire       [PIXEL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE - 1:0]  cb_res_added_int;
    wire       [PIXEL_WIDTH - 1:0]                                      cb_res_added_int_arr[OUTPUT_BLOCK_SIZE - 1:0][OUTPUT_BLOCK_SIZE - 1:0];
    reg                                                                 cb_valid_addition_reg;

    reg        [PIXEL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE - 1:0]  cr_common_predsample_4by4_int;
    reg                                                                 cr_common_predsample_4by4_valid_int;
    reg        [PIXEL_ADDR_LENGTH - 1:0]                                cr_common_predsample_4by4_x_int;
    reg        [PIXEL_ADDR_LENGTH - 1:0]                                cr_common_predsample_4by4_y_int;
    reg                                                                 cr_common_res_present_int;
    wire       [PIXEL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE - 1:0]  cr_res_added_int;
    wire       [PIXEL_WIDTH - 1:0]                                      cr_res_added_int_arr[OUTPUT_BLOCK_SIZE - 1:0][OUTPUT_BLOCK_SIZE - 1:0];
    reg                                                                 cr_valid_addition_reg;                                                          


     // INTRA
    //---------------------------------------------------------------
    wire                                        intra_enable;
    
    reg                                         intra_config_data_valid_reg;

    reg         [PIXEL_ADDR_LENGTH - 1:0]       intra_y_top_portw_addr_reg;            
    reg         [PIXEL_WIDTH - 1:0]             intra_y_top_portw_data0_reg;
    reg         [PIXEL_WIDTH - 1:0]             intra_y_top_portw_data1_reg;
    reg         [PIXEL_WIDTH - 1:0]             intra_y_top_portw_data2_reg;
    reg         [PIXEL_WIDTH - 1:0]             intra_y_top_portw_data3_reg;
    reg         [PIXEL_WIDTH - 1:0]             intra_y_top_portw_data4_reg;
    reg         [PIXEL_WIDTH - 1:0]             intra_y_top_portw_data5_reg;
    reg         [PIXEL_WIDTH - 1:0]             intra_y_top_portw_data6_reg;
    reg         [PIXEL_WIDTH - 1:0]             intra_y_top_portw_data7_reg;
    reg         [PIXEL_WIDTH - 1:0]             intra_y_top_portw_en_mask_reg;

    reg         [PIXEL_ADDR_LENGTH - 1:0]       intra_y_left_portw_addr_reg;
    reg         [PIXEL_WIDTH - 1:0]             intra_y_left_portw_data0_reg;   
    reg         [PIXEL_WIDTH - 1:0]             intra_y_left_portw_data1_reg;   
    reg         [PIXEL_WIDTH - 1:0]             intra_y_left_portw_data2_reg;   
    reg         [PIXEL_WIDTH - 1:0]             intra_y_left_portw_data3_reg;   
    reg         [PIXEL_WIDTH - 1:0]             intra_y_left_portw_data4_reg;   
    reg         [PIXEL_WIDTH - 1:0]             intra_y_left_portw_data5_reg;   
    reg         [PIXEL_WIDTH - 1:0]             intra_y_left_portw_data6_reg;   
    reg         [PIXEL_WIDTH - 1:0]             intra_y_left_portw_data7_reg;   
    reg         [PIXEL_WIDTH - 1:0]             intra_y_left_portw_en_mask_reg; 

    wire        [PIXEL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE - 1:0] intra_y_predsample_4by4_int;
    wire                                                                intra_y_predsample_4by4_valid_int;
    wire        [PIXEL_ADDR_LENGTH - 1:0]                               intra_y_predsample_4by4_x_int;
    wire        [PIXEL_ADDR_LENGTH - 1:0]                               intra_y_predsample_4by4_y_int;
    reg         [PIXEL_ADDR_LENGTH - 1:0]                               intra_y_predsample_4by4_x_reg;
    reg         [PIXEL_ADDR_LENGTH - 1:0]                               intra_y_predsample_4by4_y_reg;
    wire                                                                intra_y_predsample_4by4_last_row_int;
    wire                                                                intra_y_predsample_4by4_last_col_int;
    reg                                                                 intra_y_predsample_4by4_last_row_reg;
    reg                                                                 intra_y_predsample_4by4_last_col_reg;
    wire                                                                intra_y_cu_done_int;
    wire                                                                intra_y_res_present_int;


    reg         [PIXEL_ADDR_LENGTH - 1:0]       intra_cb_top_portw_addr_reg;            
    reg         [PIXEL_WIDTH - 1:0]             intra_cb_top_portw_data0_reg;
    reg         [PIXEL_WIDTH - 1:0]             intra_cb_top_portw_data1_reg;
    reg         [PIXEL_WIDTH - 1:0]             intra_cb_top_portw_data2_reg;
    reg         [PIXEL_WIDTH - 1:0]             intra_cb_top_portw_data3_reg;
    reg         [PIXEL_WIDTH - 1:0]             intra_cb_top_portw_data4_reg;
    reg         [PIXEL_WIDTH - 1:0]             intra_cb_top_portw_data5_reg;
    reg         [PIXEL_WIDTH - 1:0]             intra_cb_top_portw_data6_reg;
    reg         [PIXEL_WIDTH - 1:0]             intra_cb_top_portw_data7_reg;
    reg         [PIXEL_WIDTH - 1:0]             intra_cb_top_portw_en_mask_reg;

    reg         [PIXEL_ADDR_LENGTH - 1:0]       intra_cb_left_portw_addr_reg;
    reg         [PIXEL_WIDTH - 1:0]             intra_cb_left_portw_data0_reg;   
    reg         [PIXEL_WIDTH - 1:0]             intra_cb_left_portw_data1_reg;   
    reg         [PIXEL_WIDTH - 1:0]             intra_cb_left_portw_data2_reg;   
    reg         [PIXEL_WIDTH - 1:0]             intra_cb_left_portw_data3_reg;   
    reg         [PIXEL_WIDTH - 1:0]             intra_cb_left_portw_data4_reg;   
    reg         [PIXEL_WIDTH - 1:0]             intra_cb_left_portw_data5_reg;   
    reg         [PIXEL_WIDTH - 1:0]             intra_cb_left_portw_data6_reg;   
    reg         [PIXEL_WIDTH - 1:0]             intra_cb_left_portw_data7_reg;   
    reg         [PIXEL_WIDTH - 1:0]             intra_cb_left_portw_en_mask_reg; 

    wire        [PIXEL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE - 1:0] intra_cb_predsample_4by4_int;
    wire                                                                intra_cb_predsample_4by4_valid_int;
    wire        [PIXEL_ADDR_LENGTH - 1:0]                               intra_cb_predsample_4by4_x_int;
    wire        [PIXEL_ADDR_LENGTH - 1:0]                               intra_cb_predsample_4by4_y_int;
    reg         [PIXEL_ADDR_LENGTH - 1:0]                               intra_cb_predsample_4by4_x_reg;
    reg         [PIXEL_ADDR_LENGTH - 1:0]                               intra_cb_predsample_4by4_y_reg;
    wire                                                                intra_cb_predsample_4by4_last_row_int;
    wire                                                                intra_cb_predsample_4by4_last_col_int;
    reg                                                                 intra_cb_predsample_4by4_last_row_reg;
    reg                                                                 intra_cb_predsample_4by4_last_col_reg;
    wire                                                                intra_cb_cu_done_int;
    wire                                                                intra_cb_res_present_int;

    reg         [PIXEL_ADDR_LENGTH - 1:0]       intra_cr_top_portw_addr_reg;            
    reg         [PIXEL_WIDTH - 1:0]             intra_cr_top_portw_data0_reg;
    reg         [PIXEL_WIDTH - 1:0]             intra_cr_top_portw_data1_reg;
    reg         [PIXEL_WIDTH - 1:0]             intra_cr_top_portw_data2_reg;
    reg         [PIXEL_WIDTH - 1:0]             intra_cr_top_portw_data3_reg;
    reg         [PIXEL_WIDTH - 1:0]             intra_cr_top_portw_data4_reg;
    reg         [PIXEL_WIDTH - 1:0]             intra_cr_top_portw_data5_reg;
    reg         [PIXEL_WIDTH - 1:0]             intra_cr_top_portw_data6_reg;
    reg         [PIXEL_WIDTH - 1:0]             intra_cr_top_portw_data7_reg;
    reg         [PIXEL_WIDTH - 1:0]             intra_cr_top_portw_en_mask_reg;

    reg         [PIXEL_ADDR_LENGTH - 1:0]       intra_cr_left_portw_addr_reg;
    reg         [PIXEL_WIDTH - 1:0]             intra_cr_left_portw_data0_reg;   
    reg         [PIXEL_WIDTH - 1:0]             intra_cr_left_portw_data1_reg;   
    reg         [PIXEL_WIDTH - 1:0]             intra_cr_left_portw_data2_reg;   
    reg         [PIXEL_WIDTH - 1:0]             intra_cr_left_portw_data3_reg;   
    reg         [PIXEL_WIDTH - 1:0]             intra_cr_left_portw_data4_reg;   
    reg         [PIXEL_WIDTH - 1:0]             intra_cr_left_portw_data5_reg;   
    reg         [PIXEL_WIDTH - 1:0]             intra_cr_left_portw_data6_reg;   
    reg         [PIXEL_WIDTH - 1:0]             intra_cr_left_portw_data7_reg;   
    reg         [PIXEL_WIDTH - 1:0]             intra_cr_left_portw_en_mask_reg; 

    wire        [PIXEL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE - 1:0] intra_cr_predsample_4by4_int;
    wire                                                                intra_cr_predsample_4by4_valid_int;
    wire        [PIXEL_ADDR_LENGTH - 1:0]                               intra_cr_predsample_4by4_x_int;
    wire        [PIXEL_ADDR_LENGTH - 1:0]                               intra_cr_predsample_4by4_y_int;
    reg         [PIXEL_ADDR_LENGTH - 1:0]                               intra_cr_predsample_4by4_x_reg;
    reg         [PIXEL_ADDR_LENGTH - 1:0]                               intra_cr_predsample_4by4_y_reg;
    wire                                                                intra_cr_predsample_4by4_last_row_int;
    wire                                                                intra_cr_predsample_4by4_last_col_int;
    reg                                                                 intra_cr_predsample_4by4_last_row_reg;
    reg                                                                 intra_cr_predsample_4by4_last_col_reg;
    wire                                                                intra_cr_cu_done_int;
    wire                                                                intra_cr_res_present_int;
    
    wire    pref_ctu_col_writ_ctu_done_out;    
    wire    fifo_in_used_flag_wire;
    reg    [DPB_ADDR_WIDTH - 1:0]  num_rps_curr_l0;
    reg    [DPB_ADDR_WIDTH -1:0]   num_rps_curr_l1;
    
    reg  last_tu_of_cu;
    reg  [CB_SIZE_WIDTH -1:0 ]  cb_size;
    reg  [CB_SIZE_WIDTH -1:0 ]  cb_size_wire;
    reg  [TB_SIZE_WIDTH -1:0 ]  tb_size_wire;
    reg  [TB_SIZE_WIDTH -1:0 ]  tb_size;
    wire [LOG2_CB_SIZE_WIDTH -1:0 ]  log2_cb_size_wire;
    wire [LOG2_CB_SIZE_WIDTH -1:0 ]  log2_tb_size_wire;
    wire  [CIDX_WIDTH -1:0 ]  c_idx_wire;
    
    reg  [PARTMODE_WIDTH -1:0]  cb_part_mode;
    
    reg  [PART_IDX_WIDTH -1:0]  pb_part_idx;
    
    wire  [XT_WIDTH -1:0 ]          x0_tu_in_ctu_wire;
    reg   [XT_WIDTH -1:0 ]          x0_tu_in_ctu;
    reg   [CB_SIZE_WIDTH -1:0 ]     x0_tu_end_in_ctu;
    reg   [X0_WIDTH -1:0 ]          cb_xx;
    reg   [CB_SIZE_WIDTH -1:0 ]     cb_xx_end;
    wire  [XT_WIDTH -1:0 ]          y0_tu_in_ctu_wire;
    reg   [XT_WIDTH -1:0 ]          y0_tu_in_ctu;
    reg   [CB_SIZE_WIDTH -1:0 ]     y0_tu_end_in_ctu;
    reg   [X0_WIDTH -1:0 ]          cb_yy;
    reg   [CB_SIZE_WIDTH -1:0 ]     cb_yy_end;
    reg                             is_pu_header_cus_last;
    
    wire                             intra_ru_read_ready=1;
    
    wire tu_rd_en;
    wire tu_wr_en;
    wire tu_empty;

    wire [INTER_TOP_CONFIG_BUS_WIDTH-1:0] tu_data_out;
  
//---------------------------------------------------------------------------------------------------------------------
// Implmentation
//---------------------------------------------------------------------------------------------------------------------
   
   assign delta_poc_wire =  st_rps_entry_data_out[RPS_ENTRY_DELTA_POC_RANGE_HIGH:RPS_ENTRY_DELTA_POC_RANGE_LOW];
   
   
   always@(posedge clk) begin
       if(num_active_ref_idx_l0 < (num_pic_st_curr_aft + num_pic_st_curr_bef)) begin
            num_rps_curr_l0 <= num_active_ref_idx_l0;
       end
       else begin
            num_rps_curr_l0 <= num_pic_st_curr_aft + num_pic_st_curr_bef;
       end
       
       if(num_active_ref_idx_l1 < (num_pic_st_curr_aft + num_pic_st_curr_bef)) begin
            num_rps_curr_l1 <= num_active_ref_idx_l1;
       end
       else begin
            num_rps_curr_l1 <= num_pic_st_curr_aft + num_pic_st_curr_bef;
       end
   end
   
   // fifo_in[ FIFO_IN_WIDTH - HEADER_WIDTH - DPB_STATUS_WIDTH - 1: 
                                    // FIFO_IN_WIDTH - HEADER_WIDTH - DPB_STATUS_WIDTH - DELTA_POC_WIDTH];
`ifdef READ_FILE                          
    assign short_term_ref_pic_sps_wire = fifo_in[   HEADER_WIDTH + SHORT_TERM_REF_PIC_SPS_WIDTH- 1:
                                                    HEADER_WIDTH  ] ;


    assign short_term_ref_pic_idx_wire = fifo_in[   HEADER_WIDTH + SHORT_TERM_REF_PIC_SPS_WIDTH + SHORT_TERM_REF_PIC_IDX_WIDTH- 1:
                                                    HEADER_WIDTH + SHORT_TERM_REF_PIC_SPS_WIDTH  ];                                                                            


    assign slice_type_wire =                        fifo_in[    HEADER_WIDTH + SHORT_TERM_REF_PIC_SPS_WIDTH + SHORT_TERM_REF_PIC_IDX_WIDTH + TEMPORAL_MVP_ENABLED_WIDTH + SAO_LUMA_WIDTH + SAO_CHROMA_WIDTH + NUM_REF_IDX_L0_MINUS1_WIDTH + NUM_REF_IDX_L1_MINUS1_WIDTH + MAX_MERGE_CAND_WIDTH + SLICE_TYPE_WIDTH- 1:
                                                                HEADER_WIDTH + SHORT_TERM_REF_PIC_SPS_WIDTH + SHORT_TERM_REF_PIC_IDX_WIDTH + TEMPORAL_MVP_ENABLED_WIDTH + SAO_LUMA_WIDTH + SAO_CHROMA_WIDTH + NUM_REF_IDX_L0_MINUS1_WIDTH + NUM_REF_IDX_L1_MINUS1_WIDTH + MAX_MERGE_CAND_WIDTH  ];   
            
    assign fifo_in_used_flag_wire = fifo_in[     HEADER_WIDTH + RPS_ID_WIDTH + RPS_ENTRY_USED_WIDTH- 1];
`else 
   
    assign short_term_ref_pic_sps_wire = fifo_in[   FIFO_IN_WIDTH - HEADER_WIDTH - 1:

                                                    FIFO_IN_WIDTH - HEADER_WIDTH - SHORT_TERM_REF_PIC_SPS_WIDTH] ;

    assign short_term_ref_pic_idx_wire = fifo_in[   FIFO_IN_WIDTH - HEADER_WIDTH - SHORT_TERM_REF_PIC_SPS_WIDTH - 1:

                                                    FIFO_IN_WIDTH - HEADER_WIDTH - SHORT_TERM_REF_PIC_SPS_WIDTH - SHORT_TERM_REF_PIC_IDX_WIDTH];                                                                            

    assign slice_type_wire =                        fifo_in[ FIFO_IN_WIDTH - HEADER_WIDTH - SHORT_TERM_REF_PIC_SPS_WIDTH - SHORT_TERM_REF_PIC_IDX_WIDTH - TEMPORAL_MVP_ENABLED_WIDTH - SAO_LUMA_WIDTH - SAO_CHROMA_WIDTH - NUM_REF_IDX_L0_MINUS1_WIDTH - NUM_REF_IDX_L1_MINUS1_WIDTH - MAX_MERGE_CAND_WIDTH -1:
                                                             FIFO_IN_WIDTH - HEADER_WIDTH - SHORT_TERM_REF_PIC_SPS_WIDTH - SHORT_TERM_REF_PIC_IDX_WIDTH - TEMPORAL_MVP_ENABLED_WIDTH - SAO_LUMA_WIDTH - SAO_CHROMA_WIDTH - NUM_REF_IDX_L0_MINUS1_WIDTH - NUM_REF_IDX_L1_MINUS1_WIDTH - MAX_MERGE_CAND_WIDTH - SLICE_TYPE_WIDTH];   
    assign fifo_in_used_flag_wire = fifo_in[    FIFO_IN_WIDTH - HEADER_WIDTH - RPS_ID_WIDTH - RPS_ENTRY_USED_WIDTH];
`endif
    a_sync_ram  #(
        .DATA_WIDTH(REF_POC_LIST5_DATA_WIDTH),
        .ADDR_WIDTH(REF_POC_LIST5_ADDR_WIDTH),
        .DATA_DEPTH((1<<REF_POC_LIST5_ADDR_WIDTH))
    ) ref_poc_list5_block
    (
        .clk(clk), 
        .addr_in(ref_poc_list5_addr), 
        .r_data_out(ref_poc_list5_data_out), 
        .w_data_in(ref_poc_list5_data_in), 
        .w_en_in(ref_poc_list5_wr_en), 
        .enable(enable)
    );
        
    a_sync_ram  #(
        .DATA_WIDTH(DPB_DATA_WIDTH),
        .ADDR_WIDTH(DPB_ADDR_WIDTH),
        .DATA_DEPTH((1<<DPB_ADDR_WIDTH))
    ) dpb_block
    (
        .clk(clk), 
        .addr_in(dpb_addr), 
        .r_data_out(dpb_data_out), 
        .w_data_in(dpb_data_in), 
        .w_en_in(dpb_wr_en), 
        .enable(enable)
    );

    s_sync_ram  #(
        .DATA_WIDTH(RPS_HEADER_DATA_WIDTH),
        .ADDR_WIDTH(RPS_HEADER_ADDR_WIDTH),
        .DATA_DEPTH(1<<RPS_HEADER_ADDR_WIDTH)
    ) rps_header_block
    (
        .clk(clk), 
        .addr_in(rps_header_addr), 
        .r_data_out(rps_header_data_out), 
        .w_data_in(rps_header_data_in), 
        .w_en_in(rps_header_wr_en), 
        .enable(enable)
    );    

    s_sync_ram  #(
        .DATA_WIDTH(RPS_ENTRY_DATA_WIDTH),
        .ADDR_WIDTH(RPS_ENTRY_ADDR_WIDTH),
        .DATA_DEPTH((1<<RPS_HEADER_ADDR_WIDTH) * (1<<NUM_NEG_POS_POC_WIDTH))
    ) rps_entry_block
    (
        .clk(clk), 
        .addr_in(rps_entry_addr), 
        .r_data_out(rps_entry_data_out), 
        .w_data_in(rps_entry_data_in), 
        .w_en_in(rps_entry_wr_en), 
        .enable(enable)
    );


    s_sync_ram  #(
        .DATA_WIDTH(RPS_ENTRY_DATA_WIDTH),
        .ADDR_WIDTH(NUM_NEG_POS_POC_WIDTH),
        .DATA_DEPTH((1<<NUM_NEG_POS_POC_WIDTH))
    ) st_rps_entry_block
    (
        .clk(clk), 
        .addr_in(st_rps_entry_addr), 
        .r_data_out(st_rps_entry_data_out), 
        .w_data_in(st_rps_entry_data_in), 
        .w_en_in(st_rps_entry_wr_en), 
        .enable(enable)
    );


   assign tu_wr_en = (c_idx_wire ==0) && (pred_mode == `MODE_INTER) && (state == STATE_RU_PACKET_READ);
   assign tu_rd_en = pred_pixl_gen_idle_out_to_pred_top ;


       fifo tu_fifo (
        .clk(clk), 
        .reset(reset), 
        .wr_en(tu_wr_en), 
        .rd_en(tu_rd_en), 
        .d_in(fifo_in), 
        .d_out(tu_data_out), 
        .empty(tu_empty), 
        .full()
        );


   
   
    
 
    // Instantiate the module
    inter_pred_top inter_top_block (
        .clk(clk), 
        .reset(reset), 
        .enable(enable),
        .tu_empty(tu_empty),
        .tu_data_in(tu_data_out),
        .config_mode_in(inter_config_mode_in), 
        .config_bus_in(inter_config_bus_in), 
        .from_top_ref_pic_list_poc_data_in(ref_pic_list_poc_data_in),
        .from_top_ref_pic_list0_poc_wr_en(ref_pic_list0_poc_wr_en),
        .from_top_ref_pic_list1_poc_wr_en(ref_pic_list1_poc_wr_en),
        .from_top_ref_pic_list0_poc_addr(ref_pic_list0_poc_addr),
        .from_top_ref_pic_list1_poc_addr(ref_pic_list1_poc_addr),
        .from_top_ref_pic_list_idx_data_in(ref_pic_list_idx_data_in),
        .from_top_ref_pic_list_idx_wr_en(ref_pic_list_idx_wr_en),
        .mv_done_out(mv_done),
        .pred_pixl_gen_idle_out_to_pred_top(pred_pixl_gen_idle_out_to_pred_top),
        .pref_ctu_col_writ_ctu_done_out(pref_ctu_col_writ_ctu_done_out), 
        
        .xT_in_4x4_luma_out(inter_y_predsample_4by4_x_int), 
        .yT_in_4x4_luma_out(inter_y_predsample_4by4_y_int),                                  
        .xT_in_4x4_cb_out(inter_cb_predsample_4by4_x_int),                       
        .yT_in_4x4_cb_out(inter_cb_predsample_4by4_y_int),                       
        .xT_in_4x4_cr_out(inter_cr_predsample_4by4_x_int),                       
        .yT_in_4x4_cr_out(inter_cr_predsample_4by4_y_int),                       
        .luma_wgt_pred_out(inter_y_predsample_4by4_int),                         
        .cb_wgt_pred_out(inter_cb_predsample_4by4_int),                          
        .cr_wgt_pred_out(inter_cr_predsample_4by4_int),                          
        .luma_wght_pred_valid(inter_y_predsample_4by4_valid_int),                
        .cb_wght_pred_valid(inter_cb_predsample_4by4_valid_int),                 
        .cr_wght_pred_valid(inter_cr_predsample_4by4_valid_int),                 
        
        .ref_pix_axi_ar_addr(ref_pix_axi_ar_addr), 
        .ref_pix_axi_ar_len(ref_pix_axi_ar_len), 
        .ref_pix_axi_ar_size(ref_pix_axi_ar_size), 
        .ref_pix_axi_ar_burst(ref_pix_axi_ar_burst), 
        .ref_pix_axi_ar_prot(ref_pix_axi_ar_prot), 
        .ref_pix_axi_ar_valid(ref_pix_axi_ar_valid), 
        .ref_pix_axi_ar_ready(ref_pix_axi_ar_ready), 
        .ref_pix_axi_r_data(ref_pix_axi_r_data), 
        .ref_pix_axi_r_resp(ref_pix_axi_r_resp), 
        .ref_pix_axi_r_last(ref_pix_axi_r_last), 
        .ref_pix_axi_r_valid(ref_pix_axi_r_valid), 
        .ref_pix_axi_r_ready(ref_pix_axi_r_ready),
        
        .mv_col_axi_awid(mv_col_axi_awid), 
        .mv_col_axi_awlen(mv_col_axi_awlen), 
        .mv_col_axi_awsize(mv_col_axi_awsize), 
        .mv_col_axi_awburst(mv_col_axi_awburst), 
        .mv_col_axi_awlock(mv_col_axi_awlock), 
        .mv_col_axi_awcache(mv_col_axi_awcache), 
        .mv_col_axi_awprot(mv_col_axi_awprot), 
        .mv_col_axi_awvalid(mv_col_axi_awvalid), 
        .mv_col_axi_awaddr(mv_col_axi_awaddr), 
        .mv_col_axi_awready(mv_col_axi_awready), 
        .mv_col_axi_wstrb(mv_col_axi_wstrb), 
        .mv_col_axi_wlast(mv_col_axi_wlast), 
        .mv_col_axi_wvalid(mv_col_axi_wvalid), 
        .mv_col_axi_wdata(mv_col_axi_wdata), 
        .mv_col_axi_wready(mv_col_axi_wready), 
        .mv_col_axi_bresp(mv_col_axi_bresp), 
        .mv_col_axi_bvalid(mv_col_axi_bvalid), 
        .mv_col_axi_bready(mv_col_axi_bready), 
        .mv_pref_axi_araddr(mv_pref_axi_araddr), 
        .mv_pref_axi_arlen(mv_pref_axi_arlen), 
        .mv_pref_axi_arsize(mv_pref_axi_arsize), 
        .mv_pref_axi_arburst(mv_pref_axi_arburst), 
        .mv_pref_axi_arprot(mv_pref_axi_arprot), 
        .mv_pref_axi_arvalid(mv_pref_axi_arvalid), 
        .mv_pref_axi_arready(mv_pref_axi_arready), 
        .mv_pref_axi_rdata(mv_pref_axi_rdata), 
        .mv_pref_axi_rresp(mv_pref_axi_rresp), 
        .mv_pref_axi_rlast(mv_pref_axi_rlast), 
        .mv_pref_axi_rvalid(mv_pref_axi_rvalid), 
        .mv_pref_axi_rready(mv_pref_axi_rready), 
        .mv_pref_axi_arlock(mv_pref_axi_arlock), 
        .mv_pref_axi_arid(mv_pref_axi_arid), 
        .mv_pref_axi_arcache(mv_pref_axi_arcache)
        );




    assign intra_enable = (~y_dbf_fifo_is_full)&(~cb_dbf_fifo_is_full)&(~cr_dbf_fifo_is_full)&(~y_residual_fifo_is_empty_in)&(~cb_residual_fifo_is_empty_in)&(~cr_residual_fifo_is_empty_in);
 

/* // --------------------start of intra comment 
    // intra_prediction_wrapper intra_prediction_wrapper_block
    // (
        // .clk                            (clk),
        // .reset                          (reset),
        // .enable                         (intra_enable),
        
        // .config_data_bus_in             (intra_config_data_bus_reg),
        // .config_data_valid_in           (intra_config_data_valid_reg),
        // .residual_fifo_is_empty_in      (residual_fifo_is_empty_in),
        
        // //writeback y_linebuffer interface
        // .y_top_portw_addr_in            (intra_y_top_portw_addr_reg   ),
        // .y_top_portw_data0_in           (intra_y_top_portw_data0_reg  ),
        // .y_top_portw_data1_in           (intra_y_top_portw_data1_reg  ),
        // .y_top_portw_data2_in           (intra_y_top_portw_data2_reg  ),
        // .y_top_portw_data3_in           (intra_y_top_portw_data3_reg  ),
        // .y_top_portw_data4_in           (intra_y_top_portw_data4_reg  ),
        // .y_top_portw_data5_in           (intra_y_top_portw_data5_reg  ),
        // .y_top_portw_data6_in           (intra_y_top_portw_data6_reg  ),
        // .y_top_portw_data7_in           (intra_y_top_portw_data7_reg  ),
        // .y_top_portw_en_mask_in         (intra_y_top_portw_en_mask_reg),
        
        // .y_left_portw_addr_in           (intra_y_left_portw_addr_reg   ),
        // .y_left_portw_data0_in          (intra_y_left_portw_data0_reg  ),
        // .y_left_portw_data1_in          (intra_y_left_portw_data1_reg  ),
        // .y_left_portw_data2_in          (intra_y_left_portw_data2_reg  ),
        // .y_left_portw_data3_in          (intra_y_left_portw_data3_reg  ),
        // .y_left_portw_data4_in          (intra_y_left_portw_data4_reg  ),
        // .y_left_portw_data5_in          (intra_y_left_portw_data5_reg  ),
        // .y_left_portw_data6_in          (intra_y_left_portw_data6_reg  ),
        // .y_left_portw_data7_in          (intra_y_left_portw_data7_reg  ),
        // .y_left_portw_en_mask_in        (intra_y_left_portw_en_mask_reg),

        // .y_predsample_4by4_out          (intra_y_predsample_4by4_int      ),
        // .y_predsample_4by4_valid_out    (intra_y_predsample_4by4_valid_int),
        // .y_predsample_4by4_x_out        (intra_y_predsample_4by4_x_int    ),
        // .y_predsample_4by4_y_out        (intra_y_predsample_4by4_y_int    ),
        // .y_predsample_4by4_last_row_out (intra_y_predsample_4by4_last_row_int),
        // .y_predsample_4by4_last_col_out (intra_y_predsample_4by4_last_col_int),
        // .y_res_present_out              (y_res_present_int),
        // .y_residual_fifo_is_empty_in    (y_residual_fifo_is_empty_in),

        // .y_cu_done_out                  (intra_y_cu_done_int),

        // //writeback cb_linebuffer interface
        // .cb_top_portw_addr_in           (intra_cb_top_portw_addr_reg   ),
        // .cb_top_portw_data0_in          (intra_cb_top_portw_data0_reg  ),
        // .cb_top_portw_data1_in          (intra_cb_top_portw_data1_reg  ),
        // .cb_top_portw_data2_in          (intra_cb_top_portw_data2_reg  ),
        // .cb_top_portw_data3_in          (intra_cb_top_portw_data3_reg  ),
        // .cb_top_portw_data4_in          (intra_cb_top_portw_data4_reg  ),
        // .cb_top_portw_data5_in          (intra_cb_top_portw_data5_reg  ),
        // .cb_top_portw_data6_in          (intra_cb_top_portw_data6_reg  ),
        // .cb_top_portw_data7_in          (intra_cb_top_portw_data7_reg  ),
        // .cb_top_portw_en_mask_in        (intra_cb_top_portw_en_mask_reg),
        
        // .cb_left_portw_addr_in          (intra_cb_left_portw_addr_reg   ),
        // .cb_left_portw_data0_in         (intra_cb_left_portw_data0_reg  ),
        // .cb_left_portw_data1_in         (intra_cb_left_portw_data1_reg  ),
        // .cb_left_portw_data2_in         (intra_cb_left_portw_data2_reg  ),
        // .cb_left_portw_data3_in         (intra_cb_left_portw_data3_reg  ),
        // .cb_left_portw_data4_in         (intra_cb_left_portw_data4_reg  ),
        // .cb_left_portw_data5_in         (intra_cb_left_portw_data5_reg  ),
        // .cb_left_portw_data6_in         (intra_cb_left_portw_data6_reg  ),
        // .cb_left_portw_data7_in         (intra_cb_left_portw_data7_reg  ),
        // .cb_left_portw_en_mask_in       (intra_cb_left_portw_en_mask_reg),

        // .cb_predsample_4by4_out         (intra_cb_predsample_4by4_int      ),
        // .cb_predsample_4by4_valid_out   (intra_cb_predsample_4by4_valid_int),
        // .cb_predsample_4by4_x_out       (intra_cb_predsample_4by4_x_int    ),
        // .cb_predsample_4by4_y_out       (intra_cb_predsample_4by4_y_int    ),
        // .cb_predsample_4by4_last_row_out(intra_cb_predsample_4by4_last_row_int),
        // .cb_predsample_4by4_last_col_out(intra_cb_predsample_4by4_last_col_int),
        // .cb_res_present_out             (cb_res_present_int),
        // .cb_residual_fifo_is_empty_in   (cb_residual_fifo_is_empty_in),

        // .cb_cu_done_out                 (intra_cb_cu_done_int),

        // //writeback cr_linebuffer interface
        // .cr_top_portw_addr_in           (intra_cr_top_portw_addr_reg   ),
        // .cr_top_portw_data0_in          (intra_cr_top_portw_data0_reg  ),
        // .cr_top_portw_data1_in          (intra_cr_top_portw_data1_reg  ),
        // .cr_top_portw_data2_in          (intra_cr_top_portw_data2_reg  ),
        // .cr_top_portw_data3_in          (intra_cr_top_portw_data3_reg  ),
        // .cr_top_portw_data4_in          (intra_cr_top_portw_data4_reg  ),
        // .cr_top_portw_data5_in          (intra_cr_top_portw_data5_reg  ),
        // .cr_top_portw_data6_in          (intra_cr_top_portw_data6_reg  ),
        // .cr_top_portw_data7_in          (intra_cr_top_portw_data7_reg  ),
        // .cr_top_portw_en_mask_in        (intra_cr_top_portw_en_mask_reg),
       
        // .cr_left_portw_addr_in          (intra_cr_left_portw_addr_reg   ),
        // .cr_left_portw_data0_in         (intra_cr_left_portw_data0_reg  ),
        // .cr_left_portw_data1_in         (intra_cr_left_portw_data1_reg  ),
        // .cr_left_portw_data2_in         (intra_cr_left_portw_data2_reg  ),
        // .cr_left_portw_data3_in         (intra_cr_left_portw_data3_reg  ),
        // .cr_left_portw_data4_in         (intra_cr_left_portw_data4_reg  ),
        // .cr_left_portw_data5_in         (intra_cr_left_portw_data5_reg  ),
        // .cr_left_portw_data6_in         (intra_cr_left_portw_data6_reg  ),
        // .cr_left_portw_data7_in         (intra_cr_left_portw_data7_reg  ),
        // .cr_left_portw_en_mask_in       (intra_cr_left_portw_en_mask_reg),
        
        // .cr_predsample_4by4_out         (intra_cr_predsample_4by4_int      ),
        // .cr_predsample_4by4_valid_out   (intra_cr_predsample_4by4_valid_int),
        // .cr_predsample_4by4_x_out       (intra_cr_predsample_4by4_x_int    ),
        // .cr_predsample_4by4_y_out       (intra_cr_predsample_4by4_y_int    ),
        // .cr_predsample_4by4_last_row_out(intra_cr_predsample_4by4_last_row_int),
        // .cr_predsample_4by4_last_col_out(intra_cr_predsample_4by4_last_col_int),
        // .cr_res_present_out             (cr_res_present_int),
        // .cr_residual_fifo_is_empty_in   (cr_residual_fifo_is_empty_in),
        // .cr_cu_done_out                 (intra_cr_cu_done_int)
    // );

    // always @(*) begin
        // if (pred_mode == `MODE_INTRA) begin
            // y_common_predsample_4by4_int            = intra_y_predsample_4by4_int;
            // y_common_predsample_4by4_valid_int      = intra_y_predsample_4by4_valid_int;
            // y_common_predsample_4by4_x_int          = intra_y_predsample_4by4_x_int;
            // y_common_predsample_4by4_y_int          = intra_y_predsample_4by4_y_int;
            // y_common_res_present_int                = intra_y_res_present_int;

            // cb_common_predsample_4by4_int           = intra_cb_predsample_4by4_int;
            // cb_common_predsample_4by4_valid_int     = intra_cb_predsample_4by4_valid_int;
            // cb_common_predsample_4by4_x_int         = intra_cb_predsample_4by4_x_int;
            // cb_common_predsample_4by4_y_int         = intra_cb_predsample_4by4_y_int;
            // cb_common_res_present_int               = intra_cb_res_present_int;

            // cr_common_predsample_4by4_int           = intra_cr_predsample_4by4_int;
            // cr_common_predsample_4by4_valid_int     = intra_cr_predsample_4by4_valid_int;
            // cr_common_predsample_4by4_x_int         = intra_cr_predsample_4by4_x_int;
            // cr_common_predsample_4by4_y_int         = intra_cr_predsample_4by4_y_int;
            // cr_common_res_present_int               = intra_cr_res_present_int;        
        // end
        // else begin
            // y_common_predsample_4by4_int            = inter_y_predsample_4by4_int;
            // y_common_predsample_4by4_valid_int      = inter_y_predsample_4by4_valid_int;
            // y_common_predsample_4by4_x_int          = inter_y_predsample_4by4_x_int;
            // y_common_predsample_4by4_y_int          = inter_y_predsample_4by4_y_int;
            // y_common_res_present_int                = inter_y_res_present_int;

            // cb_common_predsample_4by4_int           = inter_cb_predsample_4by4_int;
            // cb_common_predsample_4by4_valid_int     = inter_cb_predsample_4by4_valid_int;
            // cb_common_predsample_4by4_x_int         = inter_cb_predsample_4by4_x_int;
            // cb_common_predsample_4by4_y_int         = inter_cb_predsample_4by4_y_int;
            // cb_common_res_present_int               = inter_cb_res_present_int;

            // cr_common_predsample_4by4_int           = inter_cr_predsample_4by4_int;
            // cr_common_predsample_4by4_valid_int     = inter_cr_predsample_4by4_valid_int;
            // cr_common_predsample_4by4_x_int         = inter_cr_predsample_4by4_x_int;
            // cr_common_predsample_4by4_y_int         = inter_cr_predsample_4by4_y_int;
            // cr_common_res_present_int               = inter_cr_res_present_int; 
        // end

    // end

    // residual_adder y_residual_adder_block
    // (
      // .clk                              (clk),
      // .reset                            (reset),             

      // .res_present_in                   (y_common_res_present_int),
      // .predsamples_4by4_valid_in        (y_common_predsample_4by4_valid_int),
      // .predsamples_4by4_in              (y_common_predsample_4by4_int),

      // .residual_4by4_in                 (y_residual_fifo_in),
      // .residual_fifo_read_en_out        (y_residual_fifo_read_en_out),
      // .residual_fifo_is_empty_in        (y_residual_fifo_is_empty_in),

      // .ans_4by4_out                     (y_res_added_int)
    // );

    // residual_adder cb_residual_adder_block
    // (
      // .clk                              (clk),
      // .reset                            (reset),             

      // .res_present_in                   (cb_common_res_present_int),
      // .predsamples_4by4_valid_in        (cb_common_predsample_4by4_valid_int),
      // .predsamples_4by4_in              (cb_common_predsample_4by4_int),

      // .residual_4by4_in                 (cb_residual_fifo_in),
      // .residual_fifo_read_en_out        (cb_residual_fifo_read_en_out),
      // .residual_fifo_is_empty_in        (cb_residual_fifo_is_empty_in),

      // .ans_4by4_out                     (cb_res_added_int)
    // );

    // residual_adder cr_residual_adder_block
    // (
      // .clk                              (clk),
      // .reset                            (reset),             

      // .res_present_in                   (cr_common_res_present_int),
      // .predsamples_4by4_valid_in        (cr_common_predsample_4by4_valid_int),
      // .predsamples_4by4_in              (cr_common_predsample_4by4_int),

      // .residual_4by4_in                 (cr_residual_fifo_in),
      // .residual_fifo_read_en_out        (cr_residual_fifo_read_en_out),
      // .residual_fifo_is_empty_in        (cr_residual_fifo_is_empty_in),

      // .ans_4by4_out                     (cr_res_added_int)
    // );    
    // generate
        // genvar i;
        // genvar j;
        
        // for (i = 0 ; i < OUTPUT_BLOCK_SIZE ; i = i + 1) begin : row_iteration
            // for (j = 0 ; j < OUTPUT_BLOCK_SIZE ; j = j + 1) begin : column_iteration
                // assign y_res_added_int_arr[i][j] = y_res_added_int[i*PIXEL_WIDTH*OUTPUT_BLOCK_SIZE +(j+1)*PIXEL_WIDTH - 1:j*PIXEL_WIDTH + i*PIXEL_WIDTH*OUTPUT_BLOCK_SIZE];
                // assign cb_res_added_int_arr[i][j] = cb_res_added_int[i*PIXEL_WIDTH*OUTPUT_BLOCK_SIZE +(j+1)*PIXEL_WIDTH - 1:j*PIXEL_WIDTH + i*PIXEL_WIDTH*OUTPUT_BLOCK_SIZE];
                // assign cr_res_added_int_arr[i][j] = cr_res_added_int[i*PIXEL_WIDTH*OUTPUT_BLOCK_SIZE +(j+1)*PIXEL_WIDTH - 1:j*PIXEL_WIDTH + i*PIXEL_WIDTH*OUTPUT_BLOCK_SIZE];
            // end
        // end
    // endgenerate

    // always @(posedge clk) begin
        // y_valid_addition_reg <= (~y_residual_fifo_is_empty_in)&y_common_predsample_4by4_valid_int;
        // cb_valid_addition_reg <= (~cb_residual_fifo_is_empty_in)&cb_common_predsample_4by4_valid_int;
        // cr_valid_addition_reg <= (~cr_residual_fifo_is_empty_in)&cr_common_predsample_4by4_valid_int;

        // intra_y_predsample_4by4_last_row_reg <= intra_y_predsample_4by4_last_row_int;
        // intra_y_predsample_4by4_last_col_reg <= intra_y_predsample_4by4_last_col_int;

        // intra_cb_predsample_4by4_last_row_reg <= intra_cb_predsample_4by4_last_row_int;
        // intra_cb_predsample_4by4_last_col_reg <= intra_cb_predsample_4by4_last_col_int;

        // intra_cr_predsample_4by4_last_row_reg <= intra_cr_predsample_4by4_last_row_int;
        // intra_cr_predsample_4by4_last_col_reg <= intra_cr_predsample_4by4_last_col_int;

        // intra_y_predsample_4by4_x_reg         <= intra_y_predsample_4by4_x_int;
        // intra_y_predsample_4by4_y_reg         <= intra_y_predsample_4by4_y_int;

        // intra_cb_predsample_4by4_x_reg         <= intra_cb_predsample_4by4_x_int;
        // intra_cb_predsample_4by4_y_reg         <= intra_cb_predsample_4by4_y_int;

        // intra_cr_predsample_4by4_x_reg         <= intra_cr_predsample_4by4_x_int;
        // intra_cr_predsample_4by4_y_reg         <= intra_cr_predsample_4by4_y_int;
    // end

    // always @(posedge clk) begin
        // if (reset) begin
            // intra_y_left_portw_en_mask_reg <= 8'b00000000;      
        // end
        // else begin
            // if(y_valid_addition_reg) begin
                // if(intra_y_predsample_4by4_last_row_reg) begin
                    // intra_y_top_portw_addr_reg      <= intra_y_predsample_4by4_x_reg;
                    // intra_y_top_portw_data0_reg     <= y_res_added_int_arr[3][0];
                    // intra_y_top_portw_data1_reg     <= y_res_added_int_arr[3][1];
                    // intra_y_top_portw_data2_reg     <= y_res_added_int_arr[3][2];
                    // intra_y_top_portw_data3_reg     <= y_res_added_int_arr[3][3];  
                    // intra_y_top_portw_en_mask_reg   <= 8'b00001111;  
                // end

                // if(intra_y_predsample_4by4_last_col_reg) begin
                    // intra_y_left_portw_addr_reg      <= intra_y_predsample_4by4_y_reg;
                    // intra_y_left_portw_data0_reg     <= y_res_added_int_arr[0][3];
                    // intra_y_left_portw_data1_reg     <= y_res_added_int_arr[1][3];
                    // intra_y_left_portw_data2_reg     <= y_res_added_int_arr[2][3];
                    // intra_y_left_portw_data3_reg     <= y_res_added_int_arr[3][3];  
                    // intra_y_left_portw_en_mask_reg   <= 8'b00001111;  
                // end
            // end
        // end
    // end

    // always @(posedge clk) begin
        // if (reset) begin
            // intra_cb_left_portw_en_mask_reg <= 8'b00000000;      
        // end
        // else begin
            // if(cb_valid_addition_reg) begin
                // if(intra_cb_predsample_4by4_last_row_reg) begin
                    // intra_cb_top_portw_addr_reg      <= intra_cb_predsample_4by4_x_reg;
                    // intra_cb_top_portw_data0_reg     <= cb_res_added_int_arr[3][0];
                    // intra_cb_top_portw_data1_reg     <= cb_res_added_int_arr[3][1];
                    // intra_cb_top_portw_data2_reg     <= cb_res_added_int_arr[3][2];
                    // intra_cb_top_portw_data3_reg     <= cb_res_added_int_arr[3][3];  
                    // intra_cb_top_portw_en_mask_reg   <= 8'b00001111;  
                // end

                // if(intra_cb_predsample_4by4_last_col_reg) begin
                    // intra_cb_left_portw_addr_reg      <= intra_cb_predsample_4by4_y_reg;
                    // intra_cb_left_portw_data0_reg     <= cb_res_added_int_arr[0][3];
                    // intra_cb_left_portw_data1_reg     <= cb_res_added_int_arr[1][3];
                    // intra_cb_left_portw_data2_reg     <= cb_res_added_int_arr[2][3];
                    // intra_cb_left_portw_data3_reg     <= cb_res_added_int_arr[3][3];  
                    // intra_cb_left_portw_en_mask_reg   <= 8'b00001111;  
                // end
            // end
        // end
    // end

    // always @(posedge clk) begin
        // if (reset) begin
            // intra_cr_left_portw_en_mask_reg <= 8'b00000000;      
        // end
        // else begin
            // if(cr_valid_addition_reg) begin
                // if(intra_cr_predsample_4by4_last_row_reg) begin
                    // intra_cr_top_portw_addr_reg      <= intra_cr_predsample_4by4_x_reg;
                    // intra_cr_top_portw_data0_reg     <= cr_res_added_int_arr[3][0];
                    // intra_cr_top_portw_data1_reg     <= cr_res_added_int_arr[3][1];
                    // intra_cr_top_portw_data2_reg     <= cr_res_added_int_arr[3][2];
                    // intra_cr_top_portw_data3_reg     <= cr_res_added_int_arr[3][3];  
                    // intra_cr_top_portw_en_mask_reg   <= 8'b00001111;  
                // end

                // if(intra_cr_predsample_4by4_last_col_reg) begin
                    // intra_cr_left_portw_addr_reg      <= intra_cr_predsample_4by4_y_reg;
                    // intra_cr_left_portw_data0_reg     <= cr_res_added_int_arr[0][3];
                    // intra_cr_left_portw_data1_reg     <= cr_res_added_int_arr[1][3];
                    // intra_cr_left_portw_data2_reg     <= cr_res_added_int_arr[2][3];
                    // intra_cr_left_portw_data3_reg     <= cr_res_added_int_arr[3][3];  
                    // intra_cr_left_portw_en_mask_reg   <= 8'b00001111;  
                // end
            // end
        // end
    // end

//--------------------------------end of intra comment


    // always @(posedge clk) begin : residual_adder_16by16
    //     integer i;
    //     integer j;
    //     if (reset) begin
            
            
    //     end
    //     else begin
    //         case(pred_mode)
    //             `PREDMODE_INTRA : begin
    //                 if(intra_y_predsample_4by4_valid_int) begin
    //                     for(i = 0 ; i < OUTPUT_BLOCK_SIZE ; i = i + 1) begin
    //                         for (j = 0 ; j < OUTPUT_BLOCK_SIZE ; j = j + 1) begin
                                
    //                         end
    //                     end  
    //                 end
                    
    //             end
    //             `PREDMODE_INTER : begin
                    
    //             end
    //         endcase
    //     end
    // end
     */
    
    
    
    // always@(posedge clk) begin
        // num_pic_ref_pic_5_total <= num_pic_st_curr_aft + num_pic_st_curr_bef + num_pic_st_fol;
    // end
    
    always@(posedge clk) begin : write_ref_fsm
        if(reset ) begin
            write_ref_pic_state <= WRITE_REF_PIC_STATE_IDLE;
            ref_pic_list0_poc_wr_en <= 0;
            ref_pic_list1_poc_wr_en <= 0;
            ref_pic_list_idx_wr_en <= 0;
            dpb_filled_flag_new <= 0;
        end
        else begin
            case(write_ref_pic_state)
                WRITE_REF_PIC_STATE_IDLE: begin
                    ref_pic_list0_poc_wr_en <= 0;
                    ref_pic_list1_poc_wr_en <= 0;
                    ref_pic_list_idx_wr_en <= 0;
                    if(state == STATE_REF_PIC_LIST_TRANSFER) begin
                        ref_poc_list5_addr_read <= 0;
                        write_ref_pic_state <= WRITE_REF_PIC_STATE_CHECK;
                        list0_poc_addr_temp <= 0;
                        list1_poc_addr_temp <= 0;
                        dpb_filled_flag_new <= 0;
                    end                   
                end
                WRITE_REF_PIC_STATE_CHECK: begin
                    ref_pic_list_idx_wr_en  <= 0;
                    ref_pic_list0_poc_wr_en <= 0;
                    ref_pic_list1_poc_wr_en <= 0;
                    if(ref_poc_list5_addr_read == num_delta_poc) begin  // same as num_pic_ref_pic_5_total but clock cycle before
                        write_ref_pic_state <= WRITE_REF_PIC_STATE_DONE;
                    end
                    else if(ref_poc_list5_data_out[REF_PIC_LIST5_DPB_STATE_HIGH:REF_PIC_LIST5_DPB_STATE_LOW] == `ST_CURR_BEF || ref_poc_list5_data_out[REF_PIC_LIST5_DPB_STATE_HIGH:REF_PIC_LIST5_DPB_STATE_LOW] == `ST_CURR_AFT) begin
                        write_ref_pic_state <= WRITE_REF_PIC_STATE_POC_WRITE;
                        temp_poc <= current__poc + {{(POC_WIDTH - DELTA_POC_WIDTH){ref_poc_list5_data_out[REF_PIC_LIST5_POC_RANGE_HIGH]}},ref_poc_list5_data_out[REF_PIC_LIST5_POC_RANGE_HIGH:REF_PIC_LIST5_POC_RANGE_LOW]};
                        // if(ref_poc_list5_data_out[REF_PIC_LIST5_DPB_STATE_HIGH:REF_PIC_LIST5_DPB_STATE_LOW] == `ST_CURR_BEF) begin
                            // list0_poc_addr_temp <= (list0_poc_addr_temp + 1)%(1<<NUM_REF_IDX_L0_MINUS1_WIDTH);
                        // end
                        // else begin
                            // list1_poc_addr_temp <= (list1_poc_addr_temp + 1)%(1<<NUM_REF_IDX_L0_MINUS1_WIDTH);
                        // end
                    end
                    else if(ref_poc_list5_data_out[REF_PIC_LIST5_DPB_STATE_HIGH:REF_PIC_LIST5_DPB_STATE_LOW] == `ST_FOLL) begin
                        ref_poc_list5_addr_read <= (ref_poc_list5_addr_read + 1)%(1<<REF_POC_LIST5_ADDR_WIDTH);
                        write_ref_pic_state <= WRITE_REF_PIC_FIND_FOL_IDX;
                        dpb_addr_read <= 0;
                    end
                    else begin
                        ref_poc_list5_addr_read <= (ref_poc_list5_addr_read + 1)%(1<<REF_POC_LIST5_ADDR_WIDTH);
                    end
                end
                WRITE_REF_PIC_STATE_POC_WRITE: begin
                    if(list0_poc_addr_temp <= num_rps_curr_l0) begin
                        ref_pic_list0_poc_wr_en <= 1;
                    end
                    if(list1_poc_addr_temp <= num_rps_curr_l1) begin
                        ref_pic_list1_poc_wr_en <= 1;
                    end
                    ref_pic_list_poc_data_in <= temp_poc;
                    if(ref_poc_list5_data_out[REF_PIC_LIST5_DPB_STATE_HIGH:REF_PIC_LIST5_DPB_STATE_LOW] == `ST_CURR_BEF) begin
                        // if(list0_poc_addr_temp <= num_rps_curr_l0) begin
                            // ref_pic_list0_poc_wr_en <= 1;
                        // end
                        // if(list0_poc_addr_temp + num_pic_st_curr_aft < num_rps_curr_l1) begin
                            // ref_pic_list1_poc_wr_en <= 1;
                        // end
                        ref_pic_list0_poc_addr <= list0_poc_addr_temp;
                        ref_pic_list1_poc_addr <= list0_poc_addr_temp + num_pic_st_curr_aft;
                        list0_poc_addr_temp <= (list0_poc_addr_temp + 1)%(1<<NUM_REF_IDX_L0_MINUS1_WIDTH);
                    end
                    else begin
                        // if(list1_poc_addr_temp <= num_rps_curr_l1) begin
                            // ref_pic_list1_poc_wr_en <= 1;
                        // end
                        // if(list1_poc_addr_temp + num_pic_st_curr_bef < num_rps_curr_l0) begin
                            // ref_pic_list0_poc_wr_en <= 1;
                        // end
                        ref_pic_list0_poc_addr <= (list1_poc_addr_temp + num_pic_st_curr_bef)%(1<<NUM_REF_IDX_L0_MINUS1_WIDTH);
                        ref_pic_list1_poc_addr <= list1_poc_addr_temp;
                        list1_poc_addr_temp <= (list1_poc_addr_temp + 1)%(1<<NUM_REF_IDX_L0_MINUS1_WIDTH);
                    end 
                    ref_poc_list5_addr_read <= (ref_poc_list5_addr_read + 1)%(1<<REF_POC_LIST5_ADDR_WIDTH);
                    write_ref_pic_state <= WRITE_REF_PIC_STATE_IDX_WRITE;
                    dpb_addr_read <= 0;
                end
                WRITE_REF_PIC_STATE_IDX_WRITE: begin
                    ref_pic_list0_poc_wr_en <= 0;
                    ref_pic_list1_poc_wr_en <= 0;
                    if((dpb_data_out == temp_poc && dpb_filled_flag[dpb_addr_read] == 1) || (dpb_addr_read == {DPB_ADDR_WIDTH{1'b1}})) begin
                        ref_pic_list_idx_wr_en <= 1;
                        ref_pic_list_idx_data_in <= dpb_addr_read;
                        write_ref_pic_state <= WRITE_REF_PIC_STATE_CHECK;
                    end
                    if(dpb_data_out == temp_poc && dpb_filled_flag[dpb_addr_read] == 1) begin
                        dpb_filled_flag_new[dpb_addr_read] <= 1;
                    end
                    dpb_addr_read <= (dpb_addr_read + 1)%(1<<DPB_ADDR_WIDTH);
                end
                WRITE_REF_PIC_FIND_FOL_IDX: begin
                    if((dpb_data_out == temp_poc && dpb_filled_flag[dpb_addr_read] == 1) || (dpb_addr_read == {DPB_ADDR_WIDTH{1'b1}})) begin
                        write_ref_pic_state <= WRITE_REF_PIC_STATE_CHECK;
                    end
                    if(dpb_data_out == temp_poc && dpb_filled_flag[dpb_addr_read] == 1) begin
                        dpb_filled_flag_new[dpb_addr_read] <= 1;
                    end
                    dpb_addr_read <= (dpb_addr_read + 1)%(1<<DPB_ADDR_WIDTH);
                end
                WRITE_REF_PIC_STATE_DONE: begin
                    write_ref_pic_state <= WRITE_REF_PIC_STATE_IDLE;
                end
                default: begin
                    write_ref_pic_state <= WRITE_REF_PIC_STATE_IDLE;
                end
            endcase
        end

    end
    
    always@(*) begin
        if(pred_mode == `MODE_INTRA) begin
            is_pu_header_cus_last <= 1;
        end
        else begin
            case(cb_part_mode)
                `PART_2Nx2N: begin
                    if(pb_part_idx == 1) begin
                        is_pu_header_cus_last <= 1;
                    end
                    else begin
                        is_pu_header_cus_last <= 0;
                    end
                end
                `PART_2NxN,`PART_Nx2N,`PART_nLx2N,`PART_nRx2N,`PART_2NxnD,`PART_2NxnU: begin
                    if(pb_part_idx == 2) begin
                        is_pu_header_cus_last <= 1;
                    end
                    else begin
                        is_pu_header_cus_last <= 0;
                    end
                end
                `PART_NxN: begin
                    if(pb_part_idx == 4) begin
                        is_pu_header_cus_last <= 1;
                    end
                    else begin
                        is_pu_header_cus_last <= 0;
                    end
                end
                default: begin
                    is_pu_header_cus_last <= 0;
                end
            endcase
        end
        
    end

`ifdef READ_FILE

    assign log2_cb_size_wire = fifo_in[     HEADER_WIDTH + 2*X0_WIDTH + LOG2_CTB_WIDTH - 1:
                                            HEADER_WIDTH + 2*X0_WIDTH];
`else
    assign log2_cb_size_wire = fifo_in[     FIFO_IN_WIDTH - HEADER_WIDTH - 2*X0_WIDTH - 1: 
                                            FIFO_IN_WIDTH - HEADER_WIDTH - 2*X0_WIDTH - LOG2_CTB_WIDTH];
`endif

`ifdef READ_FILE
    assign x0_tu_in_ctu_wire = fifo_in[   HEADER_WIDTH + XT_WIDTH -1:
                                            HEADER_WIDTH]                               ;
`else
    assign x0_tu_in_ctu_wire = fifo_in[   FIFO_IN_WIDTH - HEADER_WIDTH - 1: 
                                            FIFO_IN_WIDTH - HEADER_WIDTH - XT_WIDTH]    ;
`endif

`ifdef READ_FILE
    assign y0_tu_in_ctu_wire = fifo_in[   HEADER_WIDTH + XT_WIDTH + YT_WIDTH -1:
                                            HEADER_WIDTH + YT_WIDTH]                            ;
`else
    assign y0_tu_in_ctu_wire = fifo_in[   FIFO_IN_WIDTH - HEADER_WIDTH - XT_WIDTH- 1: 
                                            FIFO_IN_WIDTH - HEADER_WIDTH - XT_WIDTH - YT_WIDTH] ;
`endif

`ifdef READ_FILE
    assign log2_tb_size_wire = fifo_in[     HEADER_WIDTH + XT_WIDTH + YT_WIDTH + CIDX_WIDTH + LOG2_CB_SIZE_WIDTH -1:
                                            HEADER_WIDTH + XT_WIDTH + YT_WIDTH + CIDX_WIDTH];
`else
    assign log2_tb_size_wire = fifo_in[     FIFO_IN_WIDTH - HEADER_WIDTH - XT_WIDTH - YT_WIDTH - CIDX_WIDTH - 1: 
                                            FIFO_IN_WIDTH - HEADER_WIDTH - XT_WIDTH - YT_WIDTH - CIDX_WIDTH - LOG2_CB_SIZE_WIDTH];
`endif

`ifdef READ_FILE
    assign c_idx_wire = fifo_in[    HEADER_WIDTH + XT_WIDTH + YT_WIDTH + CIDX_WIDTH-1:
                                    HEADER_WIDTH + XT_WIDTH + YT_WIDTH];
`else
    assign c_idx_wire = fifo_in[    FIFO_IN_WIDTH - HEADER_WIDTH - XT_WIDTH - YT_WIDTH - 1: 
                                    FIFO_IN_WIDTH - HEADER_WIDTH - XT_WIDTH - YT_WIDTH - CIDX_WIDTH];
`endif

// assume that last cidx = 0 ru_0 packet is followed by only one cidx = 2 ru_o packet
    
    
    always@(*) begin
        case(log2_cb_size_wire)
            3: begin
                cb_size_wire = 8;
            end
            4: begin
                cb_size_wire = 16;  
            end
            5: begin
                cb_size_wire = 32;
            end
            6: begin
                cb_size_wire = 64;
            end
            default: begin
                cb_size_wire = 64;
            end
        endcase
    end
    
    always@(*) begin
        case(log2_tb_size_wire)
            2: begin
                tb_size_wire = 4;
            end
            3: begin
                tb_size_wire = 8;
            end
            4: begin
                tb_size_wire = 16;  
            end
            5: begin
                tb_size_wire = 32;
            end
            default: begin
                tb_size_wire = 32;
            end
        endcase
    end
    
    always@(posedge clk) begin
        if((x0_tu_end_in_ctu == cb_xx_end) && (y0_tu_end_in_ctu == cb_yy_end)) begin
            last_tu_of_cu <= 1;
        end
        else begin
            last_tu_of_cu <= 0;
        end
    end
    
    always@(posedge clk) begin
        cb_xx_end <= cb_xx + cb_size;
        cb_yy_end <= cb_yy + cb_size;
        x0_tu_end_in_ctu <= (x0_tu_in_ctu << 1) + tb_size;
        y0_tu_end_in_ctu <= (y0_tu_in_ctu << 1) + tb_size;

    end

    always@(*) begin    : next_state_logic
        next_state = state;
        rps_header_wr_en = 1'b0;
        rps_entry_wr_en = 1'b0;
        st_rps_entry_wr_en = 1'b0;
        rps_header_addr = {RPS_HEADER_ADDR_WIDTH{1'bx}};
        rps_header_data_in = {RPS_HEADER_DATA_WIDTH{1'bx}};
        
        rps_entry_addr = {RPS_ENTRY_ADDR_WIDTH{1'bx}};
        rps_entry_data_in = {RPS_ENTRY_DATA_WIDTH{1'bx}};
        
        st_rps_entry_data_in = {RPS_ENTRY_DATA_WIDTH{1'bx}};
        st_rps_entry_addr = {NUM_NEG_POS_POC_WIDTH{1'bx}};
        
        ref_poc_list5_addr = {REF_POC_LIST5_ADDR_WIDTH{1'bx}};
        ref_poc_list5_data_in = {REF_POC_LIST5_DATA_WIDTH{1'bx}};
        ref_poc_list5_wr_en = 0;
        
        dpb_data_in = {DPB_DATA_WIDTH{1'bx}};
        dpb_addr = {DPB_ADDR_WIDTH{1'bx}};
        dpb_wr_en = 0;
                
        read_en_out = 1'b0;
        case(state) 
            STATE_READ_WAIT: begin
                if(input_fifo_is_empty == 0) begin
                    next_state = STATE_ACTIVE;
                end
            end
            STATE_POC_READ_WAIT: begin
                if(input_fifo_is_empty == 0) begin
                    next_state = STATE_CURRENT_POC;
                end
            end    
            STATE_MVD1_READ_WAIT: begin
                if(input_fifo_is_empty == 0) begin
                    next_state = STATE_MVD1_READ;
                end
            end      
            STATE_MVD2_READ_WAIT: begin
                if(input_fifo_is_empty == 0) begin
                    next_state = STATE_MVD2_READ;
                end
            end  
            STATE_RU_PACKET_READ_WAIT: begin
                if(input_fifo_is_empty == 0) begin
                    next_state = STATE_RU_DONE_WAIT1;
                end
            end  
                    
            STATE_ACTIVE: begin
`ifdef READ_FILE
                case(fifo_in[HEADER_WIDTH- 1:0])
`else
                case(fifo_in[FIFO_IN_WIDTH-1:FIFO_IN_WIDTH - HEADER_WIDTH])
`endif
                    `HEADER_PARAMETERS_0: begin
                        if(output_fifo_is_full == 1) begin
                            next_state = STATE_WRITE_WAIT;
                        end
                        else if(input_fifo_is_empty == 1) begin
                            next_state = STATE_READ_WAIT;
                        end
                        read_en_out = 1;
                    end
                    `HEADER_PARAMETERS_1: begin
                        if(output_fifo_is_full == 1) begin
                            next_state = STATE_WRITE_WAIT;
                        end
                        else if(input_fifo_is_empty == 1) begin
                            next_state = STATE_READ_WAIT;
                        end 
                        read_en_out = 1;
                    end
                    `HEADER_PARAMETERS_2: begin
                        if(input_fifo_is_empty == 1) begin
                            next_state = STATE_READ_WAIT;
                        end  
                        read_en_out = 1;
                    end
                    `HEADER_PARAMETERS_3: begin
                        rps_header_wr_en = 1'b1;
                        read_en_out = 1;
`ifdef READ_FILE
                        rps_header_addr = (fifo_in[   HEADER_WIDTH + RPS_HEADER_ID_WIDTH- 1: HEADER_WIDTH   ]) % (1<<RPS_HEADER_ADDR_WIDTH);
                        rps_header_data_in[RPS_HEADER_NUM_POS_POC_RANGE_HIGH:RPS_HEADER_NUM_POS_POC_RANGE_LOW] =    ((fifo_in[   HEADER_WIDTH + RPS_HEADER_ID_WIDTH + NUM_POSITIVE_WIDTH- 1:
                                                                                                                                     HEADER_WIDTH + RPS_HEADER_ID_WIDTH  ])%(1<<NUM_NEG_POS_POC_WIDTH));

                        rps_header_data_in[RPS_HEADER_NUM_NEG_POC_RANGE_HIGH:RPS_HEADER_NUM_NEG_POC_RANGE_LOW] =    ((fifo_in[   HEADER_WIDTH + RPS_HEADER_ID_WIDTH + NUM_POSITIVE_WIDTH + NUM_NEGATIVE_WIDTH- 1:
                                                                                                                                     HEADER_WIDTH + RPS_HEADER_ID_WIDTH + NUM_POSITIVE_WIDTH  ])%(1<<NUM_NEG_POS_POC_WIDTH));
                        rps_header_data_in[RPS_HEADER_NUM_DELTA_POC_RANGE_HIGH:RPS_HEADER_NUM_DELTA_POC_RANGE_LOW] = (fifo_in[   HEADER_WIDTH + RPS_HEADER_ID_WIDTH + NUM_POSITIVE_WIDTH- 1:
                                                                                                                                     HEADER_WIDTH + RPS_HEADER_ID_WIDTH  ] +
                                                                                                                          fifo_in[   HEADER_WIDTH + RPS_HEADER_ID_WIDTH + NUM_POSITIVE_WIDTH + NUM_NEGATIVE_WIDTH- 1:


                                                                                                                                     HEADER_WIDTH + RPS_HEADER_ID_WIDTH + NUM_POSITIVE_WIDTH  ])%(1<<NUM_NEG_POS_POC_WIDTH);     
`else
                        rps_header_addr = (fifo_in[ FIFO_IN_WIDTH - HEADER_WIDTH -1: 
                                                    FIFO_IN_WIDTH - HEADER_WIDTH - RPS_HEADER_ID_WIDTH]) % (1<<RPS_HEADER_ADDR_WIDTH);
                        rps_header_data_in[RPS_HEADER_NUM_POS_POC_RANGE_HIGH:RPS_HEADER_NUM_POS_POC_RANGE_LOW] =   ((fifo_in[   FIFO_IN_WIDTH - HEADER_WIDTH - RPS_HEADER_ID_WIDTH - 1:
                                                                                                                                FIFO_IN_WIDTH - HEADER_WIDTH - RPS_HEADER_ID_WIDTH - NUM_POSITIVE_WIDTH])%(1<<NUM_NEG_POS_POC_WIDTH));
                        rps_header_data_in[RPS_HEADER_NUM_NEG_POC_RANGE_HIGH:RPS_HEADER_NUM_NEG_POC_RANGE_LOW] =   ((fifo_in[   FIFO_IN_WIDTH - HEADER_WIDTH - RPS_HEADER_ID_WIDTH - NUM_POSITIVE_WIDTH -1:
                                                                                                                                FIFO_IN_WIDTH - HEADER_WIDTH - RPS_HEADER_ID_WIDTH - NUM_POSITIVE_WIDTH - NUM_NEGATIVE_WIDTH])%(1<<NUM_NEG_POS_POC_WIDTH));
                                                rps_header_data_in[RPS_HEADER_NUM_DELTA_POC_RANGE_HIGH:RPS_HEADER_NUM_DELTA_POC_RANGE_LOW] = (fifo_in[  FIFO_IN_WIDTH - HEADER_WIDTH - RPS_HEADER_ID_WIDTH - 1:
                                                                                                                                FIFO_IN_WIDTH - HEADER_WIDTH - RPS_HEADER_ID_WIDTH - NUM_POSITIVE_WIDTH] +
                                                                                                                     fifo_in[   FIFO_IN_WIDTH - HEADER_WIDTH - RPS_HEADER_ID_WIDTH - NUM_POSITIVE_WIDTH -1:
                                                                                                                                FIFO_IN_WIDTH - HEADER_WIDTH - RPS_HEADER_ID_WIDTH - NUM_POSITIVE_WIDTH - NUM_NEGATIVE_WIDTH])%(1<<NUM_NEG_POS_POC_WIDTH);     
`endif
                        
                        if(input_fifo_is_empty == 1) begin
                            next_state = STATE_READ_WAIT;
                        end                          
                    end
                    `HEADER_PARAMETERS_4: begin
                        read_en_out = 1;
                        rps_entry_addr = (curr_rps_entry_idx + rps_header_addr_reg * (1<<NUM_NEG_POS_POC_WIDTH))%(1<<RPS_ENTRY_ADDR_WIDTH);
                        rps_entry_wr_en = 1'b1;
`ifdef READ_FILE
                        rps_entry_data_in = {   fifo_in[    HEADER_WIDTH + RPS_ID_WIDTH + RPS_ENTRY_USED_WIDTH- 1:
                                                            HEADER_WIDTH + RPS_ID_WIDTH   ],
                                                fifo_in[    HEADER_WIDTH + RPS_ID_WIDTH + DELTA_POC_WIDTH + RPS_ENTRY_USED_WIDTH- 1:
                                                            HEADER_WIDTH + RPS_ID_WIDTH + RPS_ENTRY_USED_WIDTH]};
`else
                        rps_entry_data_in = fifo_in[   FIFO_IN_WIDTH - HEADER_WIDTH - RPS_ID_WIDTH - 1: 
                                                        FIFO_IN_WIDTH - HEADER_WIDTH - RPS_ID_WIDTH - DELTA_POC_WIDTH - RPS_ENTRY_USED_WIDTH];
`endif
                        if(curr_rps_entry_idx >= num_delta_poc) begin
                            next_state = STATE_ERROR;
                        end
                        else if(input_fifo_is_empty == 1) begin
                            next_state = STATE_READ_WAIT;                                
                        end
                    end
                    `HEADER_PARAMETERS_5: begin
                        read_en_out = 1;
                        next_state = STATE_REF_PIC_LIST_5_UPDATE;
                    end
                    `HEADER_SLICE_0: begin
                            next_state = STATE_DPB_ADD_PREV_PIC;
                            read_en_out = 1;
                    end
                    `HEADER_SLICE_1: begin
                        read_en_out = 1;
                        if(slice_type_wire != `SLICE_I) begin
                            if(short_term_ref_pic_sps_wire == 1) begin
                                rps_header_addr = short_term_ref_pic_idx_wire;
                                if(output_fifo_is_full == 1) begin
                                    next_state = STATE_SLICE_1_WRITE_WAIT;
                                end
                                else begin
                                    next_state = STATE_GET_ST_RPS_HEADER;
                                end
                            end                        
                        end
                        else begin
                            if(output_fifo_is_full == 1) begin
                                next_state = STATE_WRITE_WAIT;
                            end
                            else if(input_fifo_is_empty == 1) begin
                                next_state = STATE_READ_WAIT;
                            end                       
                        end 
                    end
                    `HEADER_SLICE_2: begin
                        read_en_out = 1;
                        if(output_fifo_is_full == 1) begin
                            next_state = STATE_WRITE_WAIT;
                        end
                        else if(input_fifo_is_empty == 1) begin
                            next_state = STATE_READ_WAIT;
                        end
                    end
                    `HEADER_SLICE_3: begin
                        read_en_out = 1;
                        if(input_fifo_is_empty == 1) begin
                            next_state = STATE_READ_WAIT;
                        end
                    end
                    `HEADER_SLICE_4: begin
                        read_en_out = 1;
`ifdef READ_FILE
                        st_rps_entry_data_in = {fifo_in[    HEADER_WIDTH + RPS_ID_WIDTH + RPS_ENTRY_USED_WIDTH- 1:
                                                            HEADER_WIDTH + RPS_ID_WIDTH   ],
                                                fifo_in[    HEADER_WIDTH + RPS_ID_WIDTH + DELTA_POC_WIDTH + RPS_ENTRY_USED_WIDTH- 1:
                                                            HEADER_WIDTH + RPS_ID_WIDTH + RPS_ENTRY_USED_WIDTH]};
`else
                        st_rps_entry_data_in = fifo_in[    FIFO_IN_WIDTH - HEADER_WIDTH - RPS_ID_WIDTH - 1: 
                                                            FIFO_IN_WIDTH - HEADER_WIDTH - RPS_ID_WIDTH - DELTA_POC_WIDTH - RPS_ENTRY_USED_WIDTH];
`endif                        
                        st_rps_entry_addr = curr_rps_entry_idx;
                        st_rps_entry_wr_en = 1'b1;
                        if(((curr_rps_entry_idx + 1) % (1<< NUM_NEG_POS_POC_WIDTH))  == num_delta_poc) begin
                            next_state = STATE_REF_PIC_LIST_5_UPDATE;
                        end
                        else if(input_fifo_is_empty == 1) begin
                            next_state = STATE_READ_WAIT;
                        end
                    end
                    `HEADER_SLICE_5: begin
                        read_en_out = 1;
                        if(input_fifo_is_empty == 1) begin
                            next_state = STATE_READ_WAIT;
                        end                    
                    end
                    `HEADER_SLICE_6: begin
                        read_en_out = 1;
                        if(input_fifo_is_empty == 1) begin
                            next_state = STATE_READ_WAIT;
                        end                    
                    end
                    `HEADER_SLICE_7: begin
                        read_en_out = 1;
                        if(input_fifo_is_empty == 1) begin
                            next_state = STATE_READ_WAIT;
                        end                    
                    end
                    `HEADER_CTU_0: begin
                        read_en_out = 1;
                        next_state = STATE_INTER_PREFETCH_COL_WRITE_WAIT;
                    end
                    `HEADER_CTU_1: begin
                        read_en_out = 1;
                        if(input_fifo_is_empty == 1) begin
                            next_state = STATE_READ_WAIT;
                        end 
                    end
                    `HEADER_CTU_2_Y: begin
                        read_en_out = 1;
                        if(input_fifo_is_empty == 1) begin
                            next_state = STATE_READ_WAIT;
                        end 
                    end
                    `HEADER_CTU_2_CB: begin
                        read_en_out = 1;
                        if(input_fifo_is_empty == 1) begin
                            next_state = STATE_READ_WAIT;
                        end 
                    end
                    `HEADER_CTU_2_CR: begin
                        read_en_out = 1;
                        if(input_fifo_is_empty == 1) begin
                            next_state = STATE_READ_WAIT;
                        end 
                    end                    
                    `HEADER_CU_0: begin
                        read_en_out = 1;
`ifdef  READ_FILE
                        if(fifo_in[     HEADER_WIDTH + 2*X0_WIDTH + LOG2_CTB_WIDTH] == `MODE_INTRA) begin
`else
                        if(fifo_in[     FIFO_IN_WIDTH - HEADER_WIDTH - 2*X0_WIDTH - LOG2_CTB_WIDTH - 1] == `MODE_INTRA) begin
`endif
                            next_state = STATE_MV_RETURN_WAIT1;    
                        end
                        else begin
                            if(input_fifo_is_empty == 1) begin
                                next_state = STATE_READ_WAIT;
                            end
                        end
                    end
                    `HEADER_PU_0: begin
                        read_en_out = 1;
`ifdef READ_FILE
                        if(fifo_in[   HEADER_WIDTH + PART_IDX_WIDTH + PRED_IDC_WIDTH + MERGE_FLAG_WIDTH - 1] == 0) begin
`else
                        if(fifo_in[  FIFO_IN_WIDTH - HEADER_WIDTH - PART_IDX_WIDTH - PRED_IDC_WIDTH - 1] == 0) begin
`endif      
                            if(input_fifo_is_empty == 1) begin
                                next_state = STATE_MVD1_READ_WAIT;
                            end
                            else begin
                                next_state = STATE_MVD1_READ;
                            end                        
                        end
                        else begin
                            next_state = STATE_MV_RETURN_WAIT1;
                        end
                    end
                    `HEADER_RU_0: begin
                        read_en_out = 1;
                        if(input_fifo_is_empty == 1) begin
                            next_state = STATE_READ_WAIT;
                        end 
                    end
                    `HEADER_RU_1: begin
                        read_en_out = 1;
                        if(input_fifo_is_empty == 1) begin
                            next_state = STATE_RU_PACKET_READ_WAIT;
                        end
                        else begin
                            next_state = STATE_RU_DONE_WAIT2;
                        end
                    end                      
                    default: begin
                        read_en_out = 1;
                        next_state = STATE_ERROR;
                    end
                endcase
            end
            STATE_WRITE_WAIT: begin
                if(output_fifo_is_full == 0) begin
                    if(input_fifo_is_empty == 1'b0) begin
                        next_state = STATE_ACTIVE;
                    end
                    else begin
                        next_state = STATE_READ_WAIT;
                    end                
                end
            end
            STATE_SLICE_1_WRITE_WAIT: begin
                rps_header_addr = short_term_ref_pic_idx;
                if(output_fifo_is_full == 1'b0) begin
                    rps_entry_addr = short_term_ref_pic_idx * (1<<NUM_NEG_POS_POC_WIDTH);
                    next_state = STATE_GET_ST_RPS_ENTRY;
                end
            end
            STATE_GET_ST_RPS_HEADER: begin
                rps_entry_addr = short_term_ref_pic_idx * (1<<NUM_NEG_POS_POC_WIDTH);
                next_state = STATE_GET_ST_RPS_ENTRY;
            end
            STATE_GET_ST_RPS_ENTRY: begin
                rps_entry_addr = ((short_term_ref_pic_idx * (1<<NUM_NEG_POS_POC_WIDTH)) + curr_rps_entry_idx + 1)%(1<<RPS_ENTRY_ADDR_WIDTH);
                st_rps_entry_addr = curr_rps_entry_idx;
                st_rps_entry_wr_en = 1;
                st_rps_entry_data_in = rps_entry_data_out;
                if(((curr_rps_entry_idx + 1) % (1<< NUM_NEG_POS_POC_WIDTH))  == num_delta_poc) begin
                    next_state = STATE_SET_ST_RPS_ENTRY_ADDR_FOR_REF_POC5;
                end
            end
            STATE_SET_ST_RPS_ENTRY_ADDR_FOR_REF_POC5: begin
                st_rps_entry_addr = 0;
                next_state = STATE_REF_PIC_LIST_5_UPDATE;
            end
            STATE_DPB_ADD_PREV_PIC: begin
                if(initial_condition == 1) begin
                    dpb_addr = current_pic_dpb_idx;
                    dpb_data_in = current__poc;
                    dpb_wr_en = 1;
                end     
                if(input_fifo_is_empty == 1) begin
                    next_state = STATE_POC_READ_WAIT;                    
                end
                else begin
                    next_state = STATE_CURRENT_POC;
                end
            end
            STATE_CURRENT_POC: begin
                next_state = STATE_CUR_PIC_FIL_IDX;
                read_en_out = 1;
            end
            STATE_CUR_PIC_FIL_IDX: begin
                if(dpb_filled_flag[curr_rps_entry_idx] == 0) begin
                    if(output_fifo_is_full == 1) begin
                        next_state = STATE_WRITE_WAIT_CUR_DPB_IDX_TO_DBF;
                    end
                    else begin
                        next_state = STATE_SEND_POC_TO_DBF;
                    end                    
                end
            end
            STATE_SEND_POC_TO_DBF: begin
                //ref_poc_list5_addr = ref_poc_list5_addr_read;
                //dpb_addr = dpb_addr_read;
                //if(write_ref_pic_state == WRITE_REF_PIC_STATE_DONE) begin
                if(output_fifo_is_full == 1) begin
                    next_state = STATE_WRITE_WAIT;
                end
                else if(input_fifo_is_empty == 1) begin
                    next_state = STATE_READ_WAIT;
                end
                else begin
                    next_state = STATE_ACTIVE;
                end                    
                //end
            end
            STATE_REF_PIC_LIST_TRANSFER: begin
                ref_poc_list5_addr = ref_poc_list5_addr_read;
                dpb_addr = dpb_addr_read;
                if(write_ref_pic_state == WRITE_REF_PIC_STATE_DONE) begin
                    if(input_fifo_is_empty == 1) begin
                        next_state = STATE_READ_WAIT;
                    end
                    else begin
                        next_state = STATE_ACTIVE;
                    end                    
                end
            end
            STATE_WRITE_WAIT_CUR_DPB_IDX_TO_DBF: begin
                if(output_fifo_is_full == 0) begin
                    next_state = STATE_SEND_POC_TO_DBF;
                end
            end
            STATE_REF_PIC_LIST_5_UPDATE: begin
                st_rps_entry_addr = curr_rps_entry_idx + 1;
                ref_poc_list5_addr = curr_rps_entry_idx;
                ref_poc_list5_wr_en = 1;
                ref_poc_list5_data_in[REF_PIC_LIST5_POC_RANGE_HIGH:REF_PIC_LIST5_POC_RANGE_LOW] = delta_poc_wire;
                if(st_rps_entry_data_out[RPS_ENTRY_USED_FLAG_RANGE_HIGH] == 0) begin
                    ref_poc_list5_data_in[REF_PIC_LIST5_DPB_STATE_HIGH:REF_PIC_LIST5_DPB_STATE_LOW] = `ST_FOLL;
                end
                else begin
                    if(curr_rps_entry_idx < num_positive_poc) begin
                        ref_poc_list5_data_in[REF_PIC_LIST5_DPB_STATE_HIGH:REF_PIC_LIST5_DPB_STATE_LOW] = `ST_CURR_AFT;
                    end
                    else begin
                        ref_poc_list5_data_in[REF_PIC_LIST5_DPB_STATE_HIGH:REF_PIC_LIST5_DPB_STATE_LOW] = `ST_CURR_BEF;
                    end
                end
                if((curr_rps_entry_idx + 1)%(1<<NUM_NEG_POS_POC_WIDTH) == num_delta_poc) begin      // originally num_pic_ref_pic_5_total was planned as the condition here but, it has not reached its final value at the first evaluation of this condition
                    next_state = STATE_REF_PIC_LIST_TRANSFER;
                end
            end
            STATE_MVD1_READ: begin
                read_en_out = 1;
                if(input_fifo_is_empty == 1) begin
                    next_state = STATE_MVD2_READ_WAIT;
                end
                else begin
                    next_state = STATE_MVD2_READ;                
                end
            end
            STATE_MVD2_READ: begin
                read_en_out = 1;
                next_state = STATE_MV_RETURN_WAIT2;
            end
            STATE_MV_RETURN_WAIT1: begin
                next_state = STATE_MV_RETURN_WAIT2;
            end
            STATE_MV_RETURN_WAIT2: begin        /// to do think of removing unncessary wait states
                next_state = STATE_MV_RETURN_WAIT3;
            end
            STATE_MV_RETURN_WAIT3: begin
                if(mv_done) begin
                    if(is_pu_header_cus_last) begin
                        if(input_fifo_is_empty == 1) begin
                            next_state = STATE_RU_PACKET_READ_WAIT;
                        end
                        else begin
                            next_state = STATE_RU_PACKET_READ;
                        end
                    end
                    else begin
                        if(input_fifo_is_empty == 1) begin
                            next_state = STATE_READ_WAIT;
                        end
                        else begin
                            next_state = STATE_ACTIVE;
                        end  
                    end
                end
            end
            STATE_INTER_PREFETCH_COL_WRITE_WAIT: begin
                if(pref_ctu_col_writ_ctu_done_out) begin
                    if(input_fifo_is_empty == 1) begin
                        next_state = STATE_READ_WAIT;
                    end 
                    else begin
                        next_state = STATE_ACTIVE;
                    end
                end
            end
            STATE_ERROR: begin
                next_state = STATE_READ_WAIT;
            end
            // STATE_PU_TEMP_STORE: begin
                // read_en_out = 1;
                // if(is_pu_header_cus_last) begin
                    // next_state = STATE_RU_PACKET_READ;
                // end
            // end
            STATE_RU_PACKET_READ: begin
                if(intra_ru_read_ready) begin      // this is idle if block sequencer has finished a tu
                    read_en_out = 1;
                    if(c_idx_wire == 0) begin                       // read RU_1 packet and comeback!
                        if(input_fifo_is_empty == 1) begin
                            next_state = STATE_READ_WAIT;
                        end
                        else begin
                            next_state = STATE_ACTIVE;
                        end
                    end
                    else if(last_tu_of_cu && c_idx_wire == 2) begin
                        if(input_fifo_is_empty == 1) begin
                            next_state = STATE_READ_WAIT;
                        end
                        else begin
                            next_state = STATE_ACTIVE;
                        end
                    end
                    else begin
                        if(input_fifo_is_empty == 1) begin
                            next_state = STATE_RU_PACKET_READ_WAIT;
                        end
                        else begin
                            next_state = STATE_RU_DONE_WAIT1;
                        end
                    end
                end   
            end
            STATE_RU_DONE_WAIT1: begin
                next_state = STATE_RU_DONE_WAIT2;
            end
            STATE_RU_DONE_WAIT2: begin
                next_state = STATE_RU_DONE_WAIT3;
            end
            STATE_RU_DONE_WAIT3: begin
                next_state = STATE_RU_PACKET_READ;
            end
            default: begin
                next_state = STATE_READ_WAIT;
            end
        endcase
    end
   
    always@(posedge clk) begin
        if(reset) begin
            state <= STATE_READ_WAIT;
        end
        else begin
            state <= next_state;
        end
    end
  
    always @(posedge clk ) begin : pred_main_fsm
        if(reset) begin
            
            write_en_out <= 1'b0;
            dpb_filled_flag <= 16'd0;
            initial_condition <= 0;
            current__poc <= {POC_WIDTH{1'b1}};      /// assume Video sequence does not have max POC
            num_pic_st_curr_aft <= 0;
            num_pic_st_curr_bef <= 0;
            num_pic_st_curr_total <= 0;
            num_pic_st_fol <= 0;
        end 
        else if(enable) begin
                write_en_out <= 1'b0;
                inter_config_mode_in <= `INTER_TOP_CONFIG_IDLE;
            case(state)
                STATE_READ_WAIT: begin
                end
                STATE_ACTIVE: begin
`ifdef READ_FILE
                    case(fifo_in[HEADER_WIDTH -1:0])
`else
                    case(fifo_in[FIFO_IN_WIDTH-1:FIFO_IN_WIDTH - HEADER_WIDTH])
`endif
                        `HEADER_PARAMETERS_0: begin
                            inter_config_mode_in <= `INTER_TOP_PARA_0;
                            inter_config_bus_in <= fifo_in[FIFO_IN_WIDTH-1: FIFO_IN_WIDTH - FIFO_OUT_WIDTH];
`ifdef READ_FILE    
                            pic_width <=   (fifo_in[PIC_WIDTH_WIDTH + HEADER_WIDTH -1: HEADER_WIDTH])%(1<<PIC_DIM_WIDTH);
                            pic_height  <=   (fifo_in[HEADER_WIDTH + PIC_WIDTH_WIDTH + PIC_HEIGHT_WIDTH- 1: HEADER_WIDTH + PIC_WIDTH_WIDTH])%(1<<PIC_DIM_WIDTH);
`else
                            pic_width <=   (fifo_in[FIFO_IN_WIDTH - HEADER_WIDTH - 1: FIFO_IN_WIDTH - HEADER_WIDTH - PIC_WIDTH_WIDTH])%(1<<PIC_DIM_WIDTH);
                            pic_height  <=   (fifo_in[FIFO_IN_WIDTH - HEADER_WIDTH - PIC_WIDTH_WIDTH - 1: FIFO_IN_WIDTH - HEADER_WIDTH - PIC_WIDTH_WIDTH - PIC_HEIGHT_WIDTH])%(1<<PIC_DIM_WIDTH);
`endif
                            fifo_out <= fifo_in[FIFO_IN_WIDTH-1: FIFO_IN_WIDTH - FIFO_OUT_WIDTH];
                            
                            if(output_fifo_is_full == 0) begin
                                write_en_out <= 1;
                            end
                        end
                        `HEADER_PARAMETERS_1: begin
                            inter_config_mode_in <= `INTER_TOP_PARA_1;
                            inter_config_bus_in <= fifo_in[FIFO_IN_WIDTH-1: FIFO_IN_WIDTH - FIFO_OUT_WIDTH];
`ifdef READ_FILE
                            log2_ctb_size <=  fifo_in[ HEADER_WIDTH + LOG2CTBSIZEY_WIDTH -1 : HEADER_WIDTH];
                            strong_intra_smoothing <= fifo_in[  HEADER_WIDTH + LOG2CTBSIZEY_WIDTH + PPS_CB_QP_OFFSET_WIDTH + PPS_CR_QP_OFFSET_WIDTH + PPS_BETA_OFFSET_DIV2_WIDTH + PPS_TC_OFFSET_DIV2_WIDTH + STRONG_INTRA_SMOOTHING_WIDTH- 1:
                                                                HEADER_WIDTH + LOG2CTBSIZEY_WIDTH + PPS_CB_QP_OFFSET_WIDTH + PPS_CR_QP_OFFSET_WIDTH + PPS_BETA_OFFSET_DIV2_WIDTH + PPS_TC_OFFSET_DIV2_WIDTH ];
                            constrained_intra_pred <= fifo_in[  HEADER_WIDTH + LOG2CTBSIZEY_WIDTH + PPS_CB_QP_OFFSET_WIDTH + PPS_CR_QP_OFFSET_WIDTH + PPS_BETA_OFFSET_DIV2_WIDTH + PPS_TC_OFFSET_DIV2_WIDTH + STRONG_INTRA_SMOOTHING_WIDTH + CONSTRAINED_INTRA_PRED_WIDTH- 1:
                                                                HEADER_WIDTH + LOG2CTBSIZEY_WIDTH + PPS_CB_QP_OFFSET_WIDTH + PPS_CR_QP_OFFSET_WIDTH + PPS_BETA_OFFSET_DIV2_WIDTH + PPS_TC_OFFSET_DIV2_WIDTH + STRONG_INTRA_SMOOTHING_WIDTH];
`else
                            log2_ctb_size <=  fifo_in[FIFO_IN_WIDTH - HEADER_WIDTH - 1: FIFO_IN_WIDTH - HEADER_WIDTH - LOG2CTBSIZEY_WIDTH];
                            strong_intra_smoothing <= fifo_in[  FIFO_IN_WIDTH - HEADER_WIDTH - LOG2CTBSIZEY_WIDTH - PPS_CB_QP_OFFSET_WIDTH - PPS_CR_QP_OFFSET_WIDTH - PPS_BETA_OFFSET_DIV2_WIDTH - PPS_TC_OFFSET_DIV2_WIDTH - 1 : 
                                                                FIFO_IN_WIDTH - HEADER_WIDTH - LOG2CTBSIZEY_WIDTH - PPS_CB_QP_OFFSET_WIDTH - PPS_CR_QP_OFFSET_WIDTH - PPS_BETA_OFFSET_DIV2_WIDTH - PPS_TC_OFFSET_DIV2_WIDTH - STRONG_INTRA_SMOOTHING_WIDTH];
                            constrained_intra_pred <= fifo_in[  FIFO_IN_WIDTH - HEADER_WIDTH - LOG2CTBSIZEY_WIDTH - PPS_CB_QP_OFFSET_WIDTH - PPS_CR_QP_OFFSET_WIDTH - PPS_BETA_OFFSET_DIV2_WIDTH - PPS_TC_OFFSET_DIV2_WIDTH - STRONG_INTRA_SMOOTHING_WIDTH- 1 :
                                                                FIFO_IN_WIDTH - HEADER_WIDTH - LOG2CTBSIZEY_WIDTH - PPS_CB_QP_OFFSET_WIDTH - PPS_CR_QP_OFFSET_WIDTH - PPS_BETA_OFFSET_DIV2_WIDTH - PPS_TC_OFFSET_DIV2_WIDTH - STRONG_INTRA_SMOOTHING_WIDTH - CONSTRAINED_INTRA_PRED_WIDTH];
`endif
                            fifo_out <= fifo_in[FIFO_IN_WIDTH - 1: FIFO_IN_WIDTH - FIFO_OUT_WIDTH];
                            if(output_fifo_is_full == 0) begin
                                write_en_out <= 1;
                            end                      
                        end
                        `HEADER_PARAMETERS_2: begin
                            inter_config_mode_in <= `INTER_TOP_PARA_2;
                            inter_config_bus_in <= fifo_in[FIFO_IN_WIDTH-1: FIFO_IN_WIDTH - FIFO_OUT_WIDTH];
                        end
                        `HEADER_PARAMETERS_3: begin
                            inter_config_mode_in <= `INTER_TOP_CONFIG_IDLE;
`ifdef READ_FILE
                            rps_header_addr_reg <= (fifo_in[  HEADER_WIDTH + RPS_HEADER_ID_WIDTH- 1: HEADER_WIDTH   ]) % (1<<RPS_HEADER_ADDR_WIDTH);

                        
                            num_positive_poc <=   (fifo_in[     HEADER_WIDTH + RPS_HEADER_ID_WIDTH + NUM_POSITIVE_WIDTH- 1:
                                                                HEADER_WIDTH + RPS_HEADER_ID_WIDTH  ])%(1<<NUM_NEG_POS_POC_WIDTH);
                            num_negative_poc <=   (fifo_in[     HEADER_WIDTH + RPS_HEADER_ID_WIDTH + NUM_POSITIVE_WIDTH + NUM_NEGATIVE_WIDTH- 1:
                                                                HEADER_WIDTH + RPS_HEADER_ID_WIDTH + NUM_POSITIVE_WIDTH  ])%(1<<NUM_NEG_POS_POC_WIDTH);                                        
                            num_delta_poc <=      (fifo_in[     HEADER_WIDTH + RPS_HEADER_ID_WIDTH + NUM_POSITIVE_WIDTH- 1:
                                                                HEADER_WIDTH + RPS_HEADER_ID_WIDTH  ] + 

                                                   fifo_in[     HEADER_WIDTH + RPS_HEADER_ID_WIDTH + NUM_POSITIVE_WIDTH + NUM_NEGATIVE_WIDTH- 1:
                                                                HEADER_WIDTH + RPS_HEADER_ID_WIDTH + NUM_POSITIVE_WIDTH ])%(1<<NUM_NEG_POS_POC_WIDTH);                                                  
`else
                            rps_header_addr_reg <= (fifo_in[ FIFO_IN_WIDTH - HEADER_WIDTH -1: 
                                                             FIFO_IN_WIDTH - HEADER_WIDTH - RPS_HEADER_ID_WIDTH]) % (1<<RPS_HEADER_ADDR_WIDTH);
                        
                            num_positive_poc <=   (fifo_in[  FIFO_IN_WIDTH - HEADER_WIDTH - RPS_HEADER_ID_WIDTH - 1:
                                                            FIFO_IN_WIDTH - HEADER_WIDTH - RPS_HEADER_ID_WIDTH - NUM_POSITIVE_WIDTH])%(1<<NUM_NEG_POS_POC_WIDTH);
                            num_negative_poc <=   (fifo_in[  FIFO_IN_WIDTH - HEADER_WIDTH - RPS_HEADER_ID_WIDTH - NUM_POSITIVE_WIDTH -1:
                                                            FIFO_IN_WIDTH - HEADER_WIDTH - RPS_HEADER_ID_WIDTH - NUM_POSITIVE_WIDTH - NUM_NEGATIVE_WIDTH])%(1<<NUM_NEG_POS_POC_WIDTH);                                        
                            num_delta_poc <=      (fifo_in[  FIFO_IN_WIDTH - HEADER_WIDTH - RPS_HEADER_ID_WIDTH - 1:
                                                            FIFO_IN_WIDTH - HEADER_WIDTH - RPS_HEADER_ID_WIDTH - NUM_POSITIVE_WIDTH] + 
                                                   fifo_in[  FIFO_IN_WIDTH - HEADER_WIDTH - RPS_HEADER_ID_WIDTH - NUM_POSITIVE_WIDTH -1:
                                                            FIFO_IN_WIDTH - HEADER_WIDTH - RPS_HEADER_ID_WIDTH - NUM_POSITIVE_WIDTH - NUM_NEGATIVE_WIDTH])%(1<<NUM_NEG_POS_POC_WIDTH);                                                  
`endif
                            curr_rps_entry_idx <= 0;
                        end
                        `HEADER_PARAMETERS_4: begin
                            inter_config_mode_in <= `INTER_TOP_CONFIG_IDLE;
                            curr_rps_entry_idx <= (curr_rps_entry_idx + 1) % (1<< NUM_NEG_POS_POC_WIDTH);
                        end
                        `HEADER_PARAMETERS_5: begin
                            inter_config_mode_in <= `INTER_TOP_CONFIG_IDLE;
`ifdef READ_FILE
                            num_pic_st_curr_bef <= fifo_in[ HEADER_WIDTH + NUM_CURR_REF_POC_WIDTH- 1:       HEADER_WIDTH                                    ] ;
                            num_pic_st_curr_aft <= fifo_in[ HEADER_WIDTH + 2*NUM_CURR_REF_POC_WIDTH- 1:     HEADER_WIDTH + NUM_CURR_REF_POC_WIDTH           ] ;
                            num_pic_st_fol      <= fifo_in[ HEADER_WIDTH + 3*NUM_CURR_REF_POC_WIDTH- 1:     HEADER_WIDTH + 2*NUM_CURR_REF_POC_WIDTH         ] ;
`else
                            num_pic_st_curr_bef <= fifo_in[FIFO_IN_WIDTH - HEADER_WIDTH                                 - 1: FIFO_IN_WIDTH - HEADER_WIDTH - NUM_CURR_REF_POC_WIDTH] ;
                            num_pic_st_curr_aft <= fifo_in[FIFO_IN_WIDTH - HEADER_WIDTH - NUM_CURR_REF_POC_WIDTH        - 1: FIFO_IN_WIDTH - HEADER_WIDTH - 2*NUM_CURR_REF_POC_WIDTH] ;
                            num_pic_st_fol      <= fifo_in[FIFO_IN_WIDTH - HEADER_WIDTH - 2*NUM_CURR_REF_POC_WIDTH      - 1: FIFO_IN_WIDTH - HEADER_WIDTH - 3*NUM_CURR_REF_POC_WIDTH] ;
                            curr_rps_entry_idx <= 0;
                            // num_pic_ref_pic_5_total <= (fifo_in[FIFO_IN_WIDTH - HEADER_WIDTH                                 - 1: FIFO_IN_WIDTH - HEADER_WIDTH - NUM_CURR_REF_POC_WIDTH] +
                                                        // fifo_in[FIFO_IN_WIDTH - HEADER_WIDTH - NUM_CURR_REF_POC_WIDTH        - 1: FIFO_IN_WIDTH - HEADER_WIDTH - 2*NUM_CURR_REF_POC_WIDTH] +
                                                        // fifo_in[FIFO_IN_WIDTH - HEADER_WIDTH - 2*NUM_CURR_REF_POC_WIDTH      - 1: FIFO_IN_WIDTH - HEADER_WIDTH - 3*NUM_CURR_REF_POC_WIDTH])%(1<<NUM_REF_IDX_L0_MINUS1_WIDTH);
`endif
                        end
                        `HEADER_SLICE_0: begin
                            inter_config_mode_in <= `INTER_TOP_CONFIG_IDLE;
                            // TODO: goto state FILL dpb
                        end
                        `HEADER_SLICE_1: begin
                            inter_config_mode_in <= `INTER_TOP_SLICE_1;
                            inter_config_bus_in <= fifo_in[FIFO_IN_WIDTH-1: FIFO_IN_WIDTH - FIFO_OUT_WIDTH];                            
                            short_term_ref_pic_sps <= short_term_ref_pic_sps_wire;
                            if(short_term_ref_pic_sps_wire == 1) begin
                                short_term_ref_pic_idx <= short_term_ref_pic_idx_wire;
                            end
`ifdef READ_FILE
                            slice_temporal_mvp_enabled_flag <=   fifo_in[   HEADER_WIDTH + SHORT_TERM_REF_PIC_SPS_WIDTH + SHORT_TERM_REF_PIC_IDX_WIDTH + TEMPORAL_MVP_ENABLED_WIDTH- 1:
                                                                            HEADER_WIDTH + SHORT_TERM_REF_PIC_SPS_WIDTH + SHORT_TERM_REF_PIC_IDX_WIDTH  ];     
                            slice_type              <=      fifo_in[        HEADER_WIDTH + SHORT_TERM_REF_PIC_SPS_WIDTH + SHORT_TERM_REF_PIC_IDX_WIDTH + TEMPORAL_MVP_ENABLED_WIDTH + SAO_LUMA_WIDTH + SAO_CHROMA_WIDTH + NUM_REF_IDX_L0_MINUS1_WIDTH + NUM_REF_IDX_L1_MINUS1_WIDTH + MAX_MERGE_CAND_WIDTH + SLICE_TYPE_WIDTH- 1:
                                                                            HEADER_WIDTH + SHORT_TERM_REF_PIC_SPS_WIDTH + SHORT_TERM_REF_PIC_IDX_WIDTH + TEMPORAL_MVP_ENABLED_WIDTH + SAO_LUMA_WIDTH + SAO_CHROMA_WIDTH + NUM_REF_IDX_L0_MINUS1_WIDTH + NUM_REF_IDX_L1_MINUS1_WIDTH + MAX_MERGE_CAND_WIDTH  ];   
                            num_active_ref_idx_l0  <= fifo_in[              HEADER_WIDTH + SHORT_TERM_REF_PIC_SPS_WIDTH + SHORT_TERM_REF_PIC_IDX_WIDTH + TEMPORAL_MVP_ENABLED_WIDTH + SAO_LUMA_WIDTH + SAO_CHROMA_WIDTH + NUM_REF_IDX_L0_MINUS1_WIDTH- 1:
                                                                            HEADER_WIDTH + SHORT_TERM_REF_PIC_SPS_WIDTH + SHORT_TERM_REF_PIC_IDX_WIDTH + TEMPORAL_MVP_ENABLED_WIDTH + SAO_LUMA_WIDTH + SAO_CHROMA_WIDTH  ] + 1'b1;
                                                                
                            num_active_ref_idx_l1   <= fifo_in[             HEADER_WIDTH + SHORT_TERM_REF_PIC_SPS_WIDTH + SHORT_TERM_REF_PIC_IDX_WIDTH + TEMPORAL_MVP_ENABLED_WIDTH + SAO_LUMA_WIDTH + SAO_CHROMA_WIDTH + NUM_REF_IDX_L0_MINUS1_WIDTH + NUM_REF_IDX_L1_MINUS1_WIDTH- 1:
                                                                            HEADER_WIDTH + SHORT_TERM_REF_PIC_SPS_WIDTH + SHORT_TERM_REF_PIC_IDX_WIDTH + TEMPORAL_MVP_ENABLED_WIDTH + SAO_LUMA_WIDTH + SAO_CHROMA_WIDTH + NUM_REF_IDX_L0_MINUS1_WIDTH  ] + 1'b1;
`else
                            slice_temporal_mvp_enabled_flag <=   fifo_in[  FIFO_IN_WIDTH - HEADER_WIDTH - SHORT_TERM_REF_PIC_SPS_WIDTH - SHORT_TERM_REF_PIC_IDX_WIDTH - 1:
                                                                FIFO_IN_WIDTH - HEADER_WIDTH - SHORT_TERM_REF_PIC_SPS_WIDTH - SHORT_TERM_REF_PIC_IDX_WIDTH - TEMPORAL_MVP_ENABLED_WIDTH];     
                            slice_type              <=      fifo_in[ FIFO_IN_WIDTH - HEADER_WIDTH - SHORT_TERM_REF_PIC_SPS_WIDTH - SHORT_TERM_REF_PIC_IDX_WIDTH - TEMPORAL_MVP_ENABLED_WIDTH - SAO_LUMA_WIDTH - SAO_CHROMA_WIDTH - NUM_REF_IDX_L0_MINUS1_WIDTH - NUM_REF_IDX_L1_MINUS1_WIDTH - MAX_MERGE_CAND_WIDTH -1:
                                                                     FIFO_IN_WIDTH - HEADER_WIDTH - SHORT_TERM_REF_PIC_SPS_WIDTH - SHORT_TERM_REF_PIC_IDX_WIDTH - TEMPORAL_MVP_ENABLED_WIDTH - SAO_LUMA_WIDTH - SAO_CHROMA_WIDTH - NUM_REF_IDX_L0_MINUS1_WIDTH - NUM_REF_IDX_L1_MINUS1_WIDTH - MAX_MERGE_CAND_WIDTH - SLICE_TYPE_WIDTH];   
                            num_active_ref_idx_l0  <= fifo_in[   INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - SHORT_TERM_REF_PIC_SPS_WIDTH - SHORT_TERM_REF_PIC_IDX_WIDTH - TEMPORAL_MVP_ENABLED_WIDTH - SAO_LUMA_WIDTH - SAO_CHROMA_WIDTH - 1:
                                                                INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - SHORT_TERM_REF_PIC_SPS_WIDTH - SHORT_TERM_REF_PIC_IDX_WIDTH - TEMPORAL_MVP_ENABLED_WIDTH - SAO_LUMA_WIDTH - SAO_CHROMA_WIDTH - NUM_REF_IDX_L0_MINUS1_WIDTH] + 1'b1;
                                                                
                            num_active_ref_idx_l1   <= fifo_in[  INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - SHORT_TERM_REF_PIC_SPS_WIDTH - SHORT_TERM_REF_PIC_IDX_WIDTH - TEMPORAL_MVP_ENABLED_WIDTH - SAO_LUMA_WIDTH - SAO_CHROMA_WIDTH - NUM_REF_IDX_L0_MINUS1_WIDTH - 1:
                                                                INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - SHORT_TERM_REF_PIC_SPS_WIDTH - SHORT_TERM_REF_PIC_IDX_WIDTH - TEMPORAL_MVP_ENABLED_WIDTH - SAO_LUMA_WIDTH - SAO_CHROMA_WIDTH - NUM_REF_IDX_L0_MINUS1_WIDTH - NUM_REF_IDX_L1_MINUS1_WIDTH] + 1'b1;
`endif                            
                            fifo_out <= fifo_in[FIFO_IN_WIDTH - 1: FIFO_IN_WIDTH - FIFO_OUT_WIDTH];
                            if(output_fifo_is_full == 0) begin
                                write_en_out <= 1;
                            end
                        end
                        `HEADER_SLICE_2: begin
                            // collocated_ref_idx <= fifo_in[ FIFO_IN_WIDTH - HEADER_WIDTH - SLICE_CB_QP_OFFSET_WIDTH - SLICE_CR_QP_OFFSET_WIDTH - DISABLE_DBF_WIDTH - SLICE_BETA_OFFSET_DIV2_WIDTH - SLICE_TC_OFFSET_DIV2_WIDTH - 1:
                                                           // FIFO_IN_WIDTH - HEADER_WIDTH - SLICE_CB_QP_OFFSET_WIDTH - SLICE_CR_QP_OFFSET_WIDTH - DISABLE_DBF_WIDTH - SLICE_BETA_OFFSET_DIV2_WIDTH - SLICE_TC_OFFSET_DIV2_WIDTH - COLLOCATED_REF_IDX_WIDTH];
                            inter_config_mode_in <= `INTER_TOP_SLICE_2;
                            inter_config_bus_in <= fifo_in[FIFO_IN_WIDTH-1: FIFO_IN_WIDTH - INTER_TOP_CONFIG_BUS_WIDTH];    
                            fifo_out <= fifo_in[FIFO_IN_WIDTH - 1: FIFO_IN_WIDTH - FIFO_OUT_WIDTH];
                            if(output_fifo_is_full == 0) begin
                                write_en_out <= 1;
                            end
                        end
                        `HEADER_SLICE_3: begin 
                            inter_config_mode_in <= `INTER_TOP_CONFIG_IDLE;
`ifdef READ_FILE
                            num_positive_poc <=   (fifo_in[     HEADER_WIDTH + RPS_HEADER_ID_WIDTH + NUM_POSITIVE_WIDTH- 1:
                                                                HEADER_WIDTH + RPS_HEADER_ID_WIDTH  ])%(1<<NUM_NEG_POS_POC_WIDTH);
                            num_negative_poc <=   (fifo_in[     HEADER_WIDTH + RPS_HEADER_ID_WIDTH + NUM_POSITIVE_WIDTH + NUM_NEGATIVE_WIDTH- 1:
                                                                HEADER_WIDTH + RPS_HEADER_ID_WIDTH + NUM_POSITIVE_WIDTH  ])%(1<<NUM_NEG_POS_POC_WIDTH);                                        
                            num_delta_poc <=      (fifo_in[     HEADER_WIDTH + RPS_HEADER_ID_WIDTH + NUM_POSITIVE_WIDTH- 1:
                                                                HEADER_WIDTH + RPS_HEADER_ID_WIDTH  ] + 

                                                   fifo_in[     HEADER_WIDTH + RPS_HEADER_ID_WIDTH + NUM_POSITIVE_WIDTH + NUM_NEGATIVE_WIDTH- 1:
                                                                HEADER_WIDTH + RPS_HEADER_ID_WIDTH + NUM_POSITIVE_WIDTH  ])%(1<<NUM_NEG_POS_POC_WIDTH);                                                  

`else
                            num_positive_poc <=   (fifo_in[  FIFO_IN_WIDTH - HEADER_WIDTH - RPS_HEADER_ID_WIDTH - 1:
                                                            FIFO_IN_WIDTH - HEADER_WIDTH - RPS_HEADER_ID_WIDTH - NUM_POSITIVE_WIDTH])%(1<<NUM_NEG_POS_POC_WIDTH);
                            num_negative_poc <=   (fifo_in[  FIFO_IN_WIDTH - HEADER_WIDTH - RPS_HEADER_ID_WIDTH - NUM_POSITIVE_WIDTH -1:
                                                            FIFO_IN_WIDTH - HEADER_WIDTH - RPS_HEADER_ID_WIDTH - NUM_POSITIVE_WIDTH - NUM_NEGATIVE_WIDTH])%(1<<NUM_NEG_POS_POC_WIDTH);                                        
                            num_delta_poc <=      (fifo_in[  FIFO_IN_WIDTH - HEADER_WIDTH - RPS_HEADER_ID_WIDTH - 1:
                                                            FIFO_IN_WIDTH - HEADER_WIDTH - RPS_HEADER_ID_WIDTH - NUM_POSITIVE_WIDTH] + 
                                                   fifo_in[  FIFO_IN_WIDTH - HEADER_WIDTH - RPS_HEADER_ID_WIDTH - NUM_POSITIVE_WIDTH -1:
                                                            FIFO_IN_WIDTH - HEADER_WIDTH - RPS_HEADER_ID_WIDTH - NUM_POSITIVE_WIDTH - NUM_NEGATIVE_WIDTH])%(1<<NUM_NEG_POS_POC_WIDTH);                                                  
                                                            
`endif      
                            curr_rps_entry_idx <= 0;   
                            num_pic_st_curr_bef <= 0;
                            num_pic_st_curr_aft <= 0;
                            num_pic_st_fol <= 0;
                        end
                        `HEADER_SLICE_4: begin
                            inter_config_mode_in <= `INTER_TOP_CONFIG_IDLE;
                            if(((curr_rps_entry_idx + 1) % (1<< NUM_NEG_POS_POC_WIDTH))  == num_delta_poc) begin
                                curr_rps_entry_idx <= 0;
                            end
                            else begin
                                curr_rps_entry_idx <= (curr_rps_entry_idx + 1)% (1<< NUM_NEG_POS_POC_WIDTH);
                            end
                            if(fifo_in_used_flag_wire == 0) begin
                                num_pic_st_fol <= num_pic_st_fol + 1;
                            end
                            else begin
                                if(curr_rps_entry_idx < num_positive_poc) begin
                                    num_pic_st_curr_aft <= num_pic_st_curr_aft + 1;
                                end
                                else begin
                                    num_pic_st_curr_bef <= num_pic_st_curr_bef + 1;
                                end
                            end
                        end
                        `HEADER_SLICE_5: begin
                            inter_config_mode_in <= `INTER_SLICE_TILE_INFO;
                            inter_config_bus_in <= fifo_in[FIFO_IN_WIDTH-1: FIFO_IN_WIDTH - INTER_TOP_CONFIG_BUS_WIDTH];    
                        end
                        `HEADER_SLICE_6: begin
                            inter_config_mode_in <= `INTER_SLICE_TILE_INFO;
                            inter_config_bus_in <= fifo_in[FIFO_IN_WIDTH-1: FIFO_IN_WIDTH - INTER_TOP_CONFIG_BUS_WIDTH];    
                        end    
                        `HEADER_SLICE_7: begin
                            inter_config_mode_in <= `INTER_TOP_CONFIG_IDLE;
                        end
                        `HEADER_CTU_0: begin
// `ifdef READ_FILE
                            // ctb_xx <= fifo_in[HEADER_WIDTH + X_ADDR_WDTH   -2: HEADER_WIDTH];                 // 12-bit input assigned to 11- bit output MSB dropped
                            // ctb_yy <= fifo_in[HEADER_WIDTH + 2*X_ADDR_WDTH -2: HEADER_WIDTH + X_ADDR_WDTH];
// `else
                            // ctb_xx <= fifo_in[FIFO_IN_WIDTH - HEADER_WIDTH -2: FIFO_IN_WIDTH - HEADER_WIDTH - X_ADDR_WDTH];                 // 12-bit input assigned to 11- bit output MSB dropped
                            // ctb_yy <= fifo_in[FIFO_IN_WIDTH - HEADER_WIDTH - X_ADDR_WDTH - 2: FIFO_IN_WIDTH - HEADER_WIDTH - 2*X_ADDR_WDTH];
// `endif
                            inter_config_mode_in <= `INTER_CTU0_HEADER;
                            inter_config_bus_in <= fifo_in[FIFO_IN_WIDTH-1: FIFO_IN_WIDTH - INTER_TOP_CONFIG_BUS_WIDTH]; 
                        end
                        `HEADER_CTU_1: begin
                            inter_config_mode_in <= `INTER_TOP_CONFIG_IDLE;
                        end
                        `HEADER_CTU_2_Y: begin
                            inter_config_mode_in <= `INTER_TOP_CONFIG_IDLE;
                        end
                        `HEADER_CTU_2_CB: begin
                            inter_config_mode_in <= `INTER_TOP_CONFIG_IDLE;
                        end
                        `HEADER_CTU_2_CR: begin
                            inter_config_mode_in <= `INTER_TOP_CONFIG_IDLE;
                        end
                        `HEADER_CU_0: begin
                            inter_config_mode_in <= `INTER_CU_HEADER;
                            inter_config_bus_in <= fifo_in[FIFO_IN_WIDTH-1: FIFO_IN_WIDTH - INTER_TOP_CONFIG_BUS_WIDTH];
`ifdef READ_FILE
                            pred_mode <= fifo_in[   HEADER_WIDTH + X0_WIDTH + Y0_WIDTH + LOG2_CB_SIZE_WIDTH + PREDMODE_WIDTH- 1:
                                                    HEADER_WIDTH + X0_WIDTH + Y0_WIDTH + LOG2_CB_SIZE_WIDTH    ];
                            cb_xx <= (  fifo_in[     HEADER_WIDTH + X0_WIDTH - 1: 
                                                            HEADER_WIDTH])%(1<<X11_ADDR_WDTH);
                            cb_yy <= (  fifo_in[     HEADER_WIDTH + 2*X0_WIDTH -1:
                                                            HEADER_WIDTH + X0_WIDTH])%(1<<X11_ADDR_WDTH);
                            cb_part_mode <=  fifo_in[       HEADER_WIDTH + 2*X0_WIDTH + LOG2_CTB_WIDTH + PREDMODE_WIDTH + PARTMODE_WIDTH -1:
                                                            HEADER_WIDTH + 2*X0_WIDTH + LOG2_CTB_WIDTH + PREDMODE_WIDTH];
`else
                            cb_xx <= (  fifo_in[     FIFO_IN_WIDTH - HEADER_WIDTH - 1: 
                                                            FIFO_IN_WIDTH - HEADER_WIDTH - X0_WIDTH])%(1<<X11_ADDR_WDTH);
                            cb_yy <= (  fifo_in[     FIFO_IN_WIDTH - HEADER_WIDTH - X0_WIDTH - 1: 
                                                            FIFO_IN_WIDTH - HEADER_WIDTH - 2*X0_WIDTH])%(1<<X11_ADDR_WDTH);
                            pred_mode <= fifo_in[   FIFO_IN_WIDTH - HEADER_WIDTH - X0_WIDTH - Y0_WIDTH - LOG2_CB_SIZE_WIDTH - 1: 
                                                    FIFO_IN_WIDTH - HEADER_WIDTH - X0_WIDTH - Y0_WIDTH - LOG2_CB_SIZE_WIDTH - PREDMODE_WIDTH];
                            cb_part_mode <=  fifo_in[     FIFO_IN_WIDTH - HEADER_WIDTH - 2*X0_WIDTH - LOG2_CTB_WIDTH - PREDMODE_WIDTH - 1:
                                                                FIFO_IN_WIDTH - HEADER_WIDTH - 2*X0_WIDTH - LOG2_CTB_WIDTH - PREDMODE_WIDTH - PARTMODE_WIDTH];
`endif
                            pb_part_idx <= 0;   // TODO increment this when motion vector is output and before reading next pu - done
                            cb_size <=  cb_size_wire;
                        end
                        `HEADER_PU_0: begin
                            //temp comment these to keep mv derive module inactive
                            inter_config_mode_in <= `INTER_PU_HEADER;
                            inter_config_bus_in <= fifo_in[FIFO_IN_WIDTH-1: FIFO_IN_WIDTH - INTER_TOP_CONFIG_BUS_WIDTH]; 
                            pb_part_idx <= pb_part_idx + 1;
                        end
                        `HEADER_RU_0: begin
                            inter_config_mode_in <= `INTER_TOP_CONFIG_IDLE;
                        end
                        `HEADER_RU_1: begin
                            inter_config_mode_in <= `INTER_TOP_CONFIG_IDLE;
                        end    
                        default: begin
                            inter_config_mode_in <= `INTER_TOP_CONFIG_IDLE;
                        end
                    endcase
                end
                STATE_WRITE_WAIT: begin
                    if(output_fifo_is_full == 1'b0) begin
                        write_en_out <= 1'b1;
                    end
                end
                STATE_GET_ST_RPS_HEADER: begin
                    num_pic_st_curr_bef <= 0;
                    num_pic_st_curr_aft <= 0;  
                    num_pic_st_fol <= 0;
                    num_positive_poc <= rps_header_data_out[RPS_HEADER_NUM_POS_POC_RANGE_HIGH:RPS_HEADER_NUM_POS_POC_RANGE_LOW];
                    num_negative_poc <= rps_header_data_out[RPS_HEADER_NUM_NEG_POC_RANGE_HIGH:RPS_HEADER_NUM_NEG_POC_RANGE_LOW];
                    num_delta_poc    <= rps_header_data_out[RPS_HEADER_NUM_DELTA_POC_RANGE_HIGH:RPS_HEADER_NUM_DELTA_POC_RANGE_LOW];
                    curr_rps_entry_idx <= 0;
                end
                STATE_GET_ST_RPS_ENTRY: begin

                    if(((curr_rps_entry_idx + 1) % (1<< NUM_NEG_POS_POC_WIDTH))  == num_delta_poc) begin
                        curr_rps_entry_idx <= 0;
                    end
                    else begin
                        curr_rps_entry_idx <= (curr_rps_entry_idx + 1)% (1<< NUM_NEG_POS_POC_WIDTH);
                    end
                    if(rps_entry_data_out[RPS_ENTRY_USED_FLAG_RANGE_HIGH] == 0) begin
                        num_pic_st_fol <= num_pic_st_fol + 1;
                    end
                    else begin
                        if(curr_rps_entry_idx < num_positive_poc) begin
                            num_pic_st_curr_aft <= num_pic_st_curr_aft + 1;
                        end
                        else begin
                            num_pic_st_curr_bef <= num_pic_st_curr_bef + 1;
                        end
                    end
                end
                STATE_SLICE_1_WRITE_WAIT: begin
                    num_pic_st_curr_bef <= 0;
                    num_pic_st_curr_aft <= 0;
                    num_pic_st_fol <= 0;
                    if(output_fifo_is_full == 1'b0) begin
                        write_en_out <= 1'b1;
                        num_positive_poc <= rps_header_data_out[RPS_HEADER_NUM_POS_POC_RANGE_HIGH:RPS_HEADER_NUM_POS_POC_RANGE_LOW];
                        num_negative_poc <= rps_header_data_out[RPS_HEADER_NUM_NEG_POC_RANGE_HIGH:RPS_HEADER_NUM_NEG_POC_RANGE_LOW];
                        num_delta_poc    <= rps_header_data_out[RPS_HEADER_NUM_DELTA_POC_RANGE_HIGH:RPS_HEADER_NUM_DELTA_POC_RANGE_LOW];
                        curr_rps_entry_idx <= 0;                                     
                    end
                end
                STATE_DPB_ADD_PREV_PIC: begin
                    if(initial_condition == 1) begin
                        dpb_filled_flag[current_pic_dpb_idx] <= 1'b1;     
                    end
                end
                STATE_CURRENT_POC: begin
                    initial_condition <= 1;
                    curr_rps_entry_idx <= 0;
                    current__poc <=  fifo_in[    FIFO_IN_WIDTH - 1:
                                                FIFO_IN_WIDTH - FIFO_OUT_WIDTH];
                end
                STATE_CUR_PIC_FIL_IDX: begin
                    fifo_out <= {`HEADER_SLICE_0,curr_rps_entry_idx,{(FIFO_OUT_WIDTH - HEADER_WIDTH - NUM_NEG_POS_POC_WIDTH){1'b0}}};
                    if(dpb_filled_flag[curr_rps_entry_idx] == 0) begin
                        current_pic_dpb_idx <= curr_rps_entry_idx;
                        if(output_fifo_is_full == 0) begin
                            write_en_out <= 1;
                        end

                        inter_config_mode_in <= `INTER_CURRENT_PIC_DPB_IDX;
                        inter_config_bus_in <= {curr_rps_entry_idx,{(INTER_TOP_CONFIG_BUS_WIDTH - DPB_ADDR_WIDTH){1'b0}}};  
                    end
                    else begin 
                        curr_rps_entry_idx <= (curr_rps_entry_idx + 1)%(1<<NUM_NEG_POS_POC_WIDTH); 
                    end
                end
                STATE_REF_PIC_LIST_5_UPDATE: begin
                    curr_rps_entry_idx <= (curr_rps_entry_idx + 1)%(1<<NUM_NEG_POS_POC_WIDTH); 
                    num_pic_st_curr_total <= (num_pic_st_curr_aft + num_pic_st_curr_bef)%(1<<NUM_REF_IDX_L0_MINUS1_WIDTH);
                end
                STATE_WRITE_WAIT_CUR_DPB_IDX_TO_DBF: begin
                    if(output_fifo_is_full == 1'b0) begin
                        write_en_out <= 1'b1;
                    end
                end
                STATE_SEND_POC_TO_DBF: begin
                    //dpb_filled_flag <= dpb_filled_flag_new;
                    //if(write_ref_pic_state == WRITE_REF_PIC_STATE_DONE) begin
                        inter_config_mode_in <= `INTER_TOP_CURR_POC;
                        inter_config_bus_in <= current__poc; 
                        fifo_out <= {current__poc};
                        if(output_fifo_is_full == 1'b0) begin
                            write_en_out <= 1'b1;
                        end                             
                    //end
                    //else begin
                    //    inter_config_mode_in <= `INTER_REF_PIC_TRANSFER;
                    //end
                end
                STATE_REF_PIC_LIST_TRANSFER: begin
                    if(!(write_ref_pic_state == WRITE_REF_PIC_STATE_DONE))  begin
                        inter_config_mode_in <= `INTER_REF_PIC_TRANSFER;
                    end
                    else begin
                        dpb_filled_flag <= dpb_filled_flag_new;                    
                    end
                end
                STATE_MV_RETURN_WAIT1: begin
                    inter_config_mode_in <= `INTER_TOP_CONFIG_IDLE;
                end
                STATE_MV_RETURN_WAIT2,STATE_MV_RETURN_WAIT3: begin
                    inter_config_mode_in <= `INTER_TOP_CONFIG_IDLE;
                end
                STATE_MVD1_READ: begin
                    inter_config_mode_in <= `INTER_MVD_0_INFO;
                    inter_config_bus_in <= fifo_in[FIFO_IN_WIDTH - 1: FIFO_IN_WIDTH - INTER_TOP_CONFIG_BUS_WIDTH];
                end
                STATE_MVD2_READ: begin
                    inter_config_mode_in <= `INTER_MVD_1_INFO;
                    inter_config_bus_in <= fifo_in[FIFO_IN_WIDTH - 1: FIFO_IN_WIDTH - INTER_TOP_CONFIG_BUS_WIDTH];
                end
                // STATE_PU_TEMP_STORE: begin
                    // pu_config_bus_in_stack1 <= fifo_in[FIFO_IN_WIDTH - 1: FIFO_IN_WIDTH - INTER_TOP_CONFIG_BUS_WIDTH];
                    // pu_config_bus_in_stack2 <= pu_config_bus_in_stack1;
                    // pu_config_bus_in_stack3 <= pu_config_bus_in_stack2;
                    // pb_part_idx <= pb_part_idx + 1;
                // end
                STATE_RU_PACKET_READ: begin
                    if(c_idx_wire == 0) begin /// assert fifo header == RU_HEADER_0
                        tb_size <= tb_size_wire;
                        x0_tu_in_ctu <= x0_tu_in_ctu_wire;
                        y0_tu_in_ctu <= y0_tu_in_ctu_wire;
                    end
                    if(pred_pixl_gen_idle_out_to_pred_top) begin
                        inter_config_mode_in <= `INTER_RU_0_HEADER;
                        inter_config_bus_in <= fifo_in[FIFO_IN_WIDTH - 1: FIFO_IN_WIDTH - INTER_TOP_CONFIG_BUS_WIDTH];
                    end
                    else begin
                        inter_config_mode_in <= `INTER_TOP_CONFIG_IDLE;
                    end

                end
                default: begin
                end
            endcase
        end 
    end

endmodule 