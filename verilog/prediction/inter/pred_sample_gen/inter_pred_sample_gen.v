`timescale 1ns / 1ps
module inter_pred_sample_gen(
    clk,
    reset,
    config_data_bus_in ,
    config_mode_in ,
    tu_empty,
    tu_data_in,

    next_mv_field_pred_flag_l0_in,
    next_mv_field_pred_flag_l1_in,

    next_mv_field_mv_x_l0_in,
    next_mv_field_mv_y_l0_in,
    next_mv_field_mv_x_l1_in,
    next_mv_field_mv_y_l1_in,    
    next_dpb_idx_l0_in,    
    next_dpb_idx_l1_in,    
    next_mv_field_valid_in, 

    xT_in_min_luma_out ,
    yT_in_min_luma_out ,
    xT_in_min_cr_out   ,
    yT_in_min_cr_out   ,
    yT_in_min_cb_out   ,
    xT_in_min_cb_out   ,
    luma_wgt_pred_out  ,
    cb_wgt_pred_out    ,
    cr_wgt_pred_out    , 
    luma_wght_pred_valid ,
    cb_wght_pred_valid   ,
    cr_wght_pred_valid   ,



    ref_pix_axi_ar_addr,
    ref_pix_axi_ar_len,
    ref_pix_axi_ar_size,
    ref_pix_axi_ar_burst,
    ref_pix_axi_ar_prot,
    ref_pix_axi_ar_valid,
    ref_pix_axi_ar_ready,
    ref_pix_axi_r_data,
    ref_pix_axi_r_resp,
    ref_pix_axi_r_last,
    ref_pix_axi_r_valid,
    ref_pix_axi_r_ready,    
    
    pred_pixl_gen_idle_out_to_pred_top,
    pred_pixl_gen_idle_out_to_mv_derive
    
    
    
);
//---------------------------------------------------------------------------------------------------------------------
// Global constant headers
//---------------------------------------------------------------------------------------------------------------------
    `include "pred_def.v"
    `include "inter_axi_def.v"
//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------
    localparam                          STATE_INTER_PRED_SAMPLE_CONFIG_UPDATE    = 0;
    localparam                          STATE_INTER_PRED_SAMPLE_FILL_TU_DATA     = 1;
    localparam                          STATE_INTER_PRED_SAMPLE_X_8_Y_8_UPDATE   = 2;
    localparam                          STATE_INTER_PRED_SAMPLE_PU_SELECT        = 3;
    localparam                          STATE_INTER_PRED_MV_INT_DECODE           = 4;
    localparam                          STATE_INTER_PRED_MV_FRAC_DECODE          = 5;
    localparam                          STATE_INTER_PRED_MV_DECODE_READY_BI      = 6;
    localparam                          STATE_INTER_PRED_MV_DECODE_READY         = 7;
    localparam                          STATE_INTER_PRED_MV_FRAC_BI_DECODE       = 8;
    
    
    parameter                           REF_BLOCK_DIM_WDTH		    	= 4;
    parameter                           BLOCK_WIDTH_4x4                 = 3'd4;
    parameter                           BLOCK_WIDTH_2x2                 = 2'd2;
    parameter                           CHMA_DIM_WDTH               = 3;        // max 5 
    parameter                           LUMA_DIM_WDTH               = 4;        // max 11 
    parameter                           LUMA_REF_BLOCK_WIDTH        = 4'd11;
    parameter                           CHMA_REF_BLOCK_WIDTH        = 3'd5;
    
//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    
    input clk;
    input reset;
    input tu_empty;
    input [INTER_TOP_CONFIG_BUS_WIDTH-1:0] tu_data_in;
    input [INTER_TOP_CONFIG_BUS_WIDTH -1:0] config_data_bus_in;
    input [INTER_TOP_CONFIG_BUS_MODE_WIDTH -1 :0] config_mode_in;

    input                                  next_mv_field_pred_flag_l0_in;
    input                                  next_mv_field_pred_flag_l1_in;
    // input [REF_IDX_LX_WIDTH -1:0]          next_mv_field_ref_idx_l0_in;
    // input [REF_IDX_LX_WIDTH -1:0]          next_mv_field_ref_idx_l1_in;
    input signed [MVD_WIDTH -1:0]          next_mv_field_mv_x_l0_in;
    input signed [MVD_WIDTH -1:0]          next_mv_field_mv_y_l0_in;
    input signed [MVD_WIDTH -1:0]          next_mv_field_mv_x_l1_in;
    input signed [MVD_WIDTH -1:0]          next_mv_field_mv_y_l1_in;    
    input [DPB_ADDR_WIDTH -1:0]            next_dpb_idx_l0_in;    
    input [DPB_ADDR_WIDTH -1:0]            next_dpb_idx_l1_in;    
    input                                  next_mv_field_valid_in; 
    
    // input  [X11_ADDR_WDTH - 1:0]           next_xx_pb_in;
    // input  [CB_SIZE_WIDTH -1:0 ]           next_hh_pb_in;
    // input  [Y11_ADDR_WDTH - 1:0]           next_yy_pb_in;
    // input  [CB_SIZE_WIDTH -1:0 ]           next_ww_pb_in;
    
    output wire [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]                               xT_in_min_luma_out       ;
    output wire [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]                               yT_in_min_luma_out       ;
    output wire [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]                               xT_in_min_cr_out         ;
    output wire [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]                               yT_in_min_cr_out         ;
    output wire [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]                               yT_in_min_cb_out         ;
    output wire [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]                               xT_in_min_cb_out         ;
    output        [BLOCK_WIDTH_4x4 *  BLOCK_WIDTH_4x4 *     PIXEL_WIDTH -1:0]          luma_wgt_pred_out        ;    
    output        [BLOCK_WIDTH_4x4 *  BLOCK_WIDTH_4x4 *     PIXEL_WIDTH -1:0]          cb_wgt_pred_out          ;    
    output        [BLOCK_WIDTH_4x4 *  BLOCK_WIDTH_4x4 *     PIXEL_WIDTH -1:0]          cr_wgt_pred_out          ;     
    output                                                                             luma_wght_pred_valid     ;
    output                                                                             cb_wght_pred_valid       ;
    output                                                                             cr_wght_pred_valid       ;

    output  reg                             pred_pixl_gen_idle_out_to_pred_top;
    output  reg                             pred_pixl_gen_idle_out_to_mv_derive;

    // axi master interface         
    output      [AXI_ADDR_WDTH-1:0]                 ref_pix_axi_ar_addr;
    output      [7:0]                               ref_pix_axi_ar_len;
    output      [2:0]                               ref_pix_axi_ar_size;
    output      [1:0]                               ref_pix_axi_ar_burst;
    output      [2:0]                               ref_pix_axi_ar_prot;
    output                                          ref_pix_axi_ar_valid;
    input                                           ref_pix_axi_ar_ready;
                
    input       [AXI_DATA_WDTH-1:0]                 ref_pix_axi_r_data;
    input       [1:0]                               ref_pix_axi_r_resp;
    input                                           ref_pix_axi_r_last;
    input                                           ref_pix_axi_r_valid;
    output                                          ref_pix_axi_r_ready;
//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------
    integer inter_pred_sample_state;
    integer inter_pred_sample_next_state;
    
    reg                                  prev_mv_field_pred_flag_l0 [MAX_MV_PER_CU -1: 0];
    reg                                  prev_mv_field_pred_flag_l1 [MAX_MV_PER_CU -1: 0];
    // reg [REF_IDX_LX_WIDTH -1:0]          prev_mv_field_ref_idx_l0 [MAX_MV_PER_CU -1: 0];
    // reg [REF_IDX_LX_WIDTH -1:0]          prev_mv_field_ref_idx_l1 [MAX_MV_PER_CU -1: 0];
    reg signed [MVD_WIDTH -1:0]          prev_mv_field_mv_x_l0 [MAX_MV_PER_CU -1: 0];
    reg signed [MVD_WIDTH -1:0]          prev_mv_field_mv_y_l0 [MAX_MV_PER_CU -1: 0];
    reg signed [MVD_WIDTH -1:0]          prev_mv_field_mv_x_l1 [MAX_MV_PER_CU -1: 0];
    reg signed [MVD_WIDTH -1:0]          prev_mv_field_mv_y_l1 [MAX_MV_PER_CU -1: 0];    
    reg [DPB_ADDR_WIDTH -1:0]            prev_dpb_idx_l0 [MAX_MV_PER_CU -1: 0];    
    reg [DPB_ADDR_WIDTH -1:0]            prev_dpb_idx_l1 [MAX_MV_PER_CU -1: 0];    
    reg                                  prev_mv_field_valid [MAX_MV_PER_CU -1: 0]; 
    
    reg                                  cand_mv_field_pred_flag_l0;
    reg                                  cand_mv_field_pred_flag_l1;
    // reg [REF_IDX_LX_WIDTH -1:0]          cand_mv_field_ref_idx_l0;
    // reg [REF_IDX_LX_WIDTH -1:0]          cand_mv_field_ref_idx_l1;
    reg signed [MVD_WIDTH -1:0]          cand_mv_field_mv_x_l0;
    reg signed [MVD_WIDTH -1:0]          cand_mv_field_mv_y_l0;
    reg signed [MVD_WIDTH -1:0]          cand_mv_field_mv_x_l1;
    reg signed [MVD_WIDTH -1:0]          cand_mv_field_mv_y_l1;    
    reg [DPB_ADDR_WIDTH -1:0]            cand_dpb_idx_l0;    
    reg [DPB_ADDR_WIDTH -1:0]            cand_dpb_idx_l1;    
    
 

    
    
    wire  [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]           xx_pb0_strt_in_min_tus;
    wire  [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]           xx_pb0_end__in_min_tus;
    wire  [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]           yy_pb0_strt_in_min_tus;
    wire  [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]           yy_pb0_end__in_min_tus;

    wire  [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]           xx_pb1_strt_in_min_tus;
    wire  [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]           xx_pb1_end__in_min_tus;
    wire  [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]           yy_pb1_strt_in_min_tus;
    wire  [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]           yy_pb1_end__in_min_tus;
    
    wire  [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]           xx_pb2_strt_in_min_tus;
    wire  [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]           xx_pb2_end__in_min_tus;
    wire  [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]           yy_pb2_strt_in_min_tus;
    wire  [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]           yy_pb2_end__in_min_tus;
    
    wire  [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]           xx_pb3_strt_in_min_tus;
    wire  [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]           xx_pb3_end__in_min_tus;
    wire  [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]           yy_pb3_strt_in_min_tus;
    wire  [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]           yy_pb3_end__in_min_tus;
    
    
    reg  [PART_IDX_WIDTH:0]  pb_part_idx;
    reg  [MAX_MV_PER_CU -1:0]   pb_part_idx_decode;
    
    reg pu_0_in_range;
    reg pu_1_in_range;
    reg pu_2_in_range;
    reg pu_3_in_range;
    
    reg                         now_cb_pred_mode;    
    reg                         next_cb_pred_mode;    
    reg  [PARTMODE_WIDTH -1:0]  now_cb_part_mode;
    reg  [PARTMODE_WIDTH -1:0]  next_cb_part_mode;
    
    reg  [CB_SIZE_WIDTH - LOG2_MIN_TU_SIZE -1:0 ]  now_cb_size_in_min_tus;
    reg  [CB_SIZE_WIDTH - LOG2_MIN_TU_SIZE -1:0 ]  next_cb_size_in_min_tus;
    
    reg  [CB_SIZE_WIDTH - LOG2_MIN_TU_SIZE -1:0 ]  cb_size_wire_in_min_pus;
    wire [LOG2_CB_SIZE_WIDTH -1:0 ]  log2_cb_size_wire;
    
    reg  [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] now_cb_xx_end_in_min_tus;
    reg  [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] now_cb_yy_end_in_min_tus;
    
    
    reg  [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] now_ctb_xx_in_min_tus;
    reg  [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] next_ctb_xx_in_min_tus;
    reg  [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] now_ctb_yy_in_min_tus;
    reg  [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] next_ctb_yy_in_min_tus;
    
    
    reg  [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] next_cb_xx_in_min_tus;
    reg  [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] next_cb_yy_in_min_tus;
    reg  [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] now_cb_yy_in_min_tus;
    reg  [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] now_cb_xx_in_min_tus;
    
    wire  [XT_WIDTH -1:0 ]          x0_tu_in_ctu_wire;
    wire  [XT_WIDTH -1:0 ]          y0_tu_in_ctu_wire;    
    
    reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] xT_in_min_tus;
    reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] yT_in_min_tus;
    reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] next_xT_in_min_tus;
    reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] next_yT_in_min_tus;
    
    wire [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] xT_in_min_tus_out;
    wire [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] yT_in_min_tus_out;

    wire [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] xT_in_min_luma;
    wire [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] yT_in_min_luma;

    wire [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] xT_in_min_cb;
    wire [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] yT_in_min_cb;

    wire [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] xT_in_min_cr;
    wire [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] yT_in_min_cr;

    reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] x_8x8_loc;
    reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] next_x_8x8_loc;
    reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] y_8x8_loc;    
    reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] next_y_8x8_loc;  

    reg inner_x_8x8_loc;
    reg next_inner_x_8x8_loc;
    reg inner_y_8x8_loc;
    reg next_inner_y_8x8_loc;
    
    reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] x0_tu_in_min_tus;
    reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] x0_tu_end_in_min_tus;
    // reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] y0_tu_in_min_tus;
    reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] y0_tu_end_in_min_tus;
    
    reg [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH:0] luma_l0_ref_start_x_wire;
    reg [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH:0] luma_l1_ref_start_x_wire;
    reg [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH:0] luma_l0_ref_start_y_wire;
    reg [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH:0] luma_l1_ref_start_y_wire;
    reg [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH:0] chma_l0_ref_start_x_wire;
    reg [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH:0] chma_l0_ref_start_y_wire;
    reg [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH:0] chma_l1_ref_start_x_wire;
    reg [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH:0] chma_l1_ref_start_y_wire;
    
    reg [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1:0] luma_ref_start_x;
    reg [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1:0] luma_ref_start_y;
    reg [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1:0] chma_ref_start_x;
    reg [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1:0] chma_ref_start_y;

    
    reg [REF_BLOCK_DIM_WDTH - 1:0]   chma_ref_width_x            ;
    reg [REF_BLOCK_DIM_WDTH - 1:0]   chma_ref_height_y           ;
    reg [REF_BLOCK_DIM_WDTH - 1:0]   luma_ref_width_x            ;
    reg [REF_BLOCK_DIM_WDTH - 1:0]   luma_ref_height_y           ;

    reg [REF_BLOCK_DIM_WDTH - 1:0]   chma_l0_ref_width_x_wire    ;
    reg [REF_BLOCK_DIM_WDTH - 1:0]   chma_l1_ref_width_x_wire    ;
    reg [REF_BLOCK_DIM_WDTH - 1:0]   chma_l0_ref_height_y_wire   ;
    reg [REF_BLOCK_DIM_WDTH - 1:0]   chma_l1_ref_height_y_wire   ;
    
    reg [REF_BLOCK_DIM_WDTH - 1:0]   luma_l0_ref_width_x_wire    ;
    reg [REF_BLOCK_DIM_WDTH - 1:0]   luma_l1_ref_width_x_wire    ;
    reg [REF_BLOCK_DIM_WDTH - 1:0]   luma_l0_ref_height_y_wire   ;
    reg [REF_BLOCK_DIM_WDTH - 1:0]   luma_l1_ref_height_y_wire   ;
    
    reg                              ref_range_valid_out         ;
    reg [DPB_ADDR_WIDTH -1:0]        ref_dpb_idx;   
        
    wire [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1: 0] luma_l0_mid_x_wire;
    wire [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1: 0] luma_l0_mid_y_wire;
    wire [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1: 0] chma_l0_mid_x_wire;
    wire [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1: 0] chma_l0_mid_y_wire;
    wire [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1: 0] luma_l1_mid_x_wire;
    wire [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1: 0] luma_l1_mid_y_wire;
    wire [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1: 0] chma_l1_mid_x_wire;
    wire [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1: 0] chma_l1_mid_y_wire;
    
    reg [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1: 0] luma_l0_mid_x;
    reg [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1: 0] luma_l0_mid_y;
    reg [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1: 0] chma_l0_mid_x;
    reg [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1: 0] chma_l0_mid_y;
    reg [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1: 0] luma_l1_mid_x;
    reg [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1: 0] luma_l1_mid_y;
    reg [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1: 0] chma_l1_mid_x;
    reg [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1: 0] chma_l1_mid_y;

    wire  [CIDX_WIDTH -1:0 ]  c_idx_wire;
    
    reg end_of_cu;
    reg end_of_tu;
    reg is_pu_header_cus_last;
    
    reg         [PIC_DIM_WIDTH-1:0]                 pic_width;
    reg         [PIC_DIM_WIDTH-1:0]                 pic_height;
    
    wire cache_idle_out;
    wire cache_idle_in;// =(!ref_dpb_idx)?1:cache_idle_out;//= 1;  // to do connect this to cache
    assign  cache_idle_in = cache_idle_out;
    wire [LOG2_CB_SIZE_WIDTH -1:0 ]  log2_tb_size_wire;

    reg [MV_C_FRAC_WIDTH_HIGH -1:0] ch_frac_x;
    reg [MV_C_FRAC_WIDTH_HIGH -1:0] ch_frac_y;

    wire cache_block_ready;


    wire  [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1:0]  luma_ref_start_x_out   ;
    wire  [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1:0]  luma_ref_start_y_out   ;
    wire  [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1:0]  chma_ref_start_x_out   ;
    wire  [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1:0]  chma_ref_start_y_out   ;

    wire   [REF_BLOCK_DIM_WDTH - 1:0]                   chma_ref_width_x_out   ;
    wire   [REF_BLOCK_DIM_WDTH - 1:0]                   chma_ref_height_y_out  ;
    wire   [REF_BLOCK_DIM_WDTH - 1:0]                   luma_ref_width_x_out   ;
    wire   [REF_BLOCK_DIM_WDTH - 1:0]                   luma_ref_height_y_out  ;

    wire  [PIXEL_WIDTH* LUMA_REF_BLOCK_WIDTH* LUMA_REF_BLOCK_WIDTH -1:0]     block_121_out ;
    wire  [PIXEL_WIDTH* CHMA_REF_BLOCK_WIDTH* CHMA_REF_BLOCK_WIDTH -1:0]     block_25cb_out;
    wire  [PIXEL_WIDTH* CHMA_REF_BLOCK_WIDTH* CHMA_REF_BLOCK_WIDTH -1:0]     block_25cr_out;

    wire         [LUMA_DIM_WDTH-1:0]                 block_x_offset_luma;
    wire         [LUMA_DIM_WDTH-1:0]                 block_y_offset_luma;
    wire         [CHMA_DIM_WDTH-1:0]                 block_x_offset_chma;
    wire         [CHMA_DIM_WDTH-1:0]                 block_y_offset_chma; 

    wire         [LUMA_DIM_WDTH-1:0]                 block_x_end_luma;
    wire         [LUMA_DIM_WDTH-1:0]                 block_y_end_luma;
    wire         [CHMA_DIM_WDTH-1:0]                 block_x_end_chma;
    wire         [CHMA_DIM_WDTH-1:0]                 block_y_end_chma; 


    wire        luma_filter_idle;
    wire        cb_filter_idle;
    wire        cr_filter_idle;

    wire        [MV_C_FRAC_WIDTH_HIGH -1:0]         ch_frac_x_cache_out;
    wire        [MV_C_FRAC_WIDTH_HIGH -1:0]         ch_frac_y_cache_out;

    wire        luma_filter_ready;
    wire        cb_filter_ready;
    wire        cr_filter_ready;


    wire        [BLOCK_WIDTH_4x4 *  BLOCK_WIDTH_4x4 * FILTER_PIXEL_WIDTH -1:0]         luma_filter_out;    
    wire        [BLOCK_WIDTH_2x2 *  BLOCK_WIDTH_2x2 * FILTER_PIXEL_WIDTH -1:0]         cb_filter_out;    
    wire        [BLOCK_WIDTH_2x2 *  BLOCK_WIDTH_2x2 * FILTER_PIXEL_WIDTH -1:0]         cr_filter_out;    

//---------------------------------------------------------------------------------------------------------------------
// Implmentation
//---------------------------------------------------------------------------------------------------------------------

// Instantiate the module

inter_cache cache_block
(
    .clk(clk),
    .reset(reset),
    .luma_ref_start_x_in         (luma_ref_start_x)  ,
    .luma_ref_start_y_in         (luma_ref_start_y)  ,
    .chma_ref_start_x_in         (chma_ref_start_x)  ,
    .chma_ref_start_y_in         (chma_ref_start_y)  ,
        
    .chma_ref_width_x_in         (chma_ref_width_x)  ,
    .luma_ref_height_y_in        (luma_ref_height_y)  ,
    .chma_ref_height_y_in        (chma_ref_height_y)  ,
    .luma_ref_width_x_in         (luma_ref_width_x)  ,

    .luma_ref_start_x_out      (luma_ref_start_x_out    )              ,
    .luma_ref_start_y_out      (luma_ref_start_y_out    )              ,
    .chma_ref_start_x_out      (chma_ref_start_x_out    )              ,
    .chma_ref_start_y_out      (chma_ref_start_y_out    )              ,
    .luma_ref_width_x_out      (luma_ref_width_x_out    )             ,
    .chma_ref_width_x_out      (chma_ref_width_x_out    )             ,
    .luma_ref_height_y_out     (luma_ref_height_y_out   )             ,
    .chma_ref_height_y_out     (chma_ref_height_y_out   )             ,

    .block_x_offset_luma        (    block_x_offset_luma ) ,
    .block_y_offset_luma        (    block_y_offset_luma ) ,
    .block_x_offset_chma        (    block_x_offset_chma ) ,
    .block_y_offset_chma        (    block_y_offset_chma ) ,
    .block_x_end_luma           (    block_x_end_luma    ) ,
    .block_y_end_luma           (    block_y_end_luma    ) ,
    .block_x_end_chma           (    block_x_end_chma    ) ,
    .block_y_end_chma           (    block_y_end_chma    ) ,


    .cache_idle_out           (cache_idle_out)  ,
    
    .pic_width                ({3'd0,pic_width})  ,
    .pic_height               ({3'd0,pic_height})  ,
            
    .ch_frac_x                (ch_frac_x)  ,
    .ch_frac_y                (ch_frac_y)  ,
    .ch_frac_x_out            (ch_frac_x_cache_out)  ,
    .ch_frac_y_out            (ch_frac_y_cache_out),           
    .xT_in_min_tus_in         (xT_in_min_tus)  ,
    .yT_in_min_tus_in         (yT_in_min_tus)  ,
    .xT_in_min_tus_out        (xT_in_min_tus_out)  ,
    .yT_in_min_tus_out        (yT_in_min_tus_out)  ,
            
    .filer_idle_in            (luma_filter_idle & cb_filter_idle & cr_filter_idle)  ,
    .ref_idx_in_in            (ref_dpb_idx)  ,
    .valid_in                 (ref_range_valid_out)  ,
    .block_121_out            (block_121_out )  ,
    .block_25cb_out           (block_25cb_out)  ,
    .block_25cr_out           (block_25cr_out)  ,
    .block_ready              (cache_block_ready)  ,
            
    .ref_pix_axi_ar_addr      (ref_pix_axi_ar_addr   )  ,
    .ref_pix_axi_ar_len       (ref_pix_axi_ar_len    )  ,
    .ref_pix_axi_ar_size      (ref_pix_axi_ar_size   )  ,
    .ref_pix_axi_ar_burst     (ref_pix_axi_ar_burst  )  ,
    .ref_pix_axi_ar_prot      (ref_pix_axi_ar_prot   )  ,   
    .ref_pix_axi_ar_valid     (ref_pix_axi_ar_valid  )  ,
    .ref_pix_axi_ar_ready     (ref_pix_axi_ar_ready  )  ,
    .ref_pix_axi_r_data       (ref_pix_axi_r_data    )  ,
    .ref_pix_axi_r_resp       (ref_pix_axi_r_resp    )  ,
    .ref_pix_axi_r_last       (ref_pix_axi_r_last    )  ,
    .ref_pix_axi_r_valid      (ref_pix_axi_r_valid   )  ,
    .ref_pix_axi_r_ready      (ref_pix_axi_r_ready   )


);

 

luma_filter_wrapper luma_filter_wrapper_block(
    .clk                   (clk)      ,
    .reset                 (reset)      ,
    .ref_l_start_x         (luma_ref_start_x_out)    ,
    .ref_l_start_y         (luma_ref_start_y_out)    ,
    .ref_l_height          (luma_ref_height_y_out)   ,
    .ref_l_width           (luma_ref_width_x_out)    ,
    .block_start_x         (block_x_offset_luma)     ,
    .block_start_y         (block_y_offset_luma)     ,
    .block_end_x           (block_x_end_luma)      ,
    .block_end_y           (block_y_end_luma)      ,
    .xT_4x4_in             (xT_in_min_tus_out)      ,
    .yT_4x4_in             (yT_in_min_tus_out)      ,    
    .l_frac_x              (ch_frac_x_cache_out[MV_L_FRAC_WIDTH_HIGH-1:0])      ,
    .l_frac_y              (ch_frac_y_cache_out[MV_L_FRAC_WIDTH_HIGH-1:0])      ,
    
    .ref_pix_l_in          (block_121_out)           ,    
    .valid_in              (cache_block_ready)       ,
    
    .pic_width               ({1'd0,pic_width})  ,
    .pic_height              ({1'd0,pic_height})  ,

    .block_ready_out        (luma_filter_ready)   ,
    .filter_idle_out        (luma_filter_idle)     ,

    
    .pred_pix_out_4x4       (luma_filter_out)    ,
    .xT_4x4_out             (xT_in_min_luma)     ,
    .yT_4x4_out             (yT_in_min_luma)
);

 
 
   
   
chroma_filter_wrapper cb_filter_wrapper (
    .clk(clk), 
    .reset(reset), 
    .ref_c_start_x      (chma_ref_start_x_out), 
    .ref_c_start_y      (chma_ref_start_y_out), 
    .ref_c_height       (chma_ref_height_y_out), 
    .ref_c_width        (chma_ref_width_x_out), 
    .block_start_x_ch   (block_x_offset_chma), 
    .block_start_y_ch   (block_y_offset_chma), 
    .block_end_x_ch     (block_x_end_chma), 
    .block_end_y_ch     (block_y_end_chma), 
    .xT_4x4_in          (xT_in_min_tus_out), 
    .yT_4x4_in          (yT_in_min_tus_out), 
    .c_frac_x           (ch_frac_x_cache_out), 
    .c_frac_y           (ch_frac_y_cache_out), 
    .ref_pix_l_in       (block_25cb_out), 
    .valid_in           (cache_block_ready), 
    .pic_width           ({1'd0,pic_width})  ,
    .pic_height          ({1'd0,pic_height})  , 
    .block_ready_out    (cb_filter_ready), 
    .filter_idle_out    (cb_filter_idle), 
    .pred_pix_out_4x4   (cb_filter_out), 
    .xT_4x4_out         (xT_in_min_cb), 
    .yT_4x4_out         (yT_in_min_cb)
    );



   
chroma_filter_wrapper cr_filter_wrapper (
    .clk(clk), 
    .reset(reset), 
    .ref_c_start_x      (chma_ref_start_x_out), 
    .ref_c_start_y      (chma_ref_start_y_out), 
    .ref_c_height       (chma_ref_height_y_out), 
    .ref_c_width        (chma_ref_width_x_out), 
    .block_start_x_ch   (block_x_offset_chma), 
    .block_start_y_ch   (block_y_offset_chma), 
    .block_end_x_ch     (block_x_end_chma), 
    .block_end_y_ch     (block_y_end_chma), 
    .xT_4x4_in          (xT_in_min_tus_out), 
    .yT_4x4_in          (yT_in_min_tus_out), 
    .c_frac_x           (ch_frac_x_cache_out), 
    .c_frac_y           (ch_frac_y_cache_out), 
    .ref_pix_l_in       (block_25cr_out), 
    .valid_in           (cache_block_ready), 
    .pic_width           ({1'd0,pic_width})  ,
    .pic_height          ({1'd0,pic_height})  ,
    .block_ready_out    (cr_filter_ready), 
    .filter_idle_out    (cr_filter_idle), 
    .pred_pix_out_4x4   (cr_filter_out), 
    .xT_4x4_out         (xT_in_min_cr), 
    .yT_4x4_out         (yT_in_min_cr)
    );


weighted_sample_prediction weight_pred_luma_block (
    .clk(clk), 
    .reset(reset), 
    .xT_in_min_tus(xT_in_min_luma), 
    .yT_in_min_tus(yT_in_min_luma), 
    .xT_out_min_tus(xT_in_min_luma_out), 
    .yT_out_min_tus(yT_in_min_luma_out), 
    .valid_in(luma_filter_ready), 
    .pred_pix_in_4x4(luma_filter_out), 
    .pred_pix_out_4x4(luma_wgt_pred_out), 
    .valid_out(luma_wght_pred_valid)
    );

chroma_weight_pred weight_pred_cb_block (
    .clk(clk), 
    .reset(reset), 
    .xT_in_min_tus(xT_in_min_cb), 
    .yT_in_min_tus(yT_in_min_cb), 
    .xT_out_min_tus(xT_in_min_cb_out), 
    .yT_out_min_tus(yT_in_min_cb_out), 
    .valid_in(cb_filter_ready), 
    .pred_pix_in_4x4(cb_filter_out), 
    .pred_pix_out_4x4(cb_wgt_pred_out), 
    .valid_out(cb_wght_pred_valid)
    );

chroma_weight_pred weight_pred_cr_block (
    .clk(clk), 
    .reset(reset), 
    .xT_in_min_tus(xT_in_min_cr), 
    .yT_in_min_tus(yT_in_min_cr), 
    .xT_out_min_tus(xT_in_min_cr_out), 
    .yT_out_min_tus(yT_in_min_cr_out), 
    .valid_in(cr_filter_ready), 
    .pred_pix_in_4x4(cr_filter_out), 
    .pred_pix_out_4x4(cr_wgt_pred_out), 
    .valid_out(cr_wght_pred_valid)
    );

   
 

pb_xyhw_gen_modified pb_xyhw_gen_block0 (
    .clk(clk), 
    // .reset(reset), 
    .cb_part_mode(now_cb_part_mode), 
    .pb_part_idx(3'd1), 
    .cb_size(now_cb_size_in_min_tus), 
    .cb_xx(now_cb_xx_in_min_tus), 
    .cb_yy(now_cb_yy_in_min_tus), 
    .xx_pb_start(xx_pb0_strt_in_min_tus), 
    .yy_pb_start(yy_pb0_strt_in_min_tus), 
    .xx_pb_end  (xx_pb0_end__in_min_tus), 
    .yy_pb_end  (yy_pb0_end__in_min_tus)
    );

pb_xyhw_gen_modified pb_xyhw_gen_block1 (
    .clk(clk), 
    // .reset(reset), 
    .cb_part_mode(now_cb_part_mode), 
    .pb_part_idx(3'd2), 
    .cb_size(now_cb_size_in_min_tus), 
    .cb_xx(now_cb_xx_in_min_tus), 
    .cb_yy(now_cb_yy_in_min_tus), 
    .xx_pb_start(xx_pb1_strt_in_min_tus), 
    .yy_pb_start(yy_pb1_strt_in_min_tus), 
    .xx_pb_end  (xx_pb1_end__in_min_tus), 
    .yy_pb_end  (yy_pb1_end__in_min_tus)
    );
    
pb_xyhw_gen_modified pb_xyhw_gen_block2 (
    .clk(clk), 
    // .reset(reset), 
    .cb_part_mode(now_cb_part_mode), 
    .pb_part_idx(3'd3), 
    .cb_size(now_cb_size_in_min_tus), 
    .cb_xx(now_cb_xx_in_min_tus), 
    .cb_yy(now_cb_yy_in_min_tus), 
    .xx_pb_start(xx_pb2_strt_in_min_tus), 
    .yy_pb_start(yy_pb2_strt_in_min_tus), 
    .xx_pb_end  (xx_pb2_end__in_min_tus), 
    .yy_pb_end  (yy_pb2_end__in_min_tus)
    );
    
pb_xyhw_gen_modified pb_xyhw_gen_block3 (
    .clk(clk), 
    // .reset(reset), 
    .cb_part_mode(now_cb_part_mode), 
    .pb_part_idx(3'd4), 
    .cb_size(now_cb_size_in_min_tus), 
    .cb_xx(now_cb_xx_in_min_tus), 
    .cb_yy(now_cb_yy_in_min_tus), 
    .xx_pb_start(xx_pb3_strt_in_min_tus), 
    .yy_pb_start(yy_pb3_strt_in_min_tus), 
    .xx_pb_end  (xx_pb3_end__in_min_tus), 
    .yy_pb_end  (yy_pb3_end__in_min_tus)
    );
    
    
    
always@(*) begin
    if((xx_pb0_strt_in_min_tus <= xT_in_min_tus) && (xx_pb0_end__in_min_tus > xT_in_min_tus)  && (yy_pb0_strt_in_min_tus <= yT_in_min_tus) && (yy_pb0_end__in_min_tus > yT_in_min_tus)) begin
        pu_0_in_range = 1;
    end
    else begin
        pu_0_in_range = 0;
    end
    if((xx_pb1_strt_in_min_tus <= xT_in_min_tus) && (xx_pb1_end__in_min_tus > xT_in_min_tus)  && (yy_pb1_strt_in_min_tus <= yT_in_min_tus) && (yy_pb1_end__in_min_tus > yT_in_min_tus)) begin
        pu_1_in_range = 1;
    end
    else begin
        pu_1_in_range = 0;
    end
    if((xx_pb2_strt_in_min_tus <= xT_in_min_tus) && (xx_pb2_end__in_min_tus > xT_in_min_tus)  && (yy_pb2_strt_in_min_tus <= yT_in_min_tus) && (yy_pb2_end__in_min_tus > yT_in_min_tus)) begin
        pu_2_in_range = 1;
    end
    else begin
        pu_2_in_range = 0;
    end
    if((xx_pb3_strt_in_min_tus <= xT_in_min_tus) && (xx_pb3_end__in_min_tus > xT_in_min_tus)  && (yy_pb3_strt_in_min_tus <= yT_in_min_tus) && (yy_pb3_end__in_min_tus > yT_in_min_tus)) begin
        pu_3_in_range = 1;
    end
    else begin
        pu_3_in_range = 0;
    end
end

// assign luma_l0_mid_x_clipped_wire = luma_l0_mid_x[MVD_WIDTH] ? 0 : ((luma_l0_mid_x >= pic_width )? (pic_width -1 ):luma_l0_mid_x[X11_ADDR_WDTH-1:0] )   ; //12 bits
// assign luma_l0_mid_y_clipped_wire = luma_l0_mid_y[MVD_WIDTH] ? 0 : ((luma_l0_mid_y >= pic_height)? (pic_height -1):luma_l0_mid_y[X11_ADDR_WDTH-1:0] )   ;
// assign chma_l0_mid_x_clipped_wire = chma_l0_mid_x[MVD_WIDTH] ? 0 : ((chma_l0_mid_x >= pic_width )? (pic_width -1 ):chma_l0_mid_x[X11_ADDR_WDTH-1:0] )   ;
// assign chma_l0_mid_y_clipped_wire = chma_l0_mid_y[MVD_WIDTH] ? 0 : ((chma_l0_mid_y >= pic_height)? (pic_height -1):chma_l0_mid_y[X11_ADDR_WDTH-1:0] )   ;

// assign luma_l1_mid_x_clipped_wire = luma_l1_mid_x[MVD_WIDTH] ? 0 : ((luma_l1_mid_x >= pic_width )? (pic_width -1 ):luma_l1_mid_x[X11_ADDR_WDTH-1:0] )   ;
// assign luma_l1_mid_y_clipped_wire = luma_l1_mid_y[MVD_WIDTH] ? 0 : ((luma_l1_mid_y >= pic_height)? (pic_height -1):luma_l1_mid_y[X11_ADDR_WDTH-1:0] )   ;
// assign chma_l1_mid_x_clipped_wire = chma_l1_mid_x[MVD_WIDTH] ? 0 : ((chma_l1_mid_x >= pic_width )? (pic_width -1 ):chma_l1_mid_x[X11_ADDR_WDTH-1:0] )   ;
// assign chma_l1_mid_y_clipped_wire = chma_l1_mid_y[MVD_WIDTH] ? 0 : ((chma_l1_mid_y >= pic_height)? (pic_height -1):chma_l1_mid_y[X11_ADDR_WDTH-1:0] )   ;


assign luma_l0_mid_x_wire = cand_mv_field_mv_x_l0[MVD_WIDTH -1: MV_L_INT_WIDTH_LOW]         + (xT_in_min_tus<<2); //15 bits ( 16 + 1 - FRAC_BITS)
assign luma_l0_mid_y_wire = cand_mv_field_mv_y_l0[MVD_WIDTH -1: MV_L_INT_WIDTH_LOW]         + (yT_in_min_tus<<2);
assign chma_l0_mid_x_wire = (cand_mv_field_mv_x_l0[MVD_WIDTH -1: MV_C_INT_WIDTH_LOW]<<1)    + (xT_in_min_tus<<2);
assign chma_l0_mid_y_wire = (cand_mv_field_mv_y_l0[MVD_WIDTH -1: MV_C_INT_WIDTH_LOW]<<1)    + (yT_in_min_tus<<2);

assign luma_l1_mid_x_wire = cand_mv_field_mv_x_l1[MVD_WIDTH -1: MV_L_INT_WIDTH_LOW]         + (xT_in_min_tus<<2);
assign luma_l1_mid_y_wire = cand_mv_field_mv_y_l1[MVD_WIDTH -1: MV_L_INT_WIDTH_LOW]         + (yT_in_min_tus<<2);
assign chma_l1_mid_x_wire = (cand_mv_field_mv_x_l1[MVD_WIDTH -1: MV_C_INT_WIDTH_LOW]<<1)    + (xT_in_min_tus<<2);
assign chma_l1_mid_y_wire = (cand_mv_field_mv_y_l1[MVD_WIDTH -1: MV_C_INT_WIDTH_LOW]<<1)    + (yT_in_min_tus<<2);


always@(*) begin
    case(cand_mv_field_mv_x_l0[MV_L_FRAC_WIDTH_HIGH-1:0])
        2'd0: begin luma_l0_ref_start_x_wire = luma_l0_mid_x        ;luma_l0_ref_width_x_wire = (1<<LOG2_MIN_TU_SIZE)       -1; end   //15 -bits, width 4 bits
        2'd1: begin luma_l0_ref_start_x_wire = luma_l0_mid_x - 4'd3 ;luma_l0_ref_width_x_wire = (1<<LOG2_MIN_TU_SIZE) +3 +3 -1; end
        2'd3: begin luma_l0_ref_start_x_wire = luma_l0_mid_x - 4'd2 ;luma_l0_ref_width_x_wire = (1<<LOG2_MIN_TU_SIZE) +4 +2 -1; end
        2'd2: begin luma_l0_ref_start_x_wire = luma_l0_mid_x - 4'd3 ;luma_l0_ref_width_x_wire = (1<<LOG2_MIN_TU_SIZE) +4 +3 -1; end
    endcase
end


always@(*) begin
    case(cand_mv_field_mv_x_l1[MV_L_FRAC_WIDTH_HIGH-1:0])
        2'd0: begin luma_l1_ref_start_x_wire = luma_l1_mid_x        ;luma_l1_ref_width_x_wire = (1<<LOG2_MIN_TU_SIZE)       -1; end
        2'd1: begin luma_l1_ref_start_x_wire = luma_l1_mid_x - 4'd3 ;luma_l1_ref_width_x_wire = (1<<LOG2_MIN_TU_SIZE) +3 +3 -1; end
        2'd3: begin luma_l1_ref_start_x_wire = luma_l1_mid_x - 4'd2 ;luma_l1_ref_width_x_wire = (1<<LOG2_MIN_TU_SIZE) +4 +2 -1; end
        2'd2: begin luma_l1_ref_start_x_wire = luma_l1_mid_x - 4'd3 ;luma_l1_ref_width_x_wire = (1<<LOG2_MIN_TU_SIZE) +4 +3 -1; end
    endcase
end

always@(*) begin
    case(cand_mv_field_mv_y_l0[MV_L_FRAC_WIDTH_HIGH-1:0])
        2'd0: begin luma_l0_ref_start_y_wire = luma_l0_mid_y        ;luma_l0_ref_height_y_wire = (1<<LOG2_MIN_TU_SIZE)       -1; end
        2'd1: begin luma_l0_ref_start_y_wire = luma_l0_mid_y - 4'd3 ;luma_l0_ref_height_y_wire = (1<<LOG2_MIN_TU_SIZE) +3 +3 -1; end
        2'd3: begin luma_l0_ref_start_y_wire = luma_l0_mid_y - 4'd2 ;luma_l0_ref_height_y_wire = (1<<LOG2_MIN_TU_SIZE) +4 +2 -1; end
        2'd2: begin luma_l0_ref_start_y_wire = luma_l0_mid_y - 4'd3 ;luma_l0_ref_height_y_wire = (1<<LOG2_MIN_TU_SIZE) +4 +3 -1; end
    endcase
end


always@(*) begin
    case(cand_mv_field_mv_y_l1[MV_L_FRAC_WIDTH_HIGH-1:0])
        2'd0: begin luma_l1_ref_start_y_wire = luma_l1_mid_y        ;luma_l1_ref_height_y_wire = (1<<LOG2_MIN_TU_SIZE)       -1; end
        2'd1: begin luma_l1_ref_start_y_wire = luma_l1_mid_y - 4'd3 ;luma_l1_ref_height_y_wire = (1<<LOG2_MIN_TU_SIZE) +3 +3 -1; end
        2'd3: begin luma_l1_ref_start_y_wire = luma_l1_mid_y - 4'd2 ;luma_l1_ref_height_y_wire = (1<<LOG2_MIN_TU_SIZE) +4 +2 -1; end
        2'd2: begin luma_l1_ref_start_y_wire = luma_l1_mid_y - 4'd3 ;luma_l1_ref_height_y_wire = (1<<LOG2_MIN_TU_SIZE) +4 +3 -1; end
    endcase
end


always@(*) begin
    case(cand_mv_field_mv_x_l0[MV_C_FRAC_WIDTH_HIGH-1:0])
        3'd0    : begin chma_l0_ref_start_x_wire = chma_l0_mid_x        ;chma_l0_ref_width_x_wire = (1<<LOG2_MIN_TU_SIZE) -2                 ; end
        default : begin chma_l0_ref_start_x_wire = chma_l0_mid_x -1*2   ;chma_l0_ref_width_x_wire = (1<<LOG2_MIN_TU_SIZE) +(2*2) + 1*2 - 2   ; end
    endcase
end

always@(*) begin
    case(cand_mv_field_mv_x_l1[MV_C_FRAC_WIDTH_HIGH-1:0])
        3'd0    : begin chma_l1_ref_start_x_wire = chma_l1_mid_x        ;chma_l1_ref_width_x_wire = (1<<LOG2_MIN_TU_SIZE) -2                 ; end
        default : begin chma_l1_ref_start_x_wire = chma_l1_mid_x -1*2   ;chma_l1_ref_width_x_wire = (1<<LOG2_MIN_TU_SIZE) +(2*2) + 1*2 - 2   ; end
    endcase
end

always@(*) begin
    case(cand_mv_field_mv_y_l0[MV_C_FRAC_WIDTH_HIGH-1:0])
        3'd0    : begin chma_l0_ref_start_y_wire = chma_l0_mid_y        ;chma_l0_ref_height_y_wire = (1<<LOG2_MIN_TU_SIZE) -2                 ; end
        default : begin chma_l0_ref_start_y_wire = chma_l0_mid_y -1*2   ;chma_l0_ref_height_y_wire = (1<<LOG2_MIN_TU_SIZE) +(2*2) + 1*2 - 2   ; end
    endcase
end

always@(*) begin
    case(cand_mv_field_mv_y_l1[MV_C_FRAC_WIDTH_HIGH-1:0])
        3'd0    : begin chma_l1_ref_start_y_wire = chma_l1_mid_y        ;chma_l1_ref_height_y_wire = (1<<LOG2_MIN_TU_SIZE) -2                 ; end
        default : begin chma_l1_ref_start_y_wire = chma_l1_mid_y -1*2   ;chma_l1_ref_height_y_wire = (1<<LOG2_MIN_TU_SIZE) +(2*2) + 1*2 - 2   ; end
    endcase
end


    
    
    
`ifdef READ_FILE

    assign log2_cb_size_wire = config_data_bus_in[      HEADER_WIDTH + 2*X0_WIDTH + LOG2_CTB_WIDTH - 1:
                                                        HEADER_WIDTH + 2*X0_WIDTH];
`else
    assign log2_cb_size_wire = config_data_bus_in[      INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - 2*X0_WIDTH - 1: 
                                                        INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - 2*X0_WIDTH - LOG2_CTB_WIDTH];
`endif

`ifdef READ_FILE
    assign x0_tu_in_ctu_wire = tu_data_in[   HEADER_WIDTH + XT_WIDTH -1:
                                            HEADER_WIDTH]                               ;
`else
    assign x0_tu_in_ctu_wire = tu_data_in[   INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - 1: 
                                            INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - XT_WIDTH]    ;
`endif

`ifdef READ_FILE
    assign y0_tu_in_ctu_wire = tu_data_in[   HEADER_WIDTH + XT_WIDTH + YT_WIDTH -1:
                                            HEADER_WIDTH + YT_WIDTH]                            ;
`else
    assign y0_tu_in_ctu_wire = tu_data_in[   INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - XT_WIDTH- 1: 
                                            INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - XT_WIDTH - YT_WIDTH] ;
`endif

`ifdef READ_FILE
    assign log2_tb_size_wire = tu_data_in[     HEADER_WIDTH + XT_WIDTH + YT_WIDTH + CIDX_WIDTH + LOG2_CB_SIZE_WIDTH -1:
                                            HEADER_WIDTH + XT_WIDTH + YT_WIDTH + CIDX_WIDTH];
`else
    assign log2_tb_size_wire = tu_data_in[     INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - XT_WIDTH - YT_WIDTH - CIDX_WIDTH - 1: 
                                            INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - XT_WIDTH - YT_WIDTH - CIDX_WIDTH - LOG2_CB_SIZE_WIDTH];
`endif

`ifdef READ_FILE
    assign c_idx_wire = tu_data_in[    HEADER_WIDTH + XT_WIDTH + YT_WIDTH + CIDX_WIDTH-1:
                                    HEADER_WIDTH + XT_WIDTH + YT_WIDTH];
`else
    assign c_idx_wire = tu_data_in[    INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - XT_WIDTH - YT_WIDTH - 1: 
                                    INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - XT_WIDTH - YT_WIDTH - CIDX_WIDTH];
`endif

    reg  [TB_SIZE_WIDTH -LOG2_MIN_TU_SIZE:0 ]  tb_size_wire_in_min_tus;


    always@(*) begin
        case(log2_cb_size_wire)
            3: begin
                cb_size_wire_in_min_pus = 2;
            end
            4: begin
                cb_size_wire_in_min_pus = 4;  
            end
            5: begin
                cb_size_wire_in_min_pus = 8;
            end
            6: begin
                cb_size_wire_in_min_pus = 16;
            end
            default: begin
                cb_size_wire_in_min_pus = 16;
            end
        endcase
    end
    
    always@(*) begin
        case(log2_tb_size_wire)
            2: begin
                tb_size_wire_in_min_tus = 1;
            end
            3: begin
                tb_size_wire_in_min_tus = 2;
            end
            4: begin
                tb_size_wire_in_min_tus = 4;  
            end
            5: begin
                tb_size_wire_in_min_tus = 8;
            end
            6: begin
                tb_size_wire_in_min_tus = 16;
            end
        endcase
    end
    

always@(*) begin
    if(((xT_in_min_tus + 1) == now_cb_xx_end_in_min_tus ) && ((yT_in_min_tus + 1) == now_cb_yy_end_in_min_tus)) begin
        end_of_cu = 1;
    end
    else begin
        end_of_cu = 0;
    end
end

always@(*) begin
    if(((xT_in_min_tus + 1) == x0_tu_end_in_min_tus ) && ((yT_in_min_tus + 1) == y0_tu_end_in_min_tus)) begin
        end_of_tu = 1;
    end
    else begin
        end_of_tu = 0;
    end
end
      
always@(*) begin
    // if(((xT_in_min_tus + 1) == x0_tu_end_in_min_tus ) && (xT_in_min_tus[0] & yT_in_min_tus[0])) begin
        // next_xT_in_min_tus = x0_tu_in_min_tus;       
        // next_yT_in_min_tus = yT_in_min_tus + 1;
    // end
    // else begin
        next_xT_in_min_tus = x_8x8_loc + inner_x_8x8_loc;
        next_yT_in_min_tus = y_8x8_loc + inner_y_8x8_loc;    
    // end
end

always@(*) begin
    if(((x_8x8_loc + 2) == x0_tu_end_in_min_tus ) && (inner_x_8x8_loc & inner_y_8x8_loc)) begin // if both xT_in_min_tus and yT_in_min_tus are odd numbers that is  the end point of a 8x8 block
        next_y_8x8_loc = y_8x8_loc + 2;
        next_x_8x8_loc = x0_tu_in_min_tus;        
    end
    else begin
        if(inner_x_8x8_loc & inner_y_8x8_loc) begin
            next_y_8x8_loc = y_8x8_loc;
            next_x_8x8_loc = x_8x8_loc + 2;
        end
        else begin
            next_x_8x8_loc = x_8x8_loc;
            next_y_8x8_loc = y_8x8_loc;
        end
    end
end

always@(*) begin
    next_inner_x_8x8_loc = !inner_x_8x8_loc;            // inner locs and x_8x8_loc y_8x8_loc operate one clock cycle ahead
    if(inner_x_8x8_loc) begin
        next_inner_y_8x8_loc = !inner_y_8x8_loc;   
    end
    else begin
        next_inner_y_8x8_loc = inner_y_8x8_loc;   
    end
end

assign first_cu_mv_field_valid_in_cu = (next_mv_field_valid_in == 1) && (pb_part_idx == 0);

wire mv_x_l0_l1_same = (next_mv_field_mv_x_l0_in == next_mv_field_mv_x_l1_in) ? 1'b1 : 1'b0;
wire mv_y_l0_l1_same = (next_mv_field_mv_y_l0_in == next_mv_field_mv_y_l1_in) ? 1'b1 : 1'b0;
wire mv_dpb_idx_l0_l1_same = (next_dpb_idx_l0_in == next_dpb_idx_l1_in) ? 1'b1 : 1'b0;

always@(posedge clk) begin
    if(next_mv_field_valid_in) begin
        prev_mv_field_pred_flag_l0[pb_part_idx]     <=    next_mv_field_pred_flag_l0_in;
        if(mv_x_l0_l1_same & mv_y_l0_l1_same & mv_dpb_idx_l0_l1_same) begin
            prev_mv_field_pred_flag_l1[pb_part_idx]     <= 1'b0;
        end
        else begin
            prev_mv_field_pred_flag_l1[pb_part_idx]     <= next_mv_field_pred_flag_l1_in;
        end
        // prev_mv_field_ref_idx_l0[pb_part_idx]       <=    next_mv_field_ref_idx_l0_in;
        // prev_mv_field_ref_idx_l1[pb_part_idx]       <=    next_mv_field_ref_idx_l1_in;
        prev_mv_field_mv_x_l0[pb_part_idx]          <=    next_mv_field_mv_x_l0_in;
        prev_mv_field_mv_y_l0[pb_part_idx]          <=    next_mv_field_mv_y_l0_in;
        prev_mv_field_mv_x_l1[pb_part_idx]          <=    next_mv_field_mv_x_l1_in;
        prev_mv_field_mv_y_l1[pb_part_idx]          <=    next_mv_field_mv_y_l1_in;    
        prev_dpb_idx_l0[pb_part_idx]                <=    next_dpb_idx_l0_in;    
        prev_dpb_idx_l1[pb_part_idx]                <=    next_dpb_idx_l1_in;    
        
        // prev_xx_pb[pb_part_idx]                     <=    next_xx_pb_in;
        // prev_hh_pb[pb_part_idx]                     <=    next_hh_pb_in;
        // prev_yy_pb[pb_part_idx]                     <=    next_yy_pb_in;
        // prev_ww_pb[pb_part_idx]                     <=    next_ww_pb_in;  
    end
    
    if(first_cu_mv_field_valid_in_cu ) begin //&& inter_pred_sample_state == STATE_INTER_PRED_SAMPLE_CONFIG_UPDATE) begin
        now_cb_xx_in_min_tus            <= next_cb_xx_in_min_tus;
        now_cb_yy_in_min_tus            <= next_cb_yy_in_min_tus;
        now_cb_pred_mode                <= next_cb_pred_mode;
        now_cb_part_mode                <= next_cb_part_mode;
        // now_pic_dpb_idx                 <= next_pic_dpb_idx;
        now_ctb_xx_in_min_tus           <= next_ctb_xx_in_min_tus;
        now_ctb_yy_in_min_tus           <= next_ctb_yy_in_min_tus;
        now_cb_xx_end_in_min_tus        <= next_cb_xx_in_min_tus + next_cb_size_in_min_tus;
        now_cb_yy_end_in_min_tus        <= next_cb_yy_in_min_tus + next_cb_size_in_min_tus;
        now_cb_size_in_min_tus          <= next_cb_size_in_min_tus;    
    end
end

always@(*) begin
    case(pb_part_idx)
        1: begin
            pb_part_idx_decode = 4'd1;
        end
        2: pb_part_idx_decode = 4'b0011;
        3: pb_part_idx_decode = 4'b0111;
        4: pb_part_idx_decode = 4'b1111;
        default: begin
            pb_part_idx_decode = 4'd0;
        end
    endcase
end

    always@(*) begin : pu_header_termination_logic
        if(now_cb_pred_mode == `MODE_INTRA) begin
            is_pu_header_cus_last = 1;
        end
        else begin
            case(now_cb_part_mode)
                `PART_2Nx2N: begin
                    if(pb_part_idx == 1) begin
                        is_pu_header_cus_last = 1;
                    end
                    else begin
                        is_pu_header_cus_last = 0;
                    end
                end
                `PART_2NxN,`PART_Nx2N,`PART_nLx2N,`PART_nRx2N,`PART_2NxnD,`PART_2NxnU: begin
                    if(pb_part_idx == 2) begin
                        is_pu_header_cus_last = 1;
                    end
                    else begin
                        is_pu_header_cus_last = 0;
                    end
                end
                `PART_NxN: begin
                    if(pb_part_idx == 4) begin
                        is_pu_header_cus_last = 1;
                    end
                    else begin
                        is_pu_header_cus_last = 0;
                    end
                end
                default: begin
                    is_pu_header_cus_last = 0;
                end
            endcase
        end
        
    end
    
    
always@(*) begin  : idle_signal_to_mv_derive_logic
    pred_pixl_gen_idle_out_to_mv_derive = 0;
    case(inter_pred_sample_state)
        STATE_INTER_PRED_SAMPLE_CONFIG_UPDATE: begin
            pred_pixl_gen_idle_out_to_mv_derive = 1;
        end
        default: begin
            if(is_pu_header_cus_last == 0) begin
                pred_pixl_gen_idle_out_to_mv_derive = 1;
            end
            else begin
                pred_pixl_gen_idle_out_to_mv_derive = 0;
            end
        end
    endcase
end
    
always@(*) begin    : next_state_logic
    inter_pred_sample_next_state = inter_pred_sample_state;
    pred_pixl_gen_idle_out_to_pred_top = 0;
    ref_range_valid_out = 0;
    case(inter_pred_sample_state)
        STATE_INTER_PRED_SAMPLE_CONFIG_UPDATE: begin
            //pred_pixl_gen_idle_out_to_pred_top = 1;
            if(next_mv_field_valid_in) begin
                inter_pred_sample_next_state = STATE_INTER_PRED_SAMPLE_FILL_TU_DATA;
            end
            
        end
        STATE_INTER_PRED_SAMPLE_FILL_TU_DATA: begin
            if(now_cb_pred_mode == `MODE_INTRA) begin
                inter_pred_sample_next_state = STATE_INTER_PRED_SAMPLE_CONFIG_UPDATE;
            end
            else if(!tu_empty) begin
                inter_pred_sample_next_state = STATE_INTER_PRED_SAMPLE_X_8_Y_8_UPDATE;
            end
        end
        STATE_INTER_PRED_SAMPLE_X_8_Y_8_UPDATE: begin
            inter_pred_sample_next_state = STATE_INTER_PRED_SAMPLE_PU_SELECT;
            pred_pixl_gen_idle_out_to_pred_top = 1;
        end
        STATE_INTER_PRED_SAMPLE_PU_SELECT: begin
            if((pu_0_in_range && pb_part_idx_decode[0]) || (pu_1_in_range && pb_part_idx_decode[1]) || (pu_2_in_range && pb_part_idx_decode[2])  || (pu_3_in_range && pb_part_idx_decode[3]) ) begin
                inter_pred_sample_next_state = STATE_INTER_PRED_MV_INT_DECODE;
            end
        end
        STATE_INTER_PRED_MV_INT_DECODE : begin
            inter_pred_sample_next_state = STATE_INTER_PRED_MV_FRAC_DECODE;
        end
        STATE_INTER_PRED_MV_FRAC_DECODE: begin
            if(cand_mv_field_pred_flag_l0 & cand_mv_field_pred_flag_l1) begin
                inter_pred_sample_next_state = STATE_INTER_PRED_MV_DECODE_READY_BI;
            end
            else begin
                inter_pred_sample_next_state = STATE_INTER_PRED_MV_DECODE_READY;
            end
        end
        STATE_INTER_PRED_MV_DECODE_READY_BI: begin
            if(cache_idle_in) begin
                ref_range_valid_out = 1;
                inter_pred_sample_next_state = STATE_INTER_PRED_MV_FRAC_BI_DECODE;
            end
        end
        STATE_INTER_PRED_MV_FRAC_BI_DECODE: begin
            inter_pred_sample_next_state = STATE_INTER_PRED_MV_DECODE_READY;
        end
        STATE_INTER_PRED_MV_DECODE_READY: begin
            if(cache_idle_in) begin
                ref_range_valid_out = 1;
                if(end_of_cu) begin
                    inter_pred_sample_next_state = STATE_INTER_PRED_SAMPLE_CONFIG_UPDATE;
                end
                else if(end_of_tu) begin
                    inter_pred_sample_next_state = STATE_INTER_PRED_SAMPLE_FILL_TU_DATA;
                end
                else begin
                    inter_pred_sample_next_state = STATE_INTER_PRED_SAMPLE_PU_SELECT;
                end
            end
            
        end
    endcase
end

always@(posedge clk) begin    : part_idx_logic
    if(reset) begin
        pb_part_idx <= 0;
    end
    else begin
        case(inter_pred_sample_state)
            STATE_INTER_PRED_SAMPLE_CONFIG_UPDATE: begin
                if(next_mv_field_valid_in) begin
                    pb_part_idx <= 1;
                end
                else begin
                    pb_part_idx <= 0;
                end
            end
            default: begin
                if(next_mv_field_valid_in) begin
                    pb_part_idx <= pb_part_idx + 1;
                end
            end
        endcase
    end
end


always@(posedge clk) begin
    if(reset) begin
        xT_in_min_tus <= 0;
        yT_in_min_tus <= 0;
    end
    case(inter_pred_sample_state) 
        STATE_INTER_PRED_SAMPLE_FILL_TU_DATA: begin
            if(!tu_empty) begin
                // c_idx <= c_idx_wire;
                x0_tu_in_min_tus        <= (x0_tu_in_ctu_wire>>(LOG2_MIN_TU_SIZE-1)) + now_ctb_xx_in_min_tus;
                x0_tu_end_in_min_tus    <= (x0_tu_in_ctu_wire>>(LOG2_MIN_TU_SIZE-1)) + now_ctb_xx_in_min_tus + tb_size_wire_in_min_tus;
                // y0_tu_in_min_tus        <= (y0_tu_in_ctu_wire>>(LOG2_MIN_TU_SIZE-1)) + now_ctb_yy_in_min_tus;
                y0_tu_end_in_min_tus    <= (y0_tu_in_ctu_wire>>(LOG2_MIN_TU_SIZE-1)) + now_ctb_yy_in_min_tus + tb_size_wire_in_min_tus;
                x_8x8_loc               <= (x0_tu_in_ctu_wire>>(LOG2_MIN_TU_SIZE-1)) + now_ctb_xx_in_min_tus;
                y_8x8_loc               <= (y0_tu_in_ctu_wire>>(LOG2_MIN_TU_SIZE-1)) + now_ctb_yy_in_min_tus;
                inner_x_8x8_loc <= 0;
                inner_y_8x8_loc <= 0;
            end
        end
        STATE_INTER_PRED_SAMPLE_X_8_Y_8_UPDATE: begin
            xT_in_min_tus <= next_xT_in_min_tus;
            yT_in_min_tus <= next_yT_in_min_tus;
        end
        STATE_INTER_PRED_SAMPLE_PU_SELECT: begin
            if(pu_0_in_range && pb_part_idx_decode[0]) begin
                cand_mv_field_pred_flag_l0 <= prev_mv_field_pred_flag_l0[0] ;
                cand_mv_field_pred_flag_l1 <= prev_mv_field_pred_flag_l1[0] ;
                // cand_mv_field_ref_idx_l0   <= prev_mv_field_ref_idx_l0[0]   ;
                // cand_mv_field_ref_idx_l1   <= prev_mv_field_ref_idx_l1[0]   ;
                cand_mv_field_mv_x_l0      <= prev_mv_field_mv_x_l0[0]      ;
                cand_mv_field_mv_y_l0      <= prev_mv_field_mv_y_l0[0]      ;
                cand_mv_field_mv_x_l1      <= prev_mv_field_mv_x_l1[0]      ;
                cand_mv_field_mv_y_l1      <= prev_mv_field_mv_y_l1[0]      ;
                cand_dpb_idx_l0            <= prev_dpb_idx_l0[0]            ;
                cand_dpb_idx_l1            <= prev_dpb_idx_l1[0]            ;
 
            end
            if(pu_1_in_range && pb_part_idx_decode[1]) begin
                cand_mv_field_pred_flag_l0 <= prev_mv_field_pred_flag_l0[1] ;
                cand_mv_field_pred_flag_l1 <= prev_mv_field_pred_flag_l1[1] ;
                // cand_mv_field_ref_idx_l0   <= prev_mv_field_ref_idx_l0[1]   ;
                // cand_mv_field_ref_idx_l1   <= prev_mv_field_ref_idx_l1[1]   ;
                cand_mv_field_mv_x_l0      <= prev_mv_field_mv_x_l0[1]      ;
                cand_mv_field_mv_y_l0      <= prev_mv_field_mv_y_l0[1]      ;
                cand_mv_field_mv_x_l1      <= prev_mv_field_mv_x_l1[1]      ;
                cand_mv_field_mv_y_l1      <= prev_mv_field_mv_y_l1[1]      ;
                cand_dpb_idx_l0            <= prev_dpb_idx_l0[1]            ;
                cand_dpb_idx_l1            <= prev_dpb_idx_l1[1]            ;
            end
            if(pu_2_in_range && pb_part_idx_decode[2]) begin
                cand_mv_field_pred_flag_l0 <= prev_mv_field_pred_flag_l0[2] ;
                cand_mv_field_pred_flag_l1 <= prev_mv_field_pred_flag_l1[2] ;
                // cand_mv_field_ref_idx_l0   <= prev_mv_field_ref_idx_l0[2]   ;
                // cand_mv_field_ref_idx_l1   <= prev_mv_field_ref_idx_l1[2]   ;
                cand_mv_field_mv_x_l0      <= prev_mv_field_mv_x_l0[2]      ;
                cand_mv_field_mv_y_l0      <= prev_mv_field_mv_y_l0[2]      ;
                cand_mv_field_mv_x_l1      <= prev_mv_field_mv_x_l1[2]      ;
                cand_mv_field_mv_y_l1      <= prev_mv_field_mv_y_l1[2]      ;
                cand_dpb_idx_l0            <= prev_dpb_idx_l0[2]            ;
                cand_dpb_idx_l1            <= prev_dpb_idx_l1[2]            ;
            end
            if(pu_3_in_range && pb_part_idx_decode[3])begin
                cand_mv_field_pred_flag_l0 <= prev_mv_field_pred_flag_l0[3] ;
                cand_mv_field_pred_flag_l1 <= prev_mv_field_pred_flag_l1[3] ;
                // cand_mv_field_ref_idx_l0   <= prev_mv_field_ref_idx_l0[3]   ;
                // cand_mv_field_ref_idx_l1   <= prev_mv_field_ref_idx_l1[3]   ;
                cand_mv_field_mv_x_l0      <= prev_mv_field_mv_x_l0[3]      ;
                cand_mv_field_mv_y_l0      <= prev_mv_field_mv_y_l0[3]      ;
                cand_mv_field_mv_x_l1      <= prev_mv_field_mv_x_l1[3]      ;
                cand_mv_field_mv_y_l1      <= prev_mv_field_mv_y_l1[3]      ;
                cand_dpb_idx_l0            <= prev_dpb_idx_l0[3]            ;
                cand_dpb_idx_l1            <= prev_dpb_idx_l1[3]            ;
            end
        end
        STATE_INTER_PRED_MV_INT_DECODE : begin
            inner_x_8x8_loc <= next_inner_x_8x8_loc;
            inner_y_8x8_loc <= next_inner_y_8x8_loc;
            x_8x8_loc <= next_x_8x8_loc;
            y_8x8_loc <= next_y_8x8_loc;

            luma_l0_mid_x <= luma_l0_mid_x_wire;
            luma_l0_mid_y <= luma_l0_mid_y_wire;
            chma_l0_mid_x <= chma_l0_mid_x_wire;
            chma_l0_mid_y <= chma_l0_mid_y_wire;

            luma_l1_mid_x <= luma_l1_mid_x_wire;
            luma_l1_mid_y <= luma_l1_mid_y_wire;
            chma_l1_mid_x <= chma_l1_mid_x_wire;
            chma_l1_mid_y <= chma_l1_mid_y_wire;
            
        end
        STATE_INTER_PRED_MV_FRAC_DECODE: begin
            if(cand_mv_field_pred_flag_l0) begin
                luma_ref_start_x <= luma_l0_ref_start_x_wire;
                luma_ref_start_y <= luma_l0_ref_start_y_wire;
                luma_ref_width_x <= luma_l0_ref_width_x_wire;
                luma_ref_height_y <= luma_l0_ref_height_y_wire;
                chma_ref_start_x <= chma_l0_ref_start_x_wire;
                chma_ref_start_y <= chma_l0_ref_start_y_wire;
                chma_ref_width_x <= chma_l0_ref_width_x_wire;
                chma_ref_height_y <= chma_l0_ref_height_y_wire;   
                ref_dpb_idx       <= cand_dpb_idx_l0;
                ch_frac_x <= cand_mv_field_mv_x_l0[MV_C_FRAC_WIDTH_HIGH-1:0];
                ch_frac_y <= cand_mv_field_mv_y_l0[MV_C_FRAC_WIDTH_HIGH-1:0];

            end
            else if(cand_mv_field_pred_flag_l1) begin
                luma_ref_start_x <= luma_l1_ref_start_x_wire;
                luma_ref_start_y <= luma_l1_ref_start_y_wire;
                luma_ref_width_x <= luma_l1_ref_width_x_wire;
                luma_ref_height_y <= luma_l1_ref_height_y_wire;
                chma_ref_start_x <= chma_l1_ref_start_x_wire;
                chma_ref_start_y <= chma_l1_ref_start_y_wire;
                chma_ref_width_x <= chma_l1_ref_width_x_wire;
                chma_ref_height_y <= chma_l1_ref_height_y_wire;
                ref_dpb_idx       <= cand_dpb_idx_l1;
                ch_frac_x <= cand_mv_field_mv_x_l1[MV_C_FRAC_WIDTH_HIGH-1:0];      
                ch_frac_y <= cand_mv_field_mv_y_l1[MV_C_FRAC_WIDTH_HIGH-1:0];
            end            
        end
        STATE_INTER_PRED_MV_FRAC_BI_DECODE: begin
            luma_ref_start_x <= luma_l1_ref_start_x_wire;
            luma_ref_start_y <= luma_l1_ref_start_y_wire;
            luma_ref_width_x <= luma_l1_ref_width_x_wire;
            luma_ref_height_y <= luma_l1_ref_height_y_wire;
            chma_ref_start_x <= chma_l1_ref_start_x_wire;
            chma_ref_start_y <= chma_l1_ref_start_y_wire;
            chma_ref_width_x <= chma_l1_ref_width_x_wire;
            chma_ref_height_y <= chma_l1_ref_height_y_wire;
            ref_dpb_idx       <= cand_dpb_idx_l1;
            ch_frac_x <= cand_mv_field_mv_x_l1[MV_C_FRAC_WIDTH_HIGH-1:0];      
            ch_frac_y <= cand_mv_field_mv_y_l1[MV_C_FRAC_WIDTH_HIGH-1:0];
        end
        STATE_INTER_PRED_MV_DECODE_READY: begin
            if(cache_idle_in) begin
                xT_in_min_tus <= next_xT_in_min_tus;
                yT_in_min_tus <= next_yT_in_min_tus;
            end
        end
    endcase
end


always@(posedge clk) begin
    if(reset) begin
        inter_pred_sample_state <= STATE_INTER_PRED_SAMPLE_CONFIG_UPDATE;
    end
    else begin
        inter_pred_sample_state <= inter_pred_sample_next_state;
    end
end

always@(posedge clk) begin
       case(config_mode_in)
        `INTER_TOP_PARA_0: begin
 `ifdef READ_FILE    
             pic_width <=   (config_data_bus_in[PIC_WIDTH_WIDTH + HEADER_WIDTH -1: HEADER_WIDTH])%(1<<PIC_DIM_WIDTH);
             pic_height  <=   (config_data_bus_in[HEADER_WIDTH + PIC_WIDTH_WIDTH + PIC_HEIGHT_WIDTH- 1: HEADER_WIDTH + PIC_WIDTH_WIDTH])%(1<<PIC_DIM_WIDTH);
 `else
             pic_height <=   (config_data_bus_in[INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - 1: INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - PIC_WIDTH_WIDTH])%(1<<PIC_DIM_WIDTH);
             pic_width  <=   (config_data_bus_in[INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - PIC_WIDTH_WIDTH - 1: INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - PIC_WIDTH_WIDTH - PIC_HEIGHT_WIDTH])%(1<<PIC_DIM_WIDTH);
 `endif
        end
        `INTER_TOP_PARA_1: begin
// `ifdef READ_FILE
        // log2_ctb_size <=  config_data_bus_in[ HEADER_WIDTH + LOG2CTBSIZEY_WIDTH -1 : HEADER_WIDTH];
// `else                    
        // log2_ctb_size <=  config_data_bus_in[INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - 1: INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - LOG2CTBSIZEY_WIDTH];
// `endif
        end
        `INTER_TOP_PARA_2: begin 
            
            
        end
        `INTER_TOP_SLICE_1: begin
// `ifdef READ_FILE
            // slice_temporal_mvp_enabled_flag <= config_data_bus_in[   HEADER_WIDTH + SHORT_TERM_REF_PIC_SPS_WIDTH + SHORT_TERM_REF_PIC_IDX_WIDTH + TEMPORAL_MVP_ENABLED_WIDTH- 1:
                                                                // HEADER_WIDTH + SHORT_TERM_REF_PIC_SPS_WIDTH + SHORT_TERM_REF_PIC_IDX_WIDTH  ];     
            // slice_type              <=      config_data_bus_in[      HEADER_WIDTH + SHORT_TERM_REF_PIC_SPS_WIDTH + SHORT_TERM_REF_PIC_IDX_WIDTH + TEMPORAL_MVP_ENABLED_WIDTH + SAO_LUMA_WIDTH + SAO_CHROMA_WIDTH + NUM_REF_IDX_L0_MINUS1_WIDTH + NUM_REF_IDX_L1_MINUS1_WIDTH + MAX_MERGE_CAND_WIDTH + SLICE_TYPE_WIDTH- 1:
                                                                // HEADER_WIDTH + SHORT_TERM_REF_PIC_SPS_WIDTH + SHORT_TERM_REF_PIC_IDX_WIDTH + TEMPORAL_MVP_ENABLED_WIDTH + SAO_LUMA_WIDTH + SAO_CHROMA_WIDTH + NUM_REF_IDX_L0_MINUS1_WIDTH + NUM_REF_IDX_L1_MINUS1_WIDTH + MAX_MERGE_CAND_WIDTH  ];   
                                                
            // collocated_from_l0_flag <= config_data_bus_in[   HEADER_WIDTH + SHORT_TERM_REF_PIC_SPS_WIDTH + SHORT_TERM_REF_PIC_IDX_WIDTH + TEMPORAL_MVP_ENABLED_WIDTH + SAO_LUMA_WIDTH + SAO_CHROMA_WIDTH + NUM_REF_IDX_L0_MINUS1_WIDTH + NUM_REF_IDX_L1_MINUS1_WIDTH + MAX_MERGE_CAND_WIDTH + SLICE_TYPE_WIDTH + COLLOCATED_FROM_L0_FLAG_WIDTH - 1:
                                                        // HEADER_WIDTH + SHORT_TERM_REF_PIC_SPS_WIDTH + SHORT_TERM_REF_PIC_IDX_WIDTH + TEMPORAL_MVP_ENABLED_WIDTH + SAO_LUMA_WIDTH + SAO_CHROMA_WIDTH + NUM_REF_IDX_L0_MINUS1_WIDTH + NUM_REF_IDX_L1_MINUS1_WIDTH + MAX_MERGE_CAND_WIDTH + SLICE_TYPE_WIDTH];
// `else                        
            // slice_temporal_mvp_enabled_flag <=   config_data_bus_in[ INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - SHORT_TERM_REF_PIC_SPS_WIDTH - SHORT_TERM_REF_PIC_IDX_WIDTH - 1:
                                                                // INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - SHORT_TERM_REF_PIC_SPS_WIDTH - SHORT_TERM_REF_PIC_IDX_WIDTH - TEMPORAL_MVP_ENABLED_WIDTH];     
            // slice_type              <=      config_data_bus_in[  INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - SHORT_TERM_REF_PIC_SPS_WIDTH - SHORT_TERM_REF_PIC_IDX_WIDTH - TEMPORAL_MVP_ENABLED_WIDTH - SAO_LUMA_WIDTH - SAO_CHROMA_WIDTH - NUM_REF_IDX_L0_MINUS1_WIDTH - NUM_REF_IDX_L1_MINUS1_WIDTH - MAX_MERGE_CAND_WIDTH -1:
                                                            // INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - SHORT_TERM_REF_PIC_SPS_WIDTH - SHORT_TERM_REF_PIC_IDX_WIDTH - TEMPORAL_MVP_ENABLED_WIDTH - SAO_LUMA_WIDTH - SAO_CHROMA_WIDTH - NUM_REF_IDX_L0_MINUS1_WIDTH - NUM_REF_IDX_L1_MINUS1_WIDTH - MAX_MERGE_CAND_WIDTH - SLICE_TYPE_WIDTH];   
            // collocated_from_l0_flag <= config_data_bus_in[   INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - SHORT_TERM_REF_PIC_SPS_WIDTH - SHORT_TERM_REF_PIC_IDX_WIDTH - TEMPORAL_MVP_ENABLED_WIDTH - SAO_LUMA_WIDTH - SAO_CHROMA_WIDTH - NUM_REF_IDX_L0_MINUS1_WIDTH - NUM_REF_IDX_L1_MINUS1_WIDTH - MAX_MERGE_CAND_WIDTH - SLICE_TYPE_WIDTH - 1:
                                                        // INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - SHORT_TERM_REF_PIC_SPS_WIDTH - SHORT_TERM_REF_PIC_IDX_WIDTH - TEMPORAL_MVP_ENABLED_WIDTH - SAO_LUMA_WIDTH - SAO_CHROMA_WIDTH - NUM_REF_IDX_L0_MINUS1_WIDTH - NUM_REF_IDX_L1_MINUS1_WIDTH - MAX_MERGE_CAND_WIDTH - SLICE_TYPE_WIDTH - COLLOCATED_FROM_L0_FLAG_WIDTH];
// `endif
        end
        `INTER_TOP_SLICE_2: begin
// `ifdef READ_FILE
            // collocated_ref_idx <= config_data_bus_in[    HEADER_WIDTH + SLICE_CB_QP_OFFSET_WIDTH + SLICE_CR_QP_OFFSET_WIDTH + DISABLE_DBF_WIDTH + SLICE_BETA_OFFSET_DIV2_WIDTH + SLICE_TC_OFFSET_DIV2_WIDTH + COLLOCATED_REF_IDX_WIDTH - 2:
                                                    // HEADER_WIDTH + SLICE_CB_QP_OFFSET_WIDTH + SLICE_CR_QP_OFFSET_WIDTH + DISABLE_DBF_WIDTH + SLICE_BETA_OFFSET_DIV2_WIDTH + SLICE_TC_OFFSET_DIV2_WIDTH];
// `else
             // collocated_ref_idx <= config_data_bus_in[    INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - SLICE_CB_QP_OFFSET_WIDTH - SLICE_CR_QP_OFFSET_WIDTH - DISABLE_DBF_WIDTH - SLICE_BETA_OFFSET_DIV2_WIDTH - SLICE_TC_OFFSET_DIV2_WIDTH - 2:
                                                    // INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - SLICE_CB_QP_OFFSET_WIDTH - SLICE_CR_QP_OFFSET_WIDTH - DISABLE_DBF_WIDTH - SLICE_BETA_OFFSET_DIV2_WIDTH - SLICE_TC_OFFSET_DIV2_WIDTH - COLLOCATED_REF_IDX_WIDTH];
// `endif
        end        
        `INTER_TOP_CURR_POC: begin
// `ifdef READ_FILE
            // current_poc <=  config_data_bus_in[POC_WIDTH - 1: 0];
// `else
            // current_poc <=  config_data_bus_in[INTER_TOP_CONFIG_BUS_WIDTH -1 : INTER_TOP_CONFIG_BUS_WIDTH - POC_WIDTH];
// `endif
        end
        `INTER_CURRENT_PIC_DPB_IDX: begin
            // next_pic_dpb_idx <= config_data_bus_in[INTER_TOP_CONFIG_BUS_WIDTH -1: INTER_TOP_CONFIG_BUS_WIDTH - NUM_REF_IDX_L0_MINUS1_WIDTH];
        end
        `INTER_SLICE_TILE_INFO: begin
            
        end
        `INTER_CTU0_HEADER: begin
            // new_ctu_start_to_col <= 1;
`ifdef READ_FILE
            next_ctb_xx_in_min_tus <= config_data_bus_in[HEADER_WIDTH + X_ADDR_WDTH   -2: HEADER_WIDTH] >> LOG2_MIN_TU_SIZE;                 // 12-bit input assigned to 11- bit output MSB dropped
            next_ctb_yy_in_min_tus <= config_data_bus_in[HEADER_WIDTH + 2*X_ADDR_WDTH -2: HEADER_WIDTH + X_ADDR_WDTH] >> LOG2_MIN_TU_SIZE;
`else                   
            next_ctb_xx_in_min_tus <= config_data_bus_in[INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH -2: INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - X_ADDR_WDTH];                 // 12-bit input assigned to 11- bit output MSB dropped
            next_ctb_yy_in_min_tus <= config_data_bus_in[INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - X_ADDR_WDTH - 2: INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - 2*X_ADDR_WDTH];
`endif
        end
        `INTER_CU_HEADER: begin
`ifdef READ_FILE
            next_cb_xx_in_min_tus <= next_ctb_xx_in_min_tus + (config_data_bus_in[   HEADER_WIDTH + X0_WIDTH - 1: 
                                                          HEADER_WIDTH]>> LOG2_MIN_TU_SIZE);
            next_cb_yy_in_min_tus <= next_ctb_yy_in_min_tus + (config_data_bus_in[   HEADER_WIDTH + 2*X0_WIDTH -1:
                                                HEADER_WIDTH + X0_WIDTH]>> LOG2_MIN_TU_SIZE);
            next_cb_pred_mode <=  config_data_bus_in[     HEADER_WIDTH + 2*X0_WIDTH + LOG2_CTB_WIDTH];
            next_cb_part_mode <=  config_data_bus_in[     HEADER_WIDTH + 2*X0_WIDTH + LOG2_CTB_WIDTH + PREDMODE_WIDTH + PARTMODE_WIDTH -1:
                                                HEADER_WIDTH + 2*X0_WIDTH + LOG2_CTB_WIDTH + PREDMODE_WIDTH];
`else
            next_cb_xx_in_min_tus <= next_ctb_xx_in_min_tus + (config_data_bus_in[   INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - 1: 
                                                INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - X0_WIDTH]>> LOG2_MIN_TU_SIZE);
            next_cb_yy_in_min_tus <= next_ctb_yy_in_min_tus + (config_data_bus_in[   INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - X0_WIDTH - 1: 
                                                INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - 2*X0_WIDTH]>> LOG2_MIN_TU_SIZE);
            next_cb_pred_mode <=  config_data_bus_in[     INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - 2*X0_WIDTH - LOG2_CTB_WIDTH - 1];
            next_cb_part_mode <=  config_data_bus_in[     INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - 2*X0_WIDTH - LOG2_CTB_WIDTH - PREDMODE_WIDTH - 1:
                                                INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - 2*X0_WIDTH - LOG2_CTB_WIDTH - PREDMODE_WIDTH - PARTMODE_WIDTH];
`endif
            // next_cb_size <=  cb_size_wire ;
            next_cb_size_in_min_tus <=  cb_size_wire_in_min_pus ;
            end

    endcase
end

endmodule 