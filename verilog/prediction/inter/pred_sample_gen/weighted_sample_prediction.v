`timescale 1ns / 1ps
module weighted_sample_prediction(
    clk,
    reset,
    xT_in_min_tus,
    yT_in_min_tus,
    xT_out_min_tus,
    yT_out_min_tus,
    valid_in,
    pred_pix_in_4x4,
    pred_pix_out_4x4,
    valid_out
);
//---------------------------------------------------------------------------------------------------------------------
// Global constant headers
//---------------------------------------------------------------------------------------------------------------------
    `include "pred_def.v"

//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------
    parameter   BLOCK_WIDTH_4x4 = 3'd4;
    parameter   SHIFT1 = 4'd14 - PIXEL_WIDTH;
    parameter   SHIFT2 = 4'd15 - PIXEL_WIDTH;

	parameter OFFSET1 = SHIFT1 > 1'b0 ? 1<<(SHIFT1-1'b1) : 1'b0;
	parameter OFFSET2 = 1<< (SHIFT2-1'b1);

    parameter PIXEL_MAX = 255;
//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    
    input clk;
    input reset;
    input [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] xT_in_min_tus;
    input [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] yT_in_min_tus;
    output reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] xT_out_min_tus;
    output reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] yT_out_min_tus;
    input valid_in;
    input signed [BLOCK_WIDTH_4x4 *  BLOCK_WIDTH_4x4 * FILTER_PIXEL_WIDTH -1:0]         pred_pix_in_4x4;    
    output reg [BLOCK_WIDTH_4x4 *  BLOCK_WIDTH_4x4 * PIXEL_WIDTH -1:0]                  pred_pix_out_4x4;    
    output reg valid_out;

//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------
    reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] xT_in_min_tus_d;
    reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] yT_in_min_tus_d;
    
    reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] xT_in_min_tus_2d;
    reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] yT_in_min_tus_2d;
    
    reg valid_in_d;
    reg initial_cond;
    
    integer i,j,k;
    
    reg signed [FILTER_PIXEL_WIDTH - 1:0] in_store_wire   [BLOCK_WIDTH_4x4 - 1: 0][BLOCK_WIDTH_4x4 -1: 0];
    reg signed [FILTER_PIXEL_WIDTH - 1:0] in_store        [BLOCK_WIDTH_4x4 - 1: 0][BLOCK_WIDTH_4x4 -1: 0];
    reg signed [FILTER_PIXEL_WIDTH    :0] in_store_d      [BLOCK_WIDTH_4x4 - 1: 0][BLOCK_WIDTH_4x4 -1: 0];
    reg signed [FILTER_PIXEL_WIDTH    :0] in_store_2d     [BLOCK_WIDTH_4x4 - 1: 0][BLOCK_WIDTH_4x4 -1: 0];
    
    reg [PIXEL_WIDTH - 1:0] out_store[BLOCK_WIDTH_4x4 - 1: 0][BLOCK_WIDTH_4x4 -1: 0];

    reg is_prev_4x4_bi_pred;
//---------------------------------------------------------------------------------------------------------------------
// Implmentation
//---------------------------------------------------------------------------------------------------------------------

// Instantiate the module

always@(*) begin
    for(j=0;j <BLOCK_WIDTH_4x4 ; j=j+1 ) begin
        for(i=0 ; i < BLOCK_WIDTH_4x4 ; i = i+1) begin
            for(k=0; k< FILTER_PIXEL_WIDTH ; k = k+1) begin
                in_store_wire[j][i][k] = pred_pix_in_4x4[(j*BLOCK_WIDTH_4x4 + i)*FILTER_PIXEL_WIDTH + k];
            end
        end
    end
end

always@(*) begin
    for(j=0;j <BLOCK_WIDTH_4x4 ; j=j+1 ) begin
        for(i=0 ; i < BLOCK_WIDTH_4x4 ; i = i+1) begin
            for(k=0; k< PIXEL_WIDTH ; k = k+1) begin
                pred_pix_out_4x4[(j*BLOCK_WIDTH_4x4 + i)*PIXEL_WIDTH + k] = out_store[j][i][k];
            end
        end
    end
end


always@(*) begin
    if(is_prev_4x4_bi_pred) begin
        for(j=0;j <BLOCK_WIDTH_4x4 ; j=j+1 ) begin
            for(i=0 ; i < BLOCK_WIDTH_4x4 ; i = i+1) begin
                in_store_2d[j][i] = $signed($signed(in_store_d[j][i]) + $signed(OFFSET2))>>> SHIFT2;
            end
        end    
    end
    else begin
        for(j=0;j <BLOCK_WIDTH_4x4 ; j=j+1 ) begin
            for(i=0 ; i < BLOCK_WIDTH_4x4 ; i = i+1) begin
                in_store_2d[j][i] = $signed($signed(in_store_d[j][i]) + $signed(OFFSET1))>>> SHIFT1;
            end
        end    
    end
end

always@(posedge clk) begin    
    if(reset) begin
        xT_in_min_tus_d <= -1;
        yT_in_min_tus_d <= -1;
        initial_cond <= 0;
    end
    else begin
        if(valid_in) begin
            xT_in_min_tus_d <= xT_in_min_tus;
            yT_in_min_tus_d <= yT_in_min_tus;
            initial_cond <= 1;
            for(j=0;j <BLOCK_WIDTH_4x4 ; j=j+1 ) begin
                for(i=0 ; i < BLOCK_WIDTH_4x4 ; i = i+1) begin
                    in_store[j][i] <= in_store_wire[j][i];
                end
            end
        end
    end
end

always@(posedge clk) begin
    if(reset) begin
        is_prev_4x4_bi_pred <= 0;
        valid_in_d <= 0;
    end
    else begin
        if(initial_cond & valid_in) begin
            if(is_prev_4x4_bi_pred == 1) begin
                is_prev_4x4_bi_pred <= 0;
                valid_in_d <= 0;
            end
            else begin
                valid_in_d <= 1;
                xT_in_min_tus_2d <= xT_in_min_tus_d;
                yT_in_min_tus_2d <= yT_in_min_tus_d;
                
                if((xT_in_min_tus == xT_in_min_tus_d) && (yT_in_min_tus == yT_in_min_tus_d)) begin
                    is_prev_4x4_bi_pred <= 1;
                    for(j=0;j <BLOCK_WIDTH_4x4 ; j=j+1 ) begin
                        for(i=0 ; i < BLOCK_WIDTH_4x4 ; i = i+1) begin
                            in_store_d[j][i] <= in_store_wire[j][i] + in_store[j][i];
                        end
                    end
                end
                else begin
                    is_prev_4x4_bi_pred <= 0;
                    for(j=0;j <BLOCK_WIDTH_4x4 ; j=j+1 ) begin
                        for(i=0 ; i < BLOCK_WIDTH_4x4 ; i = i+1) begin
                            in_store_d[j][i] <= {in_store[j][i][FILTER_PIXEL_WIDTH-1], in_store[j][i]};
                        end
                    end
                end
            end
        end
        else begin
            valid_in_d <= 0;
        end
    end
end

always@(posedge clk) begin
    if(reset) begin
        valid_out <= 0;
    end
    else begin
        for(j=0;j <BLOCK_WIDTH_4x4 ; j=j+1 ) begin
            for(i=0 ; i < BLOCK_WIDTH_4x4 ; i = i+1) begin
                if($signed(in_store_2d[j][i]) > PIXEL_MAX) begin
                    out_store[j][i] <= {PIXEL_WIDTH{1'b1}};
                end
                else if(in_store_2d[j][i][FILTER_PIXEL_WIDTH]) begin
                    out_store[j][i] <= {PIXEL_WIDTH{1'b0}};
                end
                else begin
                    out_store[j][i] <= in_store_2d[j][i];   //warning dunnata awulak naha
                end
            end
        end
        valid_out <= valid_in_d;
        xT_out_min_tus <= xT_in_min_tus_2d;
        yT_out_min_tus <= yT_in_min_tus_2d;    
    end
end

endmodule 