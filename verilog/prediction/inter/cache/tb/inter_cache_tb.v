`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   14:29:26 08/20/2013
// Design Name:   inter_cache
// Module Name:   D:/090250V/SEMESTER 7/HEVC decoder/HDL codes/Cache/tb/inter_cache_tb.v
// Project Name:  Inter_HDL
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: inter_cache
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////


module inter_cache_tb;

    `include "cache_configs_def.v"
	// Inputs
	reg clk;
	reg reset;
	reg [11:0] start_x_in;
	reg [11:0] start_y_in;
	reg [3:0] rf_blk_hgt_in;
	reg [3:0] rf_blk_wdt_in;
	reg [3:0] ref_idx_in;
	reg valid_in;
	wire axi_ar_ready;
	wire [127:0] axi_r_data;
	wire [1:0] axi_r_resp;
	wire axi_r_last;
	wire axi_r_valid;

	// Outputs
	wire [LUMA_BITS*OUT_BLOCK_SIZE-1:0] block_121_out;
	wire block_ready;
	wire [31:0] axi_ar_addr;
	wire [7:0] axi_ar_len;
	wire [2:0] axi_ar_size;
	wire [1:0] axi_ar_burst;
	wire [2:0] axi_ar_prot;
	wire axi_ar_valid;
	wire axi_r_ready;
    wire axi_clk;
    
    integer file_in,rfile;
    integer file_out;

    wire                        SL_AXI_ARESET_OUT_N;
    wire                        SL_AXI_ACLK            ;
    wire       [3 : 0]          SL_AXI_AWID      ;
    wire       [31 : 0]         SL_AXI_AWADDR   ;
    wire       [7 : 0]          SL_AXI_AWLEN     ;
    wire       [2 : 0]          SL_AXI_AWSIZE    ;
    wire       [1 : 0]          SL_AXI_AWBURST   ;
    wire                        SL_AXI_AWLOCK            ;
    wire       [3 : 0]          SL_AXI_AWCACHE   ;
    wire       [2 : 0]          SL_AXI_AWPROT    ;
    wire       [3 : 0]          SL_AXI_AWQOS     ;
    wire                        SL_AXI_AWVALID           ;
    wire                        SL_AXI_AWREADY            ;
    wire       [127 : 0]        SL_AXI_WDATA   ;
    wire       [15 : 0]         SL_AXI_WSTRB    ;
    wire                        SL_AXI_WLAST             ;
    wire                        SL_AXI_WVALID            ;
    wire                        SL_AXI_WREADY             ;
    wire       [3 : 0]          SL_AXI_BID        ;
    wire       [1 : 0]          SL_AXI_BRESP      ;
    wire                        SL_AXI_BVALID             ;
    wire                        SL_AXI_BREADY            ;
    wire       [3 : 0]          SL_AXI_ARID      ;
    wire       [31 : 0]         SL_AXI_ARADDR   ;
    wire       [7 : 0]          SL_AXI_ARLEN     ;
    wire       [2 : 0]          SL_AXI_ARSIZE    ;
    wire       [1 : 0]          SL_AXI_ARBURST   ;
    wire                        SL_AXI_ARLOCK            ;
    wire       [3 : 0]          SL_AXI_ARCACHE   ;
    wire       [2 : 0]          SL_AXI_ARPROT    ;
    wire       [3 : 0]          SL_AXI_ARQOS     ;
    wire                        SL_AXI_ARVALID           ;
    wire                        SL_AXI_ARREADY            ;
    wire       [3 : 0]          SL_AXI_RID        ;
    wire       [127 : 0]        SL_AXI_RDATA    ;
    wire       [1 : 0]          SL_AXI_RRESP      ;
    wire                        SL_AXI_RLAST              ;
    wire                        SL_AXI_RVALID             ;
    wire                        SL_AXI_RREADY            ;

    integer block_num ;

       
	// Instantiate the Unit Under Test (UUT)
	inter_cache uut (
		.clk(clk), 
		.reset(reset), 
		.start_x_in(start_x_in), 
		.start_y_in(start_y_in), 
		.rf_blk_hgt_in(rf_blk_hgt_in), 
		.rf_blk_wdt_in(rf_blk_wdt_in), 
		.ref_idx_in(ref_idx_in), 
		.valid_in(valid_in), 
		.block_121_out(block_121_out), 
		.block_ready(block_ready), 
        .axi_clk(axi_clk),
		.axi_ar_addr(axi_ar_addr), 
		.axi_ar_len(axi_ar_len), 
		.axi_ar_size(axi_ar_size), 
		.axi_ar_burst(axi_ar_burst), 
		.axi_ar_prot(axi_ar_prot), 
		.axi_ar_valid(axi_ar_valid), 
		.axi_ar_ready(axi_ar_ready), 
		.axi_r_data(axi_r_data), 
		.axi_r_resp(axi_r_resp), 
		.axi_r_last(axi_r_last), 
		.axi_r_valid(axi_r_valid), 
		.axi_r_ready(axi_r_ready)
	);

	initial begin
		// Initialize Inputs
        file_in = $fopen("z_scn_dump.csv","r");
        file_out = $fopen("age_dump_4k.csv","r");
		clk = 0;
		reset = 1;
		start_x_in = 0;
		start_y_in = 0;
		rf_blk_hgt_in = 0;
		rf_blk_wdt_in = 0;
		ref_idx_in = 0;
		valid_in = 0;
        block_num = 0;

		// Wait 100 ns for global reset to finish
        #100;
        reset = 0;
		#1000;
        //reset stabilization time
        #30;
        repeat(58000) begin
            load_input();
            @(posedge block_ready);
            compare_results();
            #200;
            block_num = block_num + 1;
        end
		// Add stimulus here

	end
    
    initial begin
    $monitor("|t=  %d   |x=  %d   |y=  %d   |h=  %d   |w=    %d    |r=   %d   ",
    $time,start_x_in,start_y_in,rf_blk_hgt_in, rf_blk_wdt_in, ref_idx_in);
  
//    $monitor("|t=  %d   |bnk1=  %d   |bnk2=  %d   |bnk3=  %d   |bnk4=  %d   ",
//    $time,uut.ishit1,uut.ishit2,uut.ishit3, uut.ishit4);
    end


//always @ *if(block_ready) $finish;
            

always #10 clk = ~clk;

task load_input;

    reg       [X_ADDR_WDTH-1:0]    	            start_x_file_in;
    reg       [X_ADDR_WDTH-1:0]    	            end_x_file_in;    
    reg       [Y_ADDR_WDTH-1:0]    	            start_y_file_in;
    reg       [Y_ADDR_WDTH-1:0]    	            end_y_file_in;
    reg       [REF_ADDR_WDTH-1:0]		        ref_idx_file_in; 
    integer      mv_block_nm;

	begin
        rfile = $fscanf(file_in,"%d,%d,%d,%d,%d,%d\n",start_x_file_in,end_x_file_in,start_y_file_in,end_y_file_in,ref_idx_file_in,mv_block_nm);
        @(posedge clk)
		start_x_in = start_x_file_in;
        start_y_in = start_y_file_in;
        rf_blk_hgt_in = end_y_file_in - start_y_file_in;
        rf_blk_wdt_in = end_x_file_in - start_x_file_in;
        ref_idx_in = ref_idx_file_in;
        valid_in = 1;
        @(posedge clk)
        valid_in = 0;
	end
endtask

task compare_results;

    reg       [(C_N_WAY)*(1<<C_N_WAY)-1:0]    	ages_set;
    reg       [(C_N_WAY)-1:0]                   new_age_temp [(1<<C_N_WAY)-1:0];
    reg       [X_ADDR_WDTH-1:0]    	            end_x_file_in;    
    reg       [Y_ADDR_WDTH-1:0]    	            start_y_file_in;
    reg       [Y_ADDR_WDTH-1:0]    	            end_y_file_in;
    reg       [REF_ADDR_WDTH-1:0]		        ref_idx_file_in; 
    integer i;

	begin
            for(i=0;i<4;i=i+1) begin
                        rfile = $fscanf(file_out,"%d,%d,%d,%d,%d,%d,%d,%d,\n",   
                                                                    ages_set[C_N_WAY*8-1:C_N_WAY*7],
                                                                    ages_set[C_N_WAY*7-1:C_N_WAY*6],
                                                                    ages_set[C_N_WAY*6-1:C_N_WAY*5],
                                                                    ages_set[C_N_WAY*5-1:C_N_WAY*4],
                                                                    ages_set[C_N_WAY*4-1:C_N_WAY*3],
                                                                    ages_set[C_N_WAY*3-1:C_N_WAY*2],
                                                                    ages_set[C_N_WAY*2-1:C_N_WAY*1],
                                                                    ages_set[C_N_WAY*1-1:C_N_WAY*0]);
                                                                    
                if(uut.age_bank1.mem[i] != ages_set) begin
                    $display("bank 1 mismatch");
                    $stop;
                end
            end
            for(i=0;i<4;i=i+1) begin
                        rfile = $fscanf(file_out,"%d,%d,%d,%d,%d,%d,%d,%d,\n",   
                                                                    ages_set[C_N_WAY*8-1:C_N_WAY*7],
                                                                    ages_set[C_N_WAY*7-1:C_N_WAY*6],
                                                                    ages_set[C_N_WAY*6-1:C_N_WAY*5],
                                                                    ages_set[C_N_WAY*5-1:C_N_WAY*4],
                                                                    ages_set[C_N_WAY*4-1:C_N_WAY*3],
                                                                    ages_set[C_N_WAY*3-1:C_N_WAY*2],
                                                                    ages_set[C_N_WAY*2-1:C_N_WAY*1],
                                                                    ages_set[C_N_WAY*1-1:C_N_WAY*0]);
                                                                    
                if(uut.age_bank2.mem[i] != ages_set) begin
                    $display("bank 2 mismatch");
                    $stop;
                end
            end
            for(i=0;i<4;i=i+1) begin
                        rfile = $fscanf(file_out,"%d,%d,%d,%d,%d,%d,%d,%d,\n",   
                                                                    ages_set[C_N_WAY*8-1:C_N_WAY*7],
                                                                    ages_set[C_N_WAY*7-1:C_N_WAY*6],
                                                                    ages_set[C_N_WAY*6-1:C_N_WAY*5],
                                                                    ages_set[C_N_WAY*5-1:C_N_WAY*4],
                                                                    ages_set[C_N_WAY*4-1:C_N_WAY*3],
                                                                    ages_set[C_N_WAY*3-1:C_N_WAY*2],
                                                                    ages_set[C_N_WAY*2-1:C_N_WAY*1],
                                                                    ages_set[C_N_WAY*1-1:C_N_WAY*0]);
                    
                if(uut.age_bank3.mem[i] != ages_set) begin
                    $display("bank 3 mismatch");
                    $stop;
                end
            end
            for(i=0;i<4;i=i+1) begin
                        rfile = $fscanf(file_out,"%d,%d,%d,%d,%d,%d,%d,%d,\n",   
                                                                    ages_set[C_N_WAY*8-1:C_N_WAY*7],
                                                                    ages_set[C_N_WAY*7-1:C_N_WAY*6],
                                                                    ages_set[C_N_WAY*6-1:C_N_WAY*5],
                                                                    ages_set[C_N_WAY*5-1:C_N_WAY*4],
                                                                    ages_set[C_N_WAY*4-1:C_N_WAY*3],
                                                                    ages_set[C_N_WAY*3-1:C_N_WAY*2],
                                                                    ages_set[C_N_WAY*2-1:C_N_WAY*1],
                                                                    ages_set[C_N_WAY*1-1:C_N_WAY*0]);
                    
                if(uut.age_bank4.mem[i] != ages_set) begin
                    $display("bank 4 mismatch");
                    $stop;
                end
            end    
            rfile = $fscanf(file_out,",\n");
            
	end
endtask


bus_axi bus_block (
  .INTERCONNECT_ACLK(clk), // input INTERCONNECT_ACLK
  .INTERCONNECT_ARESETN(!reset), // input INTERCONNECT_ARESETN
  .S00_AXI_ARESET_OUT_N         (axi_reset), // output S00_AXI_ARESET_OUT_N
  .S00_AXI_ACLK                 (axi_clk), // input S00_AXI_ACLK
  .S00_AXI_AWID                 (1'd0 ), // input [0 : 0] S00_AXI_AWID
  .S00_AXI_AWADDR               (32'd0), // input [31 : 0] S00_AXI_AWADDR
  .S00_AXI_AWLEN                (8'd0 ), // input [7 : 0] S00_AXI_AWLEN
  .S00_AXI_AWSIZE               (3'd0 ), // input [2 : 0] S00_AXI_AWSIZE
  .S00_AXI_AWBURST              (2'd0 ), // input [1 : 0] S00_AXI_AWBURST
  .S00_AXI_AWLOCK               (1'd0), // input S00_AXI_AWLOCK
  .S00_AXI_AWCACHE              (4'd0), // input [3 : 0] S00_AXI_AWCACHE
  .S00_AXI_AWPROT               (3'd0), // input [2 : 0] S00_AXI_AWPROT
  .S00_AXI_AWQOS                (4'd0), // input [3 : 0] S00_AXI_AWQOS
  .S00_AXI_AWVALID              (1'd0), // input S00_AXI_AWVALID
  .S00_AXI_AWREADY              (), // output S00_AXI_AWREADY
  .S00_AXI_WDATA                (128'd0), // input [127 : 0] S00_AXI_WDATA
  .S00_AXI_WSTRB                (16'd0), // input [15 : 0] S00_AXI_WSTRB
  .S00_AXI_WLAST                (1'd0), // input S00_AXI_WLAST
  .S00_AXI_WVALID               (1'd0), // input S00_AXI_WVALID
  .S00_AXI_WREADY               (), // output S00_AXI_WREADY
  .S00_AXI_BID                  (), // output [0 : 0] S00_AXI_BID
  .S00_AXI_BRESP                (), // output [1 : 0] S00_AXI_BRESP
  .S00_AXI_BVALID               (), // output S00_AXI_BVALID
  .S00_AXI_BREADY               (1'd0), // input S00_AXI_BREADY
  .S00_AXI_ARID                 (1'd0), // input [0 : 0] S00_AXI_ARID
  .S00_AXI_ARADDR               (axi_ar_addr), // input [31 : 0] S00_AXI_ARADDR
  .S00_AXI_ARLEN                (axi_ar_len), // input [7 : 0] S00_AXI_ARLEN
  .S00_AXI_ARSIZE               (axi_ar_size), // input [2 : 0] S00_AXI_ARSIZE
  .S00_AXI_ARBURST              (axi_ar_burst), // input [1 : 0] S00_AXI_ARBURST
  .S00_AXI_ARLOCK               (1'd0), // input S00_AXI_ARLOCK
  .S00_AXI_ARCACHE              (4'd0), // input [3 : 0] S00_AXI_ARCACHE
  .S00_AXI_ARPROT               (axi_ar_prot), // input [2 : 0] S00_AXI_ARPROT
  .S00_AXI_ARQOS                (4'd0), // input [3 : 0] S00_AXI_ARQOS
  .S00_AXI_ARVALID              (axi_ar_valid), // input S00_AXI_ARVALID
  .S00_AXI_ARREADY              (axi_ar_ready), // output S00_AXI_ARREADY
  .S00_AXI_RID                  (), // output [0 : 0] S00_AXI_RID
  .S00_AXI_RDATA                (axi_r_data), // output [127 : 0] S00_AXI_RDATA
  .S00_AXI_RRESP                (axi_r_resp), // output [1 : 0] S00_AXI_RRESP
  .S00_AXI_RLAST                (axi_r_last), // output S00_AXI_RLAST
  .S00_AXI_RVALID               (axi_r_valid), // output S00_AXI_RVALID
  .S00_AXI_RREADY               (axi_r_ready), // input S00_AXI_RREADY
  .M00_AXI_ARESET_OUT_N         (SL_AXI_ARESET_OUT_N), // output M00_AXI_ARESET_OUT_N
  .M00_AXI_ACLK                 (SL_AXI_ACLK        ),// input M00_AXI_ACLK
  .M00_AXI_AWID                 (SL_AXI_AWID        ),// output [3 : 0] M00_AXI_AWID
  .M00_AXI_AWADDR               (SL_AXI_AWADDR      ),//// output [31 : 0] M00_AXI_AWADDR
  .M00_AXI_AWLEN                (SL_AXI_AWLEN       ),/// output [7 : 0] M00_AXI_AWLEN
  .M00_AXI_AWSIZE               (SL_AXI_AWSIZE      ),//// output [2 : 0] M00_AXI_AWSIZE
  .M00_AXI_AWBURST              (SL_AXI_AWBURST     ),// // output [1 : 0] M00_AXI_AWBURST
  .M00_AXI_AWLOCK               (SL_AXI_AWLOCK      ),//// output M00_AXI_AWLOCK
  .M00_AXI_AWCACHE              (SL_AXI_AWCACHE     ),// // output [3 : 0] M00_AXI_AWCACHE
  .M00_AXI_AWPROT               (SL_AXI_AWPROT      ),//// output [2 : 0] M00_AXI_AWPROT
  .M00_AXI_AWQOS                (SL_AXI_AWQOS       ),/// output [3 : 0] M00_AXI_AWQOS
  .M00_AXI_AWVALID              (SL_AXI_AWVALID     ),// // output M00_AXI_AWVALID
  .M00_AXI_AWREADY              (SL_AXI_AWREADY     ),// // input M00_AXI_AWREADY
  .M00_AXI_WDATA                (SL_AXI_WDATA       ),/// output [127 : 0] M00_AXI_WDATA
  .M00_AXI_WSTRB                (SL_AXI_WSTRB       ),/// output [15 : 0] M00_AXI_WSTRB
  .M00_AXI_WLAST                (SL_AXI_WLAST       ),/// output M00_AXI_WLAST
  .M00_AXI_WVALID               (SL_AXI_WVALID      ),//// output M00_AXI_WVALID
  .M00_AXI_WREADY               (SL_AXI_WREADY      ),//// input M00_AXI_WREADY
  .M00_AXI_BID                  (SL_AXI_BID         ),//input [3 : 0] M00_AXI_BID
  .M00_AXI_BRESP                (SL_AXI_BRESP       ),/// input [1 : 0] M00_AXI_BRESP
  .M00_AXI_BVALID               (SL_AXI_BVALID      ),//// input M00_AXI_BVALID
  .M00_AXI_BREADY               (SL_AXI_BREADY      ),//// output M00_AXI_BREADY
  .M00_AXI_ARID                 (SL_AXI_ARID        ),// output [3 : 0] M00_AXI_ARID
  .M00_AXI_ARADDR               (SL_AXI_ARADDR      ),//// output [31 : 0] M00_AXI_ARADDR
  .M00_AXI_ARLEN                (SL_AXI_ARLEN       ),/// output [7 : 0] M00_AXI_ARLEN
  .M00_AXI_ARSIZE               (SL_AXI_ARSIZE      ),//// output [2 : 0] M00_AXI_ARSIZE
  .M00_AXI_ARBURST              (SL_AXI_ARBURST     ),// // output [1 : 0] M00_AXI_ARBURST
  .M00_AXI_ARLOCK               (SL_AXI_ARLOCK      ),//// output M00_AXI_ARLOCK
  .M00_AXI_ARCACHE              (SL_AXI_ARCACHE     ),// // output [3 : 0] M00_AXI_ARCACHE
  .M00_AXI_ARPROT               (SL_AXI_ARPROT      ),//// output [2 : 0] M00_AXI_ARPROT
  .M00_AXI_ARQOS                (SL_AXI_ARQOS       ),/// output [3 : 0] M00_AXI_ARQOS
  .M00_AXI_ARVALID              (SL_AXI_ARVALID     ),// // output M00_AXI_ARVALID
  .M00_AXI_ARREADY              (SL_AXI_ARREADY     ),// // input M00_AXI_ARREADY
  .M00_AXI_RID                  (SL_AXI_RID         ),//input [3 : 0] M00_AXI_RID
  .M00_AXI_RDATA                (SL_AXI_RDATA       ),/// input [127 : 0] M00_AXI_RDATA
  .M00_AXI_RRESP                (SL_AXI_RRESP       ),/// input [1 : 0] M00_AXI_RRESP
  .M00_AXI_RLAST                (SL_AXI_RLAST       ),/// input M00_AXI_RLAST
  .M00_AXI_RVALID               (SL_AXI_RVALID      ),//// input M00_AXI_RVALID
  .M00_AXI_RREADY               (SL_AXI_RREADY      )// output M00_AXI_RREADY
);






// Instantiate the module
axi_ddr_slave dummy_ddr (
    .clk(clk), 
    .SL_AXI_ARESET_OUT_N(SL_AXI_ARESET_OUT_N), 
    .SL_AXI_ACLK(SL_AXI_ACLK), 
    .SL_AXI_AWID(SL_AXI_AWID), 
    .SL_AXI_AWADDR(SL_AXI_AWADDR), 
    .SL_AXI_AWLEN(SL_AXI_AWLEN), 
    .SL_AXI_AWSIZE(SL_AXI_AWSIZE), 
    .SL_AXI_AWBURST(SL_AXI_AWBURST), 
    .SL_AXI_AWLOCK(SL_AXI_AWLOCK), 
    .SL_AXI_AWCACHE(SL_AXI_AWCACHE), 
    .SL_AXI_AWPROT(SL_AXI_AWPROT), 
    .SL_AXI_AWQOS(SL_AXI_AWQOS), 
    .SL_AXI_AWVALID(SL_AXI_AWVALID), 
    .SL_AXI_AWREADY(SL_AXI_AWREADY), 
    .SL_AXI_WDATA(SL_AXI_WDATA), 
    .SL_AXI_WSTRB(SL_AXI_WSTRB), 
    .SL_AXI_WLAST(SL_AXI_WLAST), 
    .SL_AXI_WVALID(SL_AXI_WVALID), 
    .SL_AXI_WREADY(SL_AXI_WREADY), 
    .SL_AXI_BID(SL_AXI_BID), 
    .SL_AXI_BRESP(SL_AXI_BRESP), 
    .SL_AXI_BVALID(SL_AXI_BVALID), 
    .SL_AXI_BREADY(SL_AXI_BREADY), 
    .SL_AXI_ARID(SL_AXI_ARID), 
    .SL_AXI_ARADDR(SL_AXI_ARADDR), 
    .SL_AXI_ARLEN(SL_AXI_ARLEN), 
    .SL_AXI_ARSIZE(SL_AXI_ARSIZE), 
    .SL_AXI_ARBURST(SL_AXI_ARBURST), 
    .SL_AXI_ARLOCK(SL_AXI_ARLOCK), 
    .SL_AXI_ARCACHE(SL_AXI_ARCACHE), 
    .SL_AXI_ARPROT(SL_AXI_ARPROT), 
    .SL_AXI_ARQOS(SL_AXI_ARQOS), 
    .SL_AXI_ARVALID(SL_AXI_ARVALID), 
    .SL_AXI_ARREADY(SL_AXI_ARREADY), 
    .SL_AXI_RID(SL_AXI_RID), 
    .SL_AXI_RDATA(SL_AXI_RDATA), 
    .SL_AXI_RRESP(SL_AXI_RRESP), 
    .SL_AXI_RLAST(SL_AXI_RLAST), 
    .SL_AXI_RVALID(SL_AXI_RVALID), 
    .SL_AXI_RREADY(SL_AXI_RREADY)
    );




endmodule

