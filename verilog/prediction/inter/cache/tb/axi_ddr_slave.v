`timescale 1ns / 1ps
module axi_ddr_slave
(
    clk,
  SL_AXI_ARESET_OUT_N,
  SL_AXI_ACLK       ,          
  SL_AXI_AWID       ,          
  SL_AXI_AWADDR     ,          
  SL_AXI_AWLEN      ,          
  SL_AXI_AWSIZE     ,          
  SL_AXI_AWBURST    ,          
  SL_AXI_AWLOCK     ,          
  SL_AXI_AWCACHE    ,          
  SL_AXI_AWPROT     ,          
  SL_AXI_AWQOS      ,          
  SL_AXI_AWVALID    ,          
  SL_AXI_AWREADY    ,          
  SL_AXI_WDATA      ,          
  SL_AXI_WSTRB      ,          
  SL_AXI_WLAST      ,          
  SL_AXI_WVALID     ,          
  SL_AXI_WREADY     ,          
  SL_AXI_BID        ,          
  SL_AXI_BRESP      ,          
  SL_AXI_BVALID     ,          
  SL_AXI_BREADY     ,          
  SL_AXI_ARID       ,          
  SL_AXI_ARADDR     ,          
  SL_AXI_ARLEN      ,          
  SL_AXI_ARSIZE     ,          
  SL_AXI_ARBURST    ,          
  SL_AXI_ARLOCK     ,          
  SL_AXI_ARCACHE    ,          
  SL_AXI_ARPROT     ,          
  SL_AXI_ARQOS      ,          
  SL_AXI_ARVALID    ,          
  SL_AXI_ARREADY    ,          
  SL_AXI_RID        ,          
  SL_AXI_RDATA      ,          
  SL_AXI_RRESP      ,          
  SL_AXI_RLAST      ,          
  SL_AXI_RVALID     ,          
  SL_AXI_RREADY               

);

    `include "axi_interface_def.v"
    
    //---------------------------------------------------------------------------------------------------------------------
    // parameter definitions
    //---------------------------------------------------------------------------------------------------------------------

    
    //---------------------------------------------------------------------------------------------------------------------
    // localparam definitions
    //---------------------------------------------------------------------------------------------------------------------
    
    localparam                          STATE_IDLE               = 0;    // the state that is entered upon reset
    localparam                          STATE_SEND_RDATA                = 1;
    localparam                          STATE_ADDR_READY         = 2;    
    
    
    
    //---------------------------------------------------------------------------------------------------------------------
    // I/O signals
    //---------------------------------------------------------------------------------------------------------------------

    input                       SL_AXI_ARESET_OUT_N;
    output reg                  SL_AXI_ACLK            ;
    input      [3 : 0]          SL_AXI_AWID      ;
    input      [31 : 0]         SL_AXI_AWADDR   ;
    input      [7 : 0]          SL_AXI_AWLEN     ;
    input      [2 : 0]          SL_AXI_AWSIZE    ;
    input      [1 : 0]          SL_AXI_AWBURST   ;
    input                       SL_AXI_AWLOCK            ;
    input      [3 : 0]          SL_AXI_AWCACHE   ;
    input      [2 : 0]          SL_AXI_AWPROT    ;
    input      [3 : 0]          SL_AXI_AWQOS     ;
    input                       SL_AXI_AWVALID           ;
    output                      SL_AXI_AWREADY            ;
    input      [127 : 0]        SL_AXI_WDATA   ;
    input      [15 : 0]         SL_AXI_WSTRB    ;
    input                       SL_AXI_WLAST             ;
    input                       SL_AXI_WVALID            ;
    output                      SL_AXI_WREADY             ;
    output   [3 : 0]            SL_AXI_BID        ;
    output     [1 : 0]          SL_AXI_BRESP      ;
    output                      SL_AXI_BVALID             ;
    input                       SL_AXI_BREADY            ;
    input      [3 : 0]          SL_AXI_ARID      ;
    input      [31 : 0]         SL_AXI_ARADDR   ;
    input      [7 : 0]          SL_AXI_ARLEN     ;
    input      [2 : 0]          SL_AXI_ARSIZE    ;
    input      [1 : 0]          SL_AXI_ARBURST   ;
    input                       SL_AXI_ARLOCK            ;
    input      [3 : 0]          SL_AXI_ARCACHE   ;
    input      [2 : 0]          SL_AXI_ARPROT    ;
    input      [3 : 0]          SL_AXI_ARQOS     ;
    input                       SL_AXI_ARVALID           ;
    output reg                  SL_AXI_ARREADY            ;
    output     [3 : 0]          SL_AXI_RID        ;
    output reg [127 : 0]        SL_AXI_RDATA    ;
    output     [1 : 0]          SL_AXI_RRESP      ;
    output reg                  SL_AXI_RLAST              ;
    output reg                  SL_AXI_RVALID             ;
    input                       SL_AXI_RREADY            ;




    
    
    input                                           clk;
                
 
    
    //---------------------------------------------------------------------------------------------------------------------
    // Internal wires and registers
    //---------------------------------------------------------------------------------------------------------------------
    
   
    integer state;
    reg  [10:0]     bytes_sent;
    
    
    
    //---------------------------------------------------------------------------------------------------------------------
    // Implmentation
    //---------------------------------------------------------------------------------------------------------------------
    
    
    always@* begin
        SL_AXI_ACLK = clk;
    end
    
    
    assign SL_AXI_AWREADY =0;
    assign SL_AXI_WREADY  =0;
    assign SL_AXI_BID     =0;
    assign SL_AXI_BRESP   =0;
    assign SL_AXI_BVALID  =0;
    assign SL_AXI_RID     =0;
    assign SL_AXI_RRESP   =0;
  
   
    
    always@(posedge clk) begin
        if(SL_AXI_ARESET_OUT_N == 0) begin
            state <= STATE_IDLE;
            SL_AXI_RVALID <= 0;
            SL_AXI_ARREADY <= 1;
            SL_AXI_RLAST <= 0;
            
        end
        else begin
          
            case(state) 
                STATE_IDLE: begin
                    SL_AXI_RVALID <= 0;
                    SL_AXI_RLAST <= 0;
                    if(SL_AXI_ARVALID == 1) begin
                        SL_AXI_ARREADY <= 0;
                        state <= STATE_ADDR_READY;
                    end
                end
                STATE_ADDR_READY: begin
                    state <= STATE_SEND_RDATA;
                    SL_AXI_ARREADY <= 1;
                    bytes_sent <= 0;
                end
                STATE_SEND_RDATA: begin
                    SL_AXI_RVALID <= 0;
                    if(SL_AXI_RREADY) begin
                        if(bytes_sent < SL_AXI_ARLEN) begin
                            SL_AXI_RDATA <= {125'd0,bytes_sent[1:0],1'b1};
                            SL_AXI_RVALID <= 1;
                            bytes_sent <= bytes_sent + 1;
                        end
                        else begin
                            SL_AXI_RVALID <= 1;
                            SL_AXI_RDATA <= {125'd0,bytes_sent[1:0],1'b1};
                            SL_AXI_RLAST <= 1;
                            state <= STATE_IDLE;
                        end
                    end
                end
            endcase
        end
    end
    


    
endmodule