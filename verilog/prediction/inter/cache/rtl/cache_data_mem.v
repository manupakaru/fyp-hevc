
`timescale 1ns / 1ps
module cache_data_mem
(
    clk,
    addr_in,
	r_data_out,
	w_data_in,
	w_en_in

);

    `include "cache_configs_def.v"
    
    //---------------------------------------------------------------------------------------------------------------------
    // parameter definitions
    //---------------------------------------------------------------------------------------------------------------------
    parameter NUM_CACHE_LINES                               = (1<<SET_ADDR_WDTH+C_N_WAY);
	
    //---------------------------------------------------------------------------------------------------------------------
    // localparam definitions
    //---------------------------------------------------------------------------------------------------------------------
    

   	
    //---------------------------------------------------------------------------------------------------------------------
    // I/O signals
    //---------------------------------------------------------------------------------------------------------------------
    

    input                           						clk;
	input	[PIXEL_BITS*CACHE_LINE_WDTH-1:0]             				w_data_in;
	input	[SET_ADDR_WDTH+C_N_WAY-Y_BANK_BITS-1:0]				        addr_in;			
	input													w_en_in;
	output  [PIXEL_BITS*CACHE_LINE_WDTH-1:0]	        		        r_data_out;
	
	
	//---------------------------------------------------------------------------------------------------------------------
    // Internal wires and registers
    //---------------------------------------------------------------------------------------------------------------------
  

    //reg     [PIXEL_BITS*(CACHE_LINE_WDTH)-1:0]      				        mem [NUM_CACHE_LINES-1:0];      // 48 bytes in cache line
    //reg     [SET_ADDR_WDTH+C_N_WAY-1:0]				        out_addr;

  
    //---------------------------------------------------------------------------------------------------------------------
    // Implmentation
    //---------------------------------------------------------------------------------------------------------------------
    
    
//	always @(posedge clk) begin
//        if(w_en_in) begin
//            mem[addr_in] <= w_data_in;
//        end
//        else begin
//            r_data_out <= mem[addr_in];
//        end
//	end	
	
	// assign 	r_data_out = mem[addr_in];



block_ram_cache cache_data_blk (
  .clka(clk), // input clka
  .ena(1'b1), // input ena
  .wea(w_en_in), // input [0 : 0] wea
  .addra(addr_in), // input [4 : 0] addra
  .dina(w_data_in), // input [383 : 0] dina
  .douta(r_data_out) // output [383 : 0] douta
);



endmodule