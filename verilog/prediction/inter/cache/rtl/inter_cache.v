`timescale 1ns / 1ps
module inter_cache
(
    clk,
    reset,
    start_x_in,
    start_y_in,
    rf_blk_hgt_in,
    rf_blk_wdt_in,
    ref_idx_in,
    valid_in,
    block_121_out,
    block_ready,
    
    axi_clk,
    axi_ar_addr,
    axi_ar_len,
    axi_ar_size,
    axi_ar_burst,
    axi_ar_prot,
    axi_ar_valid,
    axi_ar_ready,
    axi_r_data,
    axi_r_resp,
    axi_r_last,
    axi_r_valid,
    axi_r_ready

);

    `include "cache_configs_def.v"
    `include "axi_interface_def.v"
    
    //---------------------------------------------------------------------------------------------------------------------
    // parameter definitions
    //---------------------------------------------------------------------------------------------------------------------
    
    parameter                           DIM_WDTH		    	= 4;        // out block dimension
    parameter                           DIM_ADDR_WDTH           = 7;
    parameter							Y_BANKS				    = 1<<Y_BANK_BITS;
    
    //---------------------------------------------------------------------------------------------------------------------
    // localparam definitions
    //---------------------------------------------------------------------------------------------------------------------
    
    localparam                          STATE_READY_DATA_ACCEPT         = 0;    // the state that is entered upon reset
    localparam                          STATE_C_ADDR_ADMIT              = 1;
    localparam                          STATE_CHECK_HITS                = 2;
    localparam                          STATE_WRITE_BACK                = 3;
    localparam                          STATE_DDR_FETCH                 = 4;
    localparam                          STATE_DDR_ADDR_ACK              = 5;
    
    
    
    
    //---------------------------------------------------------------------------------------------------------------------
    // I/O signals
    //---------------------------------------------------------------------------------------------------------------------
    
    
    input                                           clk;
    input                                           reset;
                
    // inter prediction filter interface            
    input                                           valid_in;
    output	reg							            block_ready; 
    input       [X_ADDR_WDTH-1:0]    	            start_x_in;
    input       [Y_ADDR_WDTH-1:0]    	            start_y_in;
    input		[REF_ADDR_WDTH-1:0]		            ref_idx_in;
    input       [DIM_WDTH-1:0]                      rf_blk_hgt_in;
    input		[DIM_WDTH-1:0]			            rf_blk_wdt_in;
    output reg  [LUMA_BITS*OUT_BLOCK_SIZE-1:0]     block_121_out;
                
    // axi master interface         
    output	reg [AXI_ADDR_WDTH-1:0]		            axi_ar_addr;
    output wire [7:0]					            axi_ar_len;
    output wire	[2:0]					            axi_ar_size;
    output wire [1:0]					            axi_ar_burst;
    output wire [2:0]					            axi_ar_prot;
    output reg							            axi_ar_valid;
    input 								            axi_ar_ready;
    output                                          axi_clk;
                
    input		[AXI_DATA_WDTH-1:0]		            axi_r_data;
    input		[1:0]					            axi_r_resp;
    input 								            axi_r_last;
    input								            axi_r_valid;
    output reg							            axi_r_ready;
    
    //---------------------------------------------------------------------------------------------------------------------
    // Internal wires and registers
    //---------------------------------------------------------------------------------------------------------------------
    
    reg     [1:0]								    curr_x; // possible 0,1,2,3
    reg     [1:0]								    curr_x_d; // possible 0,1,2,3
    reg     [1:0]								    curr_x_2d; // possible 0,1,2,3
    
    
    wire    [1:0]                                   fst_y_cline;
    
    wire    [1:0]								    delta_x;	// possible 0,1,2
    wire    [1:0] 								    delta_y;	//possible 0,1,2,3
    
    wire    [SET_ADDR_WDTH - Y_BANK_BITS-1:0] 	    set_addr_minus_y2;
    reg	    [SET_ADDR_WDTH - Y_BANK_BITS-1:0]		d_set_addr_bnk;
    
    
    wire    [TAG_ADDR_WDTH-1:0] 				    tag_addr_logic;
    
    reg     [TAG_ADDR_WDTH-1:0] 				    tag_addr_logic1;
    reg     [TAG_ADDR_WDTH-1:0] 				    tag_addr_logic2;
    reg     [TAG_ADDR_WDTH-1:0] 				    tag_addr_logic3;
    reg     [TAG_ADDR_WDTH-1:0] 				    tag_addr_logic4;

    reg	    [TAG_ADDR_WDTH-1:0]					    d_tag_address1;
    reg	    [TAG_ADDR_WDTH-1:0]					    d_tag_address2;
    reg	    [TAG_ADDR_WDTH-1:0]					    d_tag_address3;
    reg	    [TAG_ADDR_WDTH-1:0]					    d_tag_address4;
    
    wire    [X_ADDR_WDTH - C_L_H_SIZE : 0]          curr_x_addr;
    //wire    [Y_ADDR_WDTH - C_L_V_SIZE - 1 : 0]      curr_y_addr;
    reg	[C_L_H_SIZE-1:0]					    cl_strt_x_d;
	reg	[C_L_H_SIZE-1:0]					    cl_end__x_d;
    
    wire	[C_L_H_SIZE-1:0]					    cl_strt_x;
	wire	[C_L_H_SIZE-1:0]					    cl_end__x;
	wire	[C_L_V_SIZE-1:0]					    cl_strt_y_1;
	wire	[C_L_V_SIZE-1:0]					    cl_end__y_1;
	wire	[C_L_V_SIZE-1:0]					    cl_strt_y_2;
	wire	[C_L_V_SIZE-1:0]					    cl_end__y_2;
	wire	[C_L_V_SIZE-1:0]					    cl_strt_y_3;
	wire	[C_L_V_SIZE-1:0]					    cl_end__y_3;
	wire	[C_L_V_SIZE-1:0]					    cl_strt_y_4;
	wire	[C_L_V_SIZE-1:0]					    cl_end__y_4;    
    
    reg     [DIM_WDTH-1:0]                          dst_strt_x;
    reg     [DIM_WDTH-1:0]                          dest_end_x;
    
    reg     [DIM_WDTH-1:0]                          dst_strt_y1;
    //reg     [DIM_WDTH-1:0]                          dest_end_y1;
    reg     [DIM_WDTH-1:0]                          dst_strt_y2;
    //reg     [DIM_WDTH-1:0]                          dest_end_y2;
    reg     [DIM_WDTH-1:0]                          dst_strt_y3;
    //reg     [DIM_WDTH-1:0]                          dest_end_y3;
    reg     [DIM_WDTH-1:0]                          dst_strt_y4;
    //reg     [DIM_WDTH-1:0]                          dest_end_y4;    
    
    wire                                            y_bank1_en;
    wire                                            y_bank2_en;
    wire                                            y_bank3_en;
    wire                                            y_bank4_en;
    
    reg     [1:0]                                   pos_y_bank1;
    reg     [1:0]                                   pos_y_bank2;
    reg     [1:0]                                   pos_y_bank3;
    reg     [1:0]                                   pos_y_bank4;
    
    reg                                             st_trans;
    
    reg     [CACHE_LINE_WDTH-1:0]                   cline_used_mask1;
    reg     [CACHE_LINE_WDTH-1:0]                   cline_used_mask2;
    reg     [CACHE_LINE_WDTH-1:0]                   cline_used_mask3;
    reg     [CACHE_LINE_WDTH-1:0]                   cline_used_mask4;    
    
    //reg     [DIM_ADDR_WDTH-1:0]                     cline_dest_locs_x[31:0];
    reg     [DIM_ADDR_WDTH-1:0]                     cline_dest_locs_1[31:0];
    reg     [DIM_ADDR_WDTH-1:0]                     cline_dest_locs_2[31:0];
    reg     [DIM_ADDR_WDTH-1:0]                     cline_dest_locs_3[31:0];
    reg     [DIM_ADDR_WDTH-1:0]                     cline_dest_locs_4[31:0];
	
	
	wire	[(1<<C_N_WAY)*TAG_ADDR_WDTH-1:0]		tag_rdata_set1;
	wire	[(1<<C_N_WAY)*TAG_ADDR_WDTH-1:0]		tag_rdata_set2;
	wire	[(1<<C_N_WAY)*TAG_ADDR_WDTH-1:0]		tag_rdata_set3;
	wire	[(1<<C_N_WAY)*TAG_ADDR_WDTH-1:0]		tag_rdata_set4;
	
	
	wire											ishit1;
	wire											ishit2;
	wire											ishit3;
	wire											ishit4;
    
	reg	    										d1_ishit1;
	reg 											d1_ishit2;
	reg 											d1_ishit3;
	reg 											d1_ishit4;    
    
	reg	    										d2_ishit1;
	reg 											d2_ishit2;
	reg 											d2_ishit3;
	reg 											d2_ishit4;      
	
	wire	[C_N_WAY-1:0]							set_idx1;
	wire	[C_N_WAY-1:0]							set_idx2;
	wire	[C_N_WAY-1:0]							set_idx3;
	wire	[C_N_WAY-1:0]							set_idx4;

	reg 	[C_N_WAY-1:0]							set_idx_miss1_d;
	reg 	[C_N_WAY-1:0]							set_idx_miss2_d;
	reg 	[C_N_WAY-1:0]							set_idx_miss3_d;
	reg 	[C_N_WAY-1:0]							set_idx_miss4_d;    
    
	wire	[C_N_WAY-1:0]							set_idx_miss1;
	wire	[C_N_WAY-1:0]							set_idx_miss2;
	wire	[C_N_WAY-1:0]							set_idx_miss3;
	wire	[C_N_WAY-1:0]							set_idx_miss4;    
	
	wire	[(1<<C_N_WAY)-1:0]						vld_bits_bank1;
	wire	[(1<<C_N_WAY)-1:0]						vld_bits_bank2;
	wire	[(1<<C_N_WAY)-1:0]						vld_bits_bank3;
	wire	[(1<<C_N_WAY)-1:0]						vld_bits_bank4;
	
	wire 	[(1<<C_N_WAY)*C_N_WAY-1:0]				age_val_set1;
	wire 	[(1<<C_N_WAY)*C_N_WAY-1:0]				age_val_set2;
	wire 	[(1<<C_N_WAY)*C_N_WAY-1:0]				age_val_set3;
	wire 	[(1<<C_N_WAY)*C_N_WAY-1:0]				age_val_set4;
	
	wire 	[(1<<C_N_WAY)*C_N_WAY-1:0]				new_age_set1;
	wire 	[(1<<C_N_WAY)*C_N_WAY-1:0]				new_age_set2;
	wire 	[(1<<C_N_WAY)*C_N_WAY-1:0]				new_age_set3;
	wire 	[(1<<C_N_WAY)*C_N_WAY-1:0]				new_age_set4;
    
    wire    [PIXEL_BITS*CACHE_LINE_WDTH-1:0]        cache_rdata_bnk1;
	wire    [PIXEL_BITS*CACHE_LINE_WDTH-1:0]        cache_rdata_bnk2;
    wire    [PIXEL_BITS*CACHE_LINE_WDTH-1:0]        cache_rdata_bnk3;
    wire    [PIXEL_BITS*CACHE_LINE_WDTH-1:0]        cache_rdata_bnk4;
    
    reg    [C_N_WAY-1:0]              cache_addr_bnk1;
    reg    [C_N_WAY-1:0]              cache_addr_bnk2;
    reg    [C_N_WAY-1:0]              cache_addr_bnk3;
    reg    [C_N_WAY-1:0]              cache_addr_bnk4;  
    
    reg                                             cache_wr_en1;
    reg                                             cache_wr_en2;
    reg                                             cache_wr_en3;
    reg                                             cache_wr_en4;
    
    integer                                         state;                      // the internal state of the module
    reg [4:0]                                       state_i;
            

    reg     [2*AXI_DATA_WDTH-1:0]                   axi_read_buf;
    
    reg                                             age_wr_en1;
    reg                                             age_wr_en2;
    reg                                             age_wr_en3;
    reg                                             age_wr_en4;
    
    reg                                             second_burst;
    
    wire    [PIXEL_BITS*CACHE_LINE_WDTH-1:0]        axi_r_cline;
    

    
    reg                                             cache_rd_when_ddr_miss;
    
    reg     [1:0]                           ddr_serve_bank;
    
    
    
    
    
    
    
    //---------------------------------------------------------------------------------------------------------------------
    // Implmentation
    //---------------------------------------------------------------------------------------------------------------------
    
    
    
    num_val_clines_generator num_val_clines_block (
    .start_x_in(start_x_in[C_L_H_SIZE+DIM_WDTH-1:0]), 
    .start_y_in(start_y_in[C_L_V_SIZE+DIM_WDTH-1:0]), 
    .rf_blk_wdt_in(rf_blk_wdt_in), 
    .rf_blk_hgt_in(rf_blk_hgt_in), 
    .delta_x_out(delta_x), 
    .delta_y_out(delta_y)
    );
    

//Design notes-----------
//memory is instantiated in 4 bank regions considering the locality of cachelines fetched by interpolation
//tags and ages are put into 2 different memories because ages are always accessed 8 chuncks where as tags are write accessed only one by one
// ages are initilized 0 to 7 in order to make the logic reduced for decoding of address with highest age for replacement in case of a mis
// valid_in should be asserted for a single clock cycle and other inputs should be maintained until ready is asserted
    
	
	tag_mem_bank tag_bank1 (
    .clk(clk), 
    .reset(reset), 
    .r_addr_in(set_addr_minus_y2), 
    .w_addr_in({d_set_addr_bnk,cache_addr_bnk1}), 
    .r_data_out(tag_rdata_set1), 
    .w_data_in(d_tag_address1), 
    .w_en_in(cache_wr_en1), 
    //.r_en_in(y_bank1_en), 
    .valid_bits_out(vld_bits_bank1)
    );
	tag_mem_bank tag_bank2 (
    .clk(clk), 
    .reset(reset), 
    .r_addr_in(set_addr_minus_y2), 
    .w_addr_in({d_set_addr_bnk,cache_addr_bnk2}), 
    .r_data_out(tag_rdata_set2), 
    .w_data_in(d_tag_address2), 
    .w_en_in(cache_wr_en2), 
    //.r_en_in(y_bank2_en), 
    .valid_bits_out(vld_bits_bank2)
    );

 	tag_mem_bank tag_bank3 (
    .clk(clk), 
    .reset(reset), 
    .r_addr_in(set_addr_minus_y2), 
    .w_addr_in({d_set_addr_bnk,cache_addr_bnk3}), 
    .r_data_out(tag_rdata_set3), 
    .w_data_in(d_tag_address3), 
    .w_en_in(cache_wr_en3), 
    //.r_en_in(y_bank3_en), 
    .valid_bits_out(vld_bits_bank3)
    ); 

	tag_mem_bank tag_bank4 (
    .clk(clk), 
    .reset(reset), 
    .r_addr_in(set_addr_minus_y2), 
    .w_addr_in({d_set_addr_bnk,cache_addr_bnk4}), 
    .r_data_out(tag_rdata_set4), 
    .w_data_in(d_tag_address4), 
    .w_en_in(cache_wr_en4), 
    //.r_en_in(y_bank4_en), 
    .valid_bits_out(vld_bits_bank4)
    );
	
	
	compare_tags compare_tags_bank1 (
    .tags_set_in(tag_rdata_set1), 
    .the_tag_in(tag_addr_logic1), 
    .ishit(ishit1), 
    .set_idx(set_idx1),
	.valid_bits_in(vld_bits_bank1)
    );
	compare_tags compare_tags_bank2 (
    .tags_set_in(tag_rdata_set2), 
    .the_tag_in(tag_addr_logic2), 
    .ishit(ishit2), 
    .set_idx(set_idx2),
	.valid_bits_in(vld_bits_bank2)
    );
	compare_tags compare_tags_bank3 (
    .tags_set_in(tag_rdata_set3), 
    .the_tag_in(tag_addr_logic3), 
    .ishit(ishit3), 
    .set_idx(set_idx3),
	.valid_bits_in(vld_bits_bank3)
    );
	compare_tags compare_tags_bank4 (
    .tags_set_in(tag_rdata_set4), 
    .the_tag_in(tag_addr_logic4), 
    .ishit(ishit4), 
    .set_idx(set_idx4),
	.valid_bits_in(vld_bits_bank4)
    );
	

	age_mem_bank age_bank1 (
    .clk(clk), 
    .reset(reset), 
    .r_addr_in(set_addr_minus_y2), 
    .w_addr_in(set_addr_minus_y2), 
    .r_data_out(age_val_set1), 
    .w_data_in(new_age_set1), 
    .w_en_in(age_wr_en1)
    // .r_en_in(y_bank1_en)
    );

	age_mem_bank age_bank2 (
    .clk(clk), 
    .reset(reset), 
    .r_addr_in(set_addr_minus_y2), 
    .w_addr_in(set_addr_minus_y2), 
    .r_data_out(age_val_set2), 
    .w_data_in(new_age_set2), 
    .w_en_in(age_wr_en2)
    // .r_en_in(y_bank2_en)
    );
	
	age_mem_bank age_bank3 (
    .clk(clk), 
    .reset(reset), 
    .r_addr_in(set_addr_minus_y2), 
    .w_addr_in(set_addr_minus_y2), 
    .r_data_out(age_val_set3), 
    .w_data_in(new_age_set3), 
    .w_en_in(age_wr_en3)
    // .r_en_in(y_bank3_en)
    );	
	
	age_mem_bank age_bank4 (
    .clk(clk), 
    .reset(reset), 
    .r_addr_in(set_addr_minus_y2), 
    .w_addr_in(set_addr_minus_y2), 
    .r_data_out(age_val_set4), 
    .w_data_in(new_age_set4), 
    .w_en_in(age_wr_en4)
    // .r_en_in(y_bank4_en)
    );
    
    
    new_age_converter age_conv_bank1 (
    .ishit_in(ishit1), 
    .set_idx_in(set_idx1), 
    .age_vals_in(age_val_set1), 
    .new_age_vals_out(new_age_set1), 
    .set_idx_miss_out(set_idx_miss1)
    );    
    new_age_converter age_conv_bank2 (
    .ishit_in(ishit2), 
    .set_idx_in(set_idx2), 
    .age_vals_in(age_val_set2), 
    .new_age_vals_out(new_age_set2), 
    .set_idx_miss_out(set_idx_miss2)
    ); 
    new_age_converter age_conv_bank3 (
    .ishit_in(ishit3), 
    .set_idx_in(set_idx3), 
    .age_vals_in(age_val_set3), 
    .new_age_vals_out(new_age_set3), 
    .set_idx_miss_out(set_idx_miss3)
    );   
    new_age_converter age_conv_bank4 (
    .ishit_in(ishit4), 
    .set_idx_in(set_idx4), 
    .age_vals_in(age_val_set4), 
    .new_age_vals_out(new_age_set4), 
    .set_idx_miss_out(set_idx_miss4)
    );    
    
    
    cache_data_mem cache_mem_bank1(
    .clk(clk), 
    .addr_in({d_set_addr_bnk,cache_addr_bnk1}), 
    .r_data_out(cache_rdata_bnk1), 
    .w_data_in(axi_r_cline), 
    .w_en_in(cache_wr_en1)
    );
    
    cache_data_mem cache_mem_bank2(
    .clk(clk), 
    .addr_in({d_set_addr_bnk,cache_addr_bnk2}), 
    .r_data_out(cache_rdata_bnk2), 
    .w_data_in(axi_r_cline), 
    .w_en_in(cache_wr_en2)
    );

    cache_data_mem cache_mem_bank3(
    .clk(clk), 
    .addr_in({d_set_addr_bnk,cache_addr_bnk3}), 
    .r_data_out(cache_rdata_bnk3), 
    .w_data_in(axi_r_cline), 
    .w_en_in(cache_wr_en3)
    ); 
    
    cache_data_mem cache_mem_bank4(
    .clk(clk), 
    .addr_in({d_set_addr_bnk,cache_addr_bnk4}), 
    .r_data_out(cache_rdata_bnk4), 
    .w_data_in(axi_r_cline), 
    .w_en_in(cache_wr_en4)
    );    
    
    
    assign axi_ar_len  	= 8'd2;
    assign axi_ar_size	= 8'b100;	//16 bytes
    assign axi_ar_burst	= 2'b01;	//type INCREMENTAL
    assign axi_ar_prot  = 2'b01;	//LSB - 1 - priviledged access MSB-0 - secure access
    
    assign axi_clk = clk;
    
    assign curr_x_addr = curr_x + (start_x_in >> C_L_H_SIZE);
    
    
    //set address and tag address logic------
    
    assign set_addr_minus_y2 = curr_x_addr[1:0];		// make necessary changes depending on the cache config chosen
    assign tag_addr_logic = {ref_idx_in,start_y_in[Y_ADDR_WDTH-1:C_L_V_SIZE+2],curr_x_addr[X_ADDR_WDTH-C_L_H_SIZE-1:2]};    // the 2 in the expression indicates x/y bits in the set address
    
    
    //fst_y_cline logic
    assign fst_y_cline = start_y_in[Y_BANK_BITS+C_L_V_SIZE-1:C_L_V_SIZE];
    
    //cl_strt_x , y logic
    assign cl_strt_x    = (curr_x == 2'b00)         ? start_x_in[C_L_H_SIZE-1:0]                                : {C_L_H_SIZE{1'b0}};
    assign cl_end__x    = (curr_x == delta_x)       ? start_x_in[C_L_H_SIZE-1:0] + rf_blk_wdt_in[C_L_H_SIZE-1:0]: {C_L_H_SIZE{1'b1}};
      
    assign cl_strt_y_1  = (pos_y_bank1 == 2'b00)    ? start_y_in[C_L_V_SIZE-1:0]                                : {C_L_V_SIZE{1'b0}};
    assign cl_end__y_1  = (pos_y_bank1 == delta_y)  ? start_y_in[C_L_V_SIZE-1:0] + rf_blk_hgt_in[C_L_V_SIZE-1:0]: {C_L_V_SIZE{1'b1}};
  
    assign cl_strt_y_2  = (pos_y_bank2 == 2'b00)    ? start_y_in[C_L_V_SIZE-1:0]                                : {C_L_V_SIZE{1'b0}};
    assign cl_end__y_2  = (pos_y_bank2 == delta_y)  ? start_y_in[C_L_V_SIZE-1:0] + rf_blk_hgt_in[C_L_V_SIZE-1:0]: {C_L_V_SIZE{1'b1}};
  
    assign cl_strt_y_3  = (pos_y_bank3 == 2'b00)     ? start_y_in[C_L_V_SIZE-1:0]                                : {C_L_V_SIZE{1'b0}};
    assign cl_end__y_3  = (pos_y_bank3 == delta_y)  ? start_y_in[C_L_V_SIZE-1:0] + rf_blk_hgt_in[C_L_V_SIZE-1:0]: {C_L_V_SIZE{1'b1}};
  
    assign cl_strt_y_4  = (pos_y_bank4 == 2'b00)    ? start_y_in[C_L_V_SIZE-1:0]                                : {C_L_V_SIZE{1'b0}};
    assign cl_end__y_4  = (pos_y_bank4 == delta_y)  ? start_y_in[C_L_V_SIZE-1:0] + rf_blk_hgt_in[C_L_V_SIZE-1:0]: {C_L_V_SIZE{1'b1}};
    
    assign axi_r_cline = {axi_r_data,axi_read_buf};
	
    
    // determination of y_bank positions
    always@(*) begin
        case(fst_y_cline)
            2'b00: begin
                pos_y_bank1 = 0;
                pos_y_bank2 = 1;
                pos_y_bank3 = 2;
                pos_y_bank4 = 3;
                tag_addr_logic1 = {ref_idx_in,start_y_in[Y_ADDR_WDTH-1:C_L_V_SIZE+2],curr_x_addr[X_ADDR_WDTH-C_L_H_SIZE-1:2]};    // the 2 in the expression indicates x/y bits in the set address
                tag_addr_logic2 = {ref_idx_in,start_y_in[Y_ADDR_WDTH-1:C_L_V_SIZE+2],curr_x_addr[X_ADDR_WDTH-C_L_H_SIZE-1:2]};    // the 2 in the expression indicates x/y bits in the set address
                tag_addr_logic3 = {ref_idx_in,start_y_in[Y_ADDR_WDTH-1:C_L_V_SIZE+2],curr_x_addr[X_ADDR_WDTH-C_L_H_SIZE-1:2]};    // the 2 in the expression indicates x/y bits in the set address
                tag_addr_logic4 = {ref_idx_in,start_y_in[Y_ADDR_WDTH-1:C_L_V_SIZE+2],curr_x_addr[X_ADDR_WDTH-C_L_H_SIZE-1:2]};    // the 2 in the expression indicates x/y bits in the set address
                
            end
            2'b01: begin
                pos_y_bank1 = 3;
                pos_y_bank2 = 0;
                pos_y_bank3 = 1;
                pos_y_bank4 = 2;
                tag_addr_logic1 = {ref_idx_in,{(start_y_in[Y_ADDR_WDTH-1:C_L_V_SIZE+2]+1'b1)%(1<<(Y_ADDR_WDTH-C_L_V_SIZE-Y_BANK_BITS))},curr_x_addr[X_ADDR_WDTH-C_L_H_SIZE-1:2]};    // the 2 in the expression indicates x/y bits in the set address
                tag_addr_logic2 = {ref_idx_in,start_y_in[Y_ADDR_WDTH-1:C_L_V_SIZE+2],curr_x_addr[X_ADDR_WDTH-C_L_H_SIZE-1:2]};    // the 2 in the expression indicates x/y bits in the set address
                tag_addr_logic3 = {ref_idx_in,start_y_in[Y_ADDR_WDTH-1:C_L_V_SIZE+2],curr_x_addr[X_ADDR_WDTH-C_L_H_SIZE-1:2]};    // the 2 in the expression indicates x/y bits in the set address
                tag_addr_logic4 = {ref_idx_in,start_y_in[Y_ADDR_WDTH-1:C_L_V_SIZE+2],curr_x_addr[X_ADDR_WDTH-C_L_H_SIZE-1:2]};    // the 2 in the expression indicates x/y bits in the set address
                
            end
            2'b10: begin
                pos_y_bank1 = 2;
                pos_y_bank2 = 3;
                pos_y_bank3 = 0;
                pos_y_bank4 = 1;
                tag_addr_logic1 = {ref_idx_in,{(start_y_in[Y_ADDR_WDTH-1:C_L_V_SIZE+2]+1'b1)%(1<<(Y_ADDR_WDTH-C_L_V_SIZE-Y_BANK_BITS))},curr_x_addr[X_ADDR_WDTH-C_L_H_SIZE-1:2]};    // the 2 in the expression indicates x/y bits in the set address
                tag_addr_logic2 = {ref_idx_in,{(start_y_in[Y_ADDR_WDTH-1:C_L_V_SIZE+2]+1'b1)%(1<<(Y_ADDR_WDTH-C_L_V_SIZE-Y_BANK_BITS))},curr_x_addr[X_ADDR_WDTH-C_L_H_SIZE-1:2]};    // the 2 in the expression indicates x/y bits in the set address
                tag_addr_logic3 = {ref_idx_in,start_y_in[Y_ADDR_WDTH-1:C_L_V_SIZE+2],curr_x_addr[X_ADDR_WDTH-C_L_H_SIZE-1:2]};    // the 2 in the expression indicates x/y bits in the set address
                tag_addr_logic4 = {ref_idx_in,start_y_in[Y_ADDR_WDTH-1:C_L_V_SIZE+2],curr_x_addr[X_ADDR_WDTH-C_L_H_SIZE-1:2]};    // the 2 in the expression indicates x/y bits in the set address
            end
            2'b11: begin
                pos_y_bank1 = 1;
                pos_y_bank2 = 2;
                pos_y_bank3 = 3;
                pos_y_bank4 = 0;
                tag_addr_logic1 = {ref_idx_in,{(start_y_in[Y_ADDR_WDTH-1:C_L_V_SIZE+2]+1'b1)%(1<<(Y_ADDR_WDTH-C_L_V_SIZE-Y_BANK_BITS))},curr_x_addr[X_ADDR_WDTH-C_L_H_SIZE-1:2]};    // the 2 in the expression indicates x/y bits in the set address
                tag_addr_logic2 = {ref_idx_in,{(start_y_in[Y_ADDR_WDTH-1:C_L_V_SIZE+2]+1'b1)%(1<<(Y_ADDR_WDTH-C_L_V_SIZE-Y_BANK_BITS))},curr_x_addr[X_ADDR_WDTH-C_L_H_SIZE-1:2]};    // the 2 in the expression indicates x/y bits in the set address
                tag_addr_logic3 = {ref_idx_in,{(start_y_in[Y_ADDR_WDTH-1:C_L_V_SIZE+2]+1'b1)%(1<<(Y_ADDR_WDTH-C_L_V_SIZE-Y_BANK_BITS))},curr_x_addr[X_ADDR_WDTH-C_L_H_SIZE-1:2]};    // the 2 in the expression indicates x/y bits in the set address
                tag_addr_logic4 = {ref_idx_in,start_y_in[Y_ADDR_WDTH-1:C_L_V_SIZE+2],curr_x_addr[X_ADDR_WDTH-C_L_H_SIZE-1:2]};    // the 2 in the expression indicates x/y bits in the set address
            
            end                
        endcase
    end
    
    assign y_bank1_en = (pos_y_bank1 <= delta_y) ? 1'b1 : 1'b0;
    assign y_bank2_en = (pos_y_bank2 <= delta_y) ? 1'b1 : 1'b0;
    assign y_bank3_en = (pos_y_bank3 <= delta_y) ? 1'b1 : 1'b0;
    assign y_bank4_en = (pos_y_bank4 <= delta_y) ? 1'b1 : 1'b0;
    
 
    
    // always@(posedge clk) begin      // curr_x increment
        // if(reset) begin
            // curr_x <= 0;
        // end
        // else if(st_trans || valid_in) begin
            // if(delta_x != curr_x) begin     //  if curr_x has reached its maximum stop incrementing
                // curr_x <= (curr_x + 1)%Y_BANKS;
            // end
            // curr_x_d <= curr_x;
            // curr_x_2d <= curr_x_d;
        // end
        // else if(block_ready) begin
            // curr_x  <= 0;
        // end
    // end
    
    integer i;
 
    
    
    always@(*) begin
        if(valid_in == 1 || st_trans == 1) begin
            age_wr_en1 = y_bank1_en;
            age_wr_en2 = y_bank2_en;
            age_wr_en3 = y_bank3_en;
            age_wr_en4 = y_bank4_en;
        end
        else begin
            age_wr_en1 = 0;
            age_wr_en2 = 0;
            age_wr_en3 = 0;
            age_wr_en4 = 0;           
        end
    end
    
    integer j;
    
    always@(posedge clk) begin
        if(reset) begin
            state_i <= 5'b00001;
            state <= STATE_READY_DATA_ACCEPT;
            axi_r_ready <= 0;
            axi_ar_valid <= 0;
            
            d1_ishit1 <= 0;
            d1_ishit2 <= 0;
            d1_ishit3 <= 0;
            d1_ishit4 <= 0;
            
            d2_ishit1 <= 0;
            d2_ishit2 <= 0;
            d2_ishit3 <= 0;
            d2_ishit4 <= 0;
            
            cache_wr_en1 <= 0;
            cache_wr_en2 <= 0;
            cache_wr_en3 <= 0;
            cache_wr_en4 <= 0;
            
            
            st_trans <= 0;
            curr_x <= 0;   
            curr_x_2d <=0;
            curr_x_d <=0;
        end
        else begin
            if(state_i[0] == 1) begin
                if((state == STATE_READY_DATA_ACCEPT && valid_in == 1 ) || state != STATE_READY_DATA_ACCEPT) begin
                    cl_end__x_d <= cl_end__x;
                    cl_strt_x_d <= cl_strt_x;
                    d_set_addr_bnk <= set_addr_minus_y2;
                    if(curr_x == 0) begin
                        dst_strt_x <= 4'b0000;
                        dest_end_x <= {1'b0,(3'b111 - start_x_in[C_L_H_SIZE-1:0])};
                    end
                    else begin
                        dst_strt_x <= dest_end_x + 1'b1;
                        dest_end_x <= dest_end_x + 4'b1000; 
                    end
                    case(fst_y_cline)
                        2'b00: begin
                            if(y_bank1_en) begin
                                dst_strt_y1 <= 4'b0000;                    
                            end
                            if(y_bank2_en) begin
                                dst_strt_y2 <= {2'b00,(2'b11 - start_y_in[C_L_V_SIZE-1:0])} + 1'b1;
                            end
                            if(y_bank3_en) begin
                                dst_strt_y3 <= {2'b00,(2'b11 - start_y_in[C_L_V_SIZE-1:0])} + 4'd5;
                            end
                            if(y_bank4_en) begin
                                dst_strt_y4 <= {2'b00,(2'b11 - start_y_in[C_L_V_SIZE-1:0])} + 4'd9;                    
                            end
                        end
                        2'b01: begin
                            if(y_bank2_en) begin
                                dst_strt_y2 <= 4'b0000;
                            end 
                            if(y_bank3_en) begin
                                dst_strt_y3 <= {2'b00,(2'b11 - start_y_in[C_L_V_SIZE-1:0])} + 1'b1;
                            end
                            if(y_bank4_en) begin
                                dst_strt_y4 <= {2'b00,(2'b11 - start_y_in[C_L_V_SIZE-1:0])} + 4'd5;
                            end
                            if(y_bank1_en) begin
                                dst_strt_y1 <= {2'b00,(2'b11 - start_y_in[C_L_V_SIZE-1:0])} + 4'd9;                    
                            end
                        end
                        2'b10: begin
                            if(y_bank3_en) begin
                                dst_strt_y3 <= 4'b0000;
                            end                
                            if(y_bank4_en) begin
                                dst_strt_y4 <= {2'b00,(2'b11 - start_y_in[C_L_V_SIZE-1:0])} + 1'b1;
                            end
                            if(y_bank1_en) begin
                                dst_strt_y1 <= {2'b00,(2'b11 - start_y_in[C_L_V_SIZE-1:0])} + 4'd5;
                            end
                            if(y_bank2_en) begin
                                dst_strt_y2 <= {2'b00,(2'b11 - start_y_in[C_L_V_SIZE-1:0])} + 4'd9;                    
                            end
                        end
                        2'b11: begin
                            if(y_bank4_en) begin
                                dst_strt_y4 <= 4'b0000;
                            end                
                            if(y_bank1_en) begin
                                dst_strt_y1 <= {2'b00,(2'b11 - start_y_in[C_L_V_SIZE-1:0])} + 1'b1;
                            end
                            if(y_bank2_en) begin
                                dst_strt_y2 <= {2'b00,(2'b11 - start_y_in[C_L_V_SIZE-1:0])} + 4'd5;
                            end
                            if(y_bank3_en) begin
                                dst_strt_y3 <= {2'b00,(2'b11 - start_y_in[C_L_V_SIZE-1:0])} + 4'd9;                    
                            end
                        end                
                    endcase                    

                    if(y_bank1_en) begin
                        d1_ishit1 <= ishit1;                      
                    end
                    if(y_bank2_en) begin
                        d1_ishit2 <= ishit2;
                    end
                    if(y_bank3_en) begin
                        d1_ishit3 <= ishit3;
                    end
                    if(y_bank4_en) begin
                        d1_ishit4 <= ishit4;
                    end
                end
            end
            if(state_i[1] == 1) begin
            
                curr_x_d <= curr_x;
                if(y_bank1_en) begin
                    d2_ishit1 <= d1_ishit1;                  
                end
                if(y_bank2_en) begin
                    d2_ishit2 <= d1_ishit2;
                end
                if(y_bank3_en) begin
                    d2_ishit3 <= d1_ishit3;
                end
                if(y_bank4_en) begin
                    d2_ishit4 <= d1_ishit4;
                end
                if(y_bank1_en) begin
                    for (i=0;i<32;i=i+1) begin
                        if(cl_strt_x_d <= i[2:0] && cl_end__x_d >= i[2:0] && cl_strt_y_1 <= i[4:3] && cl_end__y_1 >= i[4:3]) begin
                            cline_used_mask1[i] <= 1'b1;
                            //cline_dest_locs_x[i] <= (dst_strt_x+ i[2:0] - cl_strt_x_d) ;
                            //cline_dest_locs_y1[i] <= (dst_strt_y1+ i[4:3] - cl_strt_y_1);
                            cline_dest_locs_1[i] <= (dst_strt_x+ i[2:0] - cl_strt_x_d) + ((dst_strt_y1+ i[4:3] - cl_strt_y_1)<<3)+ ((dst_strt_y1+ i[4:3] - cl_strt_y_1)<<1)+((dst_strt_y1+ i[4:3] - cl_strt_y_1));
                        end
                        else begin
                            cline_used_mask1[i] <= 1'b0;
                        end
                    end
                end
                if(y_bank2_en) begin
                    for (i=0;i<32;i=i+1) begin
                        if(cl_strt_x_d <= i[2:0] && cl_end__x_d >= i[2:0] && cl_strt_y_2 <= i[4:3] && cl_end__y_2 >= i[4:3]) begin
                            cline_used_mask2[i] <= 1'b1;
                            cline_dest_locs_2[i] <= (dst_strt_x+ i[2:0] - cl_strt_x_d) + ((dst_strt_y2+ i[4:3] - cl_strt_y_2)<<3)+((dst_strt_y2+ i[4:3] - cl_strt_y_2)<<1)+((dst_strt_y2+ i[4:3] - cl_strt_y_2));
                        end
                        else begin
                            cline_used_mask2[i] <= 1'b0;
                        end
                    end
                end
                if(y_bank3_en) begin
                    for (i=0;i<32;i=i+1) begin
                        if(cl_strt_x_d <= i[2:0] && cl_end__x_d >= i[2:0] && cl_strt_y_3 <= i[4:3] && cl_end__y_3 >= i[4:3]) begin
                            cline_used_mask3[i] <= 1'b1;
                            cline_dest_locs_3[i] <= (dst_strt_x+ i[2:0] - cl_strt_x_d) + ((dst_strt_y3+ i[4:3] - cl_strt_y_3)<<3)+((dst_strt_y3+ i[4:3] - cl_strt_y_3)<<1)+((dst_strt_y3+ i[4:3] - cl_strt_y_3));
                        end
                        else begin
                            cline_used_mask3[i] <= 1'b0;
                        end
                    end
                end
                if(y_bank4_en) begin
                    for (i=0;i<32;i=i+1) begin
                        if(cl_strt_x_d <= i[2:0] && cl_end__x_d >= i[2:0] && cl_strt_y_4 <= i[4:3] && cl_end__y_4 >= i[4:3]) begin
                            cline_used_mask4[i] <= 1'b1;
                            cline_dest_locs_4[i] <= (dst_strt_x+ i[2:0] - cl_strt_x_d) + ((dst_strt_y4+ i[4:3] - cl_strt_y_4)<<3)+((dst_strt_y4+ i[4:3] - cl_strt_y_4)<<1)+((dst_strt_y4+ i[4:3] - cl_strt_y_4));
                        end
                        else begin
                            cline_used_mask4[i] <= 1'b0;
                        end
                    end
                end      
            end
            if(state_i[2] == 1) begin  
                if(curr_x_2d == delta_x) begin
                    curr_x_2d <= 0;
                end
                else begin
                    curr_x_2d <= curr_x_d;
                end
                if(y_bank1_en) begin 
                    if(d2_ishit1) begin
                        for(j=0;j<32;j=j+1) begin
                            if(cline_used_mask1[j] == 1) begin
                                block_121_out[8*cline_dest_locs_1[j]+0]     <= cache_rdata_bnk1[8*j+0] ;
                                block_121_out[8*cline_dest_locs_1[j]+1]     <= cache_rdata_bnk1[8*j+1] ;
                                block_121_out[8*cline_dest_locs_1[j]+2]     <= cache_rdata_bnk1[8*j+2] ;
                                block_121_out[8*cline_dest_locs_1[j]+3]     <= cache_rdata_bnk1[8*j+3] ;
                                block_121_out[8*cline_dest_locs_1[j]+4]     <= cache_rdata_bnk1[8*j+4] ;
                                block_121_out[8*cline_dest_locs_1[j]+5]     <= cache_rdata_bnk1[8*j+5] ;
                                block_121_out[8*cline_dest_locs_1[j]+6]     <= cache_rdata_bnk1[8*j+6] ;
                                block_121_out[8*cline_dest_locs_1[j]+7]     <= cache_rdata_bnk1[8*j+7] ;
                                // block_121_out[8*cline_dest_locs_1[j]+8]     <= cache_rdata_bnk1[8*j+8] ;
                                // block_121_out[8*cline_dest_locs_1[j]+9]     <= cache_rdata_bnk1[8*j+9] ;
                                // block_121_out[8*cline_dest_locs_1[j]+10]    <= cache_rdata_bnk1[8*j+10];
                                // block_121_out[8*cline_dest_locs_1[j]+11]    <= cache_rdata_bnk1[8*j+11];
                            end
                        end
                    end                        
                end
                if(y_bank2_en) begin  
                    if(d2_ishit2) begin
                        for(j=0;j<32;j=j+1) begin
                            if(cline_used_mask2[j] == 1) begin
                                block_121_out[8*cline_dest_locs_2[j]+0]     <= cache_rdata_bnk2[8*j+0] ;
                                block_121_out[8*cline_dest_locs_2[j]+1]     <= cache_rdata_bnk2[8*j+1] ;
                                block_121_out[8*cline_dest_locs_2[j]+2]     <= cache_rdata_bnk2[8*j+2] ;
                                block_121_out[8*cline_dest_locs_2[j]+3]     <= cache_rdata_bnk2[8*j+3] ;
                                block_121_out[8*cline_dest_locs_2[j]+4]     <= cache_rdata_bnk2[8*j+4] ;
                                block_121_out[8*cline_dest_locs_2[j]+5]     <= cache_rdata_bnk2[8*j+5] ;
                                block_121_out[8*cline_dest_locs_2[j]+6]     <= cache_rdata_bnk2[8*j+6] ;
                                block_121_out[8*cline_dest_locs_2[j]+7]     <= cache_rdata_bnk2[8*j+7] ;
                                // block_121_out[8*cline_dest_locs_2[j]+8]     <= cache_rdata_bnk2[8*j+8] ;
                                // block_121_out[8*cline_dest_locs_2[j]+9]     <= cache_rdata_bnk2[8*j+9] ;
                                // block_121_out[8*cline_dest_locs_2[j]+10]    <= cache_rdata_bnk2[8*j+10];
                                // block_121_out[8*cline_dest_locs_2[j]+11]    <= cache_rdata_bnk2[8*j+11];
                            end
                        end        
                    end
                end
                if(y_bank3_en) begin
                    if(d2_ishit3) begin
                        for(j=0;j<32;j=j+1) begin
                            if(cline_used_mask3[j] == 1) begin
                                block_121_out[8*cline_dest_locs_3[j]+0]     <= cache_rdata_bnk3[8*j+0] ;
                                block_121_out[8*cline_dest_locs_3[j]+1]     <= cache_rdata_bnk3[8*j+1] ;
                                block_121_out[8*cline_dest_locs_3[j]+2]     <= cache_rdata_bnk3[8*j+2] ;
                                block_121_out[8*cline_dest_locs_3[j]+3]     <= cache_rdata_bnk3[8*j+3] ;
                                block_121_out[8*cline_dest_locs_3[j]+4]     <= cache_rdata_bnk3[8*j+4] ;
                                block_121_out[8*cline_dest_locs_3[j]+5]     <= cache_rdata_bnk3[8*j+5] ;
                                block_121_out[8*cline_dest_locs_3[j]+6]     <= cache_rdata_bnk3[8*j+6] ;
                                block_121_out[8*cline_dest_locs_3[j]+7]     <= cache_rdata_bnk3[8*j+7] ;
                                // block_121_out[8*cline_dest_locs_3[j]+8]     <= cache_rdata_bnk3[8*j+8] ;
                                // block_121_out[8*cline_dest_locs_3[j]+9]     <= cache_rdata_bnk3[8*j+9] ;
                                // block_121_out[8*cline_dest_locs_3[j]+10]    <= cache_rdata_bnk3[8*j+10];
                                // block_121_out[8*cline_dest_locs_3[j]+11]    <= cache_rdata_bnk3[8*j+11];
                            end
                        end 
                    end
                end
                if(y_bank4_en) begin
                    if(d2_ishit4) begin
                        for(j=0;j<32;j=j+1) begin
                            if(cline_used_mask4[j] == 1) begin
                                block_121_out[8*cline_dest_locs_4[j]+0]     <= cache_rdata_bnk4[8*j+0] ;
                                block_121_out[8*cline_dest_locs_4[j]+1]     <= cache_rdata_bnk4[8*j+1] ;
                                block_121_out[8*cline_dest_locs_4[j]+2]     <= cache_rdata_bnk4[8*j+2] ;
                                block_121_out[8*cline_dest_locs_4[j]+3]     <= cache_rdata_bnk4[8*j+3] ;
                                block_121_out[8*cline_dest_locs_4[j]+4]     <= cache_rdata_bnk4[8*j+4] ;
                                block_121_out[8*cline_dest_locs_4[j]+5]     <= cache_rdata_bnk4[8*j+5] ;
                                block_121_out[8*cline_dest_locs_4[j]+6]     <= cache_rdata_bnk4[8*j+6] ;
                                block_121_out[8*cline_dest_locs_4[j]+7]     <= cache_rdata_bnk4[8*j+7] ;
                                // block_121_out[8*cline_dest_locs_4[j]+8]     <= cache_rdata_bnk4[8*j+8] ;
                                // block_121_out[8*cline_dest_locs_4[j]+9]     <= cache_rdata_bnk4[8*j+9] ;
                                // block_121_out[8*cline_dest_locs_4[j]+10]    <= cache_rdata_bnk4[8*j+10];
                                // block_121_out[8*cline_dest_locs_4[j]+11]    <= cache_rdata_bnk4[8*j+11];
                            end
                        end    
                    end
                end
            end
        
            case(state) 
                STATE_READY_DATA_ACCEPT: begin
                    state_i = 5'b00_001;
                    block_ready <= 0;
                    st_trans <= 0;
                    if(valid_in) begin                    
                        if(y_bank1_en) begin
                            if(ishit1 == 1) begin
                                cache_addr_bnk1 <= set_idx1;
                            end                        
                        end
                        if(y_bank2_en) begin
                            if(ishit2 == 1 ) begin
                                cache_addr_bnk2 <= set_idx2;
                            end
                        end
                        if(y_bank3_en) begin
                            if(ishit3 == 1 ) begin
                                cache_addr_bnk3 <= set_idx3;
                            end
                        end
                        if(y_bank4_en) begin
                            if(ishit4 == 1 ) begin
                                cache_addr_bnk4 <= set_idx4;
                            end
                        end                        
                    
                        if(ishit1 == 0 && y_bank1_en) begin // {dpb base addr of ddr, tag, set_ad_min2,bank_addr 
                            axi_ar_addr <= {8'd0,({tag_addr_logic1, set_addr_minus_y2,2'b00}<<1) + ({tag_addr_logic1, set_addr_minus_y2,2'b00})};     
                            axi_ar_valid <= 1;
                            state <= STATE_DDR_ADDR_ACK;
                            state_i <= 5'b00_010;
                            ddr_serve_bank <= 2'd0;
                            set_idx_miss1_d <= set_idx_miss1;
                            set_idx_miss2_d <= set_idx_miss2;
                            set_idx_miss3_d <= set_idx_miss3;
                            set_idx_miss4_d <= set_idx_miss4;
                            d_tag_address1 <= tag_addr_logic1;
                            d_tag_address2 <= tag_addr_logic2;
                            d_tag_address3 <= tag_addr_logic3;
                            d_tag_address4 <= tag_addr_logic4;
                        end
                        else if(ishit2 == 0 && y_bank2_en) begin
                            axi_ar_addr <= {8'd0,({tag_addr_logic2, set_addr_minus_y2,2'b01}<<1) + ({tag_addr_logic2, set_addr_minus_y2,2'b01})};
                            axi_ar_valid <= 1;   
                            state <= STATE_DDR_ADDR_ACK;
                            state_i <= 5'b00_010;
                            ddr_serve_bank <= 2'd1;
                            set_idx_miss2_d <= set_idx_miss2;
                            set_idx_miss3_d <= set_idx_miss3;
                            set_idx_miss4_d <= set_idx_miss4;
                            d_tag_address2 <= tag_addr_logic2;
                            d_tag_address3 <= tag_addr_logic3;
                            d_tag_address4 <= tag_addr_logic4;
                            
                        end
                        else if(ishit3 == 0 && y_bank3_en) begin
                            axi_ar_addr <= {8'd0,({tag_addr_logic3, set_addr_minus_y2,2'b10}<<1) + ({tag_addr_logic3, set_addr_minus_y2,2'b10})};
                            axi_ar_valid <= 1; 
                            state <= STATE_DDR_ADDR_ACK;
                            state_i <= 5'b00_010;
                            ddr_serve_bank <= 2'd2;
                            set_idx_miss3_d <= set_idx_miss3;
                            set_idx_miss4_d <= set_idx_miss4;
                            d_tag_address3 <= tag_addr_logic3;
                            d_tag_address4 <= tag_addr_logic4;
                        end
                        else if(ishit4 == 0 && y_bank4_en) begin
                            axi_ar_addr <= {8'd0,({tag_addr_logic4, set_addr_minus_y2,2'b11}<<1) + ({tag_addr_logic4, set_addr_minus_y2,2'b11})};
                            axi_ar_valid <= 1;   
                            state <= STATE_DDR_ADDR_ACK;
                            state_i <= 5'b00_010;
                            ddr_serve_bank <= 2'd3;
                            set_idx_miss4_d <= set_idx_miss4;
                            d_tag_address4 <= tag_addr_logic4;
                        end
                        else begin   // all is ishits are true
                            state <= STATE_C_ADDR_ADMIT;
                            state_i <= 5'b00_011;
                            //curr_x incremented even if there is a miss
                            if(delta_x != curr_x) begin     //  if curr_x has reached its maximum stop incrementing
                                curr_x <= (curr_x + 1)%Y_BANKS;
                                st_trans <= 1;
                            end                             
                        end 
                    
                    end
                end
                STATE_C_ADDR_ADMIT: begin

                    if(y_bank1_en) begin
                        if(ishit1 == 1) begin
                            cache_addr_bnk1 <= set_idx1;
                        end                        
                    end
                    if(y_bank2_en) begin
                        if(ishit2 == 1 ) begin
                            cache_addr_bnk2 <= set_idx2;
                        end
                    end
                    if(y_bank3_en) begin
                        if(ishit3 == 1 ) begin
                            cache_addr_bnk3 <= set_idx3;
                        end
                    end
                    if(y_bank4_en) begin
                        if(ishit4 == 1 ) begin
                            cache_addr_bnk4 <= set_idx4;
                        end
                    end                        
                
                    if(ishit1 == 0 && y_bank1_en) begin // {dpb base addr of ddr, tag, set_ad_min2,bank_addr 
                        axi_ar_addr <= {8'd0,({tag_addr_logic1, set_addr_minus_y2,2'b00}<<1) + ({tag_addr_logic1, set_addr_minus_y2,2'b00})};     
                        axi_ar_valid <= 1;
                        state <= STATE_DDR_ADDR_ACK;
                        state_i <= 5'b00_110;
                        st_trans <= 0;
                        ddr_serve_bank <= 2'd0;
                        set_idx_miss1_d <= set_idx_miss1;
                        set_idx_miss2_d <= set_idx_miss2;
                        set_idx_miss3_d <= set_idx_miss3;
                        set_idx_miss4_d <= set_idx_miss4;
                        d_tag_address1 <= tag_addr_logic1;
                        d_tag_address2 <= tag_addr_logic2;
                        d_tag_address3 <= tag_addr_logic3;
                        d_tag_address4 <= tag_addr_logic4;
                    end
                    else if(ishit2 == 0 && y_bank2_en) begin
                        axi_ar_addr <= {8'd0,({tag_addr_logic2, set_addr_minus_y2,2'b01}<<1) + ({tag_addr_logic2, set_addr_minus_y2,2'b01})};
                        axi_ar_valid <= 1;   
                        state <= STATE_DDR_ADDR_ACK;
                        state_i <= 5'b00_110;
                        st_trans <= 0;
                        ddr_serve_bank <= 2'd1;
                        set_idx_miss2_d <= set_idx_miss2;
                        set_idx_miss3_d <= set_idx_miss3;
                        set_idx_miss4_d <= set_idx_miss4;
                        d_tag_address2 <= tag_addr_logic2;
                        d_tag_address3 <= tag_addr_logic3;
                        d_tag_address4 <= tag_addr_logic4;
                    end
                    else if(ishit3 == 0 && y_bank3_en) begin
                        axi_ar_addr <= {8'd0,({tag_addr_logic3, set_addr_minus_y2,2'b10}<<1) + ({tag_addr_logic3, set_addr_minus_y2,2'b10})};
                        axi_ar_valid <= 1; 
                        state <= STATE_DDR_ADDR_ACK;
                        state_i <= 5'b00_110;
                        st_trans <= 0;
                        ddr_serve_bank <= 2'd2;
                        set_idx_miss3_d <= set_idx_miss3;
                        set_idx_miss4_d <= set_idx_miss4;
                        d_tag_address3 <= tag_addr_logic3;
                        d_tag_address4 <= tag_addr_logic4;
                    end
                    else if(ishit4 == 0 && y_bank4_en) begin
                        axi_ar_addr <= {8'd0,({tag_addr_logic4, set_addr_minus_y2,2'b11}<<1) + ({tag_addr_logic4, set_addr_minus_y2,2'b11})};
                        axi_ar_valid <= 1;   
                        state <= STATE_DDR_ADDR_ACK;
                        state_i <= 5'b00_110;
                        st_trans <= 0;
                        ddr_serve_bank <= 2'd3;
                        set_idx_miss4_d <= set_idx_miss4;
                        d_tag_address4 <= tag_addr_logic4;
                    end
                    else begin   // all is ishits are true
                        state <= STATE_WRITE_BACK;
                        state_i <= 5'b00_110;
                        //curr_x incremented even if there is a miss
                        if(delta_x != curr_x) begin     //  if curr_x has reached its maximum stop incrementing
                            curr_x <= (curr_x + 1)%Y_BANKS;
                            st_trans <= 1;
                            state_i[0] <= 1;
                        end                             
                    end 
                end
                STATE_WRITE_BACK: begin

                    if(y_bank1_en) begin
                        if(ishit1 == 1) begin
                            cache_addr_bnk1 <= set_idx1;
                        end                    
                    end
                    if(y_bank2_en) begin
                        if(ishit2 == 1 ) begin
                            cache_addr_bnk2 <= set_idx2;
                        end
                    end
                    if(y_bank3_en) begin
                        if(ishit3 == 1 ) begin
                            cache_addr_bnk3 <= set_idx3;
                        end
                    end
                    if(y_bank4_en) begin
                        if(ishit4 == 1 ) begin
                            cache_addr_bnk4 <= set_idx4;
                        end
                    end                        
                
                    if(ishit1 == 0 && y_bank1_en) begin // {dpb base addr of ddr, tag, set_ad_min2,bank_addr 
                        axi_ar_addr <= {8'd0,({tag_addr_logic1, set_addr_minus_y2,2'b00}<<1) + ({tag_addr_logic1, set_addr_minus_y2,2'b00})};     
                        axi_ar_valid <= 1;
                        state <= STATE_DDR_ADDR_ACK;
                        state_i <= 5'b00_110;
                        cache_rd_when_ddr_miss <= 0;
                        st_trans <= 0;
                        ddr_serve_bank <= 2'd0;
                        set_idx_miss1_d <= set_idx_miss1;
                        set_idx_miss2_d <= set_idx_miss2;
                        set_idx_miss3_d <= set_idx_miss3;
                        set_idx_miss4_d <= set_idx_miss4;
                        d_tag_address1 <= tag_addr_logic1;
                        d_tag_address2 <= tag_addr_logic2;
                        d_tag_address3 <= tag_addr_logic3;
                        d_tag_address4 <= tag_addr_logic4;
                    end
                    else if(ishit2 == 0 && y_bank2_en) begin
                        axi_ar_addr <= {8'd0,({tag_addr_logic2, set_addr_minus_y2,2'b01}<<1) + ({tag_addr_logic2, set_addr_minus_y2,2'b01})};
                        axi_ar_valid <= 1;   
                        state <= STATE_DDR_ADDR_ACK;
                        state_i <= 5'b00_110;
                        st_trans <= 0;
                        cache_rd_when_ddr_miss <= 0;
                        ddr_serve_bank <= 2'd1;
                        d_tag_address2 <= tag_addr_logic2;
                        d_tag_address3 <= tag_addr_logic3;
                        d_tag_address4 <= tag_addr_logic4;
                        set_idx_miss2_d <= set_idx_miss2;
                        set_idx_miss3_d <= set_idx_miss3;
                        set_idx_miss4_d <= set_idx_miss4;
                    end
                    else if(ishit3 == 0 && y_bank3_en) begin
                        axi_ar_addr <= {8'd0,({tag_addr_logic3, set_addr_minus_y2,2'b10}<<1) + ({tag_addr_logic3, set_addr_minus_y2,2'b10})};
                        axi_ar_valid <= 1; 
                        state <= STATE_DDR_ADDR_ACK;
                        state_i <= 5'b00_110;
                        st_trans <= 0;
                        cache_rd_when_ddr_miss <=0;
                        ddr_serve_bank <= 2'd2;
                        set_idx_miss3_d <= set_idx_miss3;
                        set_idx_miss4_d <= set_idx_miss4;
                        d_tag_address3 <= tag_addr_logic3;
                        d_tag_address4 <= tag_addr_logic4;
                    end
                    else if(ishit4 == 0 && y_bank4_en) begin
                        axi_ar_addr <= {8'd0,({tag_addr_logic4, set_addr_minus_y2,2'b11}<<1) + ({tag_addr_logic4, set_addr_minus_y2,2'b11})};
                        axi_ar_valid <= 1;   
                        state <= STATE_DDR_ADDR_ACK;
                        state_i <= 5'b00_110;
                        st_trans <= 0;
                        cache_rd_when_ddr_miss <= 0;
                        ddr_serve_bank <= 2'd3;
                        set_idx_miss4_d <= set_idx_miss4;
                        d_tag_address4 <= tag_addr_logic4;
                    end
                    else begin   // all is ishits are true
                        //curr_x incremented even if there is a miss
                        if(delta_x != curr_x) begin     //  if curr_x has reached its maximum stop incrementing
                            curr_x <= (curr_x + 1)%Y_BANKS;
                            st_trans <= 1;
                            state_i <= 5'b00_111;
                        end
                        else if(delta_x != curr_x_d) begin
                            state_i <= 5'b00_110;
                        end
                        else if(delta_x != curr_x_2d) begin
                            state_i <= 5'b00_100;
                        end
                        else begin      //if(curr_x_2d == delta_x)
                            block_ready <= 1'b1;
                            curr_x <= 0;
                            state_i <= 5'b00_001;
                            state <= STATE_READY_DATA_ACCEPT;
                        end
                        
                    end
                end
                STATE_DDR_ADDR_ACK: begin
                    state_i <= (state_i<< 1);
                    if(axi_ar_ready) begin
                        axi_ar_valid <= 0;
                        state <= STATE_DDR_FETCH;
                        second_burst <= 0;
                        cache_rd_when_ddr_miss <= 0;
                    end
                end
                
                STATE_DDR_FETCH: begin
                    state_i <= (state_i<< 1);
                    axi_r_ready <= 1;
                    if(axi_r_valid) begin
                        if(axi_r_last) begin
                            case(ddr_serve_bank) // ddr serve bank
                                2'd0: begin
                                    for(j=0;j<32;j=j+1) begin   // write destination
                                        if(cline_used_mask1[j] == 1) begin  
                                            block_121_out[8*cline_dest_locs_1[j]+0]     <= axi_r_cline[8*j+0] ;    // d1_ishit1 should be high at this point
                                            block_121_out[8*cline_dest_locs_1[j]+1]     <= axi_r_cline[8*j+1] ;
                                            block_121_out[8*cline_dest_locs_1[j]+2]     <= axi_r_cline[8*j+2] ;
                                            block_121_out[8*cline_dest_locs_1[j]+3]     <= axi_r_cline[8*j+3] ;
                                            block_121_out[8*cline_dest_locs_1[j]+4]     <= axi_r_cline[8*j+4] ;
                                            block_121_out[8*cline_dest_locs_1[j]+5]     <= axi_r_cline[8*j+5] ;
                                            block_121_out[8*cline_dest_locs_1[j]+6]     <= axi_r_cline[8*j+6] ;
                                            block_121_out[8*cline_dest_locs_1[j]+7]     <= axi_r_cline[8*j+7] ;     
                                        end
                                    end
                                    cache_wr_en1 <= 0;
                                    if(d2_ishit2 == 0 && y_bank2_en) begin
                                        axi_ar_addr <= {8'd0,({d_tag_address2, d_set_addr_bnk,2'b01}<<1) + ({d_tag_address2, d_set_addr_bnk,2'b01})};
                                        axi_ar_valid <= 1;   
                                        state <= STATE_DDR_ADDR_ACK;
                                        ddr_serve_bank <= 2'd1;
                                    end
                                    else if(d2_ishit3 == 0 && y_bank3_en) begin
                                        axi_ar_addr <= {8'd0,({d_tag_address3, d_set_addr_bnk,2'b10}<<1) + ({d_tag_address3, d_set_addr_bnk,2'b10})};
                                        axi_ar_valid <= 1; 
                                        state <= STATE_DDR_ADDR_ACK;
                                        ddr_serve_bank <= 2'd2;
                                    end
                                    else if(d2_ishit4 == 0 && y_bank4_en) begin
                                        axi_ar_addr <= {8'd0,({d_tag_address4, d_set_addr_bnk,2'b11}<<1) + ({d_tag_address4, d_set_addr_bnk,2'b11})};
                                        axi_ar_valid <= 1;   
                                        state <= STATE_DDR_ADDR_ACK;
                                        ddr_serve_bank <= 2'd3;
                                    end
                                    else begin   // all is ishits are true                       
                                        if(st_trans == 1) begin
                                            if(y_bank1_en) begin
                                                if(ishit1 == 1) begin
                                                    cache_addr_bnk1 <= set_idx1;
                                                end                    
                                            end
                                            if(y_bank2_en) begin
                                                if(ishit2 == 1 ) begin
                                                    cache_addr_bnk2 <= set_idx2;
                                                end
                                            end
                                            if(y_bank3_en) begin
                                                if(ishit3 == 1 ) begin
                                                    cache_addr_bnk3 <= set_idx3;
                                                end
                                            end
                                            if(y_bank4_en) begin
                                                if(ishit4 == 1 ) begin
                                                    cache_addr_bnk4 <= set_idx4;
                                                end
                                            end 
                                            if(ishit1 == 0 && y_bank1_en) begin // {dpb base addr of ddr, tag, set_ad_min2,bank_addr 
                                                axi_ar_addr <= {8'd0,({tag_addr_logic1, set_addr_minus_y2,2'b00}<<1) + ({tag_addr_logic1, set_addr_minus_y2,2'b00})};     
                                                axi_ar_valid <= 1;
                                                state <= STATE_DDR_ADDR_ACK;
                                                state_i <= 5'b00_010;
                                                cache_rd_when_ddr_miss <= 0;
                                                st_trans <= 0;
                                                ddr_serve_bank <= 2'd0;
                                                set_idx_miss1_d <= set_idx_miss1;
                                                set_idx_miss2_d <= set_idx_miss2;
                                                set_idx_miss3_d <= set_idx_miss3;
                                                set_idx_miss4_d <= set_idx_miss4;
                                                d_tag_address1 <= tag_addr_logic1;
                                                d_tag_address2 <= tag_addr_logic2;
                                                d_tag_address3 <= tag_addr_logic3;
                                                d_tag_address4 <= tag_addr_logic4;
                                            end
                                            else if(ishit2 == 0 && y_bank2_en) begin
                                                axi_ar_addr <= {8'd0,({tag_addr_logic2, set_addr_minus_y2,2'b01}<<1) + ({tag_addr_logic2, set_addr_minus_y2,2'b01})};
                                                axi_ar_valid <= 1;   
                                                state <= STATE_DDR_ADDR_ACK;
                                                state_i <= 5'b00_010;
                                                st_trans <= 0;
                                                cache_rd_when_ddr_miss <= 0;
                                                ddr_serve_bank <= 2'd1;
                                                d_tag_address2 <= tag_addr_logic2;
                                                d_tag_address3 <= tag_addr_logic3;
                                                d_tag_address4 <= tag_addr_logic4;
                                                set_idx_miss2_d <= set_idx_miss2;
                                                set_idx_miss3_d <= set_idx_miss3;
                                                set_idx_miss4_d <= set_idx_miss4;
                                            end
                                            else if(ishit3 == 0 && y_bank3_en) begin
                                                axi_ar_addr <= {8'd0,({tag_addr_logic3, set_addr_minus_y2,2'b10}<<1) + ({tag_addr_logic3, set_addr_minus_y2,2'b10})};
                                                axi_ar_valid <= 1; 
                                                state <= STATE_DDR_ADDR_ACK;
                                                state_i <= 5'b00_010;
                                                st_trans <= 0;
                                                cache_rd_when_ddr_miss <=0;
                                                ddr_serve_bank <= 2'd2;
                                                d_tag_address3 <= tag_addr_logic3;
                                                d_tag_address4 <= tag_addr_logic4;
                                                set_idx_miss3_d <= set_idx_miss3;
                                                set_idx_miss4_d <= set_idx_miss4;
                                            end
                                            else if(ishit4 == 0 && y_bank4_en) begin
                                                axi_ar_addr <= {8'd0,({tag_addr_logic4, set_addr_minus_y2,2'b11}<<1) + ({tag_addr_logic4, set_addr_minus_y2,2'b11})};
                                                axi_ar_valid <= 1;   
                                                state <= STATE_DDR_ADDR_ACK;
                                                state_i <= 5'b00_010;
                                                st_trans <= 0;
                                                cache_rd_when_ddr_miss <= 0;
                                                ddr_serve_bank <= 2'd3;
                                                set_idx_miss4_d <= set_idx_miss4;
                                                d_tag_address4 <= tag_addr_logic4;
                                            end
                                            else begin   // all is ishits are true
                                                //curr_x incremented even if there is a miss
                                                if(delta_x != curr_x) begin     //  if curr_x has reached its maximum stop incrementing
                                                    curr_x <= (curr_x + 1)%Y_BANKS;
                                                    st_trans <= 1;
                                                    state_i <= 5'b00_011;
                                                    state <= STATE_C_ADDR_ADMIT;
                                                end
                                                else if(delta_x != curr_x_d) begin
                                                    state_i <= 5'b00_010;
                                                    state <= STATE_C_ADDR_ADMIT;
                                                    st_trans <= 0;
                                                end
                                                if(delta_x == curr_x_2d) begin      //if(curr_x_2d == delta_x)
                                                    block_ready <= 1'b1;
                                                    st_trans <= 0;
                                                    curr_x <= 0;
                                                    state_i <= 5'b00_001;
                                                    state <= STATE_READY_DATA_ACCEPT;
                                                end
                                                
                                            end                                          
                                        end
                                        else begin
                                            block_ready <= 1'b1;
                                            st_trans <= 0;
                                            curr_x <= 0;
                                            state_i <= 5'b00_001;
                                            state <= STATE_READY_DATA_ACCEPT;
                                        end
                                    end 
                                end
                                2'd1: begin
                                    for(j=0;j<32;j=j+1) begin   // write destination
                                        if(cline_used_mask1[j] == 1) begin
                                            block_121_out[8*cline_dest_locs_2[j]+0]     <= axi_r_cline[8*j+0] ;
                                            block_121_out[8*cline_dest_locs_2[j]+1]     <= axi_r_cline[8*j+1] ;
                                            block_121_out[8*cline_dest_locs_2[j]+2]     <= axi_r_cline[8*j+2] ;
                                            block_121_out[8*cline_dest_locs_2[j]+3]     <= axi_r_cline[8*j+3] ;
                                            block_121_out[8*cline_dest_locs_2[j]+4]     <= axi_r_cline[8*j+4] ;
                                            block_121_out[8*cline_dest_locs_2[j]+5]     <= axi_r_cline[8*j+5] ;
                                            block_121_out[8*cline_dest_locs_2[j]+6]     <= axi_r_cline[8*j+6] ;
                                            block_121_out[8*cline_dest_locs_2[j]+7]     <= axi_r_cline[8*j+7] ;     
                                        end
                                    end
                                    cache_wr_en2 <= 0;
                                    if(d2_ishit3 == 0 && y_bank3_en) begin
                                        axi_ar_addr <= {8'd0,({d_tag_address3, d_set_addr_bnk,2'b10}<<1) + ({d_tag_address3, d_set_addr_bnk,2'b10})};
                                        axi_ar_valid <= 1; 
                                        state <= STATE_DDR_ADDR_ACK;
                                        ddr_serve_bank <= 2'd2;
                                    end
                                    else if(d2_ishit4 == 0 && y_bank4_en) begin
                                        axi_ar_addr <= {8'd0,({d_tag_address4, d_set_addr_bnk,2'b11}<<1) + ({d_tag_address4, d_set_addr_bnk,2'b11})};
                                        axi_ar_valid <= 1;   
                                        state <= STATE_DDR_ADDR_ACK;
                                        ddr_serve_bank <= 2'd3;
                                    end
                                                                        else begin   // all is ishits are true                       
                                        if(st_trans == 1) begin
                                            if(y_bank1_en) begin
                                                if(ishit1 == 1) begin
                                                    cache_addr_bnk1 <= set_idx1;
                                                end                    
                                            end
                                            if(y_bank2_en) begin
                                                if(ishit2 == 1 ) begin
                                                    cache_addr_bnk2 <= set_idx2;
                                                end
                                            end
                                            if(y_bank3_en) begin
                                                if(ishit3 == 1 ) begin
                                                    cache_addr_bnk3 <= set_idx3;
                                                end
                                            end
                                            if(y_bank4_en) begin
                                                if(ishit4 == 1 ) begin
                                                    cache_addr_bnk4 <= set_idx4;
                                                end
                                            end 
                                            if(ishit1 == 0 && y_bank1_en) begin // {dpb base addr of ddr, tag, set_ad_min2,bank_addr 
                                                axi_ar_addr <= {8'd0,({tag_addr_logic1, set_addr_minus_y2,2'b00}<<1) + ({tag_addr_logic1, set_addr_minus_y2,2'b00})};     
                                                axi_ar_valid <= 1;
                                                state <= STATE_DDR_ADDR_ACK;
                                                state_i <= 5'b00_010;
                                                cache_rd_when_ddr_miss <= 0;
                                                st_trans <= 0;
                                                ddr_serve_bank <= 2'd0;
                                                set_idx_miss1_d <= set_idx_miss1;
                                                set_idx_miss2_d <= set_idx_miss2;
                                                set_idx_miss3_d <= set_idx_miss3;
                                                set_idx_miss4_d <= set_idx_miss4;
                                                d_tag_address1 <= tag_addr_logic1;
                                                d_tag_address2 <= tag_addr_logic2;
                                                d_tag_address3 <= tag_addr_logic3;
                                                d_tag_address4 <= tag_addr_logic4;    
                                            end
                                            else if(ishit2 == 0 && y_bank2_en) begin
                                                axi_ar_addr <= {8'd0,({tag_addr_logic2, set_addr_minus_y2,2'b01}<<1) + ({tag_addr_logic2, set_addr_minus_y2,2'b01})};
                                                axi_ar_valid <= 1;   
                                                state <= STATE_DDR_ADDR_ACK;
                                                state_i <= 5'b00_010;
                                                st_trans <= 0;
                                                cache_rd_when_ddr_miss <= 0;
                                                ddr_serve_bank <= 2'd1;
                                                d_tag_address2 <= tag_addr_logic2;
                                                d_tag_address3 <= tag_addr_logic3;
                                                d_tag_address4 <= tag_addr_logic4;                                                
                                                set_idx_miss2_d <= set_idx_miss2;
                                                set_idx_miss3_d <= set_idx_miss3;
                                                set_idx_miss4_d <= set_idx_miss4;
                                            end
                                            else if(ishit3 == 0 && y_bank3_en) begin
                                                axi_ar_addr <= {8'd0,({tag_addr_logic3, set_addr_minus_y2,2'b10}<<1) + ({tag_addr_logic3, set_addr_minus_y2,2'b10})};
                                                axi_ar_valid <= 1; 
                                                state <= STATE_DDR_ADDR_ACK;
                                                state_i <= 5'b00_010;
                                                st_trans <= 0;
                                                cache_rd_when_ddr_miss <=0;
                                                ddr_serve_bank <= 2'd2;
                                                d_tag_address3 <= tag_addr_logic3;
                                                d_tag_address4 <= tag_addr_logic4;
                                                set_idx_miss3_d <= set_idx_miss3;
                                                set_idx_miss4_d <= set_idx_miss4;
                                            end
                                            else if(ishit4 == 0 && y_bank4_en) begin
                                                axi_ar_addr <= {8'd0,({tag_addr_logic4, set_addr_minus_y2,2'b11}<<1) + ({tag_addr_logic4, set_addr_minus_y2,2'b11})};
                                                axi_ar_valid <= 1;   
                                                state <= STATE_DDR_ADDR_ACK;
                                                state_i <= 5'b00_010;
                                                st_trans <= 0;
                                                cache_rd_when_ddr_miss <= 0;
                                                ddr_serve_bank <= 2'd3;
                                                set_idx_miss4_d <= set_idx_miss4;
                                                d_tag_address4 <= tag_addr_logic4;
                                            end
                                            else begin   // all is ishits are true
                                                //curr_x incremented even if there is a miss
                                                if(delta_x != curr_x) begin     //  if curr_x has reached its maximum stop incrementing
                                                    curr_x <= (curr_x + 1)%Y_BANKS;
                                                    st_trans <= 1;
                                                    state_i <= 5'b00_011;
                                                    state <= STATE_C_ADDR_ADMIT;
                                                end
                                                else if(delta_x != curr_x_d) begin
                                                    state_i <= 5'b00_010;
                                                    state <= STATE_C_ADDR_ADMIT;
                                                    st_trans <= 0;
                                                end
                                                if(delta_x == curr_x_2d) begin      //if(curr_x_2d == delta_x)
                                                    block_ready <= 1'b1;
                                                    st_trans <= 0;
                                                    curr_x <= 0;
                                                    state_i <= 5'b00_001;
                                                    state <= STATE_READY_DATA_ACCEPT;
                                                end
                                                
                                            end                                          
                                        end
                                        else begin
                                            block_ready <= 1'b1;
                                            st_trans <= 0;
                                            curr_x <= 0;
                                            state_i <= 5'b00_001;
                                            state <= STATE_READY_DATA_ACCEPT;
                                        end
                                    end 
                                end
                                2'd2: begin
                                    for(j=0;j<32;j=j+1) begin   // write destination
                                        if(cline_used_mask1[j] == 1) begin
                                            block_121_out[8*cline_dest_locs_3[j]+0]     <= axi_r_cline[8*j+0] ;
                                            block_121_out[8*cline_dest_locs_3[j]+1]     <= axi_r_cline[8*j+1] ;
                                            block_121_out[8*cline_dest_locs_3[j]+2]     <= axi_r_cline[8*j+2] ;
                                            block_121_out[8*cline_dest_locs_3[j]+3]     <= axi_r_cline[8*j+3] ;
                                            block_121_out[8*cline_dest_locs_3[j]+4]     <= axi_r_cline[8*j+4] ;
                                            block_121_out[8*cline_dest_locs_3[j]+5]     <= axi_r_cline[8*j+5] ;
                                            block_121_out[8*cline_dest_locs_3[j]+6]     <= axi_r_cline[8*j+6] ;
                                            block_121_out[8*cline_dest_locs_3[j]+7]     <= axi_r_cline[8*j+7] ;     
                                        end
                                    end
                                    cache_wr_en3 <= 0;
                                    if(d2_ishit4 == 0 && y_bank4_en) begin
                                        axi_ar_addr <= {8'd0,({d_tag_address4, d_set_addr_bnk,2'b11}<<1) + ({d_tag_address4, d_set_addr_bnk,2'b11})};
                                        axi_ar_valid <= 1;   
                                        state <= STATE_DDR_ADDR_ACK;
                                        ddr_serve_bank <= 2'd3;
                                    end
                                                                        else begin   // all is ishits are true                       
                                        if(st_trans == 1) begin
                                            if(y_bank1_en) begin
                                                if(ishit1 == 1) begin
                                                    cache_addr_bnk1 <= set_idx1;
                                                end                    
                                            end
                                            if(y_bank2_en) begin
                                                if(ishit2 == 1 ) begin
                                                    cache_addr_bnk2 <= set_idx2;
                                                end
                                            end
                                            if(y_bank3_en) begin
                                                if(ishit3 == 1 ) begin
                                                    cache_addr_bnk3 <= set_idx3;
                                                end
                                            end
                                            if(y_bank4_en) begin
                                                if(ishit4 == 1 ) begin
                                                    cache_addr_bnk4 <= set_idx4;
                                                end
                                            end 
                                            if(ishit1 == 0 && y_bank1_en) begin // {dpb base addr of ddr, tag, set_ad_min2,bank_addr 
                                                axi_ar_addr <= {8'd0,({tag_addr_logic1, set_addr_minus_y2,2'b00}<<1) + ({tag_addr_logic1, set_addr_minus_y2,2'b00})};     
                                                axi_ar_valid <= 1;
                                                state <= STATE_DDR_ADDR_ACK;
                                                state_i <= 5'b00_010;
                                                cache_rd_when_ddr_miss <= 0;
                                                st_trans <= 0;
                                                ddr_serve_bank <= 2'd0;
                                                set_idx_miss1_d <= set_idx_miss1;
                                                set_idx_miss2_d <= set_idx_miss2;
                                                set_idx_miss3_d <= set_idx_miss3;
                                                set_idx_miss4_d <= set_idx_miss4;
                                                d_tag_address1 <= tag_addr_logic1;
                                                d_tag_address2 <= tag_addr_logic2;
                                                d_tag_address3 <= tag_addr_logic3;
                                                d_tag_address4 <= tag_addr_logic4;  
                                            end
                                            else if(ishit2 == 0 && y_bank2_en) begin
                                                axi_ar_addr <= {8'd0,({tag_addr_logic2, set_addr_minus_y2,2'b01}<<1) + ({tag_addr_logic2, set_addr_minus_y2,2'b01})};
                                                axi_ar_valid <= 1;   
                                                state <= STATE_DDR_ADDR_ACK;
                                                state_i <= 5'b00_010;
                                                st_trans <= 0;
                                                cache_rd_when_ddr_miss <= 0;
                                                ddr_serve_bank <= 2'd1;
                                                d_tag_address2 <= tag_addr_logic2;
                                                d_tag_address3 <= tag_addr_logic3;
                                                d_tag_address4 <= tag_addr_logic4;                                                
                                                set_idx_miss2_d <= set_idx_miss2;
                                                set_idx_miss3_d <= set_idx_miss3;
                                                set_idx_miss4_d <= set_idx_miss4;
                                            end
                                            else if(ishit3 == 0 && y_bank3_en) begin
                                                axi_ar_addr <= {8'd0,({tag_addr_logic3, set_addr_minus_y2,2'b10}<<1) + ({tag_addr_logic3, set_addr_minus_y2,2'b10})};
                                                axi_ar_valid <= 1; 
                                                state <= STATE_DDR_ADDR_ACK;
                                                state_i <= 5'b00_010;
                                                st_trans <= 0;
                                                cache_rd_when_ddr_miss <=0;
                                                ddr_serve_bank <= 2'd2;
                                                d_tag_address3 <= tag_addr_logic3;
                                                d_tag_address4 <= tag_addr_logic4;
                                                
                                                set_idx_miss3_d <= set_idx_miss3;
                                                set_idx_miss4_d <= set_idx_miss4;
                                            end
                                            else if(ishit4 == 0 && y_bank4_en) begin
                                                axi_ar_addr <= {8'd0,({tag_addr_logic4, set_addr_minus_y2,2'b11}<<1) + ({tag_addr_logic4, set_addr_minus_y2,2'b11})};
                                                axi_ar_valid <= 1;   
                                                state <= STATE_DDR_ADDR_ACK;
                                                state_i <= 5'b00_010;
                                                st_trans <= 0;
                                                cache_rd_when_ddr_miss <= 0;
                                                ddr_serve_bank <= 2'd3;
                                                set_idx_miss4_d <= set_idx_miss4;
                                                d_tag_address4 <= tag_addr_logic4;
                                            end
                                            else begin   // all is ishits are true
                                                //curr_x incremented even if there is a miss
                                                if(delta_x != curr_x) begin     //  if curr_x has reached its maximum stop incrementing
                                                    curr_x <= (curr_x + 1)%Y_BANKS;
                                                    st_trans <= 1;
                                                    state_i <= 5'b00_011;
                                                    state <= STATE_C_ADDR_ADMIT;
                                                end
                                                else if(delta_x != curr_x_d) begin
                                                    state_i <= 5'b00_010;
                                                    state <= STATE_C_ADDR_ADMIT;
                                                    st_trans <= 0;
                                                end
                                                if(delta_x == curr_x_2d) begin      //if(curr_x_2d == delta_x)
                                                    block_ready <= 1'b1;
                                                    st_trans <= 0;
                                                    curr_x <= 0;
                                                    state_i <= 5'b00_001;
                                                    state <= STATE_READY_DATA_ACCEPT;
                                                end
                                                
                                            end                                          
                                        end
                                        else begin
                                            block_ready <= 1'b1;
                                            st_trans <= 0;
                                            curr_x <= 0;
                                            state_i <= 5'b00_001;
                                            state <= STATE_READY_DATA_ACCEPT;
                                        end
                                    end 
                                end
                                2'd3: begin
                                    for(j=0;j<32;j=j+1) begin   // write destination
                                        if(cline_used_mask1[j] == 1) begin
                                            block_121_out[8*cline_dest_locs_4[j]+0]     <= axi_r_cline[8*j+0] ;
                                            block_121_out[8*cline_dest_locs_4[j]+1]     <= axi_r_cline[8*j+1] ;
                                            block_121_out[8*cline_dest_locs_4[j]+2]     <= axi_r_cline[8*j+2] ;
                                            block_121_out[8*cline_dest_locs_4[j]+3]     <= axi_r_cline[8*j+3] ;
                                            block_121_out[8*cline_dest_locs_4[j]+4]     <= axi_r_cline[8*j+4] ;
                                            block_121_out[8*cline_dest_locs_4[j]+5]     <= axi_r_cline[8*j+5] ;
                                            block_121_out[8*cline_dest_locs_4[j]+6]     <= axi_r_cline[8*j+6] ;
                                            block_121_out[8*cline_dest_locs_4[j]+7]     <= axi_r_cline[8*j+7] ;     
                                        end
                                    end
                                    cache_wr_en4 <= 0;
                                    if(y_bank1_en) begin
                                        if(ishit1 == 1) begin
                                            cache_addr_bnk1 <= set_idx1;
                                        end                    
                                    end
                                    if(y_bank2_en) begin
                                        if(ishit2 == 1 ) begin
                                            cache_addr_bnk2 <= set_idx2;
                                        end
                                    end
                                    if(y_bank3_en) begin
                                        if(ishit3 == 1 ) begin
                                            cache_addr_bnk3 <= set_idx3;
                                        end
                                    end
                                    if(y_bank4_en) begin
                                        if(ishit4 == 1 ) begin
                                            cache_addr_bnk4 <= set_idx4;
                                        end
                                    end                        
                    
                                    if(st_trans == 1) begin
                                        if(y_bank1_en) begin
                                            if(ishit1 == 1) begin
                                                cache_addr_bnk1 <= set_idx1;
                                            end                    
                                        end
                                        if(y_bank2_en) begin
                                            if(ishit2 == 1 ) begin
                                                cache_addr_bnk2 <= set_idx2;
                                            end
                                        end
                                        if(y_bank3_en) begin
                                            if(ishit3 == 1 ) begin
                                                cache_addr_bnk3 <= set_idx3;
                                            end
                                        end
                                        if(y_bank4_en) begin
                                            if(ishit4 == 1 ) begin
                                                cache_addr_bnk4 <= set_idx4;
                                            end
                                        end 
                                        if(ishit1 == 0 && y_bank1_en) begin // {dpb base addr of ddr, tag, set_ad_min2,bank_addr 
                                            axi_ar_addr <= {8'd0,({tag_addr_logic1, set_addr_minus_y2,2'b00}<<1) + ({tag_addr_logic1, set_addr_minus_y2,2'b00})};     
                                            axi_ar_valid <= 1;
                                            state <= STATE_DDR_ADDR_ACK;
                                            state_i <= 5'b00_010;
                                            cache_rd_when_ddr_miss <= 0;
                                            st_trans <= 0;
                                            ddr_serve_bank <= 2'd0;
                                            set_idx_miss1_d <= set_idx_miss1;
                                            set_idx_miss2_d <= set_idx_miss2;
                                            set_idx_miss3_d <= set_idx_miss3;
                                            set_idx_miss4_d <= set_idx_miss4;
                                            d_tag_address1 <= tag_addr_logic1;
                                            d_tag_address2 <= tag_addr_logic2;
                                            d_tag_address3 <= tag_addr_logic3;
                                            d_tag_address4 <= tag_addr_logic4;    
                                        end
                                        else if(ishit2 == 0 && y_bank2_en) begin
                                            axi_ar_addr <= {8'd0,({tag_addr_logic2, set_addr_minus_y2,2'b01}<<1) + ({tag_addr_logic2, set_addr_minus_y2,2'b01})};
                                            axi_ar_valid <= 1;   
                                            state <= STATE_DDR_ADDR_ACK;
                                            state_i <= 5'b00_010;
                                            st_trans <= 0;
                                            cache_rd_when_ddr_miss <= 0;
                                            ddr_serve_bank <= 2'd1;
                                            d_tag_address2 <= tag_addr_logic2;
                                            d_tag_address3 <= tag_addr_logic3;
                                            d_tag_address4 <= tag_addr_logic4;
                                            
                                            set_idx_miss2_d <= set_idx_miss2;
                                            set_idx_miss3_d <= set_idx_miss3;
                                            set_idx_miss4_d <= set_idx_miss4;
                                        end
                                        else if(ishit3 == 0 && y_bank3_en) begin
                                            axi_ar_addr <= {8'd0,({tag_addr_logic3, set_addr_minus_y2,2'b10}<<1) + ({tag_addr_logic3, set_addr_minus_y2,2'b10})};
                                            axi_ar_valid <= 1; 
                                            state <= STATE_DDR_ADDR_ACK;
                                            state_i <= 5'b00_010;
                                            st_trans <= 0;
                                            cache_rd_when_ddr_miss <=0;
                                            ddr_serve_bank <= 2'd2;
                                            d_tag_address3 <= tag_addr_logic3;
                                            d_tag_address4 <= tag_addr_logic4;
                                            set_idx_miss3_d <= set_idx_miss3;
                                            set_idx_miss4_d <= set_idx_miss4;
                                        end
                                        else if(ishit4 == 0 && y_bank4_en) begin
                                            axi_ar_addr <= {8'd0,({tag_addr_logic4, set_addr_minus_y2,2'b11}<<1) + ({tag_addr_logic4, set_addr_minus_y2,2'b11})};
                                            axi_ar_valid <= 1;   
                                            state <= STATE_DDR_ADDR_ACK;
                                            state_i <= 5'b00_010;
                                            st_trans <= 0;
                                            cache_rd_when_ddr_miss <= 0;
                                            ddr_serve_bank <= 2'd3;
                                            set_idx_miss4_d <= set_idx_miss4;
                                            d_tag_address4 <= tag_addr_logic4;
                                        end
                                        else begin   // all is ishits are true
                                            //curr_x incremented even if there is a miss
                                            if(delta_x != curr_x) begin     //  if curr_x has reached its maximum stop incrementing
                                                curr_x <= (curr_x + 1)%Y_BANKS;
                                                st_trans <= 1;
                                                state_i <= 5'b00_011;
                                                state <= STATE_C_ADDR_ADMIT;
                                            end
                                            else if(delta_x != curr_x_d) begin
                                                state_i <= 5'b00_010;
                                                state <= STATE_C_ADDR_ADMIT;
                                                st_trans <= 0;
                                            end
                                            if(delta_x == curr_x_2d) begin      //if(curr_x_2d == delta_x)
                                                block_ready <= 1'b1;
                                                st_trans <= 0;
                                                curr_x <= 0;
                                                state_i <= 5'b00_001;
                                                state <= STATE_READY_DATA_ACCEPT;
                                            end
                                            
                                        end                                          
                                    end
                                    else begin
                                        block_ready <= 1'b1;
                                        st_trans <= 0;
                                        curr_x <= 0;
                                        state_i <= 5'b00_001;
                                        state <= STATE_READY_DATA_ACCEPT;
                                    end                                         

                                end
                            endcase
                        // fill the 121 block
                        // write back to cache line
                        end
                        else begin      // not the last in the burst
                            if(second_burst == 0) begin
                                second_burst <= 1;
                                axi_read_buf[AXI_DATA_WDTH-1:0] <= axi_r_data;
                            end
                            else begin
                                axi_read_buf[2*AXI_DATA_WDTH-1:AXI_DATA_WDTH] <= axi_r_data;        
                                case(ddr_serve_bank)
                                    2'd0: begin
                                        cache_wr_en1 <= 1;
                                        cache_addr_bnk1 <= set_idx_miss1_d;
                                        if((!y_bank2_en || d2_ishit2) && (d2_ishit3 || !y_bank3_en) && (d2_ishit4 || !y_bank4_en))begin // predicting terminating condition
                                        // if y_bank1_en is high check if d2_ishit1 is also high, then axi requests terminate
                                            if(delta_x != curr_x) begin     //  if curr_x has reached its maximum stop incrementing
                                                curr_x <= (curr_x + 1)%Y_BANKS;
                                                st_trans <= 1;
                                                state_i <= 5'b00_001;
                                            end
                                        end
                                    end
                                    2'd1: begin
                                        cache_wr_en2 <= 1;
                                        cache_addr_bnk2 <= set_idx_miss2_d;
                                        if((d2_ishit3 || !y_bank3_en) && (d2_ishit4 || !y_bank4_en))begin
                                            if(delta_x != curr_x) begin     //  if curr_x has reached its maximum stop incrementing
                                                curr_x <= (curr_x + 1)%Y_BANKS;
                                                st_trans <= 1;
                                                state_i <= 5'b00_001;
                                            end                                        
                                        end
                                    end
                                    2'd2: begin
                                        cache_wr_en3 <= 1;
                                        cache_addr_bnk3 <= set_idx_miss3_d;
                                        if((d2_ishit4 || !y_bank4_en))begin
                                            if(delta_x != curr_x) begin     //  if curr_x has reached its maximum stop incrementing
                                                curr_x <= (curr_x + 1)%Y_BANKS;
                                                st_trans <= 1;
                                                state_i <= 5'b00_001;
                                            end                                        
                                        end 
                                    end
                                    2'd3: begin
                                        cache_wr_en4 <= 1;
                                        cache_addr_bnk4 <= set_idx_miss4_d;
                                        if(delta_x != curr_x) begin     //  if curr_x has reached its maximum stop incrementing
                                            curr_x <= (curr_x + 1)%Y_BANKS;
                                            st_trans <= 1;
                                            state_i <= 5'b00_001;
                                        end    
                                    end
                                endcase
                            end
                        end
                    end
                end
                
                
                
            endcase
        end
        
    end
	
    
endmodule