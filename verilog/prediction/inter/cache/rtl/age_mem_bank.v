`timescale 1ns / 1ps
module age_mem_bank
(
    clk,
	reset,
    r_addr_in,
	w_addr_in,
	r_data_out,
	w_data_in,
	w_en_in
	//r_en_in

);

    `include "cache_configs_def.v"
    
    //---------------------------------------------------------------------------------------------------------------------
    // parameter definitions
    //---------------------------------------------------------------------------------------------------------------------
	parameter							AGE_BANK_LEN = SET_ADDR_WDTH - Y_BANK_BITS;
    
	
    //---------------------------------------------------------------------------------------------------------------------
    // localparam definitions
    //---------------------------------------------------------------------------------------------------------------------
    
    localparam                          STATE_CLEAR				        = 0;    // the state that is entered upon reset
    localparam                          STATE_NORMAL	                = 1;
   	
    //---------------------------------------------------------------------------------------------------------------------
    // I/O signals
    //---------------------------------------------------------------------------------------------------------------------
    

    input                           						clk;
	input													reset;
	input	[(1<<C_N_WAY)*C_N_WAY-1:0]      				w_data_in;
	input	[SET_ADDR_WDTH - Y_BANK_BITS-1:0]				r_addr_in;			// read 8 ages at once 
	input	[SET_ADDR_WDTH - Y_BANK_BITS-1:0]				w_addr_in;			
	input													w_en_in;
    //input                                                   r_en_in;
	output  [(1<<C_N_WAY)*C_N_WAY-1:0]			      		r_data_out;
	
	
	//---------------------------------------------------------------------------------------------------------------------
    // Internal wires and registers
    //---------------------------------------------------------------------------------------------------------------------
  

    reg         [C_N_WAY*(1<<C_N_WAY)-1:0]      		mem [(1<<AGE_BANK_LEN)-1:0];
	//reg			[SET_ADDR_WDTH - Y_BANK_BITS-1:0]		out_address;
	reg [1:0]                                             count;
    reg                                                 all_clear;
	integer state;

  
    //---------------------------------------------------------------------------------------------------------------------
    // Implmentation
    //---------------------------------------------------------------------------------------------------------------------
	// always @(posedge clk) begin
		// if(reset) begin
			// count <= 0;
			// state <= STATE_CLEAR;
		// end
		// else begin
			// case(state) 
				// STATE_CLEAR: begin
					// if(count == AGE_BANK_LEN) begin
						// state <= STATE_NORMAL;
					// end
					// else begin
						// mem[count] <= count[2:0];
                        // count <= count + 1;
					// end
				// end
				// STATE_NORMAL: begin
					// if(w_en_in) begin
						// mem[w_addr_in] <= w_data_in;
					// end
				// end
			// endcase
		// end
	// end	
	
	// assign 	r_data_out = {	mem[{r_addr_in,3'b000}],
							// mem[{r_addr_in,3'b001}],
							// mem[{r_addr_in,3'b010}],
							// mem[{r_addr_in,3'b011}],
							// mem[{r_addr_in,3'b100}],
							// mem[{r_addr_in,3'b101}],
							// mem[{r_addr_in,3'b110}],
							// mem[{r_addr_in,3'b111}]};    
    
	always @(posedge clk) begin
		if(reset) begin
			count <= 0;
            all_clear <= 0;
			state <= STATE_CLEAR;
		end
		else begin
			case(state) 
				STATE_CLEAR: begin
					if(count == 0 && all_clear == 1) begin
						state <= STATE_NORMAL;
					end
					else begin
                        all_clear <= 1;
						mem[count] <= { 3'd0,
                                        3'd1,
                                        3'd2,
                                        3'd3,
                                        3'd4,
                                        3'd5,
                                        3'd6,
                                        3'd7
                                        };
                        
                        count <= (count + 1)%(1<<AGE_BANK_LEN);
					end
				end
				STATE_NORMAL: begin
					if(w_en_in) begin
						mem[w_addr_in] <= w_data_in;
					end
					// if(r_en_in) begin
						// out_address <= r_addr_in;
					// end
				end
			endcase
		end
	end	
	
	assign 	r_data_out = mem[r_addr_in];
                            // ,
							// mem[{out_address,3'b001}],
							// mem[{out_address,3'b010}],
							// mem[{out_address,3'b011}],
							// mem[{out_address,3'b100}],
							// mem[{out_address,3'b101}],
							// mem[{out_address,3'b110}],
							// mem[{out_address,3'b111}]};
						
				
						
endmodule