//Global header

localparam C_L_H_SIZE = 3;
localparam C_L_V_SIZE = 2;
localparam C_N_WAY  = 3;
localparam C_SIZE  = 12;
localparam ADDR_WDTH  = 28;
localparam TAG_ADDR_WDTH    = ADDR_WDTH - C_SIZE + C_N_WAY;
localparam OFFSET_ADDR_WDTH = C_L_H_SIZE + C_L_V_SIZE;
localparam SET_ADDR_WDTH = C_SIZE - C_N_WAY - OFFSET_ADDR_WDTH;

localparam CACHE_LINE_WDTH = 1 << (C_L_H_SIZE + C_L_V_SIZE);
localparam SET_INDEX_WDTH = C_N_WAY;

localparam Y_BANK_BITS			    = 2;
localparam  OUT_BLOCK_SIZE          = 121;

localparam X_ADDR_WDTH = 12;
localparam Y_ADDR_WDTH = 12;
localparam REF_ADDR_WDTH = 4;
localparam PIXEL_BITS = 12;
localparam LUMA_BITS = 8;




