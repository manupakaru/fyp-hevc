`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    14:10:59 11/17/2013 
// Design Name: 
// Module Name:    inter_pred_top 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module inter_pred_top(
    clk,
    reset,
    enable,
    tu_empty,
    tu_data_in,
    config_mode_in,
    config_bus_in,
    
    from_top_ref_pic_list_poc_data_in,
    from_top_ref_pic_list0_poc_wr_en,
    from_top_ref_pic_list1_poc_wr_en,
    from_top_ref_pic_list0_poc_addr,
    from_top_ref_pic_list1_poc_addr,

    from_top_ref_pic_list_idx_data_in,
    from_top_ref_pic_list_idx_wr_en,
    
    mv_done_out,
    pred_pixl_gen_idle_out_to_pred_top,
    pref_ctu_col_writ_ctu_done_out,

    xT_in_4x4_luma_out       ,    
    yT_in_4x4_luma_out       ,    
    xT_in_4x4_cb_out         ,    
    yT_in_4x4_cb_out         ,    
    xT_in_4x4_cr_out         ,    
    yT_in_4x4_cr_out         ,    
    luma_wgt_pred_out        ,      
    cb_wgt_pred_out          ,      
    cr_wgt_pred_out          ,      
    luma_wght_pred_valid     ,
    cb_wght_pred_valid       ,
    cr_wght_pred_valid       ,
    
    ref_pix_axi_ar_addr     ,
    ref_pix_axi_ar_len      ,   
    ref_pix_axi_ar_size     ,   
    ref_pix_axi_ar_burst    ,   
    ref_pix_axi_ar_prot     ,   
    ref_pix_axi_ar_valid    ,
    ref_pix_axi_ar_ready    ,   
    
    ref_pix_axi_r_data      ,   
    ref_pix_axi_r_resp      ,   
    ref_pix_axi_r_last      ,   
    ref_pix_axi_r_valid     ,   
    ref_pix_axi_r_ready     ,
    
    

    mv_col_axi_awid,     
    mv_col_axi_awlen,    
    mv_col_axi_awsize,   
    mv_col_axi_awburst,  
    mv_col_axi_awlock,   
    mv_col_axi_awcache,  
    mv_col_axi_awprot,   
    mv_col_axi_awvalid,
    mv_col_axi_awaddr,
    mv_col_axi_awready,
    mv_col_axi_wstrb,
    mv_col_axi_wlast,
    mv_col_axi_wvalid,
    mv_col_axi_wdata,
    mv_col_axi_wready,
    // mv_col_axi_bid,
    mv_col_axi_bresp,
    mv_col_axi_bvalid,
    mv_col_axi_bready,
    
    mv_pref_axi_araddr              ,
    mv_pref_axi_arlen               ,
    mv_pref_axi_arsize              ,
    mv_pref_axi_arburst             ,
    mv_pref_axi_arprot              ,
    mv_pref_axi_arvalid             ,
    mv_pref_axi_arready             ,

    mv_pref_axi_rdata               ,
    mv_pref_axi_rresp               ,
    mv_pref_axi_rlast               ,
    mv_pref_axi_rvalid              ,
    mv_pref_axi_rready              ,

    mv_pref_axi_arlock              ,
    mv_pref_axi_arid                ,
    mv_pref_axi_arcache            
    
    );


//---------------------------------------------------------------------------------------------------------------------
// Global constant headers
//---------------------------------------------------------------------------------------------------------------------
    /* STYLE_NOTES :
     * Always include global constant header files that have constant definitions before all other items
     * these files will contain constants in the form of localparams of `define directives
     */
    `include "pred_def.v"
    `include "inter_axi_def.v"
//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------
    /* STYLE_NOTES :
     * Define all paramters
     */

     
//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------
    /* STYLE_NOTES :
     * Define all localparams after parameters
     */
    localparam                          STATE_INTER_CONFIG_UPDATE = 0;
    localparam                          STATE_INTER_PREFETCH_COL_WRITE_WAIT = 1;
    localparam                          STATE_INTER_MV_DERIVE_WAIT = 2;
     
    parameter                           BLOCK_WIDTH_4x4                 = 3'd4;
    
//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    /* STYLE_NOTES
     * Eacho I/O signal must be defined on a seperate line
     * All I/Os other than the clk, reset and standard interfaces (ex. local-link, axi4) should contain
     * an '_in' or '_out' suffix
     * Comment your I/O by block and by signal
     */

    input clk;
    input reset;
    input enable;
    input tu_empty;
    input [INTER_TOP_CONFIG_BUS_WIDTH-1:0] tu_data_in;
    input [INTER_TOP_CONFIG_BUS_MODE_WIDTH -1 :0] config_mode_in;
    input [INTER_TOP_CONFIG_BUS_WIDTH -1:0] config_bus_in;

    input [REF_PIC_LIST_POC_DATA_WIDTH-1:0]     from_top_ref_pic_list_poc_data_in;
    input                                       from_top_ref_pic_list0_poc_wr_en  ;
    input                                       from_top_ref_pic_list1_poc_wr_en  ;
    input [NUM_REF_IDX_L0_MINUS1_WIDTH-1:0]     from_top_ref_pic_list0_poc_addr  ;
    input [NUM_REF_IDX_L0_MINUS1_WIDTH-1:0]     from_top_ref_pic_list1_poc_addr  ;
    input [DPB_FRAME_OFFSET_WIDTH -1:0]         from_top_ref_pic_list_idx_data_in;
    input                                       from_top_ref_pic_list_idx_wr_en;
    output                                              mv_done_out;
    output                                              pref_ctu_col_writ_ctu_done_out;

    
    
    output                                     mv_col_axi_awid    ;// = 0;
    output      [7:0]                          mv_col_axi_awlen   ;// = MV_COL_AXI_AX_LEN;
    output      [2:0]                          mv_col_axi_awsize  ;// = MV_COL_AXI_AX_SIZE;
    output      [1:0]                          mv_col_axi_awburst ;// = `AX_BURST_INC;
    output                        	            mv_col_axi_awlock  ;// = `AX_LOCK_DEFAULT;
    output      [3:0]                          mv_col_axi_awcache ;// = `AX_CACHE_DEFAULT;
    output      [2:0]                          mv_col_axi_awprot  ;// = `AX_PROT_DATA;
    output                                     mv_col_axi_awvalid;
    output      [31:0]                         mv_col_axi_awaddr;

    input                       	            mv_col_axi_awready;

    // write data channel
    output      [64-1:0]                       mv_col_axi_wstrb;// = 64'hFFFFF_FFFFF_FFFFF_FFFFF_FFFFF;       // 40 bytes
    output                                     mv_col_axi_wlast;
    output                                     mv_col_axi_wvalid;
    output      [MV_COL_AXI_DATA_WIDTH -1:0]   mv_col_axi_wdata;

    input	                                    mv_col_axi_wready;

    //write response channel
    // input                       	            mv_col_axi_bid;
    input       [1:0]                          mv_col_axi_bresp;
    input                       	            mv_col_axi_bvalid;
    output                                     mv_col_axi_bready;  

    output	    [AXI_ADDR_WDTH-1:0]		            mv_pref_axi_araddr  ;
    output wire [7:0]					            mv_pref_axi_arlen   ;
    output wire	[2:0]					            mv_pref_axi_arsize  ;
    output wire [1:0]					            mv_pref_axi_arburst ;
    output wire [2:0]					            mv_pref_axi_arprot  ;
    output 	   						                mv_pref_axi_arvalid ;
    input 								            mv_pref_axi_arready ;
            
    input		[MV_COL_AXI_DATA_WIDTH-1:0]		    mv_pref_axi_rdata   ;
    input		[1:0]					            mv_pref_axi_rresp   ;
    input 								            mv_pref_axi_rlast   ;
    input								            mv_pref_axi_rvalid  ;
    output 						                    mv_pref_axi_rready  ;

    output                        	                mv_pref_axi_arlock  ;
    output                                          mv_pref_axi_arid    ;    
    output      [3:0]                               mv_pref_axi_arcache ;      
    
    output                                          pred_pixl_gen_idle_out_to_pred_top;
    //assign  pred_pixl_gen_idle_out_to_pred_top= 1;

    output wire [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]                               xT_in_4x4_luma_out       ;
    output wire [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]                               yT_in_4x4_luma_out       ;
    output wire [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]                               xT_in_4x4_cr_out         ;
    output wire [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]                               yT_in_4x4_cr_out         ;
    output wire [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]                               yT_in_4x4_cb_out         ;
    output wire [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]                               xT_in_4x4_cb_out         ;
    output        [BLOCK_WIDTH_4x4 *  BLOCK_WIDTH_4x4 * PIXEL_WIDTH -1:0]              luma_wgt_pred_out        ;    
    output        [BLOCK_WIDTH_4x4 *  BLOCK_WIDTH_4x4 * PIXEL_WIDTH -1:0]              cb_wgt_pred_out          ;    
    output        [BLOCK_WIDTH_4x4 *  BLOCK_WIDTH_4x4 * PIXEL_WIDTH -1:0]              cr_wgt_pred_out          ;     
    output                                                                             luma_wght_pred_valid     ;
    output                                                                             cb_wght_pred_valid       ;
    output                                                                             cr_wght_pred_valid       ;

    // axi master interface         
    output      [AXI_ADDR_WDTH-1:0]                 ref_pix_axi_ar_addr     ;
    output      [7:0]                               ref_pix_axi_ar_len      ;
    output      [2:0]                               ref_pix_axi_ar_size     ;
    output      [1:0]                               ref_pix_axi_ar_burst    ;
    output      [2:0]                               ref_pix_axi_ar_prot     ;
    output                                          ref_pix_axi_ar_valid    ;
    input                                           ref_pix_axi_ar_ready    ;
                
    input       [AXI_DATA_WDTH-1:0]                 ref_pix_axi_r_data      ;
    input       [1:0]                               ref_pix_axi_r_resp      ;
    input                                           ref_pix_axi_r_last      ;
    input                                           ref_pix_axi_r_valid     ;
    output                                          ref_pix_axi_r_ready     ;
//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------
    /* STYLE_NOTES
     * Eacho wire and register must be defined on a seperate line
     * Although Verilog allows for implicit decleration of variables, all signals must be declared explicitly.
     * Group variables where necessary. Comment your variable by block and by signal.
     */
    integer                                         inter_state;                      // the internal inter_state of the module
    integer                                         inter_next_state;
                
                    
    reg         [PIC_DIM_WIDTH-1:0]                 pic_width;
    reg         [PIC_DIM_WIDTH-1:0]                 pic_height;
    reg         [LOG2_CTB_WIDTH - 1: 0]             log2_ctb_size;
    

    reg                                             weighted_bipred_flag;           // rarely used
    reg                                             weighted_pred_flag;             // rarely used
        

    reg         [SLICE_TYPE_WIDTH -1:0]                 slice_type;
    reg                                                 slice_temporal_mvp_enabled_flag;
    
    wire    [MV_FIELD_DATA_WIDTH -1:0]      top_mvs_to_bs_out;
    wire    [MV_FIELD_DATA_WIDTH -1:0]      left_mvs_to_bs_out;
    wire    [CTB_SIZE_WIDTH -1:0]           x_rel_top_out;
    wire    [CTB_SIZE_WIDTH -1:0]           y_rel_left_out;
    wire                                    bs_left_valid;
    wire                                    bs_top_valid;
    
    wire  [X11_ADDR_WDTH - 1:0]       xx_pb;
    wire  [CB_SIZE_WIDTH -1:0 ]       hh_pb;
    wire  [Y11_ADDR_WDTH - 1:0]       yy_pb;
    wire  [CB_SIZE_WIDTH -1:0 ]       ww_pb;  
    
    wire                                  current_mv_field_pred_flag_l0;
    wire                                  current_mv_field_pred_flag_l1;
    wire [REF_IDX_LX_WIDTH -1:0]          current_mv_field_ref_idx_l0;
    wire [REF_IDX_LX_WIDTH -1:0]          current_mv_field_ref_idx_l1;
    wire signed [MVD_WIDTH -1:0]          current_mv_field_mv_x_l0;
    wire signed [MVD_WIDTH -1:0]          current_mv_field_mv_y_l0;
    wire signed [MVD_WIDTH -1:0]          current_mv_field_mv_x_l1;
    wire signed [MVD_WIDTH -1:0]          current_mv_field_mv_y_l1;    
    wire [DPB_ADDR_WIDTH -1:0]            current_dpb_idx_l0;    
    wire [DPB_ADDR_WIDTH -1:0]            current_dpb_idx_l1;    
    wire                                  current_mv_field_valid; 
    
    wire                                    pred_pixl_gen_idle_out_to_mv_derive;// =1;
//---------------------------------------------------------------------------------------------------------------------
// Implmentation
//---------------------------------------------------------------------------------------------------------------------

 // Instantiate the module




// Instantiate the module
mv_derive_engine mv_derive_block (
    .clk(clk), 
    .reset(reset), 
    .enable(enable), 
    .config_mode_in(config_mode_in), 
    .config_bus_in(config_bus_in), 
    .from_top_ref_pic_list_poc_data_in(from_top_ref_pic_list_poc_data_in), 
    .from_top_ref_pic_list0_poc_wr_en(from_top_ref_pic_list0_poc_wr_en), 
    .from_top_ref_pic_list1_poc_wr_en(from_top_ref_pic_list1_poc_wr_en), 
    .from_top_ref_pic_list0_poc_addr(from_top_ref_pic_list0_poc_addr), 
    .from_top_ref_pic_list1_poc_addr(from_top_ref_pic_list1_poc_addr), 
    .from_top_ref_pic_list_idx_data_in(from_top_ref_pic_list_idx_data_in), 
    .from_top_ref_pic_list_idx_wr_en(from_top_ref_pic_list_idx_wr_en), 
    .mv_done_out(mv_done_out), 
    .pref_ctu_col_writ_ctu_done_out(pref_ctu_col_writ_ctu_done_out), 
    .mv_col_axi_awid(mv_col_axi_awid), 
    .mv_col_axi_awlen(mv_col_axi_awlen), 
    .mv_col_axi_awsize(mv_col_axi_awsize), 
    .mv_col_axi_awburst(mv_col_axi_awburst), 
    .mv_col_axi_awlock(mv_col_axi_awlock), 
    .mv_col_axi_awcache(mv_col_axi_awcache), 
    .mv_col_axi_awprot(mv_col_axi_awprot), 
    .mv_col_axi_awvalid(mv_col_axi_awvalid), 
    .mv_col_axi_awaddr(mv_col_axi_awaddr), 
    .mv_col_axi_awready(mv_col_axi_awready), 
    .mv_col_axi_wstrb(mv_col_axi_wstrb), 
    .mv_col_axi_wlast(mv_col_axi_wlast), 
    .mv_col_axi_wvalid(mv_col_axi_wvalid), 
    .mv_col_axi_wdata(mv_col_axi_wdata), 
    .mv_col_axi_wready(mv_col_axi_wready), 
    .mv_col_axi_bresp(mv_col_axi_bresp), 
    .mv_col_axi_bvalid(mv_col_axi_bvalid), 
    .mv_col_axi_bready(mv_col_axi_bready), 
    .top_mvs_to_bs_out(top_mvs_to_bs_out), 
    .left_mvs_to_bs_out(left_mvs_to_bs_out), 
    .x_rel_top_out(x_rel_top_out), 
    .y_rel_left_out(y_rel_left_out), 
    .bs_left_valid(bs_left_valid), 
    .bs_top_valid(bs_top_valid), 
    .mv_pref_axi_araddr(mv_pref_axi_araddr), 
    .mv_pref_axi_arlen(mv_pref_axi_arlen), 
    .mv_pref_axi_arsize(mv_pref_axi_arsize), 
    .mv_pref_axi_arburst(mv_pref_axi_arburst), 
    .mv_pref_axi_arprot(mv_pref_axi_arprot), 
    .mv_pref_axi_arvalid(mv_pref_axi_arvalid), 
    .mv_pref_axi_arready(mv_pref_axi_arready), 
    .mv_pref_axi_rdata(mv_pref_axi_rdata), 
    .mv_pref_axi_rresp(mv_pref_axi_rresp), 
    .mv_pref_axi_rlast(mv_pref_axi_rlast), 
    .mv_pref_axi_rvalid(mv_pref_axi_rvalid), 
    .mv_pref_axi_rready(mv_pref_axi_rready), 
    .mv_pref_axi_arlock(mv_pref_axi_arlock), 
    .mv_pref_axi_arid(mv_pref_axi_arid), 
    .mv_pref_axi_arcache(mv_pref_axi_arcache), 
    .current_mv_field_pred_flag_l0      (current_mv_field_pred_flag_l0), 
    .current_mv_field_pred_flag_l1      (current_mv_field_pred_flag_l1), 
    .current_mv_field_ref_idx_l0        (current_mv_field_ref_idx_l0), 
    .current_mv_field_ref_idx_l1        (current_mv_field_ref_idx_l1), 
    .current_mv_field_mv_x_l0           (current_mv_field_mv_x_l0), 
    .current_mv_field_mv_y_l0           (current_mv_field_mv_y_l0), 
    .current_mv_field_mv_x_l1           (current_mv_field_mv_x_l1), 
    .current_mv_field_mv_y_l1           (current_mv_field_mv_y_l1), 
    .current_dpb_idx_l0                 (current_dpb_idx_l0), 
    .current_dpb_idx_l1                 (current_dpb_idx_l1), 
    .current_mv_field_valid             (current_mv_field_valid), 
    .xx_pb                              (xx_pb), 
    .hh_pb                              (hh_pb), 
    .yy_pb                              (yy_pb), 
    .ww_pb                              (ww_pb),
    .pred_sample_gen_idle_in            (pred_pixl_gen_idle_out_to_mv_derive)
    );


inter_pred_sample_gen pred_sample_gen_block (
    .clk(clk), 
    .reset(reset), 
    .config_data_bus_in(config_bus_in), 
    .config_mode_in(config_mode_in), 
    .tu_empty(tu_empty),
    .tu_data_in(tu_data_in),
    .next_mv_field_pred_flag_l0_in(current_mv_field_pred_flag_l0), 
    .next_mv_field_pred_flag_l1_in(current_mv_field_pred_flag_l1), 
    .next_mv_field_mv_x_l0_in(current_mv_field_mv_x_l0), 
    .next_mv_field_mv_y_l0_in(current_mv_field_mv_y_l0), 
    .next_mv_field_mv_x_l1_in(current_mv_field_mv_x_l1), 
    .next_mv_field_mv_y_l1_in(current_mv_field_mv_y_l1), 
    .next_dpb_idx_l0_in(current_dpb_idx_l0), 
    .next_dpb_idx_l1_in(current_dpb_idx_l1), 
    .next_mv_field_valid_in(current_mv_field_valid), 
    
    .xT_in_min_luma_out (xT_in_4x4_luma_out), 
    .yT_in_min_luma_out (yT_in_4x4_luma_out), 
    .xT_in_min_cb_out   (xT_in_4x4_cb_out), 
    .yT_in_min_cb_out   (yT_in_4x4_cb_out), 
    .xT_in_min_cr_out   (xT_in_4x4_cr_out), 
    .yT_in_min_cr_out   (yT_in_4x4_cr_out), 
    .luma_wgt_pred_out(luma_wgt_pred_out), 
    .cb_wgt_pred_out(cb_wgt_pred_out), 
    .cr_wgt_pred_out(cr_wgt_pred_out), 
    .luma_wght_pred_valid (luma_wght_pred_valid),
    .cb_wght_pred_valid   (cb_wght_pred_valid  ),
    .cr_wght_pred_valid   (cr_wght_pred_valid  ),
        
    .pred_pixl_gen_idle_out_to_pred_top(pred_pixl_gen_idle_out_to_pred_top), 
    .pred_pixl_gen_idle_out_to_mv_derive(pred_pixl_gen_idle_out_to_mv_derive),

    .ref_pix_axi_ar_addr(ref_pix_axi_ar_addr), 
    .ref_pix_axi_ar_len(ref_pix_axi_ar_len), 
    .ref_pix_axi_ar_size(ref_pix_axi_ar_size), 
    .ref_pix_axi_ar_burst(ref_pix_axi_ar_burst), 
    .ref_pix_axi_ar_prot(ref_pix_axi_ar_prot), 
    .ref_pix_axi_ar_valid(ref_pix_axi_ar_valid), 
    .ref_pix_axi_ar_ready(ref_pix_axi_ar_ready), 
    .ref_pix_axi_r_data(ref_pix_axi_r_data), 
    .ref_pix_axi_r_resp(ref_pix_axi_r_resp), 
    .ref_pix_axi_r_last(ref_pix_axi_r_last), 
    .ref_pix_axi_r_valid(ref_pix_axi_r_valid), 
    .ref_pix_axi_r_ready(ref_pix_axi_r_ready) 
    
    );
    
  

always@(*) begin
    inter_next_state = inter_state;
    
    case(inter_state) 
        STATE_INTER_CONFIG_UPDATE: begin
            case(config_mode_in)
                `INTER_CTU0_HEADER: begin
                    if(pref_ctu_col_writ_ctu_done_out == 0) begin
                        inter_next_state = STATE_INTER_PREFETCH_COL_WRITE_WAIT;
                    end
                end
                `INTER_PU_HEADER: begin
                    if(mv_done_out == 0) begin
                        inter_next_state = STATE_INTER_MV_DERIVE_WAIT;
                    end
                end
            endcase
        end
        STATE_INTER_PREFETCH_COL_WRITE_WAIT: begin
            if(pref_ctu_col_writ_ctu_done_out == 1) begin
                inter_next_state = STATE_INTER_CONFIG_UPDATE;
            end
        end     
        STATE_INTER_MV_DERIVE_WAIT: begin
            if(mv_done_out == 1) begin
                inter_next_state = STATE_INTER_CONFIG_UPDATE;
            end            
        end
    endcase
end

always@(posedge clk) begin
    if(reset) begin
    end
    else if(enable) begin
        case(inter_state) 
            STATE_INTER_CONFIG_UPDATE: begin
                case(config_mode_in)
                    `INTER_TOP_PARA_0: begin
`ifdef READ_FILE    
                        pic_width <=   (config_bus_in[PIC_WIDTH_WIDTH + HEADER_WIDTH -1: HEADER_WIDTH])%(1<<PIC_DIM_WIDTH);
                        pic_height  <=   (config_bus_in[HEADER_WIDTH + PIC_WIDTH_WIDTH + PIC_HEIGHT_WIDTH- 1: HEADER_WIDTH + PIC_WIDTH_WIDTH])%(1<<PIC_DIM_WIDTH);
`else
                        pic_height <=   (config_bus_in[INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - 1: INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - PIC_WIDTH_WIDTH])%(1<<PIC_DIM_WIDTH);
                        pic_width  <=   (config_bus_in[INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - PIC_WIDTH_WIDTH - 1: INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - PIC_WIDTH_WIDTH - PIC_HEIGHT_WIDTH])%(1<<PIC_DIM_WIDTH);
`endif                    
                    end
                    `INTER_TOP_PARA_1: begin
`ifdef READ_FILE
                        log2_ctb_size <=  config_bus_in[ HEADER_WIDTH + LOG2CTBSIZEY_WIDTH -1 : HEADER_WIDTH];
`else                    
                        log2_ctb_size <=  config_bus_in[INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - 1: INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - LOG2CTBSIZEY_WIDTH];
`endif
                    end
                    `INTER_TOP_PARA_2: begin 
`ifdef READ_FILE
                        weighted_pred_flag          <= config_bus_in[   HEADER_WIDTH + NUM_SHORT_TERM_REF_WIDTH + WEIGHTED_PRED_WIDTH -1:
                                                                        HEADER_WIDTH + NUM_SHORT_TERM_REF_WIDTH ];
                        weighted_bipred_flag        <= config_bus_in[   HEADER_WIDTH + NUM_SHORT_TERM_REF_WIDTH + WEIGHTED_PRED_WIDTH + WEIGHTED_BIPRED_WIDTH -1:
                                                                        HEADER_WIDTH + NUM_SHORT_TERM_REF_WIDTH + WEIGHTED_PRED_WIDTH];
`else
                        weighted_pred_flag          <= config_bus_in[   INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - NUM_SHORT_TERM_REF_WIDTH  - 1:
                                                                        INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - NUM_SHORT_TERM_REF_WIDTH - WEIGHTED_PRED_WIDTH];
                        weighted_bipred_flag        <= config_bus_in[   INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - NUM_SHORT_TERM_REF_WIDTH - WEIGHTED_PRED_WIDTH - 1:
                                                                        INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - NUM_SHORT_TERM_REF_WIDTH - WEIGHTED_PRED_WIDTH - WEIGHTED_BIPRED_WIDTH];
`endif
                    end
                    `INTER_TOP_SLICE_1: begin
`ifdef READ_FILE
                        slice_temporal_mvp_enabled_flag <=   config_bus_in[ HEADER_WIDTH + SHORT_TERM_REF_PIC_SPS_WIDTH + SHORT_TERM_REF_PIC_IDX_WIDTH + TEMPORAL_MVP_ENABLED_WIDTH- 1:
                                                                            HEADER_WIDTH + SHORT_TERM_REF_PIC_SPS_WIDTH + SHORT_TERM_REF_PIC_IDX_WIDTH  ];     
                        slice_type              <=      config_bus_in[      HEADER_WIDTH + SHORT_TERM_REF_PIC_SPS_WIDTH + SHORT_TERM_REF_PIC_IDX_WIDTH + TEMPORAL_MVP_ENABLED_WIDTH + SAO_LUMA_WIDTH + SAO_CHROMA_WIDTH + NUM_REF_IDX_L0_MINUS1_WIDTH + NUM_REF_IDX_L1_MINUS1_WIDTH + MAX_MERGE_CAND_WIDTH + SLICE_TYPE_WIDTH- 1:
                                                                            HEADER_WIDTH + SHORT_TERM_REF_PIC_SPS_WIDTH + SHORT_TERM_REF_PIC_IDX_WIDTH + TEMPORAL_MVP_ENABLED_WIDTH + SAO_LUMA_WIDTH + SAO_CHROMA_WIDTH + NUM_REF_IDX_L0_MINUS1_WIDTH + NUM_REF_IDX_L1_MINUS1_WIDTH + MAX_MERGE_CAND_WIDTH  ];   
`else
                        slice_temporal_mvp_enabled_flag <=   config_bus_in[ INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - SHORT_TERM_REF_PIC_SPS_WIDTH - SHORT_TERM_REF_PIC_IDX_WIDTH - 1:
                                                                            INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - SHORT_TERM_REF_PIC_SPS_WIDTH - SHORT_TERM_REF_PIC_IDX_WIDTH - TEMPORAL_MVP_ENABLED_WIDTH];     
                        slice_type              <=      config_bus_in[  INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - SHORT_TERM_REF_PIC_SPS_WIDTH - SHORT_TERM_REF_PIC_IDX_WIDTH - TEMPORAL_MVP_ENABLED_WIDTH - SAO_LUMA_WIDTH - SAO_CHROMA_WIDTH - NUM_REF_IDX_L0_MINUS1_WIDTH - NUM_REF_IDX_L1_MINUS1_WIDTH - MAX_MERGE_CAND_WIDTH -1:
                                                                        INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - SHORT_TERM_REF_PIC_SPS_WIDTH - SHORT_TERM_REF_PIC_IDX_WIDTH - TEMPORAL_MVP_ENABLED_WIDTH - SAO_LUMA_WIDTH - SAO_CHROMA_WIDTH - NUM_REF_IDX_L0_MINUS1_WIDTH - NUM_REF_IDX_L1_MINUS1_WIDTH - MAX_MERGE_CAND_WIDTH - SLICE_TYPE_WIDTH];   
`endif
                    end
                    `INTER_TOP_SLICE_2: begin
                    end        
                    `INTER_TOP_CURR_POC: begin
                    end
                    `INTER_CURRENT_PIC_DPB_IDX: begin
                    end
                    `INTER_SLICE_TILE_INFO: begin
                    end
                    `INTER_CTU0_HEADER: begin
                    end
                    `INTER_CU_HEADER: begin
                    end
                    `INTER_PU_HEADER: begin
                    end
                endcase   
            end
        endcase
    end
end


always@(posedge clk) begin
    if(reset) begin
        inter_state <= STATE_INTER_CONFIG_UPDATE;
    end
    else begin
        inter_state <= inter_next_state;
    end
end    
    
endmodule    