`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   02:39:58 12/01/2013
// Design Name:   pred_top
// Module Name:   D:/090250V/SEMESTER 7/HEVC decoder/HDL codes/pred_top/sim/pred_top_tb.v
// Project Name:  prediction
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: pred_top
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module pred_top_tb;

    `include "pred_def.v"
    localparam                          STATE_READ_WAIT = 0;
    localparam                          STATE_ACTIVE = 1;
    localparam                          STATE_WRITE_WAIT = 2;
    localparam                          STATE_ERROR = 3;
    localparam                          STATE_CURRENT_POC = 4;
    localparam                          STATE_CUR_PIC_FIL_IDX = 5;
    localparam                          STATE_REF_PIC_LIST_5_UPDATE = 6;
    localparam                          STATE_REF_PIC_LIST_5_SCAN   = 7;
    localparam                          STATE_SEND_POC_TO_DBF = 8;
    localparam                          STATE_WRITE_WAIT_CUR_DPB_IDX_TO_DBF = 9;
    localparam                          STATE_REF_PIC_WRITE_WAIT = 10;
    localparam                          STATE_DPB_ADD_PREV_PIC = 11;
    localparam                          STATE_POC_READ_WAIT = 12;
    localparam                          STATE_MVD1_READ_WAIT = 13;
    localparam                          STATE_MVD2_READ_WAIT = 14;
    localparam                          STATE_MVD1_READ = 15;
    localparam                          STATE_MVD2_READ = 16;
    localparam                          STATE_MV_RETURN_WAIT1 = 17;
    localparam                          STATE_MV_RETURN_WAIT2 = 18;
    localparam                          STATE_MV_RETURN_WAIT3 = 19;
    localparam                          STATE_INTER_PREFETCH_COL_WRITE_WAIT = 20;
    localparam                          STATE_GET_ST_RPS_ENTRY = 21;
    localparam                          STATE_GET_ST_RPS_HEADER = 22;
    localparam                          STATE_SLICE_1_WRITE_WAIT = 23;
    localparam                          STATE_REF_PIC_LIST_TRANSFER = 24;
    localparam                          STATE_SET_ST_RPS_ENTRY_ADDR_FOR_REF_POC5 = 25;
    
    localparam                          WRITE_REF_PIC_STATE_IDLE = 0;
    localparam                          WRITE_REF_PIC_STATE_CHECK = 1;
    localparam                          WRITE_REF_PIC_STATE_POC_WRITE = 2;
    localparam                          WRITE_REF_PIC_STATE_IDX_WRITE = 3;
    localparam                          WRITE_REF_PIC_STATE_DONE = 4;
    localparam                          WRITE_REF_PIC_FIND_FOL_IDX = 5;
	// Inputs
	reg clk;
	reg reset;
	reg enable;
	reg [31:0] fifo_in;
	reg input_fifo_is_empty;
	reg output_fifo_is_full;
	reg [0:0] y_residual_fifo_in;
	reg y_residual_fifo_is_empty_in;
	reg [0:0] cb_residual_fifo_in;
	reg cb_residual_fifo_is_empty_in;
	reg [0:0] cr_residual_fifo_in;
	reg cr_residual_fifo_is_empty_in;
	reg y_dbf_fifo_is_full;
	reg cb_dbf_fifo_is_full;
	reg cr_dbf_fifo_is_full;

	reg mv_col_axi_awready;
	reg mv_col_axi_wready;
	reg [1:0] mv_col_axi_bresp;
	reg mv_col_axi_bvalid;
	reg mv_pref_axi_arready;
	reg [511:0] mv_pref_axi_rdata;
	reg [1:0] mv_pref_axi_rresp;
	reg mv_pref_axi_rlast;
	reg mv_pref_axi_rvalid;
    
    
	// Outputs
	wire read_en_out;
	wire [31:0] fifo_out;
	wire write_en_out;
	wire y_residual_fifo_read_en_out;
	wire cb_residual_fifo_read_en_out;
	wire cr_residual_fifo_read_en_out;

    wire mv_col_axi_awid;
	wire [7:0] mv_col_axi_awlen;
	wire [2:0] mv_col_axi_awsize;
	wire [1:0] mv_col_axi_awburst;
	wire mv_col_axi_awlock;
	wire [3:0] mv_col_axi_awcache;
	wire [2:0] mv_col_axi_awprot;
	wire mv_col_axi_awvalid;
	wire [31:0] mv_col_axi_awaddr;
	wire [63:0] mv_col_axi_wstrb;
	wire mv_col_axi_wlast;
	wire mv_col_axi_wvalid;
	wire [511:0] mv_col_axi_wdata;
	wire mv_col_axi_bready;
	wire [31:0] mv_pref_axi_araddr;
	wire [7:0] mv_pref_axi_arlen;
	wire [2:0] mv_pref_axi_arsize;
	wire [1:0] mv_pref_axi_arburst;
	wire [2:0] mv_pref_axi_arprot;
	wire mv_pref_axi_arvalid;
	wire mv_pref_axi_rready;
	wire mv_pref_axi_arlock;
	wire mv_pref_axi_arid;
	wire [3:0] mv_pref_axi_arcache;
    
    wire new_poc = uut.state == STATE_SEND_POC_TO_DBF;
	wire [0:0] temp_port;

    
    integer file_in,rfile;
    integer file_out;    
    integer return_value;
    reg [7:0] mem1,mem2, mem3,mem4;
    integer file_location;
    integer pic_num;
    
    wire new_ctu = uut.state == STATE_INTER_PREFETCH_COL_WRITE_WAIT;
    
    wire [MV_FIELD_DATA_WIDTH -1:0] cur_mv_field_wire = {
                            uut.inter_top_block.mv_derive_block.current_mv_field_pred_flag_l0   ,
                            uut.inter_top_block.mv_derive_block.current_mv_field_pred_flag_l1   ,
                            uut.inter_top_block.mv_derive_block.current_mv_field_ref_idx_l0     ,
                            uut.inter_top_block.mv_derive_block.current_mv_field_ref_idx_l1     ,
                            uut.inter_top_block.mv_derive_block.current_mv_field_mv_x_l0        ,
                            uut.inter_top_block.mv_derive_block.current_mv_field_mv_y_l0        ,
                            uut.inter_top_block.mv_derive_block.current_mv_field_mv_x_l1        ,
                            uut.inter_top_block.mv_derive_block.current_mv_field_mv_y_l1 };   
    
	// Instantiate the Unit Under Test (UUT)
	pred_top uut (
		.clk(clk), 
		.reset(reset), 
		.enable(enable), 
		.fifo_in(fifo_in), 
		.input_fifo_is_empty(input_fifo_is_empty), 
		.read_en_out(read_en_out), 
		.fifo_out(fifo_out), 
		.output_fifo_is_full(output_fifo_is_full), 
		.write_en_out(write_en_out), 
		.y_residual_fifo_in(y_residual_fifo_in), 
		.y_residual_fifo_is_empty_in(y_residual_fifo_is_empty_in), 
		.y_residual_fifo_read_en_out(y_residual_fifo_read_en_out), 
		.cb_residual_fifo_in(cb_residual_fifo_in), 
		.cb_residual_fifo_is_empty_in(cb_residual_fifo_is_empty_in), 
		.cb_residual_fifo_read_en_out(cb_residual_fifo_read_en_out), 
		.cr_residual_fifo_in(cr_residual_fifo_in), 
		.cr_residual_fifo_is_empty_in(cr_residual_fifo_is_empty_in), 
		.cr_residual_fifo_read_en_out(cr_residual_fifo_read_en_out), 
		.y_dbf_fifo_is_full(y_dbf_fifo_is_full), 
		.cb_dbf_fifo_is_full(cb_dbf_fifo_is_full), 
		.cr_dbf_fifo_is_full(cr_dbf_fifo_is_full), 
        .mv_col_axi_awid(mv_col_axi_awid), 
		.mv_col_axi_awlen(mv_col_axi_awlen), 
		.mv_col_axi_awsize(mv_col_axi_awsize), 
		.mv_col_axi_awburst(mv_col_axi_awburst), 
		.mv_col_axi_awlock(mv_col_axi_awlock), 
		.mv_col_axi_awcache(mv_col_axi_awcache), 
		.mv_col_axi_awprot(mv_col_axi_awprot), 
		.mv_col_axi_awvalid(mv_col_axi_awvalid), 
		.mv_col_axi_awaddr(mv_col_axi_awaddr), 
		.mv_col_axi_awready(mv_col_axi_awready), 
		.mv_col_axi_wstrb(mv_col_axi_wstrb), 
		.mv_col_axi_wlast(mv_col_axi_wlast), 
		.mv_col_axi_wvalid(mv_col_axi_wvalid), 
		.mv_col_axi_wdata(mv_col_axi_wdata), 
		.mv_col_axi_wready(mv_col_axi_wready), 
		.mv_col_axi_bresp(mv_col_axi_bresp), 
		.mv_col_axi_bvalid(mv_col_axi_bvalid), 
		.mv_col_axi_bready(mv_col_axi_bready), 
		.mv_pref_axi_araddr(mv_pref_axi_araddr), 
		.mv_pref_axi_arlen(mv_pref_axi_arlen), 
		.mv_pref_axi_arsize(mv_pref_axi_arsize), 
		.mv_pref_axi_arburst(mv_pref_axi_arburst), 
		.mv_pref_axi_arprot(mv_pref_axi_arprot), 
		.mv_pref_axi_arvalid(mv_pref_axi_arvalid), 
		.mv_pref_axi_arready(mv_pref_axi_arready), 
		.mv_pref_axi_rdata(mv_pref_axi_rdata), 
		.mv_pref_axi_rresp(mv_pref_axi_rresp), 
		.mv_pref_axi_rlast(mv_pref_axi_rlast), 
		.mv_pref_axi_rvalid(mv_pref_axi_rvalid), 
		.mv_pref_axi_rready(mv_pref_axi_rready), 
		.mv_pref_axi_arlock(mv_pref_axi_arlock), 
		.mv_pref_axi_arid(mv_pref_axi_arid), 
		.mv_pref_axi_arcache(mv_pref_axi_arcache), 
		.temp_port(temp_port)
	);

	initial begin
		// Initialize Inputs
        pic_num = -1;
        file_in = $fopen("res_to_inter_cast","rb");
        file_out = $fopen("inter_to_dbf","wb");  
        
        mv_col_axi_awready = 1; 
        mv_col_axi_wready  = 1;
        mv_col_axi_bresp   = 0;
        mv_col_axi_bvalid  = 1;
        mv_pref_axi_arready = 1;
        mv_pref_axi_rdata = 0;
        mv_pref_axi_rresp = 0;
        mv_pref_axi_rvalid = 1;
        mv_pref_axi_rlast = 0;
        
        file_location = -1;
		clk = 0;
		reset = 1;
		enable = 1;
		fifo_in = 0;
		input_fifo_is_empty = 1;
		output_fifo_is_full = 0;
		y_residual_fifo_in = 0;
		y_residual_fifo_is_empty_in = 0;
		cb_residual_fifo_in = 0;
		cb_residual_fifo_is_empty_in = 0;
		cr_residual_fifo_in = 0;
		cr_residual_fifo_is_empty_in = 0;
		y_dbf_fifo_is_full = 0;
		cb_dbf_fifo_is_full = 0;
		cr_dbf_fifo_is_full = 0;

		// Wait 100 ns for global reset to finish
		#100;
        @(posedge clk);
        reset = 0;
        input_fifo_is_empty = 0;
        
		// Add stimulus here

	end
always #10 clk = ~clk;


always @(posedge clk) begin
    if(reset) begin
    end
    else begin
        if(read_en_out) begin
            file_location = file_location + 1;
            mem1 = $fgetc( file_in); 
            mem2 = $fgetc( file_in); 
            mem3 = $fgetc( file_in); 
            mem4 = $fgetc( file_in); 
            $display("%d, header passed: %x",$time, mem1);
            fifo_in = {mem4,mem3,mem2,mem1};
            
            //$display("data passed: %x %x %x",mem4, mem3, mem2);
        end
    end
    
end
always@(posedge clk) begin
    if(uut.state == 3) begin
        $display("STATE_ERROR");
        $stop();
    end
    if(new_poc) begin
        pic_num = pic_num + 1;
        $display("%d",pic_num);
        //if(pic_num == 1)
        $stop();
    end
    
    
    
//    if(pic_num == 1 && uut.inter_top_block.mv_derive_block.xx_pb == 320 && uut.inter_top_block.mv_derive_block.yy_pb == 32)
//        $stop();
//    if(pic_num == 1 && uut.inter_top_block.mv_derive_block.current_mv_field_valid == 1) begin
//        $stop();
//    end
end


integer candidate_type_l0;
integer candidate_type_l1;

always@(*) begin
    if(uut.inter_top_block.mv_derive_block.current_mv_field_valid == 1) begin
        if(uut.inter_top_block.mv_derive_block.cb_pred_mode == `MODE_INTRA) begin
                candidate_type_l0 = "aINT";
                candidate_type_l1 = "aINT";
        end
        else begin
            if(uut.inter_top_block.mv_derive_block.merge_flag) begin
                case(uut.inter_top_block.mv_derive_block.merge_cand_list[uut.inter_top_block.mv_derive_block.merge_idx])
                    `MV_MERGE_CAND_A0: begin
                        candidate_type_l0 = "aMA0";         
                        candidate_type_l1 = "aMA0";         
                    end
                    `MV_MERGE_CAND_A1: begin
                        candidate_type_l0 = "aMA1"; 
                        candidate_type_l1 = "aMA1"; 
                    end
                    `MV_MERGE_CAND_B0: begin
                        candidate_type_l0 = "aMB0"; 
                        candidate_type_l1 = "aMB0"; 
                    end
                    `MV_MERGE_CAND_B1: begin
                        candidate_type_l0 = "aMB1"; 
                        candidate_type_l1 = "aMB1"; 
                    end
                    `MV_MERGE_CAND_B2: begin
                        candidate_type_l0 = "aMB2"; 
                        candidate_type_l1 = "aMB2"; 
                    end
                    `MV_MERGE_CAND_COL: begin
                        candidate_type_l0 = "aMCL"; 
                        candidate_type_l1 = "aMCL"; 
                    end
                    `MV_MERGE_CAND_BI: begin
                        candidate_type_l0 = "aMBI"; 
                        candidate_type_l1 = "aMBI"; 
                    end
                    `MV_MERGE_CAND_ZERO: begin
                        candidate_type_l0 = "aMZR";                         
                        candidate_type_l1 = "aMZR";                         
                    end
                endcase
            end
            else begin  // AMVP mode
                if(uut.inter_top_block.mv_derive_block.current_mv_field_pred_flag_l0) begin
                    case(uut.inter_top_block.mv_derive_block.mvpl0_cand_list[uut.inter_top_block.mv_derive_block.mvp_l0_flag])
                        `MV_AMVP_CAND_A: begin
                            candidate_type_l0 = "aPA";                         
                        end
                        `MV_AMVP_CAND_B: begin
                            candidate_type_l0 = "aPB";                                                   
                        end
                        `MV_AMVP_CAND_COL: begin
                            candidate_type_l0 = "aPC";                         
                        end
                        `MV_AMVP_CAND_ZERO: begin
                            candidate_type_l0 = "aPZR";                                       
                        end
                    endcase
                end
                else begin
                    candidate_type_l0 = "aPNA";          
                end

                if(uut.inter_top_block.mv_derive_block.current_mv_field_pred_flag_l1) begin
                    case(uut.inter_top_block.mv_derive_block.mvpl1_cand_list[uut.inter_top_block.mv_derive_block.mvp_l1_flag])
                        `MV_AMVP_CAND_A: begin
                            candidate_type_l1 = "aPA"; 
                        end                                              
                        `MV_AMVP_CAND_B: begin                           
                            candidate_type_l1 = "aPB"; 
                        end
                        `MV_AMVP_CAND_COL: begin
                            candidate_type_l1 = "aPC"; 
                        end                        
                        `MV_AMVP_CAND_ZERO: begin
                            candidate_type_l1 = "aPZR";                   
                        end
                    endcase
                end
                else begin
                    candidate_type_l1 = "aPNA";                 
                end   
            end
        end
    end
end


endmodule

