onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /pred_top_tb/clk
add wave -noupdate /pred_top_tb/file_location
add wave -noupdate /pred_top_tb/soft_file_loc
add wave -noupdate /pred_top_tb/fifo_in
add wave -noupdate /pred_top_tb/uut/mv_done
add wave -noupdate /pred_top_tb/new_poc
add wave -noupdate /pred_top_tb/uut/dpb_filled_flag
add wave -noupdate /pred_top_tb/soft_dpb_filled
add wave -noupdate -radix hexadecimal /pred_top_tb/uut/dpb_filled_flag_new
add wave -noupdate -radix decimal /pred_top_tb/uut/state
add wave -noupdate -radix decimal /pred_top_tb/uut/write_ref_pic_state
add wave -noupdate /pred_top_tb/uut/num_delta_poc
add wave -noupdate /pred_top_tb/uut/dpb_addr_read
add wave -noupdate /pred_top_tb/uut/ref_poc_list5_addr_read
add wave -noupdate /pred_top_tb/uut/write_en_out
add wave -noupdate /pred_top_tb/uut/read_en_out
add wave -noupdate -radix decimal /pred_top_tb/uut/temp_poc
add wave -noupdate /pred_top_tb/verify_dpb_poc/i
add wave -noupdate /pred_top_tb/verify_dpb_poc/soft_dpb_poc
add wave -noupdate /pred_top_tb/uut/num_pic_st_fol
add wave -noupdate /pred_top_tb/uut/num_pic_st_curr_bef
add wave -noupdate /pred_top_tb/uut/num_pic_st_curr_aft
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {212164691 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 219
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {202878779 ps} {218205911 ps}
