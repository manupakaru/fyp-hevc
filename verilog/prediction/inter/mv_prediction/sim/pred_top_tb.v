`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   02:39:58 12/01/2013
// Design Name:   pred_top
// Module Name:   D:/090250V/SEMESTER 7/HEVC decoder/HDL codes/pred_top/sim/pred_top_tb.v
// Project Name:  prediction
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: pred_top
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module pred_top_tb;

    `include "pred_def.v"
    localparam                          STATE_READ_WAIT = 0;
    localparam                          STATE_ACTIVE = 1;
    localparam                          STATE_WRITE_WAIT = 2;
    localparam                          STATE_ERROR = 3;
    localparam                          STATE_CURRENT_POC = 4;
    localparam                          STATE_CUR_PIC_FIL_IDX = 5;
    localparam                          STATE_REF_PIC_LIST_5_UPDATE = 6;
    localparam                          STATE_REF_PIC_LIST_5_SCAN   = 7;
    localparam                          STATE_SEND_POC_TO_DBF = 8;
    localparam                          STATE_WRITE_WAIT_CUR_DPB_IDX_TO_DBF = 9;
    localparam                          STATE_REF_PIC_WRITE_WAIT = 10;
    localparam                          STATE_DPB_ADD_PREV_PIC = 11;
    localparam                          STATE_POC_READ_WAIT = 12;
    localparam                          STATE_MVD1_READ_WAIT = 13;
    localparam                          STATE_MVD2_READ_WAIT = 14;
    localparam                          STATE_MVD1_READ = 15;
    localparam                          STATE_MVD2_READ = 16;
    localparam                          STATE_MV_RETURN_WAIT1 = 17;
    localparam                          STATE_MV_RETURN_WAIT2 = 18;
    localparam                          STATE_MV_RETURN_WAIT3 = 19;
    localparam                          STATE_INTER_PREFETCH_COL_WRITE_WAIT = 20;
    localparam                          STATE_GET_ST_RPS_ENTRY = 21;
    localparam                          STATE_GET_ST_RPS_HEADER = 22;
    localparam                          STATE_SLICE_1_WRITE_WAIT = 23;
    localparam                          STATE_REF_PIC_LIST_TRANSFER = 24;
    localparam                          STATE_SET_ST_RPS_ENTRY_ADDR_FOR_REF_POC5 = 25;
    
    localparam                          WRITE_REF_PIC_STATE_IDLE = 0;
    localparam                          WRITE_REF_PIC_STATE_CHECK = 1;
    localparam                          WRITE_REF_PIC_STATE_POC_WRITE = 2;
    localparam                          WRITE_REF_PIC_STATE_IDX_WRITE = 3;
    localparam                          WRITE_REF_PIC_STATE_DONE = 4;
    localparam                          WRITE_REF_PIC_FIND_FOL_IDX = 5;
    
    localparam                          STATE_MV_DERIVE_CONFIG_UPDATE = 0;
    localparam                          STATE_PREFETCH_COL_WRITE_WAIT = 1;
    localparam                          STATE_MV_DERIVE_PU_OVERWRIT_3 = 2;
    localparam                          STATE_MV_DERIVE_AVAILABLE_CHECK4 = 3;
    localparam                          STATE_MV_DERIVE_MV_AVAILABLE_MERGE_9 = 4;
    localparam                          STATE_MV_DERIVE_MV_AVAILABLE_MERGE_10 = 5;
    localparam                          STATE_MV_DERIVE_MV_AVAILABLE_MERGE_11 = 6;
    localparam                          STATE_MV_DERIVE_MV_AVAILABLE_MERGE_12 = 7;
    localparam                          STATE_MV_DERIVE_MV_AVAILABLE_MERGE_13 = 8;
    localparam                          STATE_MV_DERIVE_MV_PROCESS_MERGE_14     = 9;
    localparam                          STATE_MV_DERIVE_MV_PROCESS_MERGE_15     = 10;
    localparam                          STATE_MV_DERIVE_MV_PROCESS_MERGE_16     = 11;
    localparam                          STATE_MV_DERIVE_COMPARE_MVS       = 12;
    localparam                          STATE_MV_DERIVE_ZERO_MERGE            = 13;
    localparam                          STATE_MV_DERIVE_ADD_ZEROS_AMVP           = 14;
    localparam                          STATE_MV_DERIVE_MV_AVAILABLE_AMVP_9 = 16;
    localparam                          STATE_MV_DERIVE_MV_AVAILABLE_AMVP_10 = 17;
    localparam                          STATE_MV_DERIVE_MV_AVAILABLE_AMVP_11 = 18;
    localparam                          STATE_MV_DERIVE_MV_AVAILABLE_AMVP_12 = 19;
    localparam                          STATE_MV_DERIVE_MV_AVAILABLE_AMVP_13 = 20;
    localparam                          STATE_MV_DERIVE_MV_PROCESS_AMVP14   = 21;
    localparam                          STATE_MV_DERIVE_MV_PROCESS_AMVP15   = 22;
    localparam                          STATE_MV_DERIVE_MV_PROCESS_AMVP16   = 23;
    localparam                          STATE_MV_DERIVE_MV_PROCESS_AMVP17   = 24;
    localparam                          STATE_MV_DERIVE_MV_PROCESS_AMVP18   = 25;
    localparam                          STATE_MV_DERIVE_MV_PROCESS_AMVP19   = 26;
    localparam                          STATE_MV_DERIVE_MV_PROCESS_AMVP20   = 27;
    localparam                          STATE_MV_DERIVE_COL_MV_WAIT         = 28;
    localparam                          STATE_MV_DERIVE_MVD_ADD_AMVP       = 29;
    localparam                          STATE_MV_DERIVE_SET_DUMMY_INTRA_MV = 31;
    localparam                          STATE_MV_DERIVE_SET_PRE_INTRA_MV    = 32;    
    localparam                          STATE_MV_DERIVE_DONE    = 33;
    localparam                          STATE_MV_DERIVE_CTU_DONE    = 34;
    localparam                          STATE_MV_DERIVE_SET_INTER_MV    = 35;
    
    localparam                          STATE_MV_DERIVE_BI_PRED_CANDS1       = 36;
    localparam                          STATE_MV_DERIVE_BI_PRED_CANDS2       = 37;
    localparam                          STATE_MV_DERIVE_BI_PRED_CANDS3       = 38;
    localparam                          STATE_MV_DERIVE_BI_PRED_CANDS4       = 39;
    
	// Inputs
	reg clk;
	reg reset;
	reg enable;
	reg [31:0] fifo_in;
	reg input_fifo_is_empty;
	reg output_fifo_is_full;
	reg [0:0] y_residual_fifo_in;
	reg y_residual_fifo_is_empty_in;
	reg [0:0] cb_residual_fifo_in;
	reg cb_residual_fifo_is_empty_in;
	reg [0:0] cr_residual_fifo_in;
	reg cr_residual_fifo_is_empty_in;
	reg y_dbf_fifo_is_full;
	reg cb_dbf_fifo_is_full;
	reg cr_dbf_fifo_is_full;

	reg mv_col_axi_awready;
	reg mv_col_axi_wready;
	reg [1:0] mv_col_axi_bresp;
	reg mv_col_axi_bvalid;
	reg mv_pref_axi_arready;
	reg [511:0] mv_pref_axi_rdata;
	reg [1:0] mv_pref_axi_rresp;
	reg mv_pref_axi_rlast;
	reg mv_pref_axi_rvalid;
    
	// Outputs
	wire read_en_out;
	wire [31:0] fifo_out;
	wire write_en_out;
	wire y_residual_fifo_read_en_out;
	wire cb_residual_fifo_read_en_out;
	wire cr_residual_fifo_read_en_out;

    wire mv_col_axi_awid;
	wire [7:0] mv_col_axi_awlen;
	wire [2:0] mv_col_axi_awsize;
	wire [1:0] mv_col_axi_awburst;
	wire mv_col_axi_awlock;
	wire [3:0] mv_col_axi_awcache;
	wire [2:0] mv_col_axi_awprot;
	wire mv_col_axi_awvalid;
	wire [31:0] mv_col_axi_awaddr;
	wire [63:0] mv_col_axi_wstrb;
	wire mv_col_axi_wlast;
	wire mv_col_axi_wvalid;
	wire [511:0] mv_col_axi_wdata;
	wire mv_col_axi_bready;
	wire [31:0] mv_pref_axi_araddr;
	wire [7:0] mv_pref_axi_arlen;
	wire [2:0] mv_pref_axi_arsize;
	wire [1:0] mv_pref_axi_arburst;
	wire [2:0] mv_pref_axi_arprot;
	wire mv_pref_axi_arvalid;
	wire mv_pref_axi_rready;
	wire mv_pref_axi_arlock;
	wire mv_pref_axi_arid;
	wire [3:0] mv_pref_axi_arcache;
    
	wire [0:0] temp_port;

    reg [31:0] soft_file_loc;
    
    integer file_in,rfile;
    integer file_out;    
    integer return_value;
    reg [7:0] mem1,mem2, mem3,mem4;
    integer file_location;
    wire new_ctu = uut.state == STATE_INTER_PREFETCH_COL_WRITE_WAIT;
    
    reg [15:0] soft_dpb_filled;

    wire new_poc = uut.state == STATE_SEND_POC_TO_DBF;
    
    
    reg                                  test_mv_field_pred_flag_l0;
    reg                                  test_mv_field_pred_flag_l1;
    reg [REF_IDX_LX_WIDTH -1:0]          test_mv_field_ref_idx_l0;
    reg [REF_IDX_LX_WIDTH -1:0]          test_mv_field_ref_idx_l1;
    reg signed [MVD_WIDTH -1:0]          test_mv_field_mv_x_l0;
    reg signed [MVD_WIDTH -1:0]          test_mv_field_mv_y_l0;
    reg signed [MVD_WIDTH -1:0]          test_mv_field_mv_x_l1;
    reg signed [MVD_WIDTH -1:0]          test_mv_field_mv_y_l1;    
    
    inter_pred_iface inter_pred_iface_block();
	// Instantiate the Unit Under Test (UUT)
	pred_top uut (
		.clk(clk), 
		.reset(reset), 
		.enable(enable), 
		.fifo_in(fifo_in), 
		.input_fifo_is_empty(input_fifo_is_empty), 
		.read_en_out(read_en_out), 
		.fifo_out(fifo_out), 
		.output_fifo_is_full(output_fifo_is_full), 
		.write_en_out(write_en_out), 
		.y_residual_fifo_in(y_residual_fifo_in), 
		.y_residual_fifo_is_empty_in(y_residual_fifo_is_empty_in), 
		.y_residual_fifo_read_en_out(y_residual_fifo_read_en_out), 
		.cb_residual_fifo_in(cb_residual_fifo_in), 
		.cb_residual_fifo_is_empty_in(cb_residual_fifo_is_empty_in), 
		.cb_residual_fifo_read_en_out(cb_residual_fifo_read_en_out), 
		.cr_residual_fifo_in(cr_residual_fifo_in), 
		.cr_residual_fifo_is_empty_in(cr_residual_fifo_is_empty_in), 
		.cr_residual_fifo_read_en_out(cr_residual_fifo_read_en_out), 
		.y_dbf_fifo_is_full(y_dbf_fifo_is_full), 
		.cb_dbf_fifo_is_full(cb_dbf_fifo_is_full), 
		.cr_dbf_fifo_is_full(cr_dbf_fifo_is_full), 
        .mv_col_axi_awid(mv_col_axi_awid), 
		.mv_col_axi_awlen(mv_col_axi_awlen), 
		.mv_col_axi_awsize(mv_col_axi_awsize), 
		.mv_col_axi_awburst(mv_col_axi_awburst), 
		.mv_col_axi_awlock(mv_col_axi_awlock), 
		.mv_col_axi_awcache(mv_col_axi_awcache), 
		.mv_col_axi_awprot(mv_col_axi_awprot), 
		.mv_col_axi_awvalid(mv_col_axi_awvalid), 
		.mv_col_axi_awaddr(mv_col_axi_awaddr), 
		.mv_col_axi_awready(mv_col_axi_awready), 
		.mv_col_axi_wstrb(mv_col_axi_wstrb), 
		.mv_col_axi_wlast(mv_col_axi_wlast), 
		.mv_col_axi_wvalid(mv_col_axi_wvalid), 
		.mv_col_axi_wdata(mv_col_axi_wdata), 
		.mv_col_axi_wready(mv_col_axi_wready), 
		.mv_col_axi_bresp(mv_col_axi_bresp), 
		.mv_col_axi_bvalid(mv_col_axi_bvalid), 
		.mv_col_axi_bready(mv_col_axi_bready), 
		.mv_pref_axi_araddr(mv_pref_axi_araddr), 
		.mv_pref_axi_arlen(mv_pref_axi_arlen), 
		.mv_pref_axi_arsize(mv_pref_axi_arsize), 
		.mv_pref_axi_arburst(mv_pref_axi_arburst), 
		.mv_pref_axi_arprot(mv_pref_axi_arprot), 
		.mv_pref_axi_arvalid(mv_pref_axi_arvalid), 
		.mv_pref_axi_arready(mv_pref_axi_arready), 
		.mv_pref_axi_rdata(mv_pref_axi_rdata), 
		.mv_pref_axi_rresp(mv_pref_axi_rresp), 
		.mv_pref_axi_rlast(mv_pref_axi_rlast), 
		.mv_pref_axi_rvalid(mv_pref_axi_rvalid), 
		.mv_pref_axi_rready(mv_pref_axi_rready), 
		.mv_pref_axi_arlock(mv_pref_axi_arlock), 
		.mv_pref_axi_arid(mv_pref_axi_arid), 
		.mv_pref_axi_arcache(mv_pref_axi_arcache), 
		.temp_port(temp_port)
	);

	initial begin
		// Initialize Inputs
        
        soft_file_loc = 0;
        inter_pred_iface_block.sv_inter_pred_reset();
        inter_pred_iface_block.sv_inter_pred_step(soft_file_loc);
        file_in = $fopen("res_to_inter_cast","rb");
        file_out = $fopen("inter_to_dbf","wb");  
        
        mem1 = $fgetc( file_in); 
        mem2 = $fgetc( file_in); 
        mem3 = $fgetc( file_in); 
        mem4 = $fgetc( file_in); 
        fifo_in = {mem4,mem3,mem2,mem1};
        
        mv_col_axi_awready = 1; 
        mv_col_axi_wready  = 1;
        mv_col_axi_bresp   = 0;
        mv_col_axi_bvalid  = 1;
        mv_pref_axi_arready = 1;
        mv_pref_axi_rdata = 0;
        mv_pref_axi_rresp = 0;
        mv_pref_axi_rvalid = 1;
        mv_pref_axi_rlast = 0;
        
        file_location = -1;
		clk = 0;
		reset = 1;
		enable = 1;
		input_fifo_is_empty = 1;
		output_fifo_is_full = 0;
		y_residual_fifo_in = 0;
		y_residual_fifo_is_empty_in = 0;
		cb_residual_fifo_in = 0;
		cb_residual_fifo_is_empty_in = 0;
		cr_residual_fifo_in = 0;
		cr_residual_fifo_is_empty_in = 0;
		y_dbf_fifo_is_full = 0;
		cb_dbf_fifo_is_full = 0;
		cr_dbf_fifo_is_full = 0;

		// Wait 100 ns for global reset to finish
		#100;
        @(posedge clk);
        reset = 0;
        input_fifo_is_empty = 0;
        
		// Add stimulus here

	end
always #10 clk = ~clk;

// reg mv_status;
// reg mv_status_d;

// parameter PU_WAIT = 0;
// parameter MV_VALID_WAIT = 1;
// always@(posedge clk) begin
    // if(reset) begin
        // mv_status = PU_WAIT;
    // end
    // else begin
        // if(mv_status == PU_WAIT) begin
            // if(mem1 == `HEADER_PU_0) begin
                // mv_status = MV_VALID_WAIT;
            // end
        // end
        // if(mv_status == MV_VALID_WAIT) begin
            // if(uut.inter_top_block.mv_derive_block.mv_derive_state == STATE_MV_DERIVE_SET_DUMMY_INTRA_MV || uut.inter_top_block.mv_derive_block.mv_derive_state == STATE_MV_DERIVE_SET_INTER_MV) begin
                // mv_status = PU_WAIT;
            // end
        // end
    // end
// end

// always@(posedge clk) begin
    // mv_status_d <= mv_status;
// end

always @(posedge clk) begin
    if(reset) begin
    end
    else begin
        if(read_en_out) begin
            if(file_location  >= soft_file_loc) begin
                inter_pred_iface_block.sv_inter_pred_step(soft_file_loc);
            end
            file_location <= file_location + 1;
            mem1 = $fgetc( file_in); 
            mem2 = $fgetc( file_in); 
            mem3 = $fgetc( file_in); 
            mem4 = $fgetc( file_in); 
            $display("header passed: %x",mem1);
            fifo_in = {mem4,mem3,mem2,mem1};
            //$display("data passed: %x %x %x",mem4, mem3, mem2);

        end
        // else begin
            // if(mv_status_d == 1 && mv_status == 0) begin
                // inter_pred_iface_block.sv_inter_pred_step(soft_file_loc);
            // end
        // end
    end
    
end

always@(posedge clk) begin
    if(uut.state == 3) begin
        $display("STATE_ERROR");
        $stop();
    end
    if(new_poc) begin
        $display("new poc %d", uut.current__poc);
        $stop();
    end
    dpb_fill_verify();
    verify_dpb_poc();
    ref_pic_list_verify();
    current_mv_verify();
end


    
task verify_dpb_poc;
    integer i;
    reg [31:0] soft_dpb_poc;
    begin
        if(uut.write_ref_pic_state == WRITE_REF_PIC_STATE_DONE) begin
            for(i=0;i< 16; i=i+1) begin
                inter_pred_iface_block.sv_get_dpb_poc(i,soft_dpb_poc);
                if(uut.dpb_filled_flag_new[i] == 1) begin
                    if(soft_dpb_poc == uut.dpb_block.mem[i]) begin
                        $display("dpb_poc ok");
                    end
                    else begin
                        $display("dpb_poc no ok soft %d hard %d idx %d",soft_dpb_poc,uut.dpb_block.mem[i],i );
                        $stop();
                    end
                end
            end
        end
    end
endtask    
    
    
task dpb_fill_verify;
integer i;
begin
    for(i=0;i< 16; i=i+1) begin
        inter_pred_iface_block.sv_get_dpb_filled(i,soft_dpb_filled[i]);
    end
    if(uut.write_ref_pic_state == WRITE_REF_PIC_STATE_DONE) begin
        if(uut.dpb_filled_flag_new == soft_dpb_filled) begin
            $display("dpb ok");
        end
        else begin
            $display("dpb not ok");
            $stop();
        end
    end
end
endtask





task ref_pic_list_verify;
integer i;
integer ref_pic_list0_num_pic;
integer ref_pic_list1_num_pic;
integer ref_pic_list0_list;
integer ref_pic_list0_idx;
integer ref_pic_list1_list;
integer ref_pic_list1_idx;

begin
    if(uut.write_ref_pic_state == WRITE_REF_PIC_STATE_DONE) begin
        inter_pred_iface_block.sv_get_ref_pic_list_num_pic(1,110,ref_pic_list0_num_pic);
        inter_pred_iface_block.sv_get_ref_pic_list_num_pic(0,110,ref_pic_list1_num_pic);
        for(i=0;i< 16; i=i+1) begin
            if(i < ref_pic_list0_num_pic) begin
                inter_pred_iface_block.sv_get_ref_pic_list_poc(i,0,ref_pic_list0_list);
                inter_pred_iface_block.sv_get_ref_pic_list_idx(i,0,ref_pic_list0_idx);
                if(ref_pic_list0_list == uut.inter_top_block.mv_derive_block.ref_pic_list_0.mem[i] && ref_pic_list0_idx == uut.inter_top_block.mv_derive_block.ref_pic_list0_dpb_idx.mem[i]) begin
                    
                end
                else begin
                    $display("ref pic list 0 not ok idx=%d, soft poc=%d, hard poc= %d, soft idx=%d, hard_idx=%d", i,ref_pic_list0_list,uut.inter_top_block.mv_derive_block.ref_pic_list_0.mem[i],ref_pic_list0_idx,uut.inter_top_block.mv_derive_block.ref_pic_list0_dpb_idx.mem[i]);
                    $stop();
                end
            end
            if(i < ref_pic_list1_num_pic) begin
                inter_pred_iface_block.sv_get_ref_pic_list_poc(i,1,ref_pic_list1_list);
                inter_pred_iface_block.sv_get_ref_pic_list_idx(i,1,ref_pic_list1_idx);
                if(ref_pic_list1_list == uut.inter_top_block.mv_derive_block.ref_pic_list_1.mem[i] && ref_pic_list1_idx == uut.inter_top_block.mv_derive_block.ref_pic_list1_dpb_idx.mem[i]) begin
                    
                end
                else begin
                    $display("ref pic list 1 not ok idx=%d, soft poc=%d, hard poc= %d, soft idx=%d, hard_idx=%d", i,ref_pic_list1_list,uut.inter_top_block.mv_derive_block.ref_pic_list_1.mem[i],ref_pic_list1_idx,uut.inter_top_block.mv_derive_block.ref_pic_list1_dpb_idx.mem[i]);
                    $stop();
                end
            end
        end
    end
    
end
endtask

task current_mv_verify;
begin
    if(uut.inter_top_block.mv_derive_block.current_mv_field_valid == 1) begin
        if(uut.inter_top_block.mv_derive_block.cb_pred_mode == `MODE_INTER) begin
            if(test_mv_field_pred_flag_l0 == uut.inter_top_block.mv_derive_block.current_mv_field_pred_flag_l0) begin
                
            end
            else begin
                $display(" pred flag l0 soft = %d hard = %d @ xP = %d, yP = %d",test_mv_field_pred_flag_l0,uut.inter_top_block.mv_derive_block.current_mv_field_pred_flag_l0,uut.inter_top_block.mv_derive_block.xx_pb,uut.inter_top_block.mv_derive_block.yy_pb);
                $stop();
            end
            if(test_mv_field_pred_flag_l1 ==uut.inter_top_block.mv_derive_block.current_mv_field_pred_flag_l1) begin
            
            end
            else begin
                $display(" pred flag l1 soft = %d hard = %d @ xP = %d, yP = %d",test_mv_field_pred_flag_l1,uut.inter_top_block.mv_derive_block.current_mv_field_pred_flag_l1,uut.inter_top_block.mv_derive_block.xx_pb,uut.inter_top_block.mv_derive_block.yy_pb);
                $stop();
            end
            if(test_mv_field_pred_flag_l0) begin
                if(test_mv_field_ref_idx_l0 == uut.inter_top_block.mv_derive_block.current_mv_field_ref_idx_l0) begin
                
                end
                else begin
                    $display(" ref idx l0 not equal soft %d hard %d @ xPb = %d, yPb = %d",test_mv_field_ref_idx_l0,uut.inter_top_block.mv_derive_block.current_mv_field_ref_idx_l0,uut.inter_top_block.mv_derive_block.xx_pb,uut.inter_top_block.mv_derive_block.yy_pb);
                    $stop();
                end
                if(test_mv_field_mv_x_l0 == uut.inter_top_block.mv_derive_block.current_mv_field_mv_x_l0) begin
                
                end
                else begin
                    $display(" x l0 not equal soft %d hard %d @ xPb = %d, yPb = %d",test_mv_field_mv_x_l0,uut.inter_top_block.mv_derive_block.current_mv_field_mv_x_l0,uut.inter_top_block.mv_derive_block.xx_pb,uut.inter_top_block.mv_derive_block.yy_pb);
                    $stop();
                end
                if(test_mv_field_mv_y_l0 == uut.inter_top_block.mv_derive_block.current_mv_field_mv_y_l0) begin
                
                end
                else begin
                    $display(" y l0 not equal soft %d hard %d @ xPb = %d, yPb = %d",test_mv_field_mv_y_l0,uut.inter_top_block.mv_derive_block.current_mv_field_mv_y_l0,uut.inter_top_block.mv_derive_block.xx_pb,uut.inter_top_block.mv_derive_block.yy_pb);
                    $stop();
                end
            end
            if(test_mv_field_pred_flag_l1) begin
                if(test_mv_field_ref_idx_l1 == uut.inter_top_block.mv_derive_block.current_mv_field_ref_idx_l1) begin
                
                end
                else begin
                    $display(" x l1 not equal soft %d hard %d @ xPb = %d, yPb = %d",test_mv_field_ref_idx_l1,uut.inter_top_block.mv_derive_block.current_mv_field_ref_idx_l1,uut.inter_top_block.mv_derive_block.xx_pb,uut.inter_top_block.mv_derive_block.yy_pb);
                    $stop();
                end
                if(test_mv_field_mv_x_l1 == uut.inter_top_block.mv_derive_block.current_mv_field_mv_x_l1) begin
                
                end
                else begin
                    $display(" x l1 not equal soft %d hard %d @ xPb = %d, yPb = %d",test_mv_field_mv_x_l1,uut.inter_top_block.mv_derive_block.current_mv_field_mv_x_l1,uut.inter_top_block.mv_derive_block.xx_pb,uut.inter_top_block.mv_derive_block.yy_pb);
                    $stop();
                end
                if(test_mv_field_mv_y_l1 == uut.inter_top_block.mv_derive_block.current_mv_field_mv_y_l1) begin
                
                end
                else begin
                    $display(" y l1 not equal soft %d hard %d @ xPb = %d, yPb = %d",test_mv_field_mv_y_l1,uut.inter_top_block.mv_derive_block.current_mv_field_mv_y_l1,uut.inter_top_block.mv_derive_block.xx_pb,uut.inter_top_block.mv_derive_block.yy_pb);
                    $stop();
                end
            end
        end
        if(uut.inter_top_block.mv_derive_block.cb_pred_mode == `MODE_INTRA) begin 
            if(uut.inter_top_block.mv_derive_block.current_mv_field_pred_flag_l0 == 0 && uut.inter_top_block.mv_derive_block.current_mv_field_pred_flag_l1 == 0) begin
                
            end
            else begin
                $display(" intra pu not equal @ xPb = %d, yPb = %d",uut.inter_top_block.mv_derive_block.xx_pb,uut.inter_top_block.mv_derive_block.yy_pb);
                    $stop();
            end
        end
    end
    inter_pred_iface_block.sv_get_current_mv(0,0,test_mv_field_pred_flag_l0);
    inter_pred_iface_block.sv_get_current_mv(1,0,test_mv_field_pred_flag_l1);
    inter_pred_iface_block.sv_get_current_mv(0,1,test_mv_field_ref_idx_l0);
    inter_pred_iface_block.sv_get_current_mv(1,1,test_mv_field_ref_idx_l1);
    inter_pred_iface_block.sv_get_current_mv(0,2,test_mv_field_mv_x_l0);
    inter_pred_iface_block.sv_get_current_mv(1,2,test_mv_field_mv_x_l1);
    inter_pred_iface_block.sv_get_current_mv(0,3,test_mv_field_mv_y_l0);
    inter_pred_iface_block.sv_get_current_mv(1,3,test_mv_field_mv_y_l1);
end
endtask





endmodule

