if exist work (
	rmdir /S /Q work 2> nul
)

set SV_file=inter_pred_iface.svi 



echo vlib work
vlib work

vlog -L work -sv -dpiheader dpiheader.h %SV_file%

vlog -L work ../rtl/*.v


vlog -L work pred_top_tb.v

vsim -c -novopt -t 1ps +notimingchecks -sv_lib pred_inter_intra_software -lib work work.pred_top_tb -do sim.do -fsmdebug

echo Done !