`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    12:02:57 11/18/2013 
// Design Name: 
// Module Name:    intra_prediction_wrapper 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module intra_prediction_wrapper
    (
        clk,
        reset,
        enable,
        
        config_data_bus_in,
        config_data_valid_in,
        residual_fifo_is_empty_in,
        
        //writeback y_linebuffer interface
        y_top_portw_addr_in,
        y_top_portw_data0_in,
        y_top_portw_data1_in,
        y_top_portw_data2_in,
        y_top_portw_data3_in,
        y_top_portw_data4_in,
        y_top_portw_data5_in,
        y_top_portw_data6_in,
        y_top_portw_data7_in,
        y_top_portw_en_mask_in,
        
        y_left_portw_addr_in,
        y_left_portw_data0_in,
        y_left_portw_data1_in,
        y_left_portw_data2_in,
        y_left_portw_data3_in,
        y_left_portw_data4_in,
        y_left_portw_data5_in,
        y_left_portw_data6_in,
        y_left_portw_data7_in,
        y_left_portw_en_mask_in,

        y_predsample_4by4_out,
        y_predsample_4by4_valid_out,
        y_predsample_4by4_x_out,
        y_predsample_4by4_y_out,
        y_predsample_4by4_last_row_out,
        y_predsample_4by4_last_col_out,
        y_res_present_out,
        y_residual_fifo_is_empty_in,

        y_cu_done_out,

        //writeback cb_linebuffer interface
        cb_top_portw_addr_in,
        cb_top_portw_data0_in,
        cb_top_portw_data1_in,
        cb_top_portw_data2_in,
        cb_top_portw_data3_in,
        cb_top_portw_data4_in,
        cb_top_portw_data5_in,
        cb_top_portw_data6_in,
        cb_top_portw_data7_in,
        cb_top_portw_en_mask_in,
        
        cb_left_portw_addr_in,
        cb_left_portw_data0_in,
        cb_left_portw_data1_in,
        cb_left_portw_data2_in,
        cb_left_portw_data3_in,
        cb_left_portw_data4_in,
        cb_left_portw_data5_in,
        cb_left_portw_data6_in,
        cb_left_portw_data7_in,
        cb_left_portw_en_mask_in,

        cb_predsample_4by4_out,
        cb_predsample_4by4_valid_out,
        cb_predsample_4by4_x_out,
        cb_predsample_4by4_y_out,
        cb_predsample_4by4_last_row_out,
        cb_predsample_4by4_last_col_out,
        cb_res_present_out,
        cb_residual_fifo_is_empty_in,

        cb_cu_done_out,

        //writeback cr_linebuffer interface
        cr_top_portw_addr_in,
        cr_top_portw_data0_in,
        cr_top_portw_data1_in,
        cr_top_portw_data2_in,
        cr_top_portw_data3_in,
        cr_top_portw_data4_in,
        cr_top_portw_data5_in,
        cr_top_portw_data6_in,
        cr_top_portw_data7_in,
        cr_top_portw_en_mask_in,
        
        cr_left_portw_addr_in,
        cr_left_portw_data0_in,
        cr_left_portw_data1_in,
        cr_left_portw_data2_in,
        cr_left_portw_data3_in,
        cr_left_portw_data4_in,
        cr_left_portw_data5_in,
        cr_left_portw_data6_in,
        cr_left_portw_data7_in,
        cr_left_portw_en_mask_in,
        
        cr_predsample_4by4_out,
        cr_predsample_4by4_valid_out,
        cr_predsample_4by4_x_out,
        cr_predsample_4by4_y_out,
        cr_predsample_4by4_last_row_out,
        cr_predsample_4by4_last_col_out,
        cr_res_present_out,
        cr_residual_fifo_is_empty_in,

        cr_cu_done_out
    );
    
//----------------------------------------------------
//LOCALPARAMS
//----------------------------------------------------

    localparam  CIDX_WIDTH                  = 2;
    localparam  MAX_LOG2CTBSIZE_WIDTH       = 3;
    localparam  LOG2_FRAME_SIZE             = 12; 
    localparam  INTRA_MODE_WIDTH            = 6;
    localparam  MAX_NTBS_SIZE               = 6;
    localparam  PIXEL_ADDR_LENGTH           = 12;
    
    localparam  CONFIG_DATA_BUS_WIDTH       = 32;
    localparam  PIXEL_WIDTH                 = 8;
    localparam  OUTPUT_BLOCK_SIZE           = 4;
    localparam  HEADER_WIDTH                = 8;
    
    // localparam  STATE_CONFIG                = 0;
    // localparam  STATE_WAIT_PREDICTION       = 1;
    
    //config data headers
    localparam  [HEADER_WIDTH - 1:0]    HEADER_FRAME_SIZE   =   8'h00;
    localparam  [HEADER_WIDTH - 1:0]    HEADER_LOG2CTBSIZE  =   8'h01;
    localparam  [HEADER_WIDTH - 1:0]    HEADER_SLICE_CORD   =   8'h15;
    localparam  [HEADER_WIDTH - 1:0]    HEADER_TILE_CORD    =   8'h16;
    localparam  [HEADER_WIDTH - 1:0]    HEADER_TILE_WIDTH   =   8'h17;
    localparam  [HEADER_WIDTH - 1:0]    HEADER_CTU_CORD     =   8'h20;
    localparam  [HEADER_WIDTH - 1:0]    HEADER_RES_AND_INTRA=   8'h40;
    
    
    //output state machines
    localparam  Y_STATE_WAIT                = 0;
    localparam  Y_STATE_11_4by4             = 1;
    localparam  Y_STATE_12_4by4             = 2;
    localparam  Y_STATE_21_4by4             = 3;
    localparam  Y_STATE_22_4by4             = 4;
    
    localparam  CB_STATE_WAIT                = 0;
    localparam  CB_STATE_11_4by4             = 1;
    localparam  CB_STATE_12_4by4             = 2;
    localparam  CB_STATE_21_4by4             = 3;
    localparam  CB_STATE_22_4by4             = 4;
    
    localparam  CR_STATE_WAIT                = 0;
    localparam  CR_STATE_11_4by4             = 1;
    localparam  CR_STATE_12_4by4             = 2;
    localparam  CR_STATE_21_4by4             = 3;
    localparam  CR_STATE_22_4by4             = 4;

//----------------------------------------------------
// IO Signals
//----------------------------------------------------

    input                                                               clk;
    input                                                               reset;
    input                                                               enable;
    
    input       [CONFIG_DATA_BUS_WIDTH - 1:0]                           config_data_bus_in;
    input                                                               config_data_valid_in;
    input                                                               residual_fifo_is_empty_in;
    
// y interface
    //topLine buffer write interface
    input       [LOG2_FRAME_SIZE - 1:0]                                 y_top_portw_addr_in;
    input       [PIXEL_WIDTH - 1 :0]                                    y_top_portw_data0_in;
    input       [PIXEL_WIDTH - 1 :0]                                    y_top_portw_data1_in;
    input       [PIXEL_WIDTH - 1 :0]                                    y_top_portw_data2_in;
    input       [PIXEL_WIDTH - 1 :0]                                    y_top_portw_data3_in;
    input       [PIXEL_WIDTH - 1 :0]                                    y_top_portw_data4_in;
    input       [PIXEL_WIDTH - 1 :0]                                    y_top_portw_data5_in;
    input       [PIXEL_WIDTH - 1 :0]                                    y_top_portw_data6_in;
    input       [PIXEL_WIDTH - 1 :0]                                    y_top_portw_data7_in;
    input       [PIXEL_WIDTH - 1 :0]                                    y_top_portw_en_mask_in;
    
    //left line buffer read interface
    input       [LOG2_FRAME_SIZE - 1:0]                                 y_left_portw_addr_in;
    input       [PIXEL_WIDTH - 1 :0]                                    y_left_portw_data0_in;
    input       [PIXEL_WIDTH - 1 :0]                                    y_left_portw_data1_in;
    input       [PIXEL_WIDTH - 1 :0]                                    y_left_portw_data2_in;
    input       [PIXEL_WIDTH - 1 :0]                                    y_left_portw_data3_in;
    input       [PIXEL_WIDTH - 1 :0]                                    y_left_portw_data4_in;
    input       [PIXEL_WIDTH - 1 :0]                                    y_left_portw_data5_in;
    input       [PIXEL_WIDTH - 1 :0]                                    y_left_portw_data6_in;
    input       [PIXEL_WIDTH - 1 :0]                                    y_left_portw_data7_in;
    input       [PIXEL_WIDTH - 1 :0]                                    y_left_portw_en_mask_in;
    
    output      [PIXEL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE - 1:0] y_predsample_4by4_out;
    output                                                              y_predsample_4by4_valid_out;
    output reg  [PIXEL_ADDR_LENGTH - 1:0]                               y_predsample_4by4_x_out;
    output reg  [PIXEL_ADDR_LENGTH - 1:0]                               y_predsample_4by4_y_out;
    output reg                                                          y_predsample_4by4_last_row_out;
    output reg                                                          y_predsample_4by4_last_col_out;
    output reg                                                          y_res_present_out;
    input                                                               y_residual_fifo_is_empty_in;

// cb interface
    //topLine buffer write interface
    input       [LOG2_FRAME_SIZE - 1:0]                                 cb_top_portw_addr_in;
    input       [PIXEL_WIDTH - 1 :0]                                    cb_top_portw_data0_in;
    input       [PIXEL_WIDTH - 1 :0]                                    cb_top_portw_data1_in;
    input       [PIXEL_WIDTH - 1 :0]                                    cb_top_portw_data2_in;
    input       [PIXEL_WIDTH - 1 :0]                                    cb_top_portw_data3_in;
    input       [PIXEL_WIDTH - 1 :0]                                    cb_top_portw_data4_in;
    input       [PIXEL_WIDTH - 1 :0]                                    cb_top_portw_data5_in;
    input       [PIXEL_WIDTH - 1 :0]                                    cb_top_portw_data6_in;
    input       [PIXEL_WIDTH - 1 :0]                                    cb_top_portw_data7_in;
    input       [PIXEL_WIDTH - 1 :0]                                    cb_top_portw_en_mask_in;
    
    //left line buffer read interface
    input       [LOG2_FRAME_SIZE - 1:0]                                 cb_left_portw_addr_in;
    input       [PIXEL_WIDTH - 1 :0]                                    cb_left_portw_data0_in;
    input       [PIXEL_WIDTH - 1 :0]                                    cb_left_portw_data1_in;
    input       [PIXEL_WIDTH - 1 :0]                                    cb_left_portw_data2_in;
    input       [PIXEL_WIDTH - 1 :0]                                    cb_left_portw_data3_in;
    input       [PIXEL_WIDTH - 1 :0]                                    cb_left_portw_data4_in;
    input       [PIXEL_WIDTH - 1 :0]                                    cb_left_portw_data5_in;
    input       [PIXEL_WIDTH - 1 :0]                                    cb_left_portw_data6_in;
    input       [PIXEL_WIDTH - 1 :0]                                    cb_left_portw_data7_in;
    input       [PIXEL_WIDTH - 1 :0]                                    cb_left_portw_en_mask_in;

    output      [PIXEL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE - 1:0] cb_predsample_4by4_out;
    output                                                              cb_predsample_4by4_valid_out;
    output reg  [PIXEL_ADDR_LENGTH - 1:0]                               cb_predsample_4by4_x_out;
    output reg  [PIXEL_ADDR_LENGTH - 1:0]                               cb_predsample_4by4_y_out;
    output reg                                                          cb_predsample_4by4_last_row_out;
    output reg                                                          cb_predsample_4by4_last_col_out;
    output reg                                                          cb_res_present_out;
    input                                                               cb_residual_fifo_is_empty_in;

// cr interface
    //topLine buffer write interface
    input       [LOG2_FRAME_SIZE - 1:0]                                 cr_top_portw_addr_in;
    input       [PIXEL_WIDTH - 1 :0]                                    cr_top_portw_data0_in;
    input       [PIXEL_WIDTH - 1 :0]                                    cr_top_portw_data1_in;
    input       [PIXEL_WIDTH - 1 :0]                                    cr_top_portw_data2_in;
    input       [PIXEL_WIDTH - 1 :0]                                    cr_top_portw_data3_in;
    input       [PIXEL_WIDTH - 1 :0]                                    cr_top_portw_data4_in;
    input       [PIXEL_WIDTH - 1 :0]                                    cr_top_portw_data5_in;
    input       [PIXEL_WIDTH - 1 :0]                                    cr_top_portw_data6_in;
    input       [PIXEL_WIDTH - 1 :0]                                    cr_top_portw_data7_in;
    input       [PIXEL_WIDTH - 1 :0]                                    cr_top_portw_en_mask_in;
    
    //left line buffer read interface
    input       [LOG2_FRAME_SIZE - 1:0]                                 cr_left_portw_addr_in;
    input       [PIXEL_WIDTH - 1 :0]                                    cr_left_portw_data0_in;
    input       [PIXEL_WIDTH - 1 :0]                                    cr_left_portw_data1_in;
    input       [PIXEL_WIDTH - 1 :0]                                    cr_left_portw_data2_in;
    input       [PIXEL_WIDTH - 1 :0]                                    cr_left_portw_data3_in;
    input       [PIXEL_WIDTH - 1 :0]                                    cr_left_portw_data4_in;
    input       [PIXEL_WIDTH - 1 :0]                                    cr_left_portw_data5_in;
    input       [PIXEL_WIDTH - 1 :0]                                    cr_left_portw_data6_in;
    input       [PIXEL_WIDTH - 1 :0]                                    cr_left_portw_data7_in;
    input       [PIXEL_WIDTH - 1 :0]                                    cr_left_portw_en_mask_in;

    output      [PIXEL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE - 1:0] cr_predsample_4by4_out;
    output                                                              cr_predsample_4by4_valid_out;
    output reg  [PIXEL_ADDR_LENGTH - 1:0]                               cr_predsample_4by4_x_out;
    output reg  [PIXEL_ADDR_LENGTH - 1:0]                               cr_predsample_4by4_y_out;
    output reg                                                          cr_predsample_4by4_last_row_out;
    output reg                                                          cr_predsample_4by4_last_col_out;
    output reg                                                          cr_res_present_out;
    input                                                               cr_residual_fifo_is_empty_in;
    
    output                                                              y_cu_done_out;
    output                                                              cb_cu_done_out;
    output                                                              cr_cu_done_out;
    
    
//----------------------------------------------------
// Internal Regs and Wires
//----------------------------------------------------
    
    reg         [PIXEL_ADDR_LENGTH - 1:0]                               xt_reg;
    reg         [PIXEL_ADDR_LENGTH - 1:0]                               yt_reg;
    reg         [MAX_NTBS_SIZE - 1    :0]                               ntbs_reg;

    reg         [INTRA_MODE_WIDTH - 1 :0]                               y_intramode_reg;
    reg         [INTRA_MODE_WIDTH - 1 :0]                               cb_intramode_reg;
    reg         [INTRA_MODE_WIDTH - 1 :0]                               cr_intramode_reg;


    reg         [CIDX_WIDTH - 1       :0]                               cidx_reg;
    
    reg         [LOG2_FRAME_SIZE - 1:0]                                 frame_height_reg;
    reg         [LOG2_FRAME_SIZE - 1:0]                                 frame_width_reg;
    reg         [LOG2_FRAME_SIZE - 1:0]                                 tile_width_reg;
    reg         [LOG2_FRAME_SIZE - 1:0]                                 tile_xc_reg;
    reg         [LOG2_FRAME_SIZE - 1:0]                                 tile_yc_reg;
    reg         [LOG2_FRAME_SIZE - 1:0]                                 slice_x_reg;
    reg         [LOG2_FRAME_SIZE - 1:0]                                 slice_y_reg;
    reg                                                                 strong_intra_smoothing_flag_reg;
    
    reg         [MAX_LOG2CTBSIZE_WIDTH - 1:0]                           log2ctbsize_reg;
    reg                                                                 log2ctbsize_config_reg;
    
    reg                                                                 y_valid;
    reg                                                                 cb_valid;
    reg                                                                 cr_valid;
    
    //Top FSMs
    // integer                                                             state;
    reg         [MAX_NTBS_SIZE - 1:0]                                   y_xby8_counter;
    reg         [MAX_NTBS_SIZE - 1:0]                                   y_yby8_counter;
    integer                                                             y_state;

    reg         [MAX_NTBS_SIZE - 1:0]                                   cb_xby8_counter;
    reg         [MAX_NTBS_SIZE - 1:0]                                   cb_yby8_counter;
    integer                                                             cb_state;

    reg         [MAX_NTBS_SIZE - 1:0]                                   cr_xby8_counter;
    reg         [MAX_NTBS_SIZE - 1:0]                                   cr_yby8_counter;
    integer                                                             cr_state;

//----------------------------------------------------
// Implementation
//----------------------------------------------------


    always @(posedge clk) begin
        if(reset) begin
            log2ctbsize_config_reg <= 1'b0;
            y_valid  <= 1'b0;
            cb_valid <= 1'b0;
            cr_valid <= 1'b0;
        end
        else if(enable) begin
            if(config_data_valid_in == 1'b1) begin
                case(config_data_bus_in[CONFIG_DATA_BUS_WIDTH - 1:CONFIG_DATA_BUS_WIDTH - HEADER_WIDTH])
                    HEADER_FRAME_SIZE : begin
                        y_valid  <= 1'b0;
                        cb_valid <= 1'b0;
                        cr_valid <= 1'b0;

                        frame_width_reg     <= config_data_bus_in[12 + 12 - 1:12];
                        frame_height_reg    <= config_data_bus_in[12 - 1:0];
                    end
                    HEADER_LOG2CTBSIZE : begin
                        y_valid  <= 1'b0;
                        cb_valid <= 1'b0;
                        cr_valid <= 1'b0;

                        log2ctbsize_reg                 <= config_data_bus_in[CONFIG_DATA_BUS_WIDTH - HEADER_WIDTH - 1 : CONFIG_DATA_BUS_WIDTH - HEADER_WIDTH - 3];
                        strong_intra_smoothing_flag_reg <= config_data_bus_in[2];
                        log2ctbsize_config_reg          <= 1'b1;
                    end
                    HEADER_SLICE_CORD : begin
                        y_valid  <= 1'b0;
                        cb_valid <= 1'b0;
                        cr_valid <= 1'b0;

                        slice_x_reg     <= config_data_bus_in[12 + 12 - 1:12];
                        slice_y_reg     <= config_data_bus_in[12 - 1:0];
                    end
                    HEADER_TILE_CORD : begin
                        y_valid  <= 1'b0;
                        cb_valid <= 1'b0;
                        cr_valid <= 1'b0;

                        tile_xc_reg     <= config_data_bus_in[12 + 12 - 1:12];
                        tile_yc_reg     <= config_data_bus_in[12 - 1:0];
                    end
                    HEADER_TILE_WIDTH : begin
                        y_valid  <= 1'b0;
                        cb_valid <= 1'b0;
                        cr_valid <= 1'b0;

                        tile_width_reg <= config_data_bus_in[CONFIG_DATA_BUS_WIDTH - HEADER_WIDTH - 1 : CONFIG_DATA_BUS_WIDTH - HEADER_WIDTH - 12];
                    end                    
                    HEADER_CTU_CORD : begin
                        y_valid  <= 1'b0;
                        cb_valid <= 1'b0;
                        cr_valid <= 1'b0;

                        xt_reg     <= config_data_bus_in[12 + 12 - 1:12];
                        yt_reg     <= config_data_bus_in[12 - 1:0]; 
                    end                    
                    HEADER_RES_AND_INTRA : begin
                        xt_reg[5:1]   <= config_data_bus_in[CONFIG_DATA_BUS_WIDTH - HEADER_WIDTH - 1 : CONFIG_DATA_BUS_WIDTH - HEADER_WIDTH - 5];
                        yt_reg[5:1]   <= config_data_bus_in[CONFIG_DATA_BUS_WIDTH - HEADER_WIDTH - 5 - 1 : CONFIG_DATA_BUS_WIDTH - HEADER_WIDTH - 10];
                        cidx_reg      <= config_data_bus_in[CONFIG_DATA_BUS_WIDTH - HEADER_WIDTH - 10 - 1 : CONFIG_DATA_BUS_WIDTH - HEADER_WIDTH - 12];
                        ntbs_reg      <= 1'b1 << (config_data_bus_in[CONFIG_DATA_BUS_WIDTH - HEADER_WIDTH - 12  - 1 : CONFIG_DATA_BUS_WIDTH - HEADER_WIDTH - 15]);  
                       // intramode_reg <= config_data_bus_in[CONFIG_DATA_BUS_WIDTH - HEADER_WIDTH - 15 - 1 : CONFIG_DATA_BUS_WIDTH - HEADER_WIDTH - 21];
                        
                        
                        
                        case (config_data_bus_in[CONFIG_DATA_BUS_WIDTH - HEADER_WIDTH - 10 - 1 : CONFIG_DATA_BUS_WIDTH - HEADER_WIDTH - 12])
                            2'd0 : begin
                                y_valid  <= 1'b1;
                                cb_valid <= 1'b0;
                                cr_valid <= 1'b0;
                                y_intramode_reg <= config_data_bus_in[CONFIG_DATA_BUS_WIDTH - HEADER_WIDTH - 15 - 1 : CONFIG_DATA_BUS_WIDTH - HEADER_WIDTH - 21];
                                // y_predsample_4by4_x_out  <= xt_reg;
                                // y_predsample_4by4_y_out  <= yt_reg;
                                y_res_present_out        <= config_data_bus_in[2];
                              //  y_state  <= Y_STATE_PREDICTION;    
                            end
                            2'd1 : begin
                                y_valid  <= 1'b0;
                                cb_valid <= 1'b1;
                                cr_valid <= 1'b0;
                                cb_intramode_reg <= config_data_bus_in[CONFIG_DATA_BUS_WIDTH - HEADER_WIDTH - 15 - 1 : CONFIG_DATA_BUS_WIDTH - HEADER_WIDTH - 21];
                                // cb_predsample_4by4_x_out  <= xt_reg;
                                // cb_predsample_4by4_y_out  <= yt_reg;
                                cb_res_present_out        <= config_data_bus_in[2];
                              //  cb_state <= CB_STATE_PREDICTION;    
                            end 
                            2'd2 : begin
                                y_valid  <= 1'b0;
                                cb_valid <= 1'b0;
                                cr_valid <= 1'b1;
                                cr_intramode_reg <= config_data_bus_in[CONFIG_DATA_BUS_WIDTH - HEADER_WIDTH - 15 - 1 : CONFIG_DATA_BUS_WIDTH - HEADER_WIDTH - 21];
                                // cr_predsample_4by4_x_out  <= xt_reg;
                                // cr_predsample_4by4_y_out  <= yt_reg;
                                cr_res_present_out        <= config_data_bus_in[2];
                              //  cr_state <= CR_STATE_PREDICTION;    
                            end         
                        endcase
                    end
                    default : begin
                        y_valid  <= 1'b0;
                        cb_valid <= 1'b0;
                        cr_valid <= 1'b0;
                    end
                endcase
            end
            else begin
                y_valid  <= 1'b0;
                cb_valid <= 1'b0;
                cr_valid <= 1'b0;  
            end
        end
                // STATE_WAIT_PREDICTION : begin
                //     y_valid  <= 1'b0;
                //     cb_valid <= 1'b0;
                //     cr_valid <= 1'b0;
                    
                //     if( y_predsample_4by4_valid_int | cb_predsample_4by4_int | cr_predsample_4by4_int ) begin
                //         if(block_4by4_col_counter == ntbs_reg[MAX_NTBS_SIZE - 1:2] - 1'b1) begin
                //             block_4by4_row_counter <= block_4by4_row_counter + 1'b1;
                //             block_4by4_col_counter <= 6'd0;
                //             predsample_4by4_x_out   <= xt_reg;
                //             predsample_4by4_y_out   <= predsample_4by4_y_out + 3'd4;
                //         end
                //         else begin
                //             predsample_4by4_x_out   <= predsample_4by4_x_out + 3'd4;
                //             block_4by4_col_counter  <= block_4by4_col_counter + 1'b1;
                //         end
                //     end
                    
                    
                //     if( y_cu_done_out | cb_cu_done_out | cr_cu_done_out ) begin    
                //         state <= STATE_CONFIG;
                //     end
                // end
        //     endcase
        // end
    end

    always @(posedge clk) begin :y_state_FSM     //TODO adjust FSMs to 8x8 Z scan order 
        if (reset) begin
            y_state <=  Y_STATE_WAIT; 
        end
        else if(enable) begin
            case(y_state)
                Y_STATE_WAIT : begin
                    if(y_valid) begin

                        y_predsample_4by4_x_out  <= xt_reg;
                        y_predsample_4by4_y_out  <= yt_reg;
                        y_xby8_counter           <= {MAX_NTBS_SIZE{1'b0}};
                        y_yby8_counter           <= {MAX_NTBS_SIZE{1'b0}};


                        if(({MAX_NTBS_SIZE{1'b0}} == ntbs_reg[MAX_NTBS_SIZE - 1:3]) &&({MAX_NTBS_SIZE{1'b0}} == ntbs_reg[MAX_NTBS_SIZE - 1:3])) begin
                            y_predsample_4by4_last_row_out  <= 1'b1;
                            y_predsample_4by4_last_col_out  <= 1'b1; 
                        end
                        else begin
                            y_state                         <= Y_STATE_11_4by4;
                            y_predsample_4by4_last_row_out  <= 1'b0;
                            y_predsample_4by4_last_col_out  <= 1'b0;
                        end
                    end
                end
                // Y_STATE_PREDICTION : begin

                //     if( y_cu_done_out) begin    
                //         state <= Y_STATE_WAIT;
                //     end

                //     if(y_predsample_4by4_valid_int & (~residual_fifo_is_empty_in)) begin
                //         if(y_xby4_counter == ntbs_reg[MAX_NTBS_SIZE - 1:2] - 1'b1) && (y_yby4_counter[1] == 1'b1) begin
                //             // y_yby4_counter            <= block_4by4_row_counter + 1'b1;
                //             // y_xby4_counter            <= 6'd0;
                //             // y_predsample_4by4_x_out   <= xt_reg;
                //             // y_predsample_4by4_y_out   <= predsample_4by4_y_out + 3'd4;

                //         end
                //         else if(y_xby4_counter == ntbs_reg[MAX_NTBS_SIZE - 1:2]) && (y_yby4_counter[1] == 1'b1) begin
                            
                //         end
                //         else if ((y_xby4_counter[1] == 1'b1) && (y_yby4_counter[1] == 1'b1) begin
                //             y_predsample_4by4_x_out   <= y_predsample_4by4_x_out + 3'd4;
                //             y_predsample_4by4_y_out   <= y_predsample_4by4_y_out - 3'd4;
                //             y_yby4_counter            <= y_yby4_counter - 1'b1;
                //             y_xby4_counter            <= y_xby4_counter - 1'b1;  
                //         end
                //         else if (y_xby4_counter[1] == 1'b1) begin
                //             y_predsample_4by4_x_out   <= y_predsample_4by4_x_out - 3'd4;
                //             y_predsample_4by4_y_out   <= y_predsample_4by4_y_out + 3'd4;
                //             y_xby4_counter            <= y_xby4_counter - 1'b1;
                //             y_yby4_counter            <= y_yby4_counter + 1'b1;
                //         end
                //         else begin
                //             y_predsample_4by4_x_out   <= predsample_4by4_x_out + 3'd4;
                //             y_xby4_counter            <= y_xby4_counter + 1'b1;
                //         end
                //     end
                // end
                Y_STATE_11_4by4 : begin
                    if(y_predsample_4by4_valid_out & (~residual_fifo_is_empty_in)) begin
                        y_state <= Y_STATE_12_4by4;

                        y_predsample_4by4_x_out <= y_predsample_4by4_x_out + 3'd4;
                        y_predsample_4by4_y_out <= y_predsample_4by4_y_out;
                    end

                    y_predsample_4by4_last_row_out <= 1'b0;
                    y_predsample_4by4_last_col_out <= 1'b0;
                end
                Y_STATE_12_4by4 : begin
                    if(y_predsample_4by4_valid_out & (~residual_fifo_is_empty_in)) begin
                        y_state <= Y_STATE_21_4by4;

                        y_predsample_4by4_x_out <= y_predsample_4by4_x_out - 3'd4;
                        y_predsample_4by4_y_out <= y_predsample_4by4_y_out + 3'd4;
                    end

                    if(y_xby8_counter == ntbs_reg[MAX_NTBS_SIZE - 1:3] - 1'b1) begin
                        y_predsample_4by4_last_col_out <= 1'b1;
                    end
                    else begin
                        y_predsample_4by4_last_col_out <= 1'b0;
                    end

                    y_predsample_4by4_last_row_out <= 1'b0;
                end
                Y_STATE_21_4by4 : begin
                    if(y_predsample_4by4_valid_out & (~residual_fifo_is_empty_in)) begin
                        y_state <= Y_STATE_22_4by4;

                        y_predsample_4by4_x_out <= y_predsample_4by4_x_out + 3'd4;
                        y_predsample_4by4_y_out <= y_predsample_4by4_y_out;
                    end

                    if(y_yby8_counter == ntbs_reg[MAX_NTBS_SIZE - 1:3] - 1'b1) begin
                        y_predsample_4by4_last_row_out <= 1'b1;
                    end
                    else begin
                        y_predsample_4by4_last_row_out <= 1'b0;
                    end

                    y_predsample_4by4_last_col_out <= 1'b0;
                end
                Y_STATE_22_4by4 : begin
                    if(y_predsample_4by4_valid_out & (~residual_fifo_is_empty_in)) begin
                        if((y_xby8_counter == ntbs_reg[MAX_NTBS_SIZE - 1:3] - 1'b1) &&(y_yby8_counter == ntbs_reg[MAX_NTBS_SIZE - 1:3] - 1'b1)) begin
                            y_state <= Y_STATE_WAIT;
                            y_predsample_4by4_last_col_out <= 1'b1;
                        end
                        else if(y_xby8_counter == ntbs_reg[MAX_NTBS_SIZE - 1:3] - 1'b1) begin
                            y_xby8_counter <= {MAX_NTBS_SIZE{1'b0}};
                            y_yby8_counter <= y_yby8_counter + 1'b1;
                            y_state <= Y_STATE_11_4by4;
                            y_predsample_4by4_last_col_out <= 1'b1;

                            y_predsample_4by4_x_out <= xt_reg;
                            y_predsample_4by4_y_out <= y_predsample_4by4_y_out + 3'd4;
                        end
                        else begin
                            y_state <= Y_STATE_11_4by4;
                            y_predsample_4by4_last_col_out <= 1'b0;
                            y_xby8_counter  <= y_xby8_counter + 1'b1;

                            y_predsample_4by4_x_out <= y_predsample_4by4_x_out + 3'd4;
                            y_predsample_4by4_y_out <= y_predsample_4by4_y_out - 3'd4;
                        end

                        if(y_yby8_counter == ntbs_reg[MAX_NTBS_SIZE - 1:3] - 1'b1) begin
                            y_predsample_4by4_last_row_out <= 1'b1;
                        end
                        else begin
                            y_predsample_4by4_last_row_out <= 1'b0;
                        end
                    end
                end
            endcase
        end
    end

    always @(posedge clk) begin :cb_state_FSM     //TODO adjust FSMs to 8x8 Z scan order 
        if (reset) begin
            cb_state <=  CB_STATE_WAIT; 
        end
        else if (enable) begin
            case(cb_state)
                CB_STATE_WAIT : begin
                    if(cb_valid) begin

                        cb_predsample_4by4_x_out  <= xt_reg >> 1;
                        cb_predsample_4by4_y_out  <= yt_reg >> 1;
                        cb_xby8_counter           <= {MAX_NTBS_SIZE{1'b0}};
                        cb_yby8_counter           <= {MAX_NTBS_SIZE{1'b0}};

                        // if(({MAX_NTBS_SIZE{1'b0}} == ntbs_reg[MAX_NTBS_SIZE - 1:3]) &&({MAX_NTBS_SIZE{1'b0}} == ntbs_reg[MAX_NTBS_SIZE - 1:3])) begin
                        //     cb_predsample_4by4_last_row_out <= 1'b1;
                        //     cb_predsample_4by4_last_col_out <= 1'b1;
                        // end
                        // else begin
                            cb_state                  <= CB_STATE_22_4by4;
                            cb_predsample_4by4_last_row_out <= 1'b0;
                            cb_predsample_4by4_last_col_out <= 1'b0;
                        // end
                    end
                end
                // Y_STATE_PREDICTION : begin

                //     if( y_cu_done_out) begin    
                //         state <= Y_STATE_WAIT;
                //     end

                //     if(y_predsample_4by4_valid_int & (~residual_fifo_is_empty_in)) begin
                //         if(y_xby4_counter == ntbs_reg[MAX_NTBS_SIZE - 1:2] - 1'b1) && (y_yby4_counter[1] == 1'b1) begin
                //             // y_yby4_counter            <= block_4by4_row_counter + 1'b1;
                //             // y_xby4_counter            <= 6'd0;
                //             // y_predsample_4by4_x_out   <= xt_reg;
                //             // y_predsample_4by4_y_out   <= predsample_4by4_y_out + 3'd4;

                //         end
                //         else if(y_xby4_counter == ntbs_reg[MAX_NTBS_SIZE - 1:2]) && (y_yby4_counter[1] == 1'b1) begin
                            
                //         end
                //         else if ((y_xby4_counter[1] == 1'b1) && (y_yby4_counter[1] == 1'b1) begin
                //             y_predsample_4by4_x_out   <= y_predsample_4by4_x_out + 3'd4;
                //             y_predsample_4by4_y_out   <= y_predsample_4by4_y_out - 3'd4;
                //             y_yby4_counter            <= y_yby4_counter - 1'b1;
                //             y_xby4_counter            <= y_xby4_counter - 1'b1;  
                //         end
                //         else if (y_xby4_counter[1] == 1'b1) begin
                //             y_predsample_4by4_x_out   <= y_predsample_4by4_x_out - 3'd4;
                //             y_predsample_4by4_y_out   <= y_predsample_4by4_y_out + 3'd4;
                //             y_xby4_counter            <= y_xby4_counter - 1'b1;
                //             y_yby4_counter            <= y_yby4_counter + 1'b1;
                //         end
                //         else begin
                //             y_predsample_4by4_x_out   <= predsample_4by4_x_out + 3'd4;
                //             y_xby4_counter            <= y_xby4_counter + 1'b1;
                //         end
                //     end
                // end
                // CB_STATE_11_4by4 : begin
                //     if(cb_predsample_4by4_valid_out & (~residual_fifo_is_empty_in)) begin
                //         cb_state <= CB_STATE_12_4by4;
                //     end

                //     cb_predsample_4by4_last_row_out <= 1'b0;
                //     cb_predsample_4by4_last_col_out <= 1'b0;
                // end
                // CB_STATE_12_4by4 : begin
                //     if(cb_predsample_4by4_valid_out & (~residual_fifo_is_empty_in)) begin
                //         cb_state <= CB_STATE_21_4by4;
                //     end

                //     if(cb_xby8_counter == ntbs_reg[MAX_NTBS_SIZE - 1:4] - 1'b1) begin
                //         cb_predsample_4by4_last_col_out <= 1'b1;
                //     end
                //     else begin
                //         cb_predsample_4by4_last_col_out <= 1'b0;
                //     end

                //     cb_predsample_4by4_last_row_out <= 1'b0;
                // end
                // CB_STATE_21_4by4 : begin
                //     if(cb_predsample_4by4_valid_out & (~residual_fifo_is_empty_in)) begin
                //         cb_state <= CB_STATE_22_4by4;
                //     end

                //     if(cb_yby8_counter == ntbs_reg[MAX_NTBS_SIZE - 1:4] - 1'b1) begin
                //         cb_predsample_4by4_last_row_out <= 1'b1;
                //     end
                //     else begin
                //         cb_predsample_4by4_last_row_out <= 1'b0;
                //     end

                //     cb_predsample_4by4_last_col_out <= 1'b0;
                // end
                CB_STATE_22_4by4 : begin
                    if(cb_predsample_4by4_valid_out & (~residual_fifo_is_empty_in)) begin
                        if((cb_xby8_counter == ntbs_reg[MAX_NTBS_SIZE - 1:3] - 1'b1) &&(cb_yby8_counter == ntbs_reg[MAX_NTBS_SIZE - 1:3] - 1'b1)) begin
                            cb_state <= CB_STATE_WAIT;
                            cb_predsample_4by4_last_col_out <= 1'b1;
                        end
                        else if(cb_xby8_counter == ntbs_reg[MAX_NTBS_SIZE - 1:3] - 1'b1) begin
                            cb_xby8_counter <= {MAX_NTBS_SIZE{1'b0}};
                            cb_yby8_counter <= cb_yby8_counter + 1'b1;
                            // cb_state <= CB_STATE_11_4by4;
                            cb_predsample_4by4_last_col_out <= 1'b1;

                            cb_predsample_4by4_x_out <= xt_reg >> 1;
                            cb_predsample_4by4_y_out <= cb_predsample_4by4_y_out + 3'd4;
                        end
                        else begin
                            // cb_state <= CB_STATE_11_4by4;
                            cb_predsample_4by4_last_col_out <= 1'b0;
                            cb_xby8_counter  <= cb_xby8_counter + 1'b1;

                            cb_predsample_4by4_x_out <= cb_predsample_4by4_x_out + 3'd4;
                            cb_predsample_4by4_y_out <= cb_predsample_4by4_y_out;
                        end

                        if(cb_yby8_counter == ntbs_reg[MAX_NTBS_SIZE - 1:3] - 1'b1) begin
                            cb_predsample_4by4_last_row_out <= 1'b1;
                        end
                        else begin
                            cb_predsample_4by4_last_row_out <= 1'b0;
                        end
                    end
                end
            endcase
        end
    end

    always @(posedge clk) begin :cr_state_FSM     //TODO adjust FSMs to 8x8 Z scan order 
        if (reset) begin
            cr_state <=  CR_STATE_WAIT; 
        end
        else if(enable) begin
            case(cr_state)
                CR_STATE_WAIT : begin
                    if(cr_valid) begin

                        cr_predsample_4by4_x_out  <= xt_reg >> 1;
                        cr_predsample_4by4_y_out  <= yt_reg >> 1;
                        cr_xby8_counter           <= {MAX_NTBS_SIZE{1'b0}};
                        cr_yby8_counter           <= {MAX_NTBS_SIZE{1'b0}};

                        // if(({MAX_NTBS_SIZE{1'b0}} == ntbs_reg[MAX_NTBS_SIZE - 1:3]) &&({MAX_NTBS_SIZE{1'b0}} == ntbs_reg[MAX_NTBS_SIZE - 1:3])) begin
                        //     cr_predsample_4by4_last_row_out <= 1'b1;
                        //     cr_predsample_4by4_last_col_out <= 1'b1;
                        // end
                        // else begin
                            cr_state                        <= CR_STATE_22_4by4;
                            cr_predsample_4by4_last_row_out <= 1'b0;
                            cr_predsample_4by4_last_col_out <= 1'b0;
                        // end
                    end
                end
                // Y_STATE_PREDICTION : begin

                //     if( y_cu_done_out) begin    
                //         state <= Y_STATE_WAIT;
                //     end

                //     if(y_predsample_4by4_valid_int & (~residual_fifo_is_empty_in)) begin
                //         if(y_xby4_counter == ntbs_reg[MAX_NTBS_SIZE - 1:2] - 1'b1) && (y_yby4_counter[1] == 1'b1) begin
                //             // y_yby4_counter            <= block_4by4_row_counter + 1'b1;
                //             // y_xby4_counter            <= 6'd0;
                //             // y_predsample_4by4_x_out   <= xt_reg;
                //             // y_predsample_4by4_y_out   <= predsample_4by4_y_out + 3'd4;

                //         end
                //         else if(y_xby4_counter == ntbs_reg[MAX_NTBS_SIZE - 1:2]) && (y_yby4_counter[1] == 1'b1) begin
                            
                //         end
                //         else if ((y_xby4_counter[1] == 1'b1) && (y_yby4_counter[1] == 1'b1) begin
                //             y_predsample_4by4_x_out   <= y_predsample_4by4_x_out + 3'd4;
                //             y_predsample_4by4_y_out   <= y_predsample_4by4_y_out - 3'd4;
                //             y_yby4_counter            <= y_yby4_counter - 1'b1;
                //             y_xby4_counter            <= y_xby4_counter - 1'b1;  
                //         end
                //         else if (y_xby4_counter[1] == 1'b1) begin
                //             y_predsample_4by4_x_out   <= y_predsample_4by4_x_out - 3'd4;
                //             y_predsample_4by4_y_out   <= y_predsample_4by4_y_out + 3'd4;
                //             y_xby4_counter            <= y_xby4_counter - 1'b1;
                //             y_yby4_counter            <= y_yby4_counter + 1'b1;
                //         end
                //         else begin
                //             y_predsample_4by4_x_out   <= predsample_4by4_x_out + 3'd4;
                //             y_xby4_counter            <= y_xby4_counter + 1'b1;
                //         end
                //     end
                // end
                // CR_STATE_11_4by4 : begin
                //     if(cr_predsample_4by4_valid_out & (~residual_fifo_is_empty_in)) begin
                //         cr_state <= CR_STATE_12_4by4;
                //     end

                //     cr_predsample_4by4_last_row_out <= 1'b0;
                //     cr_predsample_4by4_last_col_out <= 1'b0;
                // end
                // CR_STATE_12_4by4 : begin
                //     if(cr_predsample_4by4_valid_out & (~residual_fifo_is_empty_in)) begin
                //         cr_state <= CR_STATE_21_4by4;
                //     end

                //     if(cr_xby8_counter == ntbs_reg[MAX_NTBS_SIZE - 1:4] - 1'b1) begin
                //         cr_predsample_4by4_last_col_out <= 1'b1;
                //     end
                //     else begin
                //         cr_predsample_4by4_last_col_out <= 1'b0;
                //     end

                //     cr_predsample_4by4_last_row_out <= 1'b0;
                // end
                // CR_STATE_21_4by4 : begin
                //     if(cr_predsample_4by4_valid_out & (~residual_fifo_is_empty_in)) begin
                //         cr_state <= CR_STATE_22_4by4;
                //     end

                //     if(cr_yby8_counter == ntbs_reg[MAX_NTBS_SIZE - 1:4] - 1'b1) begin
                //         cr_predsample_4by4_last_row_out <= 1'b1;
                //     end
                //     else begin
                //         cr_predsample_4by4_last_row_out <= 1'b0;
                //     end

                //     cr_predsample_4by4_last_col_out <= 1'b0;
                // end
                CR_STATE_22_4by4 : begin
                    if(cr_predsample_4by4_valid_out & (~residual_fifo_is_empty_in)) begin
                        if((cr_xby8_counter == ntbs_reg[MAX_NTBS_SIZE - 1:3] - 1'b1) &&(cr_yby8_counter == ntbs_reg[MAX_NTBS_SIZE - 1:3] - 1'b1)) begin
                            cr_state <= CR_STATE_WAIT;
                            cr_predsample_4by4_last_col_out <= 1'b1;
                        end
                        else if(cr_xby8_counter == ntbs_reg[MAX_NTBS_SIZE - 1:3] - 1'b1) begin
                            cr_xby8_counter <= {MAX_NTBS_SIZE{1'b0}};
                            cr_yby8_counter <= cr_yby8_counter + 1'b1;
                            // cr_state <= CR_STATE_11_4by4;
                            cr_predsample_4by4_last_col_out <= 1'b1;

                            cr_predsample_4by4_x_out <= xt_reg >> 1;
                            cr_predsample_4by4_y_out <= cr_predsample_4by4_y_out + 3'd4;
                        end
                        else begin
                            // cr_state <= CR_STATE_11_4by4;
                            cr_predsample_4by4_last_col_out <= 1'b0;
                            cr_xby8_counter  <= cr_xby8_counter + 1'b1;

                            cr_predsample_4by4_x_out <= cr_predsample_4by4_x_out + 3'd4;
                            cr_predsample_4by4_y_out <= cr_predsample_4by4_y_out;
                        end

                        if(cr_yby8_counter == ntbs_reg[MAX_NTBS_SIZE - 1:3] - 1'b1) begin
                            cr_predsample_4by4_last_row_out <= 1'b1;
                        end
                        else begin
                            cr_predsample_4by4_last_row_out <= 1'b0;
                        end
                    end
                end
            endcase
        end
    end

//    always @(posedge clk) begin : y_state_FSM
//        if (reset) begin
//            y_state <=  Y_STATE_WAIT; 
//        end
//        else begin
//            case(y_state)
//                Y_STATE_WAIT : begin
//                    if(y_valid) begin
//                        y_state                  <= Y_STATE_PREDICTION;
//                        y_predsample_4by4_x_out  <= xt_reg;
//                        y_predsample_4by4_y_out  <= yt_reg;
//                        y_xby4_counter           <= {MAX_NTBS_SIZE{1'b0}};
//                        y_yby4_counter           <= {MAX_NTBS_SIZE{1'b0}};
//
//                    end
//                end
//                Y_STATE_PREDICTION : begin
//
//                    if( y_cu_done_out) begin    
//                        state <= Y_STATE_WAIT;
//                    end
//
//                    if(y_predsample_4by4_valid_int) begin
//                        if(y_xby4_counter == ntbs_reg[MAX_NTBS_SIZE - 1:2] - 1'b1) begin
//                            y_yby4_counter          <= block_4by4_row_counter + 1'b1;
//                            y_xby4_counter          <= 6'd0;
//                            predsample_4by4_x_out   <= xt_reg;
//                            predsample_4by4_y_out   <= predsample_4by4_y_out + 3'd4;
//                        end
//                        else begin
//                            predsample_4by4_x_out   <= predsample_4by4_x_out + 3'd4;
//                            y_xby4_counter          <= y_xby4_counter + 1'b1;
//                        end
//                    end
//                end
//            endcase
//        end
//    end


    
    // always @(*) begin
    //    if( y_predsample_4by4_valid_int | cb_predsample_4by4_int | cr_predsample_4by4_int ) begin
    //         if(block_4by4_row_counter == (ntbs_reg[MAX_NTBS_SIZE - 1:2] - 1'b1)) begin
    //         end
    //         else begin
    //         end
            
    //         if(block_4by4_col_counter == (ntbs_reg[MAX_NTBS_SIZE - 1:2] - 1'b1)) begin
    //         end
    //         else begin
    //         end
       
    //    end
    //    else begin
    //    end
    // end
    
    // always @(*) begin
    //     // case(cidx_reg)
    //         2'd0 : begin
    //             predsample_4by4_valid_out   = y_predsample_4by4_valid_int;

    //             y_top_portw_en_mask_int      = top_portw_en_mask_in;
    //             y_left_portw_en_mask_int     = left_portw_en_mask_in;
    //             cb_top_portw_en_mask_int     = {PIXEL_WIDTH{1'b0}};
    //             cb_left_portw_en_mask_int    = {PIXEL_WIDTH{1'b0}};
    //             cr_top_portw_en_mask_int     = {PIXEL_WIDTH{1'b0}};
    //             cr_left_portw_en_mask_int    = {PIXEL_WIDTH{1'b0}};
    //         end
    //         2'd1 : begin
    //             predsample_4by4_out         = cb_predsample_4by4_int;
    //             predsample_4by4_valid_out   = cb_predsample_4by4_valid_int;

    //             y_top_portw_en_mask_int      = {PIXEL_WIDTH{1'b0}};
    //             y_left_portw_en_mask_int     = {PIXEL_WIDTH{1'b0}};
    //             cb_top_portw_en_mask_int     = top_portw_en_mask_in;
    //             cb_left_portw_en_mask_int    = left_portw_en_mask_in;
    //             cr_top_portw_en_mask_int     = {PIXEL_WIDTH{1'b0}};
    //             cr_left_portw_en_mask_int    = {PIXEL_WIDTH{1'b0}};
    //         end
    //         default/*2'd2*/ : begin
    //             predsample_4by4_out         = cr_predsample_4by4_int;
    //             predsample_4by4_valid_out   = cr_predsample_4by4_valid_int;

    //             y_top_portw_en_mask_int      = {PIXEL_WIDTH{1'b0}};
    //             y_left_portw_en_mask_int     = {PIXEL_WIDTH{1'b0}};
    //             cb_top_portw_en_mask_int     = {PIXEL_WIDTH{1'b0}};
    //             cb_left_portw_en_mask_int    = {PIXEL_WIDTH{1'b0}};
    //             cr_top_portw_en_mask_int     = top_portw_en_mask_in;
    //             cr_left_portw_en_mask_int    = left_portw_en_mask_in;
    //         end
            
    //     endcase
    // end
    
    // always @(*) begin
    
    // end

    intra_prediction_top
    #(
        .CIDX(0)
    )
    intra_prediction_top_y_block
    (
        .clk                            (clk),
        .reset                          (reset),
        .enable                         (enable),
        
        .valid_in                       (y_valid),
        .xt_in                          (xt_reg),
        .yt_in                          (yt_reg),
        .ntbs_in                        (ntbs_reg),
        .intramode_in                   (y_intramode_reg),

        .predsample_4by4_out            (y_predsample_4by4_out),
        .predsample_4by4_valid_out      (y_predsample_4by4_valid_out),
        .residual_fifo_is_empty_in      (y_residual_fifo_is_empty_in),
        .cu_done_out                    (y_cu_done_out),
        
        //Line buffer write interfaces
        .top_portw_addr_in              (y_top_portw_addr_in),
        .top_portw_data0_in             (y_top_portw_data0_in),
        .top_portw_data1_in             (y_top_portw_data1_in),
        .top_portw_data2_in             (y_top_portw_data2_in),
        .top_portw_data3_in             (y_top_portw_data3_in),
        .top_portw_data4_in             (y_top_portw_data4_in),
        .top_portw_data5_in             (y_top_portw_data5_in),
        .top_portw_data6_in             (y_top_portw_data6_in),
        .top_portw_data7_in             (y_top_portw_data7_in),
        .top_portw_en_mask_in           (y_top_portw_en_mask_in),
        
        
        .left_portw_addr_in             (y_left_portw_addr_in),
        .left_portw_data0_in            (y_left_portw_data0_in),
        .left_portw_data1_in            (y_left_portw_data1_in),
        .left_portw_data2_in            (y_left_portw_data2_in),
        .left_portw_data3_in            (y_left_portw_data3_in),
        .left_portw_data4_in            (y_left_portw_data4_in),
        .left_portw_data5_in            (y_left_portw_data5_in),
        .left_portw_data6_in            (y_left_portw_data6_in),
        .left_portw_data7_in            (y_left_portw_data7_in),
        .left_portw_en_mask_in          (y_left_portw_en_mask_in),
        
        //CONFIG
        .frame_height_in                (frame_height_reg),
        .frame_width_in                 (frame_width_reg),
        .tile_width_in                  (tile_width_reg),
        .tile_xc_in                     (tile_xc_reg),
        .tile_yc_in                     (tile_yc_reg),
        .slice_x_in                     (slice_x_reg),
        .slice_y_in                     (slice_y_reg),
        .strong_intra_smoothing_flag_in (strong_intra_smoothing_flag_reg),
        
        .log2ctbsize_in                 (log2ctbsize_reg),
        .log2ctbsize_config_in          (log2ctbsize_config_reg)
        
        // config_data
        // config_data_in,
        // config_data_valid_in

    );
    
    intra_prediction_top
    #(
        .CIDX(1)
    )
        intra_prediction_top_cb_block
    (
        .clk                            (clk),
        .reset                          (reset),
        .enable                         (enable),
        
        .valid_in                       (cb_valid),
        .xt_in                          (xt_reg),
        .yt_in                          (yt_reg),
        .ntbs_in                        (ntbs_reg),
        .intramode_in                   (cb_intramode_reg),

        .predsample_4by4_out            (cb_predsample_4by4_out),
        .predsample_4by4_valid_out      (cb_predsample_4by4_valid_out),
        .residual_fifo_is_empty_in      (cb_residual_fifo_is_empty_in),
        .cu_done_out                    (cb_cu_done_out),
        
        //Line buffer write interfaces
        .top_portw_addr_in              (cb_top_portw_addr_in),
        .top_portw_data0_in             (cb_top_portw_data0_in),
        .top_portw_data1_in             (cb_top_portw_data1_in),
        .top_portw_data2_in             (cb_top_portw_data2_in),
        .top_portw_data3_in             (cb_top_portw_data3_in),
        .top_portw_data4_in             (cb_top_portw_data4_in),
        .top_portw_data5_in             (cb_top_portw_data5_in),
        .top_portw_data6_in             (cb_top_portw_data6_in),
        .top_portw_data7_in             (cb_top_portw_data7_in),
        .top_portw_en_mask_in           (cb_top_portw_en_mask_in),
        
        
        .left_portw_addr_in             (cb_left_portw_addr_in),
        .left_portw_data0_in            (cb_left_portw_data0_in),
        .left_portw_data1_in            (cb_left_portw_data1_in),
        .left_portw_data2_in            (cb_left_portw_data2_in),
        .left_portw_data3_in            (cb_left_portw_data3_in),
        .left_portw_data4_in            (cb_left_portw_data4_in),
        .left_portw_data5_in            (cb_left_portw_data5_in),
        .left_portw_data6_in            (cb_left_portw_data6_in),
        .left_portw_data7_in            (cb_left_portw_data7_in),
        .left_portw_en_mask_in          (cb_left_portw_en_mask_in),
        
        //CONFIG
        .frame_height_in                (frame_height_reg),
        .frame_width_in                 (frame_width_reg),
        .tile_width_in                  (tile_width_reg),
        .tile_xc_in                     (tile_xc_reg),
        .tile_yc_in                     (tile_yc_reg),
        .slice_x_in                     (slice_x_reg),
        .slice_y_in                     (slice_y_reg),
        .strong_intra_smoothing_flag_in (strong_intra_smoothing_flag_reg),
        
        .log2ctbsize_in                 (log2ctbsize_reg),
        .log2ctbsize_config_in          (log2ctbsize_config_reg)
        
        // config_data
        // config_data_in,
        // config_data_valid_in

    );
    
    intra_prediction_top
    #(
        .CIDX(2)
    )
        intra_prediction_top_cr_block
    (
        .clk                            (clk),
        .reset                          (reset),
        .enable                         (enable),
        
        .valid_in                       (cr_valid),
        .xt_in                          (xt_reg),
        .yt_in                          (yt_reg),
        .ntbs_in                        (ntbs_reg),
        .intramode_in                   (cr_intramode_reg),

        .predsample_4by4_out            (cr_predsample_4by4_out),
        .predsample_4by4_valid_out      (cr_predsample_4by4_valid_out),
        .residual_fifo_is_empty_in      (cr_residual_fifo_is_empty_in),
        .cu_done_out                    (cr_cu_done_out),
        
        //Line buffer write interfaces
        .top_portw_addr_in              (cr_top_portw_addr_in),
        .top_portw_data0_in             (cr_top_portw_data0_in),
        .top_portw_data1_in             (cr_top_portw_data1_in),
        .top_portw_data2_in             (cr_top_portw_data2_in),
        .top_portw_data3_in             (cr_top_portw_data3_in),
        .top_portw_data4_in             (cr_top_portw_data4_in),
        .top_portw_data5_in             (cr_top_portw_data5_in),
        .top_portw_data6_in             (cr_top_portw_data6_in),
        .top_portw_data7_in             (cr_top_portw_data7_in),
        .top_portw_en_mask_in           (cr_top_portw_en_mask_in),
        
        
        .left_portw_addr_in             (cr_left_portw_addr_in),
        .left_portw_data0_in            (cr_left_portw_data0_in),
        .left_portw_data1_in            (cr_left_portw_data1_in),
        .left_portw_data2_in            (cr_left_portw_data2_in),
        .left_portw_data3_in            (cr_left_portw_data3_in),
        .left_portw_data4_in            (cr_left_portw_data4_in),
        .left_portw_data5_in            (cr_left_portw_data5_in),
        .left_portw_data6_in            (cr_left_portw_data6_in),
        .left_portw_data7_in            (cr_left_portw_data7_in),
        .left_portw_en_mask_in          (cr_left_portw_en_mask_in),
        
        //CONFIG
        .frame_height_in                (frame_height_reg),
        .frame_width_in                 (frame_width_reg),
        .tile_width_in                  (tile_width_reg),
        .tile_xc_in                     (tile_xc_reg),
        .tile_yc_in                     (tile_yc_reg),
        .slice_x_in                     (slice_x_reg),
        .slice_y_in                     (slice_y_reg),
        .strong_intra_smoothing_flag_in (strong_intra_smoothing_flag_reg),
        
        .log2ctbsize_in                 (log2ctbsize_reg),
        .log2ctbsize_config_in          (log2ctbsize_config_reg)
        
        // config_data
        // config_data_in,
        // config_data_valid_in

    );
endmodule


