if exist work (
	rmdir /S /Q work 2> nul
)

set SV_file=intra_dpi_iface.svi 

echo vlib work
vlib work

vlog -L work -sv -dpiheader dpiheader.h %SV_file%
vlog -L work -sv intra_tb_sv.svi
vlog -L work ../intra/rtl/*.v

rem vlog -L work intra_tb.v

vsim -novopt -t 1ps +notimingchecks +TESTNAME=tx_test -sv_lib intra_dpilib -lib work work.intra_tb -do intra_wave.do

echo Done !