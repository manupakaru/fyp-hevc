`timescale 1ns / 1ps

module intra_tb;

    localparam  CIDX_WIDTH                  = 2;
    localparam  MAX_LOG2CTBSIZE_WIDTH       = 3;
    localparam  LOG2_FRAME_SIZE             = 12; 
    localparam  INTRA_MODE_WIDTH            = 6;
    localparam  MAX_NTBS_SIZE               = 6;
    localparam  PIXEL_ADDR_LENGTH           = 12;
    
    localparam  CONFIG_DATA_BUS_WIDTH       = 32;
    localparam  PIXEL_WIDTH                 = 8;
    localparam  OUTPUT_BLOCK_SIZE           = 4;
    localparam  HEADER_WIDTH                = 8;

	reg 				clk;
	reg 				reset;
	reg [31:0] 			packet;
	reg [31:0] 			temp_packet;

	wire				cu_done_int;
	wire				y_cu_done_int;
	wire				cb_cu_done_int;
	wire				cr_cu_done_int;

    wire [PIXEL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE - 1:0] y_predsample_4by4_out;
    wire                                                         y_predsample_4by4_valid_out;
    wire [PIXEL_ADDR_LENGTH - 1:0]                               y_predsample_4by4_x_out;
    wire [PIXEL_ADDR_LENGTH - 1:0]                               y_predsample_4by4_y_out;
    wire                                                         y_predsample_4by4_last_row_out;
    wire                                                         y_predsample_4by4_last_col_out;
    wire                                                         y_res_present_out;

    wire [PIXEL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE - 1:0] cb_predsample_4by4_out;
    wire                                                         cb_predsample_4by4_valid_out;
    wire [PIXEL_ADDR_LENGTH - 1:0]                               cb_predsample_4by4_x_out;
    wire [PIXEL_ADDR_LENGTH - 1:0]                               cb_predsample_4by4_y_out;
    wire                                                         cb_predsample_4by4_last_row_out;
    wire                                                         cb_predsample_4by4_last_col_out;
    wire                                                         cb_res_present_out;

    wire [PIXEL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE - 1:0] cr_predsample_4by4_out;
    wire                                                         cr_predsample_4by4_valid_out;
    wire [PIXEL_ADDR_LENGTH - 1:0]                               cr_predsample_4by4_x_out;
    wire [PIXEL_ADDR_LENGTH - 1:0]                               cr_predsample_4by4_y_out;
    wire                                                         cr_predsample_4by4_last_row_out;
    wire                                                         cr_predsample_4by4_last_col_out;
    wire                                                         cr_res_present_out;


    //tb regs and wires
    wire [PIXEL_WIDTH - 1:0]                                     y_predsample_4by4_int_arr[OUTPUT_BLOCK_SIZE - 1:0][OUTPUT_BLOCK_SIZE - 1:0];
    reg  [PIXEL_WIDTH - 1:0]                                     y_predsample_4by4_res_added[OUTPUT_BLOCK_SIZE - 1:0][OUTPUT_BLOCK_SIZE - 1:0];
    reg  [PIXEL_WIDTH*4 - 1:0]                                   y_predsample_4by4_res_added_lastrow;
    reg  [PIXEL_WIDTH*4 - 1:0]                                   y_predsample_4by4_res_added_lastcol;
    reg  [8 - 1:0]                                               y_write_en_mask;

    wire [PIXEL_WIDTH - 1:0]                                     cb_predsample_4by4_int_arr[OUTPUT_BLOCK_SIZE - 1:0][OUTPUT_BLOCK_SIZE - 1:0];
    reg  [PIXEL_WIDTH - 1:0]                                     cb_predsample_4by4_res_added[OUTPUT_BLOCK_SIZE - 1:0][OUTPUT_BLOCK_SIZE - 1:0];
    reg  [PIXEL_WIDTH*4 - 1:0]                                   cb_predsample_4by4_res_added_lastrow;
    reg  [PIXEL_WIDTH*4 - 1:0]                                   cb_predsample_4by4_res_added_lastcol;
    reg  [8 - 1:0]                                               cb_write_en_mask;

    wire [PIXEL_WIDTH - 1:0]                                     cr_predsample_4by4_int_arr[OUTPUT_BLOCK_SIZE - 1:0][OUTPUT_BLOCK_SIZE - 1:0];
    reg  [PIXEL_WIDTH - 1:0]                                     cr_predsample_4by4_res_added[OUTPUT_BLOCK_SIZE - 1:0][OUTPUT_BLOCK_SIZE - 1:0];
    reg  [PIXEL_WIDTH*4 - 1:0]                                   cr_predsample_4by4_res_added_lastrow;
    reg  [PIXEL_WIDTH*4 - 1:0]                                   cr_predsample_4by4_res_added_lastcol;
    reg  [8 - 1:0]                                               cr_write_en_mask;

	intra_dpi_iface intra_dpi_iface_block();

	initial begin
			clk <= 1'b1;
			reset <= 1'b1;
			packet <= {32{1'b1}};

			#100;
			reset <= 1'b0;

			intra_dpi_iface_block.start();

			repeat (1000) begin
			//while (packet[32 - 1: 32 - 8] != 8'h40) begin

                while(cr_cu_done_int == 1'b0)
                @(posedge clk);

				temp_packet = intra_dpi_iface_block.get_packet_sv();
				if(temp_packet[32 - 1: 32 - 8] == 8'h50) begin
					temp_packet = intra_dpi_iface_block.get_packet_sv();
					temp_packet = intra_dpi_iface_block.get_packet_sv();
					temp_packet = intra_dpi_iface_block.get_packet_sv();
				end
				if(temp_packet[32 - 1: 32 - 8] == 8'h10) begin
					temp_packet = intra_dpi_iface_block.get_packet_sv();
					temp_packet = intra_dpi_iface_block.get_packet_sv();
				end
				@(posedge clk); 				
				packet <= temp_packet;

			end

			intra_dpi_iface_block.finish();

			repeat (1000) begin
				@(posedge clk); 
			end

			$finish;
	end

	always begin
		#5 clk <= ~clk;
	end

    generate
        genvar i;
        genvar j;
        
        for (i = 0 ; i < OUTPUT_BLOCK_SIZE ; i = i + 1) begin : row_iteration
            for (j = 0 ; j < OUTPUT_BLOCK_SIZE ; j = j + 1) begin : column_iteration
                assign y_predsample_4by4_int_arr[i][j] = y_predsample_4by4_out[i*PIXEL_WIDTH*OUTPUT_BLOCK_SIZE +(j+1)*PIXEL_WIDTH - 1:j*PIXEL_WIDTH + i*PIXEL_WIDTH*OUTPUT_BLOCK_SIZE];
                assign cb_predsample_4by4_int_arr[i][j] = cb_predsample_4by4_out[i*PIXEL_WIDTH*OUTPUT_BLOCK_SIZE +(j+1)*PIXEL_WIDTH - 1:j*PIXEL_WIDTH + i*PIXEL_WIDTH*OUTPUT_BLOCK_SIZE]; 
                assign cr_predsample_4by4_int_arr[i][j] = cr_predsample_4by4_out[i*PIXEL_WIDTH*OUTPUT_BLOCK_SIZE +(j+1)*PIXEL_WIDTH - 1:j*PIXEL_WIDTH + i*PIXEL_WIDTH*OUTPUT_BLOCK_SIZE];  
            end
        end
    endgenerate

    always @(posedge clk) begin
        if (reset) begin
            y_write_en_mask = 8'b00000000;  
        end
        else begin
            if(y_predsample_4by4_valid_out) begin
                intra_dpi_iface_block.add_res_data(2'd0,y_predsample_4by4_int_arr,y_predsample_4by4_x_out,y_predsample_4by4_y_out,y_res_present_out,y_predsample_4by4_res_added,y_predsample_4by4_res_added_lastrow,y_predsample_4by4_res_added_lastcol);
                y_write_en_mask = 8'b00001111;
            end
            else begin
                y_write_en_mask = 8'b00000000;
            end
        end
    end

    always @(posedge clk) begin
        if (reset) begin
            cb_write_en_mask = 8'b00000000; 
        end
        else begin
            if(cb_predsample_4by4_valid_out) begin
                intra_dpi_iface_block.add_res_data(2'd1,cb_predsample_4by4_int_arr,cb_predsample_4by4_x_out,cb_predsample_4by4_y_out,cb_res_present_out,cb_predsample_4by4_res_added,cb_predsample_4by4_res_added_lastrow,cb_predsample_4by4_res_added_lastcol);
                cb_write_en_mask = 8'b00001111;
            end
            else begin
                cb_write_en_mask = 8'b00000000;
            end
        end
    end

    always @(posedge clk) begin
        if (reset) begin
            cr_write_en_mask = 8'b00000000;     
        end
        else begin
            if(cr_predsample_4by4_valid_out) begin
                intra_dpi_iface_block.add_res_data(2'd2,cr_predsample_4by4_int_arr,cr_predsample_4by4_x_out,cr_predsample_4by4_y_out,cr_res_present_out,cr_predsample_4by4_res_added,cr_predsample_4by4_res_added_lastrow,cr_predsample_4by4_res_added_lastcol);
                cr_write_en_mask = 8'b00001111;
            end
            else begin
                cr_write_en_mask = 8'b00000000;
            end
        end
    end

	intra_prediction_wrapper intra_prediction_wrapper_block
    (
        .clk 								(clk),
        .reset 								(reset),
        .enable 							(1'b1),
        
        .config_data_bus_in					(packet),
        .config_data_valid_in				(1'b1),
        .residual_fifo_is_empty_in 			(1'b0),
        
        //writeback y_linebuffer interface
        .y_top_portw_addr_in				(y_predsample_4by4_x_out),
        .y_top_portw_data0_in				(y_predsample_4by4_res_added_lastrow[1*PIXEL_WIDTH - 1:0*PIXEL_WIDTH]),
        .y_top_portw_data1_in				(y_predsample_4by4_res_added_lastrow[2*PIXEL_WIDTH - 1:1*PIXEL_WIDTH]),
        .y_top_portw_data2_in				(y_predsample_4by4_res_added_lastrow[3*PIXEL_WIDTH - 1:2*PIXEL_WIDTH]),
        .y_top_portw_data3_in				(y_predsample_4by4_res_added_lastrow[4*PIXEL_WIDTH - 1:3*PIXEL_WIDTH]),
        .y_top_portw_data4_in				(),
        .y_top_portw_data5_in				(),
        .y_top_portw_data6_in				(),
        .y_top_portw_data7_in				(),
        .y_top_portw_en_mask_in				(),
        
        .y_left_portw_addr_in				(y_predsample_4by4_y_out),
        .y_left_portw_data0_in				(y_predsample_4by4_res_added_lastcol[1*PIXEL_WIDTH - 1:0*PIXEL_WIDTH]),
        .y_left_portw_data1_in				(y_predsample_4by4_res_added_lastcol[2*PIXEL_WIDTH - 1:1*PIXEL_WIDTH]),
        .y_left_portw_data2_in				(y_predsample_4by4_res_added_lastcol[3*PIXEL_WIDTH - 1:2*PIXEL_WIDTH]),
        .y_left_portw_data3_in				(y_predsample_4by4_res_added_lastcol[4*PIXEL_WIDTH - 1:3*PIXEL_WIDTH]),
        .y_left_portw_data4_in				(),
        .y_left_portw_data5_in				(),
        .y_left_portw_data6_in				(),
        .y_left_portw_data7_in				(),
        .y_left_portw_en_mask_in			(),

        .y_predsample_4by4_out 				(y_predsample_4by4_out),
        .y_predsample_4by4_valid_out 		(y_predsample_4by4_valid_out),
        .y_predsample_4by4_x_out  			(y_predsample_4by4_x_out),
        .y_predsample_4by4_y_out 			(y_predsample_4by4_y_out),
        .y_predsample_4by4_last_row_out 	(y_predsample_4by4_last_row_out),
        .y_predsample_4by4_last_col_out 	(y_predsample_4by4_last_col_out),
        .y_res_present_out  				(y_res_present_out),
        .y_residual_fifo_is_empty_in		(1'b0),

        .y_cu_done_out					    (y_cu_done_int),

        //writeback cb_linebuffer interface
        .cb_top_portw_addr_in  				(cb_predsample_4by4_x_out),
        .cb_top_portw_data0_in 				(cb_predsample_4by4_res_added_lastrow[1*PIXEL_WIDTH - 1:0*PIXEL_WIDTH]),
        .cb_top_portw_data1_in 				(cb_predsample_4by4_res_added_lastrow[2*PIXEL_WIDTH - 1:1*PIXEL_WIDTH]),
        .cb_top_portw_data2_in 				(cb_predsample_4by4_res_added_lastrow[3*PIXEL_WIDTH - 1:2*PIXEL_WIDTH]),
        .cb_top_portw_data3_in 				(cb_predsample_4by4_res_added_lastrow[4*PIXEL_WIDTH - 1:3*PIXEL_WIDTH]),
        .cb_top_portw_data4_in 				(),
        .cb_top_portw_data5_in 				(),
        .cb_top_portw_data6_in 				(),
        .cb_top_portw_data7_in 				(),
        .cb_top_portw_en_mask_in 			(),
        
        .cb_left_portw_addr_in              (cb_predsample_4by4_y_out),
        .cb_left_portw_data0_in  			(cb_predsample_4by4_res_added_lastcol[1*PIXEL_WIDTH - 1:0*PIXEL_WIDTH]),
        .cb_left_portw_data1_in  			(cb_predsample_4by4_res_added_lastcol[2*PIXEL_WIDTH - 1:1*PIXEL_WIDTH]),
        .cb_left_portw_data2_in  			(cb_predsample_4by4_res_added_lastcol[3*PIXEL_WIDTH - 1:2*PIXEL_WIDTH]),
        .cb_left_portw_data3_in				(cb_predsample_4by4_res_added_lastcol[4*PIXEL_WIDTH - 1:3*PIXEL_WIDTH]),
        .cb_left_portw_data4_in				(),
        .cb_left_portw_data5_in				(),
        .cb_left_portw_data6_in				(),
        .cb_left_portw_data7_in				(),
        .cb_left_portw_en_mask_in 			(),

        .cb_predsample_4by4_out 			(cb_predsample_4by4_out),
        .cb_predsample_4by4_valid_out 		(cb_predsample_4by4_valid_out),
        .cb_predsample_4by4_x_out  			(cb_predsample_4by4_x_out),
        .cb_predsample_4by4_y_out  			(cb_predsample_4by4_y_out),
        .cb_predsample_4by4_last_row_out 	(cb_predsample_4by4_last_row_out),
        .cb_predsample_4by4_last_col_out 	(cb_predsample_4by4_last_col_out),
        .cb_res_present_out 				(cb_res_present_out),
        .cb_residual_fifo_is_empty_in		(1'b0),

        .cb_cu_done_out						(cb_cu_done_int),

        //writeback cr_linebuffer interface
        .cr_top_portw_addr_in  				(cr_predsample_4by4_x_out),
        .cr_top_portw_data0_in  			(cr_predsample_4by4_res_added_lastrow[1*PIXEL_WIDTH - 1:0*PIXEL_WIDTH]),
        .cr_top_portw_data1_in 				(cr_predsample_4by4_res_added_lastrow[2*PIXEL_WIDTH - 1:1*PIXEL_WIDTH]),
        .cr_top_portw_data2_in  			(cr_predsample_4by4_res_added_lastrow[3*PIXEL_WIDTH - 1:2*PIXEL_WIDTH]),
        .cr_top_portw_data3_in  			(cr_predsample_4by4_res_added_lastrow[4*PIXEL_WIDTH - 1:3*PIXEL_WIDTH]),
        .cr_top_portw_data4_in  			(),
        .cr_top_portw_data5_in  			(),
        .cr_top_portw_data6_in  			(),
        .cr_top_portw_data7_in  			(),
        .cr_top_portw_en_mask_in 			(),
        
        .cr_left_portw_addr_in  			(cr_predsample_4by4_y_out),
        .cr_left_portw_data0_in  			(cr_predsample_4by4_res_added_lastcol[1*PIXEL_WIDTH - 1:0*PIXEL_WIDTH]),
        .cr_left_portw_data1_in  			(cr_predsample_4by4_res_added_lastcol[2*PIXEL_WIDTH - 1:1*PIXEL_WIDTH]),
        .cr_left_portw_data2_in  			(cr_predsample_4by4_res_added_lastcol[3*PIXEL_WIDTH - 1:2*PIXEL_WIDTH]),
        .cr_left_portw_data3_in  			(cr_predsample_4by4_res_added_lastcol[4*PIXEL_WIDTH - 1:3*PIXEL_WIDTH]),
        .cr_left_portw_data4_in  			(),
        .cr_left_portw_data5_in  			(),
        .cr_left_portw_data6_in  			(),
        .cr_left_portw_data7_in  			(),
        .cr_left_portw_en_mask_in  			(),
        
        .cr_predsample_4by4_out 			(cr_predsample_4by4_out),
        .cr_predsample_4by4_valid_out  		(cr_predsample_4by4_valid_out),
        .cr_predsample_4by4_x_out  			(cr_predsample_4by4_x_out),
        .cr_predsample_4by4_y_out  			(cr_predsample_4by4_y_out),
        .cr_predsample_4by4_last_row_out  	(cr_predsample_4by4_last_row_out),
        .cr_predsample_4by4_last_col_out  	(cr_predsample_4by4_last_col_out),
        .cr_res_present_out  				(cr_res_present_out),
        .cr_residual_fifo_is_empty_in		(1'b0),

        .cr_cu_done_out						(cr_cu_done_int)
    );
 




endmodule