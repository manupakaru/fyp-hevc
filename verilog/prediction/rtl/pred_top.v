module pred_top
    (
        clk,
        reset,
        enable,

        fifo_in,
        input_fifo_is_empty,
        read_en_out,

        fifo_out,
        output_fifo_is_full,
        write_en_out,

        //luma residual fifo interface
        y_residual_fifo_in,
        y_residual_fifo_is_empty_in,
        y_residual_fifo_read_en_out,

        //cb residual fifo interface
        cb_residual_fifo_in,
        cb_residual_fifo_is_empty_in,
        cb_residual_fifo_read_en_out,

        //cr residual fifo interface
        cr_residual_fifo_in,
        cr_residual_fifo_is_empty_in,
        cr_residual_fifo_read_en_out,
        
        //OUTPUT FIFO INTERFACES
        
        //luma dbf fifo interface
        y_dbf_fifo_is_full,
        
        //cb dbf fifo interface
        cb_dbf_fifo_is_full,
        
        //cr dbf fifo interface
        cr_dbf_fifo_is_full,
        
        
        temp_port
        );
//---------------------------------------------------------------------------------------------------------------------
// Global constant headers
//---------------------------------------------------------------------------------------------------------------------
    /* STYLE_NOTES :
     * Always include global constant header files that have constant definitions before all other items
     * these files will contain constants in the form of localparams of `define directives
     */
    `include "pred_def.v"
//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------
    /* STYLE_NOTES :
     * Define all paramters
     */

    
//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------
    /* STYLE_NOTES :
     * Define all localparams after parameters
     */
    localparam                          FIFO_IN_WIDTH       = 36;
    localparam                          FIFO_OUT_WIDTH      = 32;  


    localparam                          STATE_READ_WAIT = 0;
    localparam                          STATE_ACTIVE = 1;
    localparam                          STATE_WRITE_WAIT = 2;
    localparam                          STATE_ERROR = 3;
    localparam                          STATE_CURRENT_POC = 4;
    localparam                          STATE_CUR_PIC_FIL_IDX = 5;
    localparam                          STATE_REF_PIC_LIST_5_UPDATE = 6;
    localparam                          STATE_REF_PIC_LIST_5_SCAN   = 7;
    localparam                          STATE_SEND_POC_TO_DBF = 8;
    localparam                          STATE_WRITE_WAIT_CUR_DPB_IDX_TO_DBF = 9;
    localparam                          STATE_REF_PIC_WRITE_WAIT = 10;
    localparam                          STATE_DPB_ADD_PREV_PIC = 11;
    localparam                          STATE_POC_READ_WAIT = 12;
    localparam                          STATE_MVD_READ_WAIT = 13;
    localparam                          STATE_MVD_READ = 13;
    localparam                          STATE_MV_RETURN_WAIT = 13;

    localparam                          WRITE_REF_PIC_STATE_IDLE = 0;
    localparam                          WRITE_REF_PIC_STATE_CHECK = 1;
    localparam                          WRITE_REF_PIC_STATE_POC_WRITE = 2;
    localparam                          WRITE_REF_PIC_STATE_IDX_WRITE = 3;
    localparam                          WRITE_REF_PIC_STATE_DONE = 4;
    localparam                          WRITE_REF_PIC_FIND_FOL_IDX = 5;
    
    
    input                                   clk;
    input                                   reset;
    input                                   enable;
    input                                   output_fifo_is_full;
    
    input           [FIFO_IN_WIDTH-1:0]     fifo_in;
    output      reg [FIFO_OUT_WIDTH-1:0]    fifo_out;
    input                                   input_fifo_is_empty;
    output       reg                        read_en_out;
    output       reg                        write_en_out;
    
    output          [REF_PIC_LIST_POC_DATA_WIDTH-1:0]       temp_port;


    //luma residual fifo interface
    input       [RESIDUAL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE - 1 :0]     y_residual_fifo_in;
    input                                                                       y_residual_fifo_is_empty_in;
    output reg                                                                  y_residual_fifo_read_en_out;
    //cb residual fifo interface
    input       [RESIDUAL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE - 1 :0]     cb_residual_fifo_in;
    input                                                                       cb_residual_fifo_is_empty_in;
    output reg                                                                  cb_residual_fifo_read_en_out;
    //cr residual fifo interface                                
    input       [RESIDUAL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE - 1 :0]     cr_residual_fifo_in;
    input                                                                       cr_residual_fifo_is_empty_in;
    output reg                                                                  cr_residual_fifo_read_en_out;

    // OUTPUT FIFO INTERFACES
    
    //luma dbf fifo interface
    input                                                                       y_dbf_fifo_is_full;
    
    //cb dbf fifo interface
    input                                                                       cb_dbf_fifo_is_full;
    
    //cr dbf fifo interface
    input                                                                       cr_dbf_fifo_is_full;

//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------
    /* STYLE_NOTES
     * Eacho wire and register must be defined on a seperate line
     * Although Verilog allows for implicit decleration of variables, all signals must be declared explicitly.
     * Group variables where necessary. Comment your variable by block and by signal.
     */


    integer                                         state;                      // the internal state of the module
    integer                                         next_state;
    integer                                         write_ref_pic_state;

    reg         [PIC_DIM_WIDTH-1:0]                 pic_width;
    reg         [PIC_DIM_WIDTH-1:0]                 pic_height;
    reg         [LOG2_CTB_WIDTH - 1: 0]             log2_ctb_size;
    reg                                             strong_intra_smoothing;
    reg                                             constrained_intra_pred;

    reg         [SLICE_TYPE_WIDTH -1:0]             slice_type;
    reg                                             slice_temporal_mvp_enabled_flag;
    
    reg         [NUM_REF_IDX_L0_MINUS1_WIDTH - 1:0]     num_active_ref_idx_l0;
    reg         [NUM_REF_IDX_L1_MINUS1_WIDTH - 1:0]     num_active_ref_idx_l1;
    
    reg         [DPB_ADDR_WIDTH -1: 0]              current_pic_dpb_idx;
    
    reg         [POC_WIDTH -1: 0]                   current__poc;
     
    reg         [INTER_TOP_CONFIG_BUS_MODE_WIDTH -1 :0] inter_config_mode_in;
    reg         [INTER_TOP_CONFIG_BUS_WIDTH -1:0]       inter_config_bus_in;
    
    reg         [NUM_REF_IDX_L0_MINUS1_WIDTH - 1:0]     num_pic_st_curr_bef;
    reg         [NUM_REF_IDX_L0_MINUS1_WIDTH - 1:0]     num_pic_st_curr_aft;
    reg         [NUM_REF_IDX_L0_MINUS1_WIDTH - 1:0]     num_pic_ref_pic_5_total;
    reg         [NUM_REF_IDX_L0_MINUS1_WIDTH - 1:0]     num_pic_st_fol;
    reg         [NUM_REF_IDX_L0_MINUS1_WIDTH - 1:0]     num_pic_st_curr_total;
    
    reg                                                 ref_poc_list5_wr_en;   
    reg         [REF_POC_LIST5_DATA_WIDTH -1: 0]        ref_poc_list5_data_in;   
    reg         [REF_POC_LIST5_ADDR_WIDTH -1: 0]        ref_poc_list5_addr;   
    reg         [REF_POC_LIST5_ADDR_WIDTH -1: 0]        ref_poc_list5_addr_read;   
    wire        [REF_POC_LIST5_DATA_WIDTH -1: 0]        ref_poc_list5_data_out; 
    
    wire         [DPB_DATA_WIDTH -1: 0]                 dpb_data_out;
    reg         [DPB_DATA_WIDTH -1: 0]                  dpb_data_in;
    reg                                                 dpb_wr_en;
    reg         [DPB_ADDR_WIDTH -1: 0]                  dpb_addr;
    reg         [DPB_ADDR_WIDTH -1: 0]                  dpb_addr_read;
    reg         [(1<<DPB_ADDR_WIDTH) -1:0]              dpb_filled_flag;
    reg         [(1<<DPB_ADDR_WIDTH) -1:0]              dpb_filled_flag_new;
    
    reg         [NUM_NEG_POS_POC_WIDTH -1:0]        curr_rps_entry_idx;  // make this a temp country
    wire        [DELTA_POC_WIDTH -1:0]              delta_poc_wire;
    
    reg [REF_PIC_LIST_POC_DATA_WIDTH-1:0]     ref_pic_list_poc_data_in;
    reg                                       ref_pic_list_poc_wr_en  ;
    reg [NUM_REF_IDX_L0_MINUS1_WIDTH-1:0]     ref_pic_list0_poc_addr  ;
    reg [NUM_REF_IDX_L0_MINUS1_WIDTH-1:0]     list0_poc_addr_temp  ;
    reg [NUM_REF_IDX_L0_MINUS1_WIDTH-1:0]     ref_pic_list1_poc_addr  ;
    reg [NUM_REF_IDX_L0_MINUS1_WIDTH-1:0]     list1_poc_addr_temp  ;
    
    reg [DPB_FRAME_OFFSET_WIDTH -1:0]           ref_pic_list_idx_data_in;
    reg                                         ref_pic_list_idx_wr_en;
    
    reg         [POC_WIDTH -1:0]                temp_poc;
    reg                                         initial_condition;
    
    reg                                         pred_mode;
    reg         [MAX_NTBS_SIZE - 1:0]           ntbs_reg;   //TODO : Geethan , feed this register with relavant data once recieved. 
    
    wire                                        mv_done;
    
    
    wire        [PIXEL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE - 1:0] inter_y_predsample_4by4_int;
    wire                                                                inter_y_predsample_4by4_valid_int;
    wire        [PIXEL_ADDR_LENGTH - 1:0]                               inter_y_predsample_4by4_x_int;
    wire        [PIXEL_ADDR_LENGTH - 1:0]                               inter_y_predsample_4by4_y_int;
    wire                                                                inter_y_res_present_int;

    wire        [PIXEL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE - 1:0] inter_cb_predsample_4by4_int;
    wire                                                                inter_cb_predsample_4by4_valid_int;
    wire        [PIXEL_ADDR_LENGTH - 1:0]                               inter_cb_predsample_4by4_x_int;
    wire        [PIXEL_ADDR_LENGTH - 1:0]                               inter_cb_predsample_4by4_y_int;
    wire                                                                inter_cb_res_present_int;

    wire        [PIXEL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE - 1:0] inter_cr_predsample_4by4_int;
    wire                                                                inter_cr_predsample_4by4_valid_int;
    wire        [PIXEL_ADDR_LENGTH - 1:0]                               inter_cr_predsample_4by4_x_int;
    wire        [PIXEL_ADDR_LENGTH - 1:0]                               inter_cr_predsample_4by4_y_int;
    wire                                                                inter_cr_res_present_int;


    // Residual Adder Signals
    reg        [PIXEL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE - 1:0]  y_common_predsample_4by4_int;
    reg                                                                 y_common_predsample_4by4_valid_int;
    reg        [PIXEL_ADDR_LENGTH - 1:0]                                y_common_predsample_4by4_x_int;
    reg        [PIXEL_ADDR_LENGTH - 1:0]                                y_common_predsample_4by4_y_int;
    reg                                                                 y_common_res_present_int;
    wire       [PIXEL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE - 1:0]  y_res_added_int;
    wire       [PIXEL_WIDTH - 1:0]                                      y_res_added_int_arr[OUTPUT_BLOCK_SIZE - 1:0][OUTPUT_BLOCK_SIZE - 1:0];
    reg                                                                 y_valid_addition_reg;
 
    reg        [PIXEL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE - 1:0]  cb_common_predsample_4by4_int;
    reg                                                                 cb_common_predsample_4by4_valid_int;
    reg        [PIXEL_ADDR_LENGTH - 1:0]                                cb_common_predsample_4by4_x_int;
    reg        [PIXEL_ADDR_LENGTH - 1:0]                                cb_common_predsample_4by4_y_int;
    reg                                                                 cb_common_res_present_int;
    wire       [PIXEL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE - 1:0]  cb_res_added_int;
    wire       [PIXEL_WIDTH - 1:0]                                      cb_res_added_int_arr[OUTPUT_BLOCK_SIZE - 1:0][OUTPUT_BLOCK_SIZE - 1:0];
    reg                                                                 cb_valid_addition_reg;

    reg        [PIXEL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE - 1:0]  cr_common_predsample_4by4_int;
    reg                                                                 cr_common_predsample_4by4_valid_int;
    reg        [PIXEL_ADDR_LENGTH - 1:0]                                cr_common_predsample_4by4_x_int;
    reg        [PIXEL_ADDR_LENGTH - 1:0]                                cr_common_predsample_4by4_y_int;
    reg                                                                 cr_common_res_present_int;
    wire       [PIXEL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE - 1:0]  cr_res_added_int;
    wire       [PIXEL_WIDTH - 1:0]                                      cr_res_added_int_arr[OUTPUT_BLOCK_SIZE - 1:0][OUTPUT_BLOCK_SIZE - 1:0];
    reg                                                                 cr_valid_addition_reg;                                                           


    // INTRA
    //---------------------------------------------------------------
    wire                                        intra_enable;
    
    reg         [CONFIG_DATA_BUS_WIDTH - 1:0]   intra_config_data_bus_reg;
    reg                                         intra_config_data_valid_reg;

    reg         [PIXEL_ADDR_LENGTH - 1:0]       intra_y_top_portw_addr_reg;            
    reg         [PIXEL_WIDTH - 1:0]             intra_y_top_portw_data0_reg;
    reg         [PIXEL_WIDTH - 1:0]             intra_y_top_portw_data1_reg;
    reg         [PIXEL_WIDTH - 1:0]             intra_y_top_portw_data2_reg;
    reg         [PIXEL_WIDTH - 1:0]             intra_y_top_portw_data3_reg;
    reg         [PIXEL_WIDTH - 1:0]             intra_y_top_portw_data4_reg;
    reg         [PIXEL_WIDTH - 1:0]             intra_y_top_portw_data5_reg;
    reg         [PIXEL_WIDTH - 1:0]             intra_y_top_portw_data6_reg;
    reg         [PIXEL_WIDTH - 1:0]             intra_y_top_portw_data7_reg;
    reg         [PIXEL_WIDTH - 1:0]             intra_y_top_portw_en_mask_reg;

    reg         [PIXEL_ADDR_LENGTH - 1:0]       intra_y_left_portw_addr_reg;
    reg         [PIXEL_WIDTH - 1:0]             intra_y_left_portw_data0_reg;   
    reg         [PIXEL_WIDTH - 1:0]             intra_y_left_portw_data1_reg;   
    reg         [PIXEL_WIDTH - 1:0]             intra_y_left_portw_data2_reg;   
    reg         [PIXEL_WIDTH - 1:0]             intra_y_left_portw_data3_reg;   
    reg         [PIXEL_WIDTH - 1:0]             intra_y_left_portw_data4_reg;   
    reg         [PIXEL_WIDTH - 1:0]             intra_y_left_portw_data5_reg;   
    reg         [PIXEL_WIDTH - 1:0]             intra_y_left_portw_data6_reg;   
    reg         [PIXEL_WIDTH - 1:0]             intra_y_left_portw_data7_reg;   
    reg         [PIXEL_WIDTH - 1:0]             intra_y_left_portw_en_mask_reg; 

    wire        [PIXEL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE - 1:0] intra_y_predsample_4by4_int;
    wire                                                                intra_y_predsample_4by4_valid_int;
    wire        [PIXEL_ADDR_LENGTH - 1:0]                               intra_y_predsample_4by4_x_int;
    wire        [PIXEL_ADDR_LENGTH - 1:0]                               intra_y_predsample_4by4_y_int;
    reg         [PIXEL_ADDR_LENGTH - 1:0]                               intra_y_predsample_4by4_x_reg;
    reg         [PIXEL_ADDR_LENGTH - 1:0]                               intra_y_predsample_4by4_y_reg;
    wire                                                                intra_y_predsample_4by4_last_row_int;
    wire                                                                intra_y_predsample_4by4_last_col_int;
    reg                                                                 intra_y_predsample_4by4_last_row_reg;
    reg                                                                 intra_y_predsample_4by4_last_col_reg;
    wire                                                                intra_y_cu_done_int;
    wire                                                                intra_y_res_present_int;


    reg         [PIXEL_ADDR_LENGTH - 1:0]       intra_cb_top_portw_addr_reg;            
    reg         [PIXEL_WIDTH - 1:0]             intra_cb_top_portw_data0_reg;
    reg         [PIXEL_WIDTH - 1:0]             intra_cb_top_portw_data1_reg;
    reg         [PIXEL_WIDTH - 1:0]             intra_cb_top_portw_data2_reg;
    reg         [PIXEL_WIDTH - 1:0]             intra_cb_top_portw_data3_reg;
    reg         [PIXEL_WIDTH - 1:0]             intra_cb_top_portw_data4_reg;
    reg         [PIXEL_WIDTH - 1:0]             intra_cb_top_portw_data5_reg;
    reg         [PIXEL_WIDTH - 1:0]             intra_cb_top_portw_data6_reg;
    reg         [PIXEL_WIDTH - 1:0]             intra_cb_top_portw_data7_reg;
    reg         [PIXEL_WIDTH - 1:0]             intra_cb_top_portw_en_mask_reg;

    reg         [PIXEL_ADDR_LENGTH - 1:0]       intra_cb_left_portw_addr_reg;
    reg         [PIXEL_WIDTH - 1:0]             intra_cb_left_portw_data0_reg;   
    reg         [PIXEL_WIDTH - 1:0]             intra_cb_left_portw_data1_reg;   
    reg         [PIXEL_WIDTH - 1:0]             intra_cb_left_portw_data2_reg;   
    reg         [PIXEL_WIDTH - 1:0]             intra_cb_left_portw_data3_reg;   
    reg         [PIXEL_WIDTH - 1:0]             intra_cb_left_portw_data4_reg;   
    reg         [PIXEL_WIDTH - 1:0]             intra_cb_left_portw_data5_reg;   
    reg         [PIXEL_WIDTH - 1:0]             intra_cb_left_portw_data6_reg;   
    reg         [PIXEL_WIDTH - 1:0]             intra_cb_left_portw_data7_reg;   
    reg         [PIXEL_WIDTH - 1:0]             intra_cb_left_portw_en_mask_reg; 

    wire        [PIXEL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE - 1:0] intra_cb_predsample_4by4_int;
    wire                                                                intra_cb_predsample_4by4_valid_int;
    wire        [PIXEL_ADDR_LENGTH - 1:0]                               intra_cb_predsample_4by4_x_int;
    wire        [PIXEL_ADDR_LENGTH - 1:0]                               intra_cb_predsample_4by4_y_int;
    reg         [PIXEL_ADDR_LENGTH - 1:0]                               intra_cb_predsample_4by4_x_reg;
    reg         [PIXEL_ADDR_LENGTH - 1:0]                               intra_cb_predsample_4by4_y_reg;
    wire                                                                intra_cb_predsample_4by4_last_row_int;
    wire                                                                intra_cb_predsample_4by4_last_col_int;
    reg                                                                 intra_cb_predsample_4by4_last_row_reg;
    reg                                                                 intra_cb_predsample_4by4_last_col_reg;
    wire                                                                intra_cb_cu_done_int;
    wire                                                                intra_cb_res_present_int;

    reg         [PIXEL_ADDR_LENGTH - 1:0]       intra_cr_top_portw_addr_reg;            
    reg         [PIXEL_WIDTH - 1:0]             intra_cr_top_portw_data0_reg;
    reg         [PIXEL_WIDTH - 1:0]             intra_cr_top_portw_data1_reg;
    reg         [PIXEL_WIDTH - 1:0]             intra_cr_top_portw_data2_reg;
    reg         [PIXEL_WIDTH - 1:0]             intra_cr_top_portw_data3_reg;
    reg         [PIXEL_WIDTH - 1:0]             intra_cr_top_portw_data4_reg;
    reg         [PIXEL_WIDTH - 1:0]             intra_cr_top_portw_data5_reg;
    reg         [PIXEL_WIDTH - 1:0]             intra_cr_top_portw_data6_reg;
    reg         [PIXEL_WIDTH - 1:0]             intra_cr_top_portw_data7_reg;
    reg         [PIXEL_WIDTH - 1:0]             intra_cr_top_portw_en_mask_reg;

    reg         [PIXEL_ADDR_LENGTH - 1:0]       intra_cr_left_portw_addr_reg;
    reg         [PIXEL_WIDTH - 1:0]             intra_cr_left_portw_data0_reg;   
    reg         [PIXEL_WIDTH - 1:0]             intra_cr_left_portw_data1_reg;   
    reg         [PIXEL_WIDTH - 1:0]             intra_cr_left_portw_data2_reg;   
    reg         [PIXEL_WIDTH - 1:0]             intra_cr_left_portw_data3_reg;   
    reg         [PIXEL_WIDTH - 1:0]             intra_cr_left_portw_data4_reg;   
    reg         [PIXEL_WIDTH - 1:0]             intra_cr_left_portw_data5_reg;   
    reg         [PIXEL_WIDTH - 1:0]             intra_cr_left_portw_data6_reg;   
    reg         [PIXEL_WIDTH - 1:0]             intra_cr_left_portw_data7_reg;   
    reg         [PIXEL_WIDTH - 1:0]             intra_cr_left_portw_en_mask_reg; 

    wire        [PIXEL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE - 1:0] intra_cr_predsample_4by4_int;
    wire                                                                intra_cr_predsample_4by4_valid_int;
    wire        [PIXEL_ADDR_LENGTH - 1:0]                               intra_cr_predsample_4by4_x_int;
    wire        [PIXEL_ADDR_LENGTH - 1:0]                               intra_cr_predsample_4by4_y_int;
    reg         [PIXEL_ADDR_LENGTH - 1:0]                               intra_cr_predsample_4by4_x_reg;
    reg         [PIXEL_ADDR_LENGTH - 1:0]                               intra_cr_predsample_4by4_y_reg;
    wire                                                                intra_cr_predsample_4by4_last_row_int;
    wire                                                                intra_cr_predsample_4by4_last_col_int;
    reg                                                                 intra_cr_predsample_4by4_last_row_reg;
    reg                                                                 intra_cr_predsample_4by4_last_col_reg;
    wire                                                                intra_cr_cu_done_int;
    wire                                                                intra_cr_res_present_int;
    
    //----------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------
// Implmentation
//---------------------------------------------------------------------------------------------------------------------
   
   assign delta_poc_wire = fifo_in[ FIFO_IN_WIDTH - HEADER_WIDTH - DPB_STATUS_WIDTH - 1: 
                                    FIFO_IN_WIDTH - HEADER_WIDTH - - DPB_STATUS_WIDTH - DELTA_POC_WIDTH];
   
    s_sync_ram  #(
        .DATA_WIDTH(REF_POC_LIST5_DATA_WIDTH),
        .ADDR_WIDTH(REF_POC_LIST5_ADDR_WIDTH),
        .DATA_DEPTH((1<<REF_POC_LIST5_ADDR_WIDTH))
    ) ref_poc_list5_block
    (
        .clk(clk), 
        .addr_in(ref_poc_list5_addr), 
        .r_data_out(ref_poc_list5_data_out), 
        .w_data_in(ref_poc_list5_data_in), 
        .w_en_in(ref_poc_list5_wr_en), 
        .enable(enable)
    );
        
    s_sync_ram  #(
        .DATA_WIDTH(DPB_DATA_WIDTH),
        .ADDR_WIDTH(DPB_ADDR_WIDTH),
        .DATA_DEPTH((1<<DPB_ADDR_WIDTH))
    ) dpb_block
    (
        .clk(clk), 
        .addr_in(dpb_addr), 
        .r_data_out(dpb_data_out), 
        .w_data_in(dpb_data_in), 
        .w_en_in(dpb_wr_en), 
        .enable(enable)
    );
        
            
    // Instantiate the module
    inter_pred_top inter_top_block (
        .clk(clk), 
        .reset(reset), 
        .enable(enable),
        .config_mode_in(inter_config_mode_in), 
        .config_bus_in(inter_config_bus_in), 
        .from_top_ref_pic_list_poc_data_in(ref_pic_list_poc_data_in),
        .from_top_ref_pic_list_poc_wr_en(ref_pic_list_poc_wr_en),
        .from_top_ref_pic_list0_poc_addr(ref_pic_list0_poc_addr),
        .from_top_ref_pic_list1_poc_addr(ref_pic_list1_poc_addr),
        .from_top_ref_pic_list_idx_data_in(ref_pic_list_idx_data_in),
        .from_top_ref_pic_list_idx_wr_en(ref_pic_list_idx_wr_en),
        .mv_done_out(mv_done),
        .temp_port(temp_port)
        );
        
    assign intra_enable = (~y_dbf_fifo_is_full)&(~cb_dbf_fifo_is_full)&(~cr_dbf_fifo_is_full)&(~y_residual_fifo_is_empty_in)&(~cb_residual_fifo_is_empty_in)&(~cr_residual_fifo_is_empty_in);
        
    intra_prediction_wrapper intra_prediction_wrapper_block
    (
        .clk                            (clk),
        .reset                          (reset),
        .enable                         (intra_enable),
        
        .config_data_bus_in             (intra_config_data_bus_reg),
        .config_data_valid_in           (intra_config_data_valid_reg),
        .residual_fifo_is_empty_in      (residual_fifo_is_empty_in),
        
        //writeback y_linebuffer interface
        .y_top_portw_addr_in            (intra_y_top_portw_addr_reg   ),
        .y_top_portw_data0_in           (intra_y_top_portw_data0_reg  ),
        .y_top_portw_data1_in           (intra_y_top_portw_data1_reg  ),
        .y_top_portw_data2_in           (intra_y_top_portw_data2_reg  ),
        .y_top_portw_data3_in           (intra_y_top_portw_data3_reg  ),
        .y_top_portw_data4_in           (intra_y_top_portw_data4_reg  ),
        .y_top_portw_data5_in           (intra_y_top_portw_data5_reg  ),
        .y_top_portw_data6_in           (intra_y_top_portw_data6_reg  ),
        .y_top_portw_data7_in           (intra_y_top_portw_data7_reg  ),
        .y_top_portw_en_mask_in         (intra_y_top_portw_en_mask_reg),
        
        .y_left_portw_addr_in           (intra_y_left_portw_addr_reg   ),
        .y_left_portw_data0_in          (intra_y_left_portw_data0_reg  ),
        .y_left_portw_data1_in          (intra_y_left_portw_data1_reg  ),
        .y_left_portw_data2_in          (intra_y_left_portw_data2_reg  ),
        .y_left_portw_data3_in          (intra_y_left_portw_data3_reg  ),
        .y_left_portw_data4_in          (intra_y_left_portw_data4_reg  ),
        .y_left_portw_data5_in          (intra_y_left_portw_data5_reg  ),
        .y_left_portw_data6_in          (intra_y_left_portw_data6_reg  ),
        .y_left_portw_data7_in          (intra_y_left_portw_data7_reg  ),
        .y_left_portw_en_mask_in        (intra_y_left_portw_en_mask_reg),

        .y_predsample_4by4_out          (intra_y_predsample_4by4_int      ),
        .y_predsample_4by4_valid_out    (intra_y_predsample_4by4_valid_int),
        .y_predsample_4by4_x_out        (intra_y_predsample_4by4_x_int    ),
        .y_predsample_4by4_y_out        (intra_y_predsample_4by4_y_int    ),
        .y_predsample_4by4_last_row_out (intra_y_predsample_4by4_last_row_int),
        .y_predsample_4by4_last_col_out (intra_y_predsample_4by4_last_col_int),
        .y_res_present_out              (y_res_present_int),
        .y_residual_fifo_is_empty_in    (y_residual_fifo_is_empty_in),

        .y_cu_done_out                  (intra_y_cu_done_int),

        //writeback cb_linebuffer interface
        .cb_top_portw_addr_in           (intra_cb_top_portw_addr_reg   ),
        .cb_top_portw_data0_in          (intra_cb_top_portw_data0_reg  ),
        .cb_top_portw_data1_in          (intra_cb_top_portw_data1_reg  ),
        .cb_top_portw_data2_in          (intra_cb_top_portw_data2_reg  ),
        .cb_top_portw_data3_in          (intra_cb_top_portw_data3_reg  ),
        .cb_top_portw_data4_in          (intra_cb_top_portw_data4_reg  ),
        .cb_top_portw_data5_in          (intra_cb_top_portw_data5_reg  ),
        .cb_top_portw_data6_in          (intra_cb_top_portw_data6_reg  ),
        .cb_top_portw_data7_in          (intra_cb_top_portw_data7_reg  ),
        .cb_top_portw_en_mask_in        (intra_cb_top_portw_en_mask_reg),
        
        .cb_left_portw_addr_in          (intra_cb_left_portw_addr_reg   ),
        .cb_left_portw_data0_in         (intra_cb_left_portw_data0_reg  ),
        .cb_left_portw_data1_in         (intra_cb_left_portw_data1_reg  ),
        .cb_left_portw_data2_in         (intra_cb_left_portw_data2_reg  ),
        .cb_left_portw_data3_in         (intra_cb_left_portw_data3_reg  ),
        .cb_left_portw_data4_in         (intra_cb_left_portw_data4_reg  ),
        .cb_left_portw_data5_in         (intra_cb_left_portw_data5_reg  ),
        .cb_left_portw_data6_in         (intra_cb_left_portw_data6_reg  ),
        .cb_left_portw_data7_in         (intra_cb_left_portw_data7_reg  ),
        .cb_left_portw_en_mask_in       (intra_cb_left_portw_en_mask_reg),

        .cb_predsample_4by4_out         (intra_cb_predsample_4by4_int      ),
        .cb_predsample_4by4_valid_out   (intra_cb_predsample_4by4_valid_int),
        .cb_predsample_4by4_x_out       (intra_cb_predsample_4by4_x_int    ),
        .cb_predsample_4by4_y_out       (intra_cb_predsample_4by4_y_int    ),
        .cb_predsample_4by4_last_row_out(intra_cb_predsample_4by4_last_row_int),
        .cb_predsample_4by4_last_col_out(intra_cb_predsample_4by4_last_col_int),
        .cb_res_present_out             (cb_res_present_int),
        .cb_residual_fifo_is_empty_in   (cb_residual_fifo_is_empty_in),

        .cb_cu_done_out                 (intra_cb_cu_done_int),

        //writeback cr_linebuffer interface
        .cr_top_portw_addr_in           (intra_cr_top_portw_addr_reg   ),
        .cr_top_portw_data0_in          (intra_cr_top_portw_data0_reg  ),
        .cr_top_portw_data1_in          (intra_cr_top_portw_data1_reg  ),
        .cr_top_portw_data2_in          (intra_cr_top_portw_data2_reg  ),
        .cr_top_portw_data3_in          (intra_cr_top_portw_data3_reg  ),
        .cr_top_portw_data4_in          (intra_cr_top_portw_data4_reg  ),
        .cr_top_portw_data5_in          (intra_cr_top_portw_data5_reg  ),
        .cr_top_portw_data6_in          (intra_cr_top_portw_data6_reg  ),
        .cr_top_portw_data7_in          (intra_cr_top_portw_data7_reg  ),
        .cr_top_portw_en_mask_in        (intra_cr_top_portw_en_mask_reg),
       
        .cr_left_portw_addr_in          (intra_cr_left_portw_addr_reg   ),
        .cr_left_portw_data0_in         (intra_cr_left_portw_data0_reg  ),
        .cr_left_portw_data1_in         (intra_cr_left_portw_data1_reg  ),
        .cr_left_portw_data2_in         (intra_cr_left_portw_data2_reg  ),
        .cr_left_portw_data3_in         (intra_cr_left_portw_data3_reg  ),
        .cr_left_portw_data4_in         (intra_cr_left_portw_data4_reg  ),
        .cr_left_portw_data5_in         (intra_cr_left_portw_data5_reg  ),
        .cr_left_portw_data6_in         (intra_cr_left_portw_data6_reg  ),
        .cr_left_portw_data7_in         (intra_cr_left_portw_data7_reg  ),
        .cr_left_portw_en_mask_in       (intra_cr_left_portw_en_mask_reg),
        
        .cr_predsample_4by4_out         (intra_cr_predsample_4by4_int      ),
        .cr_predsample_4by4_valid_out   (intra_cr_predsample_4by4_valid_int),
        .cr_predsample_4by4_x_out       (intra_cr_predsample_4by4_x_int    ),
        .cr_predsample_4by4_y_out       (intra_cr_predsample_4by4_y_int    ),
        .cr_predsample_4by4_last_row_out(intra_cr_predsample_4by4_last_row_int),
        .cr_predsample_4by4_last_col_out(intra_cr_predsample_4by4_last_col_int),
        .cr_res_present_out             (cr_res_present_int),
        .cr_residual_fifo_is_empty_in   (cr_residual_fifo_is_empty_in),
        
        .cr_cu_done_out                 (intra_cr_cu_done_int),
    );

    always @(*) begin
        if (pred_mode == `PREDMODE_INTRA) begin
            y_common_predsample_4by4_int            = intra_y_predsample_4by4_int;
            y_common_predsample_4by4_valid_int      = intra_y_predsample_4by4_valid_int;
            y_common_predsample_4by4_x_int          = intra_y_predsample_4by4_x_int;
            y_common_predsample_4by4_y_int          = intra_y_predsample_4by4_y_int;
            y_common_res_present_int                = intra_y_res_present_int;

            cb_common_predsample_4by4_int           = intra_cb_predsample_4by4_int;
            cb_common_predsample_4by4_valid_int     = intra_cb_predsample_4by4_valid_int;
            cb_common_predsample_4by4_x_int         = intra_cb_predsample_4by4_x_int;
            cb_common_predsample_4by4_y_int         = intra_cb_predsample_4by4_y_int;
            cb_common_res_present_int               = intra_cb_res_present_int;

            cr_common_predsample_4by4_int           = intra_cr_predsample_4by4_int;
            cr_common_predsample_4by4_valid_int     = intra_cr_predsample_4by4_valid_int;
            cr_common_predsample_4by4_x_int         = intra_cr_predsample_4by4_x_int;
            cr_common_predsample_4by4_y_int         = intra_cr_predsample_4by4_y_int;
            cr_common_res_present_int               = intra_cr_res_present_int;        
        end
        else begin
            y_common_predsample_4by4_int            = inter_y_predsample_4by4_int;
            y_common_predsample_4by4_valid_int      = inter_y_predsample_4by4_valid_int;
            y_common_predsample_4by4_x_int          = inter_y_predsample_4by4_x_int;
            y_common_predsample_4by4_y_int          = inter_y_predsample_4by4_y_int;
            y_common_res_present_int                = inter_y_res_present_int;

            cb_common_predsample_4by4_int           = inter_cb_predsample_4by4_int;
            cb_common_predsample_4by4_valid_int     = inter_cb_predsample_4by4_valid_int;
            cb_common_predsample_4by4_x_int         = inter_cb_predsample_4by4_x_int;
            cb_common_predsample_4by4_y_int         = inter_cb_predsample_4by4_y_int;
            cb_common_res_present_int               = inter_cb_res_present_int;

            cr_common_predsample_4by4_int           = inter_cr_predsample_4by4_int;
            cr_common_predsample_4by4_valid_int     = inter_cr_predsample_4by4_valid_int;
            cr_common_predsample_4by4_x_int         = inter_cr_predsample_4by4_x_int;
            cr_common_predsample_4by4_y_int         = inter_cr_predsample_4by4_y_int;
            cr_common_res_present_int               = inter_cr_res_present_int; 
        end

    end

    residual_adder y_residual_adder_block
    (
      .clk                              (clk),
      .reset                            (reset),             

      .res_present_in                   (y_common_res_present_int),
      .predsamples_4by4_valid_in        (y_common_predsample_4by4_valid_int),
      .predsamples_4by4_in              (y_common_predsample_4by4_int),

      .residual_4by4_in                 (y_residual_fifo_in),
      .residual_fifo_read_en_out        (y_residual_fifo_read_en_out),
      .residual_fifo_is_empty_in        (y_residual_fifo_is_empty_in),

      .ans_4by4_out                     (y_res_added_int)
    );

    residual_adder cb_residual_adder_block
    (
      .clk                              (clk),
      .reset                            (reset),             

      .res_present_in                   (cb_common_res_present_int),
      .predsamples_4by4_valid_in        (cb_common_predsample_4by4_valid_int),
      .predsamples_4by4_in              (cb_common_predsample_4by4_int),

      .residual_4by4_in                 (cb_residual_fifo_in),
      .residual_fifo_read_en_out        (cb_residual_fifo_read_en_out),
      .residual_fifo_is_empty_in        (cb_residual_fifo_is_empty_in),

      .ans_4by4_out                     (cb_res_added_int)
    );

    residual_adder cr_residual_adder_block
    (
      .clk                              (clk),
      .reset                            (reset),             

      .res_present_in                   (cr_common_res_present_int),
      .predsamples_4by4_valid_in        (cr_common_predsample_4by4_valid_int),
      .predsamples_4by4_in              (cr_common_predsample_4by4_int),

      .residual_4by4_in                 (cr_residual_fifo_in),
      .residual_fifo_read_en_out        (cr_residual_fifo_read_en_out),
      .residual_fifo_is_empty_in        (cr_residual_fifo_is_empty_in),

      .ans_4by4_out                     (cr_res_added_int)
    );

    generate
        genvar i;
        genvar j;
        
        for (i = 0 ; i < OUTPUT_BLOCK_SIZE ; i = i + 1) begin : row_iteration
            for (j = 0 ; j < OUTPUT_BLOCK_SIZE ; j = j + 1) begin : column_iteration
                assign y_res_added_int_arr[i][j] = y_res_added_int[i*PIXEL_WIDTH*OUTPUT_BLOCK_SIZE +(j+1)*PIXEL_WIDTH - 1:j*PIXEL_WIDTH + i*PIXEL_WIDTH*OUTPUT_BLOCK_SIZE];
                assign cb_res_added_int_arr[i][j] = cb_res_added_int[i*PIXEL_WIDTH*OUTPUT_BLOCK_SIZE +(j+1)*PIXEL_WIDTH - 1:j*PIXEL_WIDTH + i*PIXEL_WIDTH*OUTPUT_BLOCK_SIZE];
                assign cr_res_added_int_arr[i][j] = cr_res_added_int[i*PIXEL_WIDTH*OUTPUT_BLOCK_SIZE +(j+1)*PIXEL_WIDTH - 1:j*PIXEL_WIDTH + i*PIXEL_WIDTH*OUTPUT_BLOCK_SIZE];
            end
        end
    endgenerate

    always @(posedge clk) begin
        y_valid_addition_reg <= (~y_residual_fifo_is_empty_in)&y_common_predsample_4by4_valid_int;
        cb_valid_addition_reg <= (~cb_residual_fifo_is_empty_in)&cb_common_predsample_4by4_valid_int;
        cr_valid_addition_reg <= (~cr_residual_fifo_is_empty_in)&cr_common_predsample_4by4_valid_int;

        intra_y_predsample_4by4_last_row_reg <= intra_y_predsample_4by4_last_row_int;
        intra_y_predsample_4by4_last_col_reg <= intra_y_predsample_4by4_last_col_int;

        intra_cb_predsample_4by4_last_row_reg <= intra_cb_predsample_4by4_last_row_int;
        intra_cb_predsample_4by4_last_col_reg <= intra_cb_predsample_4by4_last_col_int;

        intra_cr_predsample_4by4_last_row_reg <= intra_cr_predsample_4by4_last_row_int;
        intra_cr_predsample_4by4_last_col_reg <= intra_cr_predsample_4by4_last_col_int;

        intra_y_predsample_4by4_x_reg         <= intra_y_predsample_4by4_x_int;
        intra_y_predsample_4by4_y_reg         <= intra_y_predsample_4by4_y_int;

        intra_cb_predsample_4by4_x_reg         <= intra_cb_predsample_4by4_x_int;
        intra_cb_predsample_4by4_y_reg         <= intra_cb_predsample_4by4_y_int;

        intra_cr_predsample_4by4_x_reg         <= intra_cr_predsample_4by4_x_int;
        intra_cr_predsample_4by4_y_reg         <= intra_cr_predsample_4by4_y_int;
    end

    always @(posedge clk) begin
        if (reset) begin
            intra_y_left_portw_en_mask_reg <= 8'b00000000;      
        end
        else begin
            if(y_valid_addition_reg) begin
                if(intra_y_predsample_4by4_last_row_reg) begin
                    intra_y_top_portw_addr_reg      <= intra_y_predsample_4by4_x_reg;
                    intra_y_top_portw_data0_reg     <= y_res_added_int_arr[3][0];
                    intra_y_top_portw_data1_reg     <= y_res_added_int_arr[3][1];
                    intra_y_top_portw_data2_reg     <= y_res_added_int_arr[3][2];
                    intra_y_top_portw_data3_reg     <= y_res_added_int_arr[3][3];  
                    intra_y_top_portw_en_mask_reg   <= 8'b00001111;  
                end

                if(intra_y_predsample_4by4_last_col_reg) begin
                    intra_y_left_portw_addr_reg      <= intra_y_predsample_4by4_y_reg;
                    intra_y_left_portw_data0_reg     <= y_res_added_int_arr[0][3];
                    intra_y_left_portw_data1_reg     <= y_res_added_int_arr[1][3];
                    intra_y_left_portw_data2_reg     <= y_res_added_int_arr[2][3];
                    intra_y_left_portw_data3_reg     <= y_res_added_int_arr[3][3];  
                    intra_y_left_portw_en_mask_reg   <= 8'b00001111;  
                end
            end
        end
    end

    always @(posedge clk) begin
        if (reset) begin
            intra_cb_left_portw_en_mask_reg <= 8'b00000000;      
        end
        else begin
            if(cb_valid_addition_reg) begin
                if(intra_cb_predsample_4by4_last_row_reg) begin
                    intra_cb_top_portw_addr_reg      <= intra_cb_predsample_4by4_x_reg;
                    intra_cb_top_portw_data0_reg     <= cb_res_added_int_arr[3][0];
                    intra_cb_top_portw_data1_reg     <= cb_res_added_int_arr[3][1];
                    intra_cb_top_portw_data2_reg     <= cb_res_added_int_arr[3][2];
                    intra_cb_top_portw_data3_reg     <= cb_res_added_int_arr[3][3];  
                    intra_cb_top_portw_en_mask_reg   <= 8'b00001111;  
                end

                if(intra_cb_predsample_4by4_last_col_reg) begin
                    intra_cb_left_portw_addr_reg      <= intra_cb_predsample_4by4_y_reg;
                    intra_cb_left_portw_data0_reg     <= cb_res_added_int_arr[0][3];
                    intra_cb_left_portw_data1_reg     <= cb_res_added_int_arr[1][3];
                    intra_cb_left_portw_data2_reg     <= cb_res_added_int_arr[2][3];
                    intra_cb_left_portw_data3_reg     <= cb_res_added_int_arr[3][3];  
                    intra_cb_left_portw_en_mask_reg   <= 8'b00001111;  
                end
            end
        end
    end

    always @(posedge clk) begin
        if (reset) begin
            intra_cr_left_portw_en_mask_reg <= 8'b00000000;      
        end
        else begin
            if(cr_valid_addition_reg) begin
                if(intra_cr_predsample_4by4_last_row_reg) begin
                    intra_cr_top_portw_addr_reg      <= intra_cr_predsample_4by4_x_reg;
                    intra_cr_top_portw_data0_reg     <= cr_res_added_int_arr[3][0];
                    intra_cr_top_portw_data1_reg     <= cr_res_added_int_arr[3][1];
                    intra_cr_top_portw_data2_reg     <= cr_res_added_int_arr[3][2];
                    intra_cr_top_portw_data3_reg     <= cr_res_added_int_arr[3][3];  
                    intra_cr_top_portw_en_mask_reg   <= 8'b00001111;  
                end

                if(intra_cr_predsample_4by4_last_col_reg) begin
                    intra_cr_left_portw_addr_reg      <= intra_cr_predsample_4by4_y_reg;
                    intra_cr_left_portw_data0_reg     <= cr_res_added_int_arr[0][3];
                    intra_cr_left_portw_data1_reg     <= cr_res_added_int_arr[1][3];
                    intra_cr_left_portw_data2_reg     <= cr_res_added_int_arr[2][3];
                    intra_cr_left_portw_data3_reg     <= cr_res_added_int_arr[3][3];  
                    intra_cr_left_portw_en_mask_reg   <= 8'b00001111;  
                end
            end
        end
    end


    // always @(posedge clk) begin : residual_adder_16by16
    //     integer i;
    //     integer j;
    //     if (reset) begin
            
            
    //     end
    //     else begin
    //         case(pred_mode)
    //             `PREDMODE_INTRA : begin
    //                 if(intra_y_predsample_4by4_valid_int) begin
    //                     for(i = 0 ; i < OUTPUT_BLOCK_SIZE ; i = i + 1) begin
    //                         for (j = 0 ; j < OUTPUT_BLOCK_SIZE ; j = j + 1) begin
                                
    //                         end
    //                     end  
    //                 end
                    
    //             end
    //             `PREDMODE_INTER : begin
                    
    //             end
    //         endcase
    //     end
    // end

   
    always@(posedge clk) begin
        if(reset ) begin
            write_ref_pic_state <= WRITE_REF_PIC_STATE_IDLE;
            ref_pic_list_poc_wr_en <= 0;
            ref_pic_list_idx_wr_en <= 0;
            dpb_filled_flag_new <= 0;
        end
        else begin
            case(write_ref_pic_state)
                WRITE_REF_PIC_STATE_IDLE: begin
                    ref_pic_list_poc_wr_en <= 0;
                    ref_pic_list_idx_wr_en <= 0;
                    if(state == STATE_SEND_POC_TO_DBF) begin
                        ref_poc_list5_addr_read <= 0;
                        write_ref_pic_state <= WRITE_REF_PIC_STATE_CHECK;
                        list0_poc_addr_temp <= 0;
                        list1_poc_addr_temp <= 0;
                        dpb_filled_flag_new <= 0;
                    end                   
                end
                WRITE_REF_PIC_STATE_CHECK: begin
                    ref_pic_list_idx_wr_en  <= 0;
                    ref_pic_list_poc_wr_en <= 0;
                    if(ref_poc_list5_addr_read == num_pic_ref_pic_5_total) begin
                        write_ref_pic_state <= WRITE_REF_PIC_STATE_DONE;
                    end
                    else if(ref_poc_list5_data_out[REF_PIC_LIST5_DPB_STATE_HIGH:REF_PIC_LIST5_DPB_STATE_LOW] == `ST_CURR_BEF || ref_poc_list5_data_out[REF_PIC_LIST5_DPB_STATE_HIGH:REF_PIC_LIST5_DPB_STATE_LOW] == `ST_CURR_AFT) begin
                        write_ref_pic_state <= WRITE_REF_PIC_STATE_POC_WRITE;
                        temp_poc <= current__poc + {{(POC_WIDTH - DELTA_POC_WIDTH){ref_poc_list5_data_out[REF_PIC_LIST5_POC_RANGE_HIGH]}},ref_poc_list5_data_out[REF_PIC_LIST5_POC_RANGE_HIGH:REF_PIC_LIST5_POC_RANGE_LOW]};
                        if(ref_poc_list5_data_out[REF_PIC_LIST5_DPB_STATE_HIGH:REF_PIC_LIST5_DPB_STATE_LOW] == `ST_CURR_BEF) begin
                            
                        end
                        else begin
                            list1_poc_addr_temp <= (list1_poc_addr_temp + 1)%(1<<NUM_REF_IDX_L0_MINUS1_WIDTH);
                        end
                    end
                    else if(ref_poc_list5_data_out[REF_PIC_LIST5_DPB_STATE_HIGH:REF_PIC_LIST5_DPB_STATE_LOW] == `ST_FOLL) begin
                        ref_poc_list5_addr_read <= (ref_poc_list5_addr_read + 1)%(1<<REF_POC_LIST5_ADDR_WIDTH);
                        write_ref_pic_state <= WRITE_REF_PIC_FIND_FOL_IDX;
                        dpb_addr_read <= 0;
                    end
                    else begin
                        ref_poc_list5_addr_read <= (ref_poc_list5_addr_read + 1)%(1<<REF_POC_LIST5_ADDR_WIDTH);
                    end
                end
                WRITE_REF_PIC_STATE_POC_WRITE: begin
                    ref_pic_list_poc_wr_en <= 1;
                    ref_pic_list_poc_data_in <= temp_poc;
                    if(ref_poc_list5_data_out[REF_PIC_LIST5_DPB_STATE_HIGH:REF_PIC_LIST5_DPB_STATE_LOW] == `ST_CURR_BEF) begin
                        ref_pic_list0_poc_addr <= list0_poc_addr_temp;
                        ref_pic_list1_poc_addr <= list0_poc_addr_temp + num_pic_st_curr_aft;
                        list0_poc_addr_temp <= (list0_poc_addr_temp + 1)%(1<<NUM_REF_IDX_L0_MINUS1_WIDTH);
                    end
                    else begin
                        ref_pic_list0_poc_addr <= (list1_poc_addr_temp + num_pic_st_curr_bef)%(1<<NUM_REF_IDX_L0_MINUS1_WIDTH);
                        ref_pic_list1_poc_addr <= list1_poc_addr_temp;
                        list1_poc_addr_temp <= (list1_poc_addr_temp + 1)%(1<<NUM_REF_IDX_L0_MINUS1_WIDTH);
                    end 
                    ref_poc_list5_addr_read <= (ref_poc_list5_addr_read + 1)%(1<<REF_POC_LIST5_ADDR_WIDTH);
                    write_ref_pic_state <= WRITE_REF_PIC_STATE_IDX_WRITE;
                    dpb_addr_read <= 0;
                end
                WRITE_REF_PIC_STATE_IDX_WRITE: begin
                    if((dpb_data_out == temp_poc && dpb_filled_flag[dpb_addr_read] == 1) || (dpb_addr_read == {DPB_ADDR_WIDTH{1'b1}})) begin
                        ref_pic_list_idx_wr_en <= 1;
                        ref_pic_list_idx_data_in <= dpb_addr_read;
                        write_ref_pic_state <= WRITE_REF_PIC_STATE_CHECK;
                    end
                    if(dpb_data_out == temp_poc && dpb_filled_flag[dpb_addr_read] == 1) begin
                        dpb_filled_flag_new[dpb_addr_read] <= 1;
                    end
                    dpb_addr_read <= (dpb_addr_read + 1)%(1<<DPB_ADDR_WIDTH);
                end
                WRITE_REF_PIC_FIND_FOL_IDX: begin
                    if((dpb_data_out == temp_poc && dpb_filled_flag[dpb_addr_read] == 1) || (dpb_addr_read == {DPB_ADDR_WIDTH{1'b1}})) begin
                        write_ref_pic_state <= WRITE_REF_PIC_STATE_CHECK;
                    end
                    if(dpb_data_out == temp_poc && dpb_filled_flag[dpb_addr_read] == 1) begin
                        dpb_filled_flag_new[dpb_addr_read] <= 1;
                    end
                    dpb_addr_read <= (dpb_addr_read + 1)%(1<<DPB_ADDR_WIDTH);
                end
                WRITE_REF_PIC_STATE_DONE: begin
                    write_ref_pic_state <= WRITE_REF_PIC_STATE_IDLE;
                end
            endcase
        end

    end
    
    always@(*) begin
        next_state = state;
        ref_poc_list5_addr = {REF_POC_LIST5_ADDR_WIDTH{1'bx}};
        ref_poc_list5_data_in = {REF_POC_LIST5_DATA_WIDTH{1'bx}};
        ref_poc_list5_wr_en = 0;
        
        dpb_data_in = {DPB_DATA_WIDTH{1'bx}};
        dpb_addr = {DPB_ADDR_WIDTH{1'bx}};
        dpb_wr_en = 0;
                
        case(state) 
            STATE_READ_WAIT: begin
                if(input_fifo_is_empty == 0) begin
                    next_state = STATE_ACTIVE;
                end
            end
            STATE_POC_READ_WAIT: begin
                if(input_fifo_is_empty == 0) begin
                    next_state = STATE_CURRENT_POC;
                end
            end    
            STATE_MVD_READ_WAIT: begin
                if(input_fifo_is_empty == 0) begin
                    next_state = STATE_MVD_READ;
                end
            end             
            STATE_ACTIVE: begin
                case(fifo_in[FIFO_IN_WIDTH-1:FIFO_IN_WIDTH - HEADER_WIDTH])
                    `HEADER_PARAMETERS_0: begin
                        if(output_fifo_is_full == 1) begin
                            next_state = STATE_WRITE_WAIT;
                        end
                        else if(input_fifo_is_empty == 1) begin
                            next_state = STATE_READ_WAIT;
                        end
                    end
                    `HEADER_PARAMETERS_1: begin
                        if(output_fifo_is_full == 1) begin
                            next_state = STATE_WRITE_WAIT;
                        end
                        else if(input_fifo_is_empty == 1) begin
                            next_state = STATE_READ_WAIT;
                        end                        
                    end
                    `HEADER_PARAMETERS_2: begin
                        if(input_fifo_is_empty == 1) begin
                            next_state = STATE_READ_WAIT;
                        end  
                    end
                    `HEADER_PARAMETERS_3: begin
                        if(input_fifo_is_empty == 1) begin
                            next_state = STATE_READ_WAIT;
                        end                          
                    end
                    `HEADER_PARAMETERS_4: begin
                        if(input_fifo_is_empty == 1) begin
                            next_state = STATE_READ_WAIT;                                
                        end
                    end
                    `HEADER_PARAMETERS_5: begin
                        next_state = STATE_REF_PIC_LIST_5_UPDATE;
                    end
                    `HEADER_SLICE_0: begin
                            next_state = STATE_DPB_ADD_PREV_PIC;
                    end
                    `HEADER_SLICE_1: begin
                        if(output_fifo_is_full == 1) begin
                            next_state = STATE_WRITE_WAIT;
                        end
                        else if(input_fifo_is_empty == 1) begin
                            next_state = STATE_READ_WAIT;
                        end 
                    end
                    `HEADER_SLICE_2: begin
                        if(output_fifo_is_full == 1) begin
                            next_state = STATE_WRITE_WAIT;
                        end
                        else if(input_fifo_is_empty == 1) begin
                            next_state = STATE_READ_WAIT;
                        end
                    end
                    `HEADER_SLICE_3: begin
                        if(input_fifo_is_empty == 1) begin
                            next_state = STATE_READ_WAIT;
                        end
                    end
                    `HEADER_SLICE_4: begin
                        if(input_fifo_is_empty == 1) begin
                            next_state = STATE_READ_WAIT;
                        end
                    end
                    `HEADER_SLICE_5: begin
                        if(input_fifo_is_empty == 1) begin
                            next_state = STATE_READ_WAIT;
                        end                    
                    end
                    `HEADER_SLICE_6: begin
                        if(input_fifo_is_empty == 1) begin
                            next_state = STATE_READ_WAIT;
                        end                    
                    end
                    `HEADER_SLICE_7: begin
                        if(input_fifo_is_empty == 1) begin
                            next_state = STATE_READ_WAIT;
                        end                    
                    end
                    `HEADER_CTU_0: begin
                        if(input_fifo_is_empty == 1) begin
                            next_state = STATE_READ_WAIT;
                        end 
                    end
                    `HEADER_CTU_1: begin
                        if(input_fifo_is_empty == 1) begin
                            next_state = STATE_READ_WAIT;
                        end 
                    end
                    `HEADER_CTU_2_Y: begin
                        if(input_fifo_is_empty == 1) begin
                            next_state = STATE_READ_WAIT;
                        end 
                    end
                    `HEADER_CTU_2_CB: begin
                        if(input_fifo_is_empty == 1) begin
                            next_state = STATE_READ_WAIT;
                        end 
                    end
                    `HEADER_CTU_2_CR: begin
                        if(input_fifo_is_empty == 1) begin
                            next_state = STATE_READ_WAIT;
                        end 
                    end                    
                    `HEADER_CU_0: begin
                        if(input_fifo_is_empty == 1) begin
                            next_state = STATE_READ_WAIT;
                        end 
                    end
                    `HEADER_PU_0: begin
                        if(fifo_in[  FIFO_IN_WIDTH - HEADER_WIDTH - PART_IDX_WIDTH - PRED_IDC_WIDTH - 1] == 1) begin
                            if(input_fifo_is_empty == 1) begin
                                next_state = STATE_MVD_READ_WAIT;
                            end
                            else begin
                                next_state = STATE_MVD_READ;
                            end                        
                        end
                        else begin
                            next_state = STATE_MV_RETURN_WAIT;                          
                        end
                    end
                    `HEADER_RU_0: begin
                        if(input_fifo_is_empty == 1) begin
                            next_state = STATE_READ_WAIT;
                        end 
                    end
                    `HEADER_RU_1: begin
                        if(input_fifo_is_empty == 1) begin
                            next_state = STATE_READ_WAIT;
                        end 
                    end                      
                    default: begin
                        next_state = STATE_ERROR;
                    end
                endcase
            end
            STATE_WRITE_WAIT: begin
                if(output_fifo_is_full == 0) begin
                    if(input_fifo_is_empty == 1'b0) begin
                        next_state = STATE_ACTIVE;
                    end
                    else begin
                        next_state = STATE_READ_WAIT;
                    end                
                end
            end            
            STATE_DPB_ADD_PREV_PIC: begin
                if(initial_condition == 1) begin
                    dpb_addr = current_pic_dpb_idx;
                    dpb_data_in = current__poc;
                    dpb_wr_en = 1;
                end     
                if(input_fifo_is_empty == 1) begin
                    next_state = STATE_POC_READ_WAIT;                    
                end
                else begin
                    next_state = STATE_CURRENT_POC;
                end
            end
            STATE_CURRENT_POC: begin
                next_state = STATE_CUR_PIC_FIL_IDX;
            end
            STATE_CUR_PIC_FIL_IDX: begin
                if(dpb_filled_flag[curr_rps_entry_idx] == 0) begin
                    if(output_fifo_is_full == 1) begin
                        next_state = STATE_WRITE_WAIT_CUR_DPB_IDX_TO_DBF;
                    end
                    else begin
                        next_state = STATE_SEND_POC_TO_DBF;
                    end                    
                end
            end
            STATE_SEND_POC_TO_DBF: begin
                ref_poc_list5_addr = ref_poc_list5_addr_read;
                if(write_ref_pic_state == WRITE_REF_PIC_STATE_DONE) begin
                    if(output_fifo_is_full == 1) begin
                        next_state = STATE_WRITE_WAIT;
                    end
                    else if(input_fifo_is_empty == 1) begin
                        next_state = STATE_READ_WAIT;
                    end
                    else begin
                        next_state = STATE_ACTIVE;
                    end                    
                end
            end
            STATE_WRITE_WAIT_CUR_DPB_IDX_TO_DBF: begin
                if(output_fifo_is_full == 0) begin
                    next_state = STATE_SEND_POC_TO_DBF;
                end
            end
            STATE_REF_PIC_LIST_5_UPDATE: begin
                ref_poc_list5_addr = curr_rps_entry_idx;
                ref_poc_list5_wr_en = 1;
                ref_poc_list5_data_in[REF_PIC_LIST5_POC_RANGE_HIGH:REF_PIC_LIST5_POC_RANGE_LOW] = delta_poc_wire;
                ref_poc_list5_data_in[REF_PIC_LIST5_DPB_STATE_HIGH:REF_PIC_LIST5_DPB_STATE_LOW] = fifo_in[FIFO_IN_WIDTH - HEADER_WIDTH - 1: FIFO_IN_WIDTH - HEADER_WIDTH - DPB_STATUS_WIDTH];
                if((curr_rps_entry_idx + 1)%(1<<NUM_NEG_POS_POC_WIDTH) == num_pic_ref_pic_5_total) begin
                    if(input_fifo_is_empty == 1) begin
                        next_state = STATE_READ_WAIT;                                
                    end
                    else begin
                        next_state = STATE_ACTIVE;
                    end
                end
            end
            STATE_MVD_READ: begin
                next_state = STATE_MV_RETURN_WAIT;
            end
            STATE_MV_RETURN_WAIT: begin
                if(mv_done) begin
                    if(input_fifo_is_empty == 1) begin
                        next_state = STATE_READ_WAIT;
                    end
                    else begin
                        next_state = STATE_ACTIVE;
                    end                     
                end
            end
        endcase
    end
   
    always@(posedge clk) begin
        if(reset) begin
            state <= STATE_READ_WAIT;
        end
        else begin
            state <= next_state;
        end
    end
  
    always @(posedge clk ) begin : pred_main_fsm
        if(reset) begin
            read_en_out <= 1'b0;
            write_en_out <= 1'b0;
            dpb_filled_flag <= 16'd0;
            initial_condition <= 0;
            current__poc <= {POC_WIDTH{1'b1}};      /// assume Video sequence does not have max POC
            num_pic_st_curr_aft <= 0;
            num_pic_st_curr_bef <= 0;
            num_pic_st_curr_total <= 0;
            num_pic_st_curr_total <= 0;
            num_pic_st_fol <= 0;
        end 
        else if(enable) begin
                read_en_out <= 1'b0;
                write_en_out <= 1'b0;
                inter_config_mode_in <= `INTER_TOP_CONFIG_IDLE;
            case(state)
                STATE_READ_WAIT: begin
                end
                STATE_ACTIVE: begin
                    case(fifo_in[FIFO_IN_WIDTH-1:FIFO_IN_WIDTH - HEADER_WIDTH])
                        `HEADER_PARAMETERS_0: begin
                            inter_config_mode_in <= `INTER_TOP_PARA_0;
                            inter_config_bus_in <= fifo_in[FIFO_IN_WIDTH-1: FIFO_IN_WIDTH - FIFO_OUT_WIDTH];
                            pic_height <=   (fifo_in[FIFO_IN_WIDTH - HEADER_WIDTH - 1: FIFO_IN_WIDTH - HEADER_WIDTH - PIC_WIDTH_WIDTH])%(1<<PIC_DIM_WIDTH);
                            pic_width  <=   (fifo_in[FIFO_IN_WIDTH - HEADER_WIDTH - PIC_WIDTH_WIDTH - 1: FIFO_IN_WIDTH - HEADER_WIDTH - PIC_WIDTH_WIDTH - PIC_HEIGHT_WIDTH])%(1<<PIC_DIM_WIDTH);
                            fifo_out <= fifo_in[FIFO_IN_WIDTH-1: FIFO_IN_WIDTH - FIFO_OUT_WIDTH];
                            read_en_out <= 1;
                            if(output_fifo_is_full == 0) begin
                                write_en_out <= 1;
                            end
                        end
                        `HEADER_PARAMETERS_1: begin
                            inter_config_mode_in <= `INTER_TOP_PARA_1;
                            inter_config_bus_in <= fifo_in[FIFO_IN_WIDTH-1: FIFO_IN_WIDTH - FIFO_OUT_WIDTH];
                            log2_ctb_size <=  fifo_in[FIFO_IN_WIDTH - HEADER_WIDTH - 1: FIFO_IN_WIDTH - HEADER_WIDTH - LOG2CTBSIZEY_WIDTH];
                            strong_intra_smoothing <= fifo_in[  FIFO_IN_WIDTH - HEADER_WIDTH - LOG2CTBSIZEY_WIDTH - PPS_CB_QP_OFFSET_WIDTH - PPS_CR_QP_OFFSET_WIDTH - PPS_BETA_OFFSET_DIV2_WIDTH - PPS_TC_OFFSET_DIV2_WIDTH - 1 : 
                                                                FIFO_IN_WIDTH - HEADER_WIDTH - LOG2CTBSIZEY_WIDTH - PPS_CB_QP_OFFSET_WIDTH - PPS_CR_QP_OFFSET_WIDTH - PPS_BETA_OFFSET_DIV2_WIDTH - PPS_TC_OFFSET_DIV2_WIDTH - STRONG_INTRA_SMOOTHING_WIDTH];
                            constrained_intra_pred <= fifo_in[  FIFO_IN_WIDTH - HEADER_WIDTH - LOG2CTBSIZEY_WIDTH - PPS_CB_QP_OFFSET_WIDTH - PPS_CR_QP_OFFSET_WIDTH - PPS_BETA_OFFSET_DIV2_WIDTH - PPS_TC_OFFSET_DIV2_WIDTH - STRONG_INTRA_SMOOTHING_WIDTH- 1 :
                                                                FIFO_IN_WIDTH - HEADER_WIDTH - LOG2CTBSIZEY_WIDTH - PPS_CB_QP_OFFSET_WIDTH - PPS_CR_QP_OFFSET_WIDTH - PPS_BETA_OFFSET_DIV2_WIDTH - PPS_TC_OFFSET_DIV2_WIDTH - STRONG_INTRA_SMOOTHING_WIDTH - CONSTRAINED_INTRA_PRED_WIDTH];
                            fifo_out <= fifo_in[FIFO_IN_WIDTH - 1: FIFO_IN_WIDTH - FIFO_OUT_WIDTH];
                            read_en_out <= 1;
                            if(output_fifo_is_full == 0) begin
                                write_en_out <= 1;
                            end                      
                        end
                        `HEADER_PARAMETERS_2: begin
                            inter_config_mode_in <= `INTER_TOP_PARA_1;
                            inter_config_bus_in <= fifo_in[FIFO_IN_WIDTH-1: FIFO_IN_WIDTH - FIFO_OUT_WIDTH];
                            read_en_out <= 1;                                                                 
                        end
                        `HEADER_PARAMETERS_3: begin
                            read_en_out <= 1;
                        end
                        `HEADER_PARAMETERS_4: begin
                            read_en_out <= 1;
                        end
                        `HEADER_PARAMETERS_5: begin
                            read_en_out <= 1;
                            num_pic_st_curr_bef <= fifo_in[FIFO_IN_WIDTH - HEADER_WIDTH                                 - 1: FIFO_IN_WIDTH - HEADER_WIDTH - NUM_CURR_REF_POC_WIDTH] ;
                            num_pic_st_curr_aft <= fifo_in[FIFO_IN_WIDTH - HEADER_WIDTH - NUM_CURR_REF_POC_WIDTH        - 1: FIFO_IN_WIDTH - HEADER_WIDTH - 2*NUM_CURR_REF_POC_WIDTH] ;
                            num_pic_st_fol      <= fifo_in[FIFO_IN_WIDTH - HEADER_WIDTH - 2*NUM_CURR_REF_POC_WIDTH      - 1: FIFO_IN_WIDTH - HEADER_WIDTH - 3*NUM_CURR_REF_POC_WIDTH] ;
                            curr_rps_entry_idx <= 0;
                            num_pic_ref_pic_5_total <= (fifo_in[FIFO_IN_WIDTH - HEADER_WIDTH                                 - 1: FIFO_IN_WIDTH - HEADER_WIDTH - NUM_CURR_REF_POC_WIDTH] +
                                                        fifo_in[FIFO_IN_WIDTH - HEADER_WIDTH - NUM_CURR_REF_POC_WIDTH        - 1: FIFO_IN_WIDTH - HEADER_WIDTH - 2*NUM_CURR_REF_POC_WIDTH] +
                                                        fifo_in[FIFO_IN_WIDTH - HEADER_WIDTH - 2*NUM_CURR_REF_POC_WIDTH      - 1: FIFO_IN_WIDTH - HEADER_WIDTH - 3*NUM_CURR_REF_POC_WIDTH])%(1<<NUM_REF_IDX_L0_MINUS1_WIDTH);
                        end
                        `HEADER_SLICE_0: begin
                            // TODO: goto state FILL dpb
                            read_en_out <= 1;
                        end
                        `HEADER_SLICE_1: begin
                            inter_config_mode_in <= `INTER_TOP_SLICE_1;
                            inter_config_bus_in <= fifo_in[FIFO_IN_WIDTH-1: FIFO_IN_WIDTH - FIFO_OUT_WIDTH];                            
                            slice_temporal_mvp_enabled_flag <=   fifo_in[  FIFO_IN_WIDTH - HEADER_WIDTH - SHORT_TERM_REF_PIC_SPS_WIDTH - SHORT_TERM_REF_PIC_IDX_WIDTH - 1:
                                                                FIFO_IN_WIDTH - HEADER_WIDTH - SHORT_TERM_REF_PIC_SPS_WIDTH - SHORT_TERM_REF_PIC_IDX_WIDTH - TEMPORAL_MVP_ENABLED_WIDTH];     
                            slice_type              <=      fifo_in[ FIFO_IN_WIDTH - HEADER_WIDTH - SHORT_TERM_REF_PIC_SPS_WIDTH - SHORT_TERM_REF_PIC_IDX_WIDTH - TEMPORAL_MVP_ENABLED_WIDTH - SAO_LUMA_WIDTH - SAO_CHROMA_WIDTH - NUM_REF_IDX_L0_MINUS1_WIDTH - NUM_REF_IDX_L1_MINUS1_WIDTH - MAX_MERGE_CAND_WIDTH -1:
                                                                     FIFO_IN_WIDTH - HEADER_WIDTH - SHORT_TERM_REF_PIC_SPS_WIDTH - SHORT_TERM_REF_PIC_IDX_WIDTH - TEMPORAL_MVP_ENABLED_WIDTH - SAO_LUMA_WIDTH - SAO_CHROMA_WIDTH - NUM_REF_IDX_L0_MINUS1_WIDTH - NUM_REF_IDX_L1_MINUS1_WIDTH - MAX_MERGE_CAND_WIDTH - SLICE_TYPE_WIDTH];   
                            num_active_ref_idx_l0  <= fifo_in[   INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - SHORT_TERM_REF_PIC_SPS_WIDTH - SHORT_TERM_REF_PIC_IDX_WIDTH - TEMPORAL_MVP_ENABLED_WIDTH - SAO_LUMA_WIDTH - SAO_CHROMA_WIDTH - 1:
                                                                INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - SHORT_TERM_REF_PIC_SPS_WIDTH - SHORT_TERM_REF_PIC_IDX_WIDTH - TEMPORAL_MVP_ENABLED_WIDTH - SAO_LUMA_WIDTH - SAO_CHROMA_WIDTH - NUM_REF_IDX_L0_MINUS1_WIDTH] + 1'b1;
                                                                
                            num_active_ref_idx_l1   <= fifo_in[  INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - SHORT_TERM_REF_PIC_SPS_WIDTH - SHORT_TERM_REF_PIC_IDX_WIDTH - TEMPORAL_MVP_ENABLED_WIDTH - SAO_LUMA_WIDTH - SAO_CHROMA_WIDTH - NUM_REF_IDX_L0_MINUS1_WIDTH - 1:
                                                                INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - SHORT_TERM_REF_PIC_SPS_WIDTH - SHORT_TERM_REF_PIC_IDX_WIDTH - TEMPORAL_MVP_ENABLED_WIDTH - SAO_LUMA_WIDTH - SAO_CHROMA_WIDTH - NUM_REF_IDX_L0_MINUS1_WIDTH - NUM_REF_IDX_L1_MINUS1_WIDTH] + 1'b1;
                            
                            fifo_out <= fifo_in[FIFO_IN_WIDTH - 1: FIFO_IN_WIDTH - FIFO_OUT_WIDTH];
                            read_en_out <= 1;
                            if(output_fifo_is_full == 0) begin
                                write_en_out <= 1;
                            end
                        end
                        `HEADER_SLICE_2: begin
                            // collocated_ref_idx <= fifo_in[ FIFO_IN_WIDTH - HEADER_WIDTH - SLICE_CB_QP_OFFSET_WIDTH - SLICE_CR_QP_OFFSET_WIDTH - DISABLE_DBF_WIDTH - SLICE_BETA_OFFSET_DIV2_WIDTH - SLICE_TC_OFFSET_DIV2_WIDTH - 1:
                                                           // FIFO_IN_WIDTH - HEADER_WIDTH - SLICE_CB_QP_OFFSET_WIDTH - SLICE_CR_QP_OFFSET_WIDTH - DISABLE_DBF_WIDTH - SLICE_BETA_OFFSET_DIV2_WIDTH - SLICE_TC_OFFSET_DIV2_WIDTH - COLLOCATED_REF_IDX_WIDTH];
                            inter_config_mode_in <= `INTER_TOP_SLICE_2;
                            inter_config_bus_in <= fifo_in[FIFO_IN_WIDTH-1: FIFO_IN_WIDTH - INTER_TOP_CONFIG_BUS_WIDTH];    
                            fifo_out <= fifo_in[FIFO_IN_WIDTH - 1: FIFO_IN_WIDTH - FIFO_OUT_WIDTH];
                            read_en_out <= 1;
                            if(output_fifo_is_full == 0) begin
                                write_en_out <= 1;
                            end
                        end
                        `HEADER_SLICE_3: begin 
                            read_en_out <= 1;  
                        end
                        `HEADER_SLICE_4: begin
                            read_en_out <= 1;
                        end
                        `HEADER_SLICE_5: begin
                            read_en_out <= 1;
                            inter_config_mode_in <= `INTER_SLICE_INFO;
                            inter_config_bus_in <= fifo_in[FIFO_IN_WIDTH-1: FIFO_IN_WIDTH - INTER_TOP_CONFIG_BUS_WIDTH];    
                        end
                        `HEADER_SLICE_6: begin
                            read_en_out <= 1;
                            inter_config_mode_in <= `INTER_TILE_INFO;
                            inter_config_bus_in <= fifo_in[FIFO_IN_WIDTH-1: FIFO_IN_WIDTH - INTER_TOP_CONFIG_BUS_WIDTH];    
                        end    
                        `HEADER_SLICE_7: begin
                            read_en_out <= 1;
                        end
                        `HEADER_CTU_0: begin
                            read_en_out <= 1;
                            inter_config_mode_in <= `INTER_CTU0_HEADER;
                            inter_config_bus_in <= fifo_in[FIFO_IN_WIDTH-1: FIFO_IN_WIDTH - INTER_TOP_CONFIG_BUS_WIDTH]; 
                        end
                        `HEADER_CTU_1: begin
                            read_en_out <= 1;
                        end
                        `HEADER_CTU_2_Y: begin
                            read_en_out <= 1;
                        end
                        `HEADER_CTU_2_CB: begin
                            read_en_out <= 1;
                        end
                        `HEADER_CTU_2_CR: begin
                            read_en_out <= 1;
                        end
                        `HEADER_CU_0: begin
                            read_en_out <= 1;
                            inter_config_mode_in <= `INTER_CU_HEADER;
                            inter_config_bus_in <= fifo_in[FIFO_IN_WIDTH-1: FIFO_IN_WIDTH - INTER_TOP_CONFIG_BUS_WIDTH];
                            pred_mode <= fifo_in[   FIFO_IN_WIDTH - HEADER_WIDTH - X0_WIDTH - Y0_WIDTH - LOG2_CB_SIZE_WIDTH - 1: 
                                                    FIFO_IN_WIDTH - HEADER_WIDTH - X0_WIDTH - Y0_WIDTH - LOG2_CB_SIZE_WIDTH - PREDMODE_WIDTH];
                        end
                        `HEADER_PU_0: begin
                            read_en_out <= 1;
                            inter_config_mode_in <= `INTER_PU_HEADER;
                            inter_config_bus_in <= fifo_in[FIFO_IN_WIDTH-1: FIFO_IN_WIDTH - INTER_TOP_CONFIG_BUS_WIDTH];       
                        end
                        `HEADER_RU_0: begin
                            read_en_out <= 1;
                        end
                        `HEADER_RU_1: begin
                            read_en_out <= 1;
                        end                       
                    endcase
                end
                STATE_WRITE_WAIT: begin
                    if(output_fifo_is_full == 1'b0) begin
                        write_en_out <= 1'b1;
                    end
                end
                STATE_DPB_ADD_PREV_PIC: begin
                    if(initial_condition == 1) begin
                        dpb_filled_flag[current_pic_dpb_idx] <= 1'b1;     
                    end
                end
                STATE_CURRENT_POC: begin
                    initial_condition <= 1;
                    curr_rps_entry_idx <= 0;
                    current__poc <=  fifo_in[    FIFO_IN_WIDTH - 1:
                                                FIFO_IN_WIDTH - FIFO_OUT_WIDTH];
                    read_en_out <= 1;
                end
                STATE_CUR_PIC_FIL_IDX: begin
                    fifo_out <= {`HEADER_SLICE_0,curr_rps_entry_idx,{(FIFO_OUT_WIDTH - HEADER_WIDTH - NUM_NEG_POS_POC_WIDTH){1'b0}}};
                    if(dpb_filled_flag[curr_rps_entry_idx] == 0) begin
                        current_pic_dpb_idx <= curr_rps_entry_idx;
                        if(output_fifo_is_full == 0) begin
                            write_en_out <= 1;
                        end

                        inter_config_mode_in <= `INTER_CURRENT_PIC_DPB_IDX;
                        inter_config_bus_in <= {curr_rps_entry_idx,{(INTER_TOP_CONFIG_BUS_WIDTH - DPB_ADDR_WIDTH){1'b0}}};  
                    end
                    else begin 
                        curr_rps_entry_idx <= (curr_rps_entry_idx + 1)%(1<<NUM_NEG_POS_POC_WIDTH); 
                    end
                end
                STATE_REF_PIC_LIST_5_UPDATE: begin
                    curr_rps_entry_idx <= (curr_rps_entry_idx + 1)%(1<<NUM_NEG_POS_POC_WIDTH); 
                    num_pic_st_curr_total <= (num_pic_st_curr_aft + num_pic_st_curr_bef)%(1<<NUM_REF_IDX_L0_MINUS1_WIDTH);
                end
                STATE_WRITE_WAIT_CUR_DPB_IDX_TO_DBF: begin
                    if(output_fifo_is_full == 1'b0) begin
                        write_en_out <= 1'b1;
                    end
                end
                STATE_SEND_POC_TO_DBF: begin
                    dpb_filled_flag <= dpb_filled_flag_new;
                    if(write_ref_pic_state == WRITE_REF_PIC_STATE_DONE) begin
                        inter_config_mode_in <= `INTER_TOP_CURR_POC;
                        inter_config_bus_in <= current__poc; 
                        fifo_out <= {current__poc};
                        if(output_fifo_is_full == 1'b0) begin
                            write_en_out <= 1'b1;
                        end                             
                    end
                    else begin
                        inter_config_mode_in <= `INTER_REF_PIC_TRANSFER;
                    end
                end
                STATE_MVD_READ: begin
                    read_en_out <= 1;
                    inter_config_bus_in <= fifo_in[FIFO_IN_WIDTH - 1: FIFO_IN_WIDTH - INTER_TOP_CONFIG_BUS_WIDTH];
                end
            endcase
        end 
    end

endmodule 